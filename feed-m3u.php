<?php
/**
 * xspf template
 *
 * @package WordPress
 */

header('Content-type: audio/x-mpegurl', true);
header('Content-Disposition: attachment; filename="playlist.m3u"');
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
#header('Content-Type: text; charset=' . get_option('blog_charset'), true);
$more = 1;

?>#EXTM3U
<?php 
		  while( have_posts()) : the_post(); 
			$nm_file_ = getPodcastMeta($post->ID);
			
			if (!empty($nm_file_)) {
				$meta = get_post_meta($post->ID, 'podPressMedia', true);
		
				$fname = basename($meta[$pos]["URI"]); 
				$fimg = $meta[$pos]["previewImage"]; 
				$img = (trim($fimg)!=""? $fimg:$imgpath = "http://".$_SERVER["HTTP_HOST"]."/wp-content/plugins/suararadio/images/vpreview_center.png");
?>
#EXTINF:-1,<?php the_title() ?>
<?php echo "\n".$nm_file_; echo "\n"; ?>
<?php } endwhile; ?>
<?php wp_reset_query(); ?>