/*
suararadio.js
javascript fungsi untuk suara radio
dependency: jquery
*/

$(document).ready(function(){
    jQuery("#id-playlist-streaming").show();
    
});


function suararadio_playlist_refresh() {
    $(".soundplayer").unbind();
	jQuery.ajax({
		type: "POST",
		url:  "/suararadio_api/load_playlist_item/0",
		dataType: "html",
		success: function (data) {
				if (data!=null) {
					$("#listItem").html(data);
					suararadioPlayer.makeSoundPlayer();
					suararadioPlayer.makePlaylist();		
				}
			},
		error: function (req, stat, err) {
				str = "Playlist: data gagal dimuat.";
				suararadio_flash(str);
			}
	});
}



function suararadio_playlist_add(postid,pos) {
	jQuery.ajax({
		type: "GET",
		url:  "/suararadio_api/add_playlist_item/"+postid+"/"+pos,
		dataType: "json",
		success: function (data) {
				console.debug(data);
				str = "File: "+data.title+", telah ditambahkan.";
				suararadio_flash(str); 
				if ($("ul#listItem").length>0) {
					suararadio_playlist_refresh();
                    
				}
                json_playlist_refresh();
                json_load_playlist();                
                $('#playlist-bottom').show();
                
				//window.location.reload();
				//suararadioPlayer.makePlaylist();
		},
		error: function(data) {
			str = "Gagal menambahkan item.";
			suararadio_flash(str);
		}
	});
}

function suararadio_playlist_clear() {
	jQuery.ajax({
		type: "GET",
		url:  "/suararadio_api/clear_playlist/1",
		dataType: "json",
		success: function (data) {
				str = "Playlist dikosongkan.";
				suararadio_flash(str);
				suararadio_playlist_refresh();
                json_playlist_refresh();
                json_load_playlist();
			}
	});
}

function suararadio_playlist_del(idx) {
	jQuery.ajax({
		type: "GET",
		url:  "/suararadio_api/del_playlist_item/"+idx,
		dataType: "json",
		success: function (data) {
				str = "File: "+data.title+", telah dihapus.";
				suararadio_flash(str);
			}
	});
}

function suararadio_flash(str) {
	$("div#flash-msg").html(str).show();
	setTimeout(function(){ $("div#flash-msg").fadeOut("slow"); },3000);
}
         

function suararadio_create_player(divid,playlist,width,height) {
	swfobject.embedSWF("/wp-content/plugins/suararadio/xspf_player.swf?autoload=true&repeat_playlist=true&player_title=e-Broadcasting Institute&playlist_url="+playlist, divid, width, height, "9.0.0", "wp-content/plugins/suararadio/js/swfobject/expressInstall.swf");
	return true;
}

function suararadio_hide_player() {
	jQuery("#suararadio_player").css("display","none");
	jQuery("#suararadio_player_menu").css("display","none");
	jQuery("#suararadio_player_container").css("height","16px");
	jQuery("#suararadio_player_container").css("width","16px");
}

function suararadio_show_player() {
	jQuery("#suararadio_player").css("display","inline");
	jQuery("#suararadio_player_menu").css("display","inline");
	jQuery("#suararadio_player_container").css("height","130px");
	jQuery("#suararadio_player_container").css("width","180px");
}

function suararadio_refresh_player(divid) {
}

function json_playlist_refresh(listid) {
    $(".soundplayer").unbind();
    //alert('Playlist Under Development !');
    //return false;
    //var listid = '0';
    $('.add-play-songs ').empty();    
    			// show on progress
				$.ajax({
					type: "POST",
					url:  "/suararadio_api/load_playlist_item/"+listid+"?json",
					dataType: "html",
					success: function (data) {
					   var take_data = JSON.parse(data); 
                       var cek_data = take_data.items;
                       if(!cek_data){
                        $('.add-play-songs ').html('<div style="color:#fff; font-size:12px; text-align:center; margin:20px">- playlist kosong -</div>')  
						return false; 
                       }
                       var cek = cek_data.length;
                       
							if (cek!=0) {
								//str = "Playlist: telah dimuat.";
								//suararadio_flash(str);       
                                
							    var take_data = JSON.parse(data); 
                                var items = take_data.items;
                                var len = items.length;
                                           
                                var newContent = "";
                            	for(i=0; i<len; ++i){
                            	var title = items[i].title;
                                var location = items[i].location;
                                var id = items[i].id;
                                var image = items[i].image;
                                var duration = items[i].duration;
                                var type = items[i].type;
                                
                                if(type =='melon'){
                                    var songid = items[i].songid;
                                    var type_melon = "songid="+songid+""; 
                                    var soundplayer = "melon";   
                                }else{
                                    var type_melon = "";
                                    var soundplayer = "";
                                }
                                
                                var res = id.split("|");
                            	var postid = res[1];
                                
                                var default_img = "http://"+window.location.host+"/wp-content/plugins/suararadio/images/default_mini.jpg";                                
                                                                                                                                
                                newContent = newContent 
                                +"<li class='active'>"
                                +"<span class='minicover-box-kr'>"
                                +"<a href="+location+" "+type_melon+" class='soundplayer "+soundplayer+"'><img class='mini-albums-list' src='"+image+"'></a>" 
                                +"</span>"
                                +"<p class='text-album-list-player'>"+title+" <br>"    
                                +"</p>"
                                +"<a href='javascript:void(0);' num="+i+" onClick='delItemClick("+i+");'>"
                                +"<button class='btn btn-danger btn-mini' type='button' style='text-align:right; margin-left:100px; height:15px; margin-top:-15px;'>"
                                +"<sup>x</sup>"
                                +"</button>"
                                +"</a>"
                                
                                +"</li>";                                
                            	}
                            	
                            	$('.add-play-songs ').prepend(newContent);
                                
                                suararadio_show_player();                       
                                
                                
                                $('#flash-player').show('fast');
                                //$('#playlist-bottom').show('fast');
                                
                                suararadioPlayer.makeSoundPlayer(); 
                                //suararadioPlayer.makePlaylist();	
							}else{
							    $('.add-play-songs ').html('<div style="color:#fff; font-size:12px; text-align:center; margin:20px">- playlist kosong -</div>')  
					        }
						},
					error: function (req, stat, err) {
							str = "Playlist: data gagal dimuat.";
							suararadio_flash(str);
						}
				});
}


function json_load_playlist(listid) {
    $(".soundplayer").unbind();
    //alert('Playlist Under Development !');
    //return false;
    //var listid = '0';
    
    $('#loading_playlist_'+listid).html('loading...');
    //$('.box-kr').hide(); 
    $('.list-play-songs_'+listid).empty();    
    			// show on progress
				$.ajax({
					type: "POST",
					url:  "/suararadio_api/load_playlist_item/"+listid+"?json",
					dataType: "html",
					success: function (data) {
					   var take_data = JSON.parse(data); 
                       var cek_data = take_data.items;
                       if(!cek_data){
                        $('.list-play-songs_'+listid).html('<div style="color:#fff; font-size:12px; text-align:center; margin:20px">- playlist kosong -</div>')  
						return false; 
                       }
                       var cek = cek_data.length;
                       
							if (cek!=0) {
								//str = "Playlist: telah dimuat.";
								//suararadio_flash(str);       
                                
							    var take_data = JSON.parse(data); 
                                var items = take_data.items;
                                var len = items.length;
                                           
                                var newContent = "";
                            	for(i=0; i<len; ++i){
                            	var title = items[i].title;
                                var location = items[i].location;
                                var id = items[i].id;
                                var image = items[i].image;
                                var duration = items[i].duration;
                                var type = items[i].type;
                                 
                                var res = id.split("|");
                            	var postid = res[1];
                                
                                if(type =='melon'){
                                    var songid = items[i].songid;
                                    var type_melon = "songid="+songid+""; 
                                    var soundplayer = "melon";   
                                    var add = "javascript:suararadio_playlist_add('melon',"+songid+")";
                                }else{
                                    var type_melon = "";
                                    var soundplayer = "";
                                    var add = "javascript:suararadio_playlist_add("+postid+",0);";
                                }
                               
                                
                                var default_img = "http://"+window.location.host+"/wp-content/plugins/suararadio/images/default_mini.jpg";                                
                                                                                                                                
                               /* newContent = newContent 
                                +"<li class='active'>"
                                +"<span class='minicover-box-kr'>"
                                +"<a href="+location+" "+type_melon+" class='soundplayer "+soundplayer+"'><img class='mini-albums-list' src='"+image+"'></a>" 
                                +"</span>"
                                +"<p class='text-album-list-player'>"+title+" <br>"    
                                +"</p>"
                                +"<a href='javascript:void(0);' num="+i+" onClick='delItemClick("+i+");'>"
                                +"<button class='btn btn-danger btn-mini' type='button' style='text-align:right; margin-left:100px; height:15px; margin-top:-15px;'>"
                                +"<sup>x</sup>"
                                +"</button>"
                                +"</a>"
                                
                                +"</li>";*/
                                
                                
                                        newContent = newContent 
                                        +"<li style='padding:5px;'>"
                                        +"<img class='cover-artist' src='"+image+"' style='width:40px; height:40px;'>"
                                        +"<p class='playlist-title' style='margin:0px; text-align:left;'>"
                                        
                                        //+"<a href='javascript:void(0);' num="+i+" onClick='delItemClick("+i+");' class='marker pull-right'>"
							            //+"<button class='btn btn-mini btn-danger btn-add ml'><i class='icon-minus'></i></button>"
										//+"</a>"
                                        
                                        
                                        +"<a href="+add+" num="+i+" class='marker pull-right'>"
							            +"<button class='btn btn-mini btn-warning btn-add ml'><i class='icon-plus'></i></button>"
										+"</a>"
							
										+"<a href="+location+" "+type_melon+" class='soundplayer "+soundplayer+" marker pull-right'>"
										+"<button class='btn btn-mini btn-primary btn-add ml'>Play</button>"
										+"</a>"
										+""+title+"<br>"
										+"<span class='small-text'><i class='icon-time'></i> "+duration+"</span>"
														
                                        
                                        +"</p>"
                                        +"</li>"; 
                                                                
                            	}
                            	
                               
                            	$('.list-play-songs_'+listid).prepend(newContent);
                                $('#box-kr_'+listid).show();
                                $('#loading_playlist_'+listid).empty(); 
                                
                                
                                suararadio_show_player();                       
                                
                                //$('#flash-player').show('fast');
                                //$('#playlist-bottom').show('fast');
                                
                                suararadioPlayer.makeSoundPlayer(); 
                                //suararadioPlayer.makePlaylist();	
							}else{
							    $('.list-play-songs_'+listid).html('<div style="color:#fff; font-size:12px; text-align:center; margin:20px">- playlist kosong -</div>')  
					        }
						},
					error: function (req, stat, err) {
							str = "Playlist: data gagal dimuat.";
							suararadio_flash(str);
						}
				});
}

