$.fn.rundownPlugin = function(url, count){
	count = (typeof count === "undefined") ? 0 : count;

	var mySelector = this.selector;

	$.get(url+"/apis/data/rundown", function(data){
		if(!data.length) return;

		/* do all */
		var rdList = "<ul id='rundown-list'>";
		
		$.each(data, function(index, rdItem){
			rdList += "<li log='"+rdItem.log_group+"'>";
			rdList += 	"<span class='additional-info'><span class='log_group' log='"+rdItem.log_group+"'>"+rdItem.log_group.charAt(0)+"</span></span>";
			rdList +=	"<div>";
			rdList +=		"<span class='log_group_name' log='"+rdItem.log_group+"'>"+rdItem.log_group+"</span> <span class='waktu'>"+rdItem.waktu+" </span>";
			rdList +=		"<span class='remark'>"+(rdItem.remark).replace(/start :/gi,'').replace(/start:/gi,'')+"</span>";
			rdList +=	"</div>";
			rdList += "</li>";
			if((count >0) && (index == count)) return false;
		});
		rdList +="</ul>";

		$(mySelector).empty().html(rdList);
		var inputS = $("<input>").attr("type", "hidden")
				.attr("name","mkeyword")
				.attr("value","");
		var inputM = $("<input>").attr("type","hidden")
				.attr("name","morder")
				;
		var frSearch = $("<form>").attr("method","post")
				.attr("action","/member/music")
				.attr("id","rdFSearch")
				.append(inputS).append(inputM).appendTo(mySelector);
		
	},"json");


	return this;
}


$(document).ready(function(){

	$('li[log="SONG"]').live("click",function(){	
		//alert();
		var what = $(this).find(".remark").html().replace("-"," ");
		$('input[name="mkeyword"]').val(what);
		$("#rdFSearch").submit();	
	});


});
