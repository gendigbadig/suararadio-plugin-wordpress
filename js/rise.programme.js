$.fn.programmePlugin = function(url){
	//count = (typeof count === "undefined") ? 0 : count;

	var mySelector = this.selector;

	$.get(url+"/apis/data/programme", function(data){
		if(!data.length) return;

		/* do all */
		var options = "";
		for (var i in data){
			options += "	<div class='program'>";
			options += "		<div class='foto-penyiar'>";
			options += "			<a><img src='https:\/\/graph.facebook.com\/"+data[i].foto+"\/picture?width=200&height=200' ></span>";
			options += "		</div>";
			options += "		<div class='info-program'>";
			options += "			<span class='on-air'>Now On Air</span>";
			options += "			<span class='info-all-program' behavior='scroll' direction='left' loop='-1' scrollamount='2'><a>"+data[i].nama+" with ";
			options +=					 data[i].penyiar+"<br/>";
			options += 					 data[i].mulai+" - "+data[i].selesai+"</a></span>";
			options += "		</div>";
			options += "	</div>";
		}
		$(mySelector).empty().html(options);
	},"json");

	return this;
}
