$.fn.requestPlugin = function(url, count){
	count = (typeof count === "undefined") ? 0 : count;
    
	var mySelector = this.selector;
	$.get(url+"/apis/data/request", function(data){		
		if(!data.length) return;
		/* do all */
		var reqList= "<ul id='request-list'>"; 
		$.each(data, function(key, val) {
			
			var reqPhoto, reqIcon;
			var reqSender = (val.pengirim === null || val.pengirim === "")? ((val.creator === null || val.creator === "" )? 'Unregistered user' : val.creator): val.pengirim;
			if(val.foto === ""){
				reqPhoto = "<span class='req-image' >";
				reqPhoto += reqSender.substring(0,1).toUpperCase()+"</span>";
			}else{
				reqPhoto = "<span class='req-image'>";
				reqPhoto += "<img src='"+val.foto+"' ></span>";
			}
			var theIcon = "";
			switch(val.jenis){
				case "t":
					theIcon="src='/wp-content/plugins/suararadio/images/request/twitter.png'";
					break;
				case "f":
					theIcon="src='/wp-content/plugins/suararadio/images/request/fb.png'";
					//reqIcon="<span class='req-icon'><img class='icon' src='themes/img/fb.png'></span>";
					break;
				case "2":
					theIcon="src='/wp-content/plugins/suararadio/images/request/sms.png'";
					//reqIcon="<span class='req-icon'><img class='icon' src='themes/img/sms.png'></span>";
					break;
				case "3":
					switch(val.creator){
						case "a":
							theIcon="src='/wp-content/plugins/suararadio/images/request/android.png'";
							//reqIcon="<span class='req-icon'><img class='icon' src='themes/img/android.png' ></span>";
							break;
						case "b":
							theIcon="src='/wp-content/plugins/suararadio/images/request/bb.png'";
							//reqIcon="<span class='req-icon'><img class='icon' src='themes/img/bb.png'></span>";
							break;
						case "c":
							theIcon="src='/wp-content/plugins/suararadio/images/request/web.png'";
							//reqIcon="<span class='req-icon'><img class='icon' src='themes/img/web.png'></span>";
							break;
						case "i":
							theIcon="src='/wp-content/plugins/suararadio/images/request/iphone.png'";
							//reqIcon="<span class='req-icon'><img class='icon' src='themes/img/iphone.png' ></span>";
							break;
						case "w":
							theIcon="src='/wp-content/plugins/suararadio/images/request/windows.png'";
							//reqIcon="<span class='req-icon'><img class='icon' src='themes/img/windows.png' ></span>";
							break;
						default: 
							//reqIcon="<span class='req-icon'><img class='icon' ></span>";
					}
					break;
				default:
					//reqIcon="<span class='req-icon'><img class='icon' ></span>";	
			}
		
			reqIcon="<span class='req-icon'><img class='icon' "+theIcon+" ></span>";	

			reqList += "<li class='request' id='"+val.id+"' data-type='"+val.jenis+"'>";
			reqList += "<div>";
			reqList += "	<span class='foto-req' firstString='"+reqSender.substring(0,1)+"'>";
			reqList += 			reqIcon;
			reqList += 			reqPhoto;
			reqList += "	</span>";
			reqList += "	<span class='info-req'>";
			reqList += "		<span class='info-atas'><span class='req-sender'>"+reqSender+"</span>";
			reqList += "		<span class='req-time'>"+val.waktu+"</span></span>";
			reqList += "		<span class='req-message'>";
			reqList += 				val.pesan;
			reqList += "		</span>";
			reqList += "	</span>";
			reqList += "</div>";
			reqList += "</li>";

			if((count >0) && (key == count)) return false;
		});
		reqList += "</ul>";

		$(mySelector).empty().html(reqList);
		
	},"json");

	return this;
}
