/**
suararadio_player
dependency: jquery,soundManager,swfobject.js
by: MZR
versi 1.6
last change:
- 5/4/2011 5:38:34 PM, whileloading, menampilkan track buffering
- perbaikan untuk upgrade soundmanager
*/

function SuararadioPlayer() {
	var self = this;
	var pl = this;
	var sm = soundManager;
	
/*
	var _event;
	var _cleanup;
*/
	var _ui;
	
  var isIE = (navigator.userAgent.match(/msie/i));
  var isOpera = (navigator.userAgent.match(/opera/i));
  var isSafari = (navigator.userAgent.match(/safari/i));
  var isChrome = (navigator.userAgent.match(/chrome/i));
  var isFirefox = (navigator.userAgent.match(/firefox/i));
  var isTouchDevice = (navigator.userAgent.match(/ipad|ipod|iphone/i));
  var plugindir = '/wp-content/plugins/suararadio/';
  //var imgOn = plugindir+'images/spka.gif';
  var imgOn = plugindir+'images/spka.png'; // edit untuk themes baru
  var imgOff = plugindir+'images/spk.png';
  
  this.links = [];
  this.playlist = [];
  this.tmpImg = '';
  this.index = 0;
  this.idx = 0;
  this.playObj = null;
  this.lastSound = null;
  this.detIdx = 0;
  this.detCount = 0;
  this.dragActive = false;
  this.dragExec = new Date();
  this.dragTimer = null;
  this.lastWPExec = new Date();
  this.lastWLExec = new Date();
  this.isMelon = "N";
  this.jwPlayer = false;
  this.songId = "110113973";
  this.playtime = 0;
  
  this.config = {
  	playNext: true,
  	usePeakData: true,
  	mode: 0, // 0: links, 1: playlist, 2: streaming
  	useThrottling: true
  }

/*  
  _event = (function() {
    var old = (window.attachEvent && !window.addEventListener),
    _slice = Array.prototype.slice,
    evt = {
      add: (old?'attachEvent':'addEventListener'),
      remove: (old?'detachEvent':'removeEventListener')
    };
    function getArgs(oArgs) {
      var args = _slice.call(oArgs), len = args.length;
      if (old) {
        args[1] = 'on' + args[1]; // prefix
        if (len > 3) {
          args.pop(); // no capture
        }
      } else if (len === 3) {
        args.push(false);
      }
      return args;
    }
    function apply(args, sType) {
      var element = args.shift(),
          method = [evt[sType]];
      if (old) {
        element[method](args[0], args[1]);
      } else {
        element[method].apply(element, args);
      }
    }
    function add() {
      apply(getArgs(arguments), 'add');
    }
    function remove() {
      apply(getArgs(arguments), 'remove');
    }
    return {
      'add': add,
      'remove': remove
    };
  }());
*/

  this.events = {
  	play: function() {
  	    //$('#playlist-bottom').show(); 
  		self._ui.player.show();
  		self._ui.buPause.show();
		var atrcls = $(self.playObj).attr("class");
		if (atrcls!='streamplayer') {
  		  //self.tmpImg = $(self.playObj).children('img').attr('src');
  		  $(self.playObj).children('img').attr('src',imgOn);
  		  $(self.playObj).children('img').attr('played',"1");
		}
  		self._ui.buPlay.hide();
  	},
  	resume: function() {
  		if (pl.dragActive) {
  			return false;
  		}
  		self._ui.buPause.show();
  		self._ui.buPlay.hide();
  		self._ui.timeline.removeClass("paused");
  	},
  	stop: function() {
              
  		if (self.isMelon=='Y') {
  			self.deleteMelonPlayer();
  		} else {
  			self.lastSound.destruct();
  		}
  		//sm.destroySound('sr_soundplay');
		//self._ui.player.hide();
		var atrcls = $(self.playObj).attr("class");
		if (atrcls!='streamplayer') {
			$(self.playObj).children('img').attr('src',imgOff);
			$(self.playObj).children('img').removeAttr('played');
		}
		self._ui.timeline.text('00:00:00');
		self._ui.barPos.width('0%');
		if (self.config.mode==2) {
			self.hideStreamPlayer();
		}
		self.lastSound = null;
  	},
  	load: function(bSuccess) {
  		if (this.readyState==2) {
  		//if (!bSuccess) {
  			self.events.stop();
  			self.debug('Gagal loading file!');
  		}
  	},
  	pause: function() {
  		if (pl.dragActive) {
  			return false;
  		}
  		self._ui.buPause.hide();
  		self._ui.buPlay.show();
  		self._ui.timeline.addClass("paused");
  	},
  	finish: function() {
  		if (self.isMelon=='Y') {
  			self.deleteMelonPlayer();
  		} else {
  			self.lastSound.destruct();
  		}
		self._ui.player.hide();
		self._ui.timeline.text('00:00:00');
		self._ui.barPos.width('0%');
		$('#listTimeLine').text('00:00:00');  // di playlist member
		$(self.playObj).children('img').attr('src',imgOff);
		$(self.playObj).children('img').removeAttr('played');
		self.lastSound = null;
		self.debug("playnext: "+self.config.playNext+" mode: "+self.config.mode);
		if (self.config.mode==1 && self.config.playNext) {
			self.nextPlaylist();
			//return true;
		}
		if (self.config.mode==0 && self.config.playNext) {
			self.nextPlay();
			//return true;
		}
  	},
  	whileloading: function() {
  		function doWork() {
  			self.setLoad(this.bytesLoaded,this.bytesTotal);
  		}
  		if (!pl.config.useThrottling) {
  			doWork.apply(this);
  		} else {
  			var d = new Date();
        if (d && d-self.lastWLExec>30 || this.bytesLoaded === this.bytesTotal) {
          doWork.apply(this);
          self.lastWLExec = d;
        }
  		}
  	},
  	whileplaying: function() {
  		var d = null;
  		if (pl.dragActive || !pl.config.useThrottling) {
  		} else {
  			d = new Date();
  			if (d-self.lastWPExec>60) {
	      	if (sm.flashVersion >= 9) {
	      		if (pl.config.usePeakData && this.instanceOptions.usePeakData) {
	            self.updatePeaks.apply(this);
	          }
	      	}
	  			self.updateTimer.apply(this);
	  			self.updateBar.apply(this);
	  			self.lastWPExec = d;
	  		}
	  	}
  	},
  	bufferchange: function() {
  	},
  	id3: function(){
  		var prop = null;
  		var data = '';
  		for (prop in this.id3) {
  			data += prop+': '+this.id3[prop]+','; // eg. title: Loser, artist: Beck
			}	
  		self.debug("id3: "+data);
  	},
  	melload: function(obj) {
        self.setLoad(obj.loaded,obj.total);
  	},
	melstate: function(obj) {
		//IDLE, BUFFERING, PLAYING, PAUSED, COMPLETED
		currentState = obj.newstate;
        previousState = obj.oldstate;
        if(currentState == "COMPLETED"){
        	self.events.finish();
        }
        if(currentState=="PLAYING"){
        	self.events.play();
        }
        if(currentState=="PAUSED"){
            self.events.pause();
        }
	},
	meltime: function(obj) {
        self.setTimer(obj.position);
        self.setBar(obj.position,self.playtime);
	},
	melvolume: function() {},
	melLoadCallback: null,
  }
  this.debug = function(str) {
  	sm._writeDebug(str);
  } //endfunc debug
  this.updatePeaks = function() {
  	self._ui.peakLeft.width((this.peakData.left*100)+'%');
  	self._ui.peakRight.width((this.peakData.right*100)+'%');
  } //endunc updatePeaks
  this.setLoad = function(curload,totload) {
	  self._ui.barLoad.width((curload/totload*100)+'%');
  }
  this.setBar = function(curpos,duration) {
	  self._ui.barPos.width((curpos/duration*100)+'%');
   
  }
  this.updateBar = function () {
	self.setBar(this.position, this.durationEstimate);
  } //endfunc updateBar
  this.setTimer = function (curpos) {
	  var secs = 0;
	  var mins = 0;
	  var hours = 0;
	  
	  secs = Math.round(curpos);
	  hours = Math.floor(secs/3600);
	  secs = secs%3600;
	  mins = Math.floor(secs/60);
	  secs = secs%60;
	  hours = hours.toString();
	  mins = mins.toString();
	  secs = secs.toString();
	  self._ui.timeline.text((hours.length>1?'':'0')+hours+':'+(mins.length>1?'':'0')+mins+':'
		+(secs.length>1?'':'0')+secs);
	  if (self.config.mode==1) $('#listTimeLine').text((hours.length>1?'':'0')+hours+':'+(mins.length>1?'':'0')+mins+':'
		+(secs.length>1?'':'0')+secs);
  }
  this.updateTimer = function () {
	  self.setTimer(this.position/1000);
  } //endfunc updateTimer
  this.togglePlayNext = function() {
  	if (self.config.playNext) {
  		self.config.playNext=false;
  		self._ui.buList.removeClass('listactive').addClass('list');
  	} else {
  		self.config.playNext=true;
  		self._ui.buList.removeClass('list').addClass('listactive');
  	}
  } //endfunc togglePlayNext
  this.makeSoundPlayer = function(selector) {
  	if (selector==undefined) selector='a.soundplayer,a.streamplayer';
  	self.links = $(selector);
    //$(document).on('click',selector,this.handleClick);
  	 for(i=0;i<self.links.length;i++) {
  		$(self.links[i]).attr('id','_srsnd_'+i).bind('click',this.handleClick);
                
  	 }
  } //endfunc makeSoundPlayer
/*
  this.makeStreamPlayer = function(selector) {
  	if (selector==undefined) selector='a.streamplayer';
  	var listlinks = $(selector);
  	for(i=0;i<listlinks.length;i++) {
  		$(listlinks[i]).attr('id','_srstr_'+i).bind('click',this.handleClick);
  	}
  } //endfunc makeStreamPlayer
*/
  this.makePlaylist = function(selector) {
  	if (selector==undefined) {
  		selector='#listItem a.soundplayer';
  	}
  	if (typeof selector=='string') {
  		self.playlist = $(selector);
  	} else if (typeof selector=='object') {
  		self.playlist = $(selector).children('a.soundplayer');
  	}
  } //endfunc makePlaylist
  
  this.init = function() {
  	// tambahkan div template, bisa diganti sesuai kebutuhan
  	var str ='<div class="player-fixed-bottom-new">'+  
             
             '<div id="box-lyric" class="span4" style="display:none; font-size:12px; color:#fff; cursor:pointer">'+
             '<div id="close_lirik" style="float:right;" onclick="close_lirik();">X</div>'+
			 '<p id="info_lirik"> </p>'+ 
             '<p id="teks_lirik"> </p>'+ 						
			 '</div>'+
             
                '<div class="flash-player " id="flash-player" style="display: none;">'+
				    '<div class="">'+
                        '<div class="bottom-player-box">'+
                        '<div class="row">'+
                    	   '<div class="span13">'+	
                                '<div class="audio-controll-box ">'+
                                    '<div class="plays-button">'+
                                    '<div class="btn-group">'+
								        '<a href="#back" role="button" class="btn btn_player btn-inverse marker mb-backward buttonplayer prev " title="Prev"><img src='+plugindir+'images/prev-2.png style="width:15px;height:15px;"></a>'+
										'<a href="#stop" role="button" class="btn btn_player btn-inverse marker buttonplayer stop" title="Stop"><img src='+plugindir+'images/stop-2.png style="width:15px;height:15px;"></a>'+										
                                        '<a href="#play" role="button" class="btn btn_player btn-inverse marker buttonplayer play" style="display:none;" title="Play"><img src='+plugindir+'images/play-2.png style="width:15px;height:15px;"></a>'+
										'<a href="#pause" role="button" class="btn btn_player btn-inverse marker buttonplayer pause" style="display:none;" title="Pause"><img src='+plugindir+'images/pause-2.png style="width:15px;height:15px;"></a>'+
                                        '<a href="#next" role="button" class="btn btn_player btn-inverse marker buttonplayer next" title="Next"><img src='+plugindir+'images/next-2.png style="width:15px;height:15px;"></a>'+
								    
                                        '</div>'+
                                    '</div>'+
                                    '<div class="music-bars hidden-phone">'+
                                        '<!--<div class="hidden-phones pull-right">'+
    											'<a href="#" class="marker" data-original-title="share"><i class="icon-heart"></i></a> &nbsp'+
    											'<a href="#" class="marker" data-original-title="add to playlist"><i class="icon-ok"></i></a>'+
    										'</div>-->'+
                                            '<div class="vumeter">'+
                    							     '<div class="left" style="width: 0%;"></div>'+
                    							     '<div class="right" style="width: 0%;"></div>'+
                                            '</div>'+
    										'<img id="img" class="cover left" src="http://'+window.location.host+'/wp-content/plugins/suararadio/images/default_mini.jpg">'+
    											
    											'<p id="white"> - </p>'+
                                                
    										
    										'<div class="wrapper-table">'+
    											 '<p class="time-track right"><span class="timeline">00:00:00</span></p>'+
    											  '<!--<div class="progres progress-striped active ">'+   											
    											     '<div class="bar bar-danger " style="width: 40%;"></div>'+
    											  '</div>-->'+
                                                  '<div class="statbar"><div class="position" style="width: 0%;"></div><div class="loading" style="width: 0%;"></div>'+
                							      '</div>'+
                                                  
                                                  '<!--<div class="vumeter">'+
                    							     '<div class="left" style="width: 0%;"></div>'+
                    							     '<div class="right" style="width: 0%;"></div>'+
                    							'</div>-->'+

    										'<div class="clear"></div>'+
                                            
    										'</div>'+
                                    '</div>'+
                                    '<!--<div class="music-bars-phone visible-phone">'+
                                        	
    										'<div class="wrapper-table">'+
    											 '<p class="time-track-phone right"><span class="timeline">00:00:00</span></p>'+
						  						 '<div class="statbar"><div class="position" style="width: 0%;"></div><div class="loading" style="width: 0%;"></div>'+
                							      '</div>'+
                                                  
                                                 '<div class="clear"></div>'+
                                                    
    										'</div>'+
                                    '</div>-->'+
                                '</div>'+	
                           
                           '<div class="info-player hidden-phone">'+
                               '<div class="volume-prev-next pull-right">'+
									
										'<div class="btn-group">'+
										  '<!--<button class="btn  btn-inverse mb-volume marker" data-original-title="volume"><i class="icon-volume-up"></i></button>'+
										  '<button class="btn  btn-inverse mb-repeat marker" data-original-title="repeat" ><i class="icon-heart"></i></button>'+									  
                                          '<button class="btn  btn-inverse mb-repeat marker" data-original-title="repeat" ><i class="icon-repeat"></i></button>'+
										  '<button class="btn  btn-inverse mb-random"><i class="icon-random"></i></button>-->'+
                                          
										  '<!--<button class="btn  btn-inverse mb-random" onclick="lirik();" title="Lirik"><i class="icon-music"></i></button>'+
										  '<button class="btn  btn-inverse mb-random" title=Playlist"><i class="icon-list"></i></button>-->'+					
										  
                                          '<input type="hidden" name="id_streaming" id="id_streaming" value="">'+ 
                                          
                                          '<div class="btn-group" >'+
                                              '<a href="#Save" role="button" class="btn btn_player btn-inverse marker" data-toggle="modal" title="Save Playlist" id="Save_button"><img src='+plugindir+'images/save-2.png style="width:15px;height:15px;"></a>'+
                                              '<a href="#Load" role="button" class="btn btn_player btn-inverse marker" data-toggle="modal" title="Load Playlist" id="Load_button" onclick="loadbutton();"><img src='+plugindir+'images/folder-2.png style="width:15px;height:15px;"></a>'+                                             
                                              '<a href="#clear" role="button" class="btn  btn_player btn-inverse marker" data-toggle="modal" title="Clear All" id="clearPlaylist" onclick="clearPlaylist();"><img src='+plugindir+'images/trash-2.png style="width:13px;height:13px;"></a>'+
                                          '<!--</div>'+
                                          
                                          '<div class="btn-group" style="margin-left:10px;">-->'+
                                              '<input type="hidden" name="listid2" id="listid2" value="">'+
                                              '<!--<button class="btn btn-inverse marker" onclick="show_lirik_streaming();" data-original-title="lyric" href="#" id="info-lyric-streaming" title="Lirik" style="display:none;"><img src="img/trash-2.png" style="width:15px;height:15px;"></button>'+
										      '<button class="btn btn-inverse marker" onclick="show_lirik_melon();" data-original-title="lyric" href="#" id="info-lyric-melon" title="Lirik" style="display:none;"><i class="icon-music"></i></button>'+	  
                                              'button class="btn btn-inverse marker" onclick="playlist();" data-original-title="playlist" href="#" id="showplaylist" title="Playlist"><i class="icon-list"></i></button>-->'+					
										      
                                              '<a id="info-lyric-streaming" class="btn btn_player btn-inverse marker" href="#lirik" onclick="show_lirik_streaming();" data-original-title="lyric" style="display:none;"><img src='+plugindir+'images/lyric.png style="width:16px;height:16px;"></a>'+
                                              '<a id="info-lyric-melon" class="btn btn_player btn-inverse marker" href="#lirik" onclick="show_lirik_melon();" data-original-title="lyric"><img src='+plugindir+'images/lyric.png style="width:16px;height:16px;"></a>'+
                                              '<a id="showplaylist" title="Playlist" class="btn btn_player btn-inverse marker" href="#playlist" onclick="playlist();" data-original-title="playlist"><img src='+plugindir+'images/playlist-2.png style="width:15px;height:15px;"></a>'+
                                          '</div>'+
                                          
                                          
                                          
                                          '</div>'+
										'<br>'+
										
									'</div>'+
                           '</div>'+
                           '</div>'+	
                        '</div>'+
                        '</div>'+
                    '</div>'+           
                '</div>'+
                
                        '<div class="add-play-list " id="playlist-bottom" style="display:none;width:100%;height:100%;">'+
							'<ul class="add-play-songs ">'+
													
							'</ul>'+
                        '</div>'+
                        
                        '<div id="Save" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">'+
                        '<div class="modal-header">'+
                            '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>'+
                            '<h3 id="myModalLabel">Save Playlist</h3>'+
                            '</div>'+
                            '<div class="modal-body" style="text-align:center;">'+
                            '<p>'+
                            '<div class="input-append" id="form_saveas" style="display:none;">'+
                            '<input class="span2" id="playlistname" type="text">'+
                            '<button class="btn btn-warning" id="doSaveAsPlaylist" onclick="doSaveAsPlaylist();" type="button">Save As</button>'+
                            '</div><br>'+          
                            '<button class="btn btn-primary" onclick="doSavePlaylist();" id="savePlaylist2" >Save</button>&nbsp;&nbsp;&nbsp;&nbsp;'+
                            '<button class="btn" id="saveAsPlaylist" onclick="open_formsaveas();">Save As</button>'+
                            '<button class="btn" id="backPlaylist" onclick="back_form();" style="display:none;">Back</button>'+
                            '</p>'+
                        '</div>'+
                        '<div class="modal-footer">'+
                        '</div>'+
                        '</div>'+  
                        
                        '<div id="Load" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">'+
                        '<div class="modal-header">'+
                            '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>'+
                            '<h3 id="myModalLabel">Load Playlist</h3>'+
                            '</div>'+
                            '<div class="modal-body" style="text-align:center;">'+
                            '<ul class="add-playlist">'+
                            '</ul>'+
                        '</div>'+
                        '<div class="modal-footer">'+
                        '</div>'+
                        '</div>'+        
                
                
            '</div>'+
	        '<div id="meloncontainer" style="width: 0px; height: 0px;"><div id="melonplayer"></div></div>';
		$('body').prepend(str);
		self._ui = {
			player: $('#flash-player'),
			buPause: $('#flash-player .pause'),
			buPlay: $('#flash-player .play'),
			buStop: $('#flash-player .stop'),
			buNext: $('#flash-player .next'),
			buPrev: $('#flash-player .prev'),
			buList: $('#flash-player .listactive'),
			controls: $('#flash-player .controls'),
			statbar: $('#flash-player .statbar'),
			barPos: $('#flash-player .statbar .position'),
			barLoad: $('#flash-player .statbar .loading'),
			timeline: $('#flash-player .timeline'),
			peakLeft: $('#flash-player .vumeter .left'),
			peakRight: $('#flash-player .vumeter .right')
		}
		
		if (sm.flashVersion >= 9) {
      //sm.useMovieStar = this.config.useMovieStar; // enable playing FLV, MP4 etc.
      sm.defaultOptions.usePeakData = this.config.usePeakData;
    }
		
		self.makeSoundPlayer();
  	self._ui.buPause.bind('click',function(event){
  		self.togglePause();
  		return false;
  	});
  	self._ui.buPlay.bind('click',function(event){
  		self.togglePause();
  		return false;
  	});
  	self._ui.buStop.bind('click',function(event){
  		self.stop();
        self._ui.player.hide();
        $('#box-lyric').hide();
        $('#playlist-bottom').hide();  
  		return false;
  	});
  	self._ui.buNext.bind('click',function(event){
  		self.nextPlay();
  		return false;
  	});
  	self._ui.buPrev.bind('click',function(event){
  		self.prevPlay();
  		return false;
  	});
  	self._ui.statbar.bind('click',this.handleMouseDown);
  	self._ui.buList.bind('click',function(event){
  		self.togglePlayNext();
  		return false;
  	});
  	this.index = 0;
  }
  
  this.getLink = (isIE)?function(e) {
    // I really didn't want to have to do this.
    return (e && e.target?e.target:window.event.srcElement);
  }:function(e) {
    return e.target;
  } //end getLink
  
  this.handleClick = function(e) {
  	if (e.button > 1) {
	  	// only catch left-clicks
	  	return true;
    }
    var o = self.getLink(e);
    if (o.nodeName.toLowerCase() != 'a') {
    	o = $(o).parent('a');
    	if (!o) return true;
    }
    // multifile gimana ?
    //var soundURL = $(o).attr('href');
    //alert(soundURL);
    
    e.stopPropagation();
    
    if (self.lastSound!=null) {
  		self.stop();
  	}
  	self.config.mode = 0; 
  	if (o.hasClass('streamplayer')) {
	  	    self.playStream(o);
		} else {
			self.play(o);
		}
    
    return false;
  } //end handleClick
  this.setSeek = function(curpos) {
	  if (self.isMelon=='Y') {
		  nsecoffset = Math.floor(curpos*self.playtime);
		  self.lastSound.sendEvent('PLAY');
	      self.lastSound.sendEvent('SEEK',nsecoffset);
	  } else {
		  nMsecOffset = Math.floor(curpos*self.lastSound.durationEstimate);
		  if (!isNaN(nMsecOffset)) {
			  nMsecOffset = Math.min(nMsecOffset,self.lastSound.duration);
		  }
		  if (!isNaN(nMsecOffset)) {
			  self.lastSound.setPosition(nMsecOffset);
		  }
	  }
  } //end setSeek
  this.setPosition = function(e) {
    x = parseInt(e.clientX,10);
    var bar = $('#flash-player .statbar');
    var ostat = bar.offset();
    var wdh = bar.width();
    //self.debug("position: "+x+" bar "+ostat.left+","+wdh);
    var curpos = (x-ostat.left)/wdh;
    self.setSeek(curpos);
  } //endfunc setPosition
  
  this.handleMouseDown = function(e) {
    // a sound link was clicked
    if (isTouchDevice && e.touches) {
      e = e.touches[0];
    }
    if (e.button === 2) {
      return false; // ignore right-clicks
    }
    if (self.config.mode!=2) {
	    //self.lastSound.pause();
	    self.setPosition(e);
    }
    return true;
  } // endfunc handlemousedown

  this.stop = function() {
	if (self.lastSound!=null) {
        if (self.isMelon == "Y" || self.jwPlayer) {
            //self.lastSound.sendEvent('STOP');
            self.events.stop();
        } else {
            self.lastSound.stop();
        }
  	}
  } //end stop
  
  this.togglePause = function() {
	if (self.lastSound!=null) {
        if (self.isMelon == "Y") {
            self.lastSound.sendEvent('PLAY');
        } else {
            self.lastSound.togglePause();
        }
  	}
  } //end pause
  
  ////////////  melon function  ////////
  this.createMelonPlayer = function(data) {
	    var params = {
	        allowfullscreen:"true",
	        allowscriptaccess:"always"
	    }
	    var attributes = {
	        id:'sr_soundplay',
	        name:'sr_soundplay'
	    }
	    var valstreamer = "rtmp://stm.melon.co.id/prelisten";
	    if (data.payed=='Y') {
	    	valstreamer = "rtmp://stm.melon.co.id/melon";
	    } 
	    var flashvars = {
	        streamer: valstreamer,
	        file:"melon",
	        autostart:"true",
	        icons:false
	    }
	  	if (data.payed=='Y') {
	  		swfobject.embedSWF("http://www.melon.co.id/flash/player.swf?certificationId="+data.cert, 
	    		'melonplayer', "0", "0", "9.0.115", false, flashvars, params, attributes);
	  	} else {
	  		swfobject.embedSWF("http://www.melon.co.id/flash/player.swf?songId="+data.songId+"&contentType=M&channelCd=CH0023", 
		    	'melonplayer', "0", "0", "9.0.115", false, flashvars, params, attributes);
	  	}
  }
  
  this.deleteMelonPlayer = function() {
	  swfobject.removeSWF('sr_soundplay');
      var tmp=document.getElementById('meloncontainer');
      if (tmp) tmp.innerHTML = '<div id="melonplayer"></div>';
  }
  //////////// end melon function ////////////
  
  this.play = function(soundURL) {
    button_playlist_kr();
        
  	if (self.lastSound!=null) {
  		if (self.lastSound.playState==0) self.lastSound.play();
  	} else {
  		var sURL = "";
  		if (typeof soundURL=='object') {
  		    var href = $(soundURL).attr('href');
  			var split = href.split('/');
            var postid = split[5];
  		    
            var stream_diradio = $(soundURL).attr('stream_diradio');
            var kontenpil = $(soundURL).attr('kontenpil');
            /* MeLON */
            
            var postid_melon = $(soundURL).attr('songid');
            
            if(kontenpil =='1'){
                api_get_kontenpildetail(postid);  
            }else if(postid_melon){
                //api_get_detail_melon(postid_melon);
            }else if(stream_diradio){
                get_logo_diradio(stream_diradio);
            }else{
                api_get_detail(postid);     
            }
            
            
            
            
            
            
  			var tmp = $(soundURL).attr('id');
  			tmp = tmp.split('_');
  			var idx = parseInt(tmp[tmp.length-1]);
  			self.idx = idx;
  			self.debug("play idx: "+idx);
  			sURL = $(soundURL).attr('href');
  			var tmp = sURL.split(',');
  			self.detCount = tmp.length;
  			self.playObj = soundURL;
  			if (self.detIdx>=0 && self.detIdx<self.detCount) {
  				sURL = tmp[self.detIdx];
  			} else {
  				// idx tidak valid
  				return false;
  			}
  			self.isMelon = "N";
  			if ($(soundURL).hasClass('melon')) {
  				self.songId = $(soundURL).attr('songid');
  				self.isMelon = "Y";
  			}
  		} else if (typeof soundURL=='string'){
  			self.detIdx = 0;
  			self.detCount = 0;
  			sURL = soundURL;
  		} else {
  			return false;
  		}
  		//if (!sURL || !sm.canPlayURL(sURL)) {
	    //	return true; // pass-thru for non-MP3/non-links
	    //}
	    if (self.lastSound==null) {
	    	if (self.isMelon=='Y') {
	    		$.getJSON('/suararadio_api/melon/certification/'+self.songId,function(data){
	    			self.playtime = data.playtime;
	    			self.createMelonPlayer(data);
	    			if (self.events.melLoadCallback) self.events.melLoadCallback.call(this,data);
	    		});
	    	} else {
		    	self.lastSound = sm.createSound({
		    		id:'sr_soundplay',
		    		url:sURL,
		    		onplay:self.events.play,
		    		onstop:self.events.stop,
		    		onpause:self.events.pause,
		    		onresume:self.events.resume,
		    		onfinish:self.events.finish,
		    		onload:self.events.load,
		    		onid3:self.events.id3,
		    		onbufferchange:self.events.bufferchange,
		    		whileloading:self.events.whileloading,
		    		whileplaying:self.events.whileplaying
		    	});
		    	self.lastSound.play();
	    	}
	    }
  	}
  	return false;
  } //end play
  
  this.showStreamPlayer = function() {
  	self._ui.buPrev.hide();
  	self._ui.buNext.hide();
  	self._ui.buList.hide();
  	self._ui.controls.width('40px');
  	//self._ui.player.width('97%');
  }
  this.hideStreamPlayer = function() {
  	self._ui.buPrev.show();
  	self._ui.buNext.show();
  	self._ui.buList.show();
  	self._ui.controls.width('100px');
  	//self._ui.player.width('210px');
  }
  
  this.playStream = function(soundURL) {
  	self.config.mode = 2;
  	this.showStreamPlayer();
  	return self.play(soundURL);
  } //end play
  
  this.playPlaylist = function() {
  		if (self.playlist.length>0) {
  			self.config.playNext = true;
  			self.config.mode = 1; //playlist mode
  			//var firstURL = $(self.playlist[0]).attr('href');
  			//alert($(firstURL).attr('href'));
  			//self.playObj = self.playlist[0];
  			self.index = 0;
  			//self.play(firstURL);
  			self.play(self.playlist[0]);
  		}
  } //end playPlaylist
  this.stopPlaylist = function() {
  	self.stop();
  	//self.config.playNext = false;
  	self.config.mode = 0; //playlist mode
  	//self.playObj = null;
  }//end stopPlaylist
  
  this.nextPlaylist = function() {
  	if (self.lastSound!=null) {
  		self.stop();
  	}
	//alert(self.index);
	var idx = self.index+1;
	if (self.playlist.length>=idx) {
		self.index = idx;
		self.play(self.playlist[idx]);
	}
/*
  	var idx = $(self.playObj).parent().parent().attr('id');
		idx = parseInt(idx)+1;
		if (self.playlist.length>idx) {
			//var soundURL = $(self.playlist[idx]).attr('href');
			self.index = idx;
			//self.playObj = self.playlist[idx];
			//self.play(soundURL);
			self.play(self.playlist[idx]);
		}
*/
  }//end stopPlaylist
  
  this.prevPlaylist = function() {
  	if (self.lastSound!=null) {
  		self.stop();
  	}
	var idx = self.index-1;
	if (0<=idx) {
		self.index = idx;
		self.play(self.playlist[idx]);
	}
/*
  	var idx = $(self.playObj).parent().parent().attr('id');
		idx = parseInt(idx)-1;
		if (0<=idx) {
			//var soundURL = $(self.playlist[idx]).attr('href');
			self.index = idx;
			//self.playObj = self.playlist[idx];
			//self.play(soundURL);
			self.play(self.playlist[idx]);
		}
*/
  }//end stopPlaylist
  
  this.nextPlay = function() {
  	if (self.lastSound!=null) {
  		self.stop();
  	}
/*
  	var tmp = $(self.playObj).attr('id');
  	self.debug("id obj: "+tmp);
  	var tmp2 = $(self.playObj).next('a.soundplayer').html();
  	self.debug("id next: "+tmp2);
  	
  	tmp = tmp.split('_');
  	var idx = parseInt(tmp[tmp.length-1])+1;
*/
	var idx = self.idx+1;
  	//if (idx>=0 && idx<self.links.length) {
  		self.play($('#_srsnd_'+idx));
  	//}
  }//end nextPlay
  this.prevPlay = function() {
  	if (self.lastSound!=null) {
  		self.stop();
  	}
	/*
  	var tmp = $(self.playObj).attr('id');
  	tmp = tmp.split('_');
  	var idx = parseInt(tmp[tmp.length-1])-1;
	*/
	var idx = self.idx-1;
  	//if (idx>=0 && idx<self.links.length) {
  		self.play($('#_srsnd_'+idx));
  	//}
  } //end prevPlay
}

suararadioPlayer = new SuararadioPlayer();
soundManager.useFlashBlock = true;
soundManager.debugMode = true;
soundManager.consoleOnly = true;
soundManager.url = '/wp-content/plugins/suararadio/sm/swf/'; //'./swf/';
soundManager.flashVersion = 9;
soundManager.useHTML5Audio = true;
soundManager.preferFlash = true;
//soundManager.html5Test = /^(probably)$/i;
//soundManager.waitForWindowLoad = true;
soundManager.onready(function() {
  suararadioPlayer.init();
  if (streaming_on_home>0) suararadioPlayer.play($('#_srsnd_0'));
});

function playerReady(thePlayer) {
	//alert(thePlayer.id.search('podPress'));
	if (thePlayer.id.search('podPress')<0) {
		suararadioPlayer.lastSound = window.document[thePlayer.id]; 
		suararadioPlayer.lastSound.sendEvent('VOLUME', 90);
	   	suararadioPlayer.lastSound.addModelListener("LOADED", "suararadioPlayer.events.melload");
		suararadioPlayer.lastSound.addModelListener("STATE", "suararadioPlayer.events.melstate");
		suararadioPlayer.lastSound.addModelListener("TIME", "suararadioPlayer.events.meltime");
		suararadioPlayer.lastSound.addModelListener("VOLUME", "suararadioPlayer.events.melvolume");
		suararadioPlayer.jwPlayer = true;
	}
	//alert(suararadioPlayer.jwPlayer);
}

/*konten*/
function api_get_detail(postid){
    $('#info-lyric-streaming').hide();
    $('#info-lyric-konten').hide();
    $('#info-lyric-melon').hide();
    
    var tes = 'http://'+window.location.host+'/wp-content/plugins/suararadio/images/loader.gif';
    $('#img').attr('src',tes);
    document.getElementById('white').innerHTML = '-'; 
    
    /* streaming */ 
    if(!postid){
        
        
        var idradio = $('#idradio').val();
        
        var tes = 'http://'+window.location.host+'/wp-content/plugins/suararadio/images/loader.gif';
        $('#img').attr('src',tes);
        document.getElementById('white').innerHTML = '- Streaming -'; 
        
        $.post('http://www.diradio.net/apis/data/logo_radio?id='+idradio,
        {},
        function(data_logo){
            //alert(data_logo);
            var take_logo = JSON.parse(data_logo);  
            var logo = 'http://www.diradio.net/wp-content/themes/suararadio/'+take_logo.logo;
            $('#img').attr('src',logo); 
            $('#id_streaming').attr('value',idradio)
            $('#info-lyric-streaming').hide();
            $('#info-lyric-melon').hide();
            
            //lirik_streaming(idradio);
        });
        
        return false;           
    }
    
    $.get('http://'+window.location.host+'/apis/data/detail/'+postid+'/',
        function(data){
            
            var take_data = JSON.parse(data);  
            var judul = take_data.post_title;
            document.getElementById('white').innerHTML = judul; 
            var img = take_data.post_image;
            //alert(data);
            $('#img').attr('src',img);  
            
        });
    
}

/* streaming diradio */
function get_logo_diradio(stream_diradio){
        
        var tes = 'http://'+window.location.host+'/wp-content/plugins/suararadio/images/loader.gif';
        $('#img').attr('src',tes);
        document.getElementById('white').innerHTML = '- Streaming -'; 
        
        $.get('http://www.diradio.net/apis/data/logo_radio?id='+stream_diradio,
        function(data_logo){
            //alert(data_logo);
            var take_logo = JSON.parse(data_logo);  
            var nama_radio = take_logo.name;
            var logo = 'http://www.diradio.net/wp-content/themes/suararadio/'+take_logo.logo;
            $('#img').attr('src',logo); 
            document.getElementById('white').innerHTML = '- Streaming '+nama_radio+' -'; 
            $('#id_streaming').attr('value',stream_diradio);
             
            $('#info-lyric-streaming').hide();
            $('#info-lyric-melon').hide(); 
     
            lirik_streaming(stream_diradio);
        });
        
        return false;        
}

/* kontenpilihan */
function api_get_kontenpildetail(postid){
    var tes = 'http://'+window.location.host+'/wp-content/plugins/suararadio/images/loader.gif';
    $('#img').attr('src',tes);
    
    document.getElementById('white').innerHTML = '-'; 
    //var def_img = 'wp-content/themes/suararadio/images/ad_2.png';
    //$('#img').attr('src',def_img);
    $.get('http://api.suararadio.com/kontenradio/konten/getKontenDetail?id='+postid,
        function(data){
            //alert(data);
            var take_data = JSON.parse(data); 
            var judul = take_data.title;
            document.getElementById('white').innerHTML = judul; 
            var img = take_data.img_url;
            $('#img').attr('src',img);  
            $('#info-lyric-streaming').hide();
        });    
}

function lirik_streaming(idradio){
    document.getElementById('teks_lirik').innerHTML = '';
    document.getElementById('info_lirik').innerHTML = ''; 
    
            $.ajax({
         			type: 'GET',
                    url:'http://api.suararadio.com/lyrics/streaming/info?id='+idradio,
         			data:{
                        //id : idradio                        			    
                    },
         			success: function(data){
                         //alert(data);
                        var take_data = JSON.parse(data); 
                        var status = take_data.status;
                        var artist = take_data.artist;
                        var song = take_data.song;
                         
                        var info_lirik = artist+' - '+song;
                                               
                        if(status =='1'){
                            $('#info-lyric-streaming').show();                            
                            get_lirik(info_lirik,artist,song);                          
                  	    }else{
                  	         document.getElementById('teks_lirik').innerHTML = '- Lirik tidak tersedia -'; 
                  	    }
         			}
            });  
   
}

function get_lirik(info_lirik,artist,song){
    document.getElementById('teks_lirik').innerHTML = '';
    document.getElementById('info_lirik').innerHTML = ''; 
    //$('#info-lyric-streaming').hide();
    
    var loader = '<img src="http://'+window.location.host+'/wp-content/plugins/suararadio/images/loader.gif">';
    $('#info_lirik').html('<b>Loading... </b>');
    

    $.ajax({
 			type: 'GET',
            url:'http://api.suararadio.com/lyrics/lyricsFinder/displaylyrics',
 			data:{
                song : song,
                artist : artist                        			    
            },
 			success: function(data){
                 //alert(data);
                var take_data = JSON.parse(data); 
                var datalirik = take_data.data;
                var status = take_data.status;
                
                document.getElementById('info_lirik').innerHTML = info_lirik; 
                
                if(status =='1'){
                    $( "#teks_lirik" ).html( "<p>"+datalirik+"</p>" );
                    //document.getElementById('teks_lirik').innerHTML = datalirik;    
                }else{
                    document.getElementById('teks_lirik').innerHTML = 'Lirik tidak tersedia'; 
                }
                
                
 			}
    });
   
}



/* MELON */
/*function api_get_detail_melon(postid_melon){
    $('#box-lyric').hide();
    document.getElementById('teks_lirik').innerHTML = '';
    document.getElementById('info_lirik').innerHTML = '';
    
    var tes = 'http://'+window.location.host+'/wp-content/plugins/suararadio/images/loader.gif';
    $('#img').attr('src',tes);   
    document.getElementById('white').innerHTML = '-'; 
    
    $.ajax({
 			type: 'GET',
            url: 'http://'+window.location.host+'/suararadio_api/melon/album/'+postid_melon,
 			data:{
                                      			    
            },
 			success: function(data){
                 //alert(data);
            var take_data_melon = JSON.parse(data); 
            var songName = take_data_melon.songName;
            var artistName = take_data_melon.artistName;
            var judul = songName+' - '+artistName;
            document.getElementById('white').innerHTML = judul; 
            
            var img = 'http://www.melon.co.id/image.do?fileuid='+take_data_melon.albumMImgPath;
            $('#img').attr('src',img); 
            
            $('#info-lyric-streaming').hide();
            $('#info-lyric-melon').show();
            
            get_lirik(judul,artistName,songName)
             	
 			}
    });
     
} */

/* function button player */

function show_lirik_streaming(){
    var id_streaming = $('#id_streaming').val();
    lirik_streaming(id_streaming);
    $('#box-lyric').toggle('fast');  
}

function show_lirik_melon(){
    $('#box-lyric').toggle('fast');  
}

function close_lirik(){
    $('#box-lyric').hide('fast');  
}

function playlist(){
    //alert('playlist under development !');
    //return false;
    $('#playlist-bottom').toggle('fast');  
	json_playlist_refresh();		
}

function clearPlaylist(){
    if(confirm("Are you sure ?")){
    suararadio_playlist_clear();
    }
}

function open_formsaveas(){
    $('#form_saveas').show(); 
    $('#backPlaylist').show();
    $('#saveAsPlaylist').hide();
    $('#savePlaylist2').hide();
}

function back_form(){
    $('#form_saveas').hide(); 
    $('#backPlaylist').hide();
    $('#saveAsPlaylist').show();
    $('#savePlaylist2').show();
}

$('#Save_button').on('click', function(ev) {
    $('#Save').modal('show'); 
    ev.preventDefault();
});


$('#Load_button').on('click', function() {
    $('#Load').modal('show');
    
});

function loadbutton(){
    loadListPlayist();
}

function doSavePlaylist(){
    var cek = $('#listid2').val();
    if(cek ==''){
        alert('Playlist Kosong !');
        return false;
    }
    var vdata = {};
    vdata.listid = $('#listid2').val();
    savePlaylist(vdata);
}

function doSaveAsPlaylist(){
   
    var vdata = {};
    vdata.name = $('#playlistname').val();
    if (vdata.name.length>0) {
        savePlaylist(vdata);
    } else {
        alert("empty string.");
    }
}


			

  
