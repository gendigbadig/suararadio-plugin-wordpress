		var playlist_page = 1;
		
function loadCallback(data) {
    $('#info-lyric-streaming').hide();
    var tes = 'http://'+window.location.host+'/wp-content/plugins/suararadio/images/loader.gif';
    $('#img').attr('src',tes);
    document.getElementById('white').innerHTML = '-'; 
   
    $.getJSON('/suararadio_api/melon/album/'+data.songId,function(rdata){
		//alert(rdata.albumMImgPath);
		$("#imageAlbum img").attr('src',"http://www.melon.co.id/image.do?fileuid="+rdata.albumMImgPath);
		$("#titleSongs").text(rdata.artistName+" - "+rdata.songName);
            var songName = rdata.songName;
            var artistName = rdata.artistName;
            var judul = songName+' - '+artistName;
            
            document.getElementById('white').innerHTML = judul; 
            
            var img = 'http://www.melon.co.id/image.do?fileuid='+rdata.albumMImgPath;
            $('#img').attr('src',img); 
            
            $('#info-lyric-streaming').hide();
            $('#info-lyric-melon').show();
            
            get_lirik(judul,artistName,songName)        
	});
    
            
}
		  
		$(document).ready(function() {
			$("#listItem").sortable();
			//$("#listItem").disableSelection();
			$("#listItem").bind("sortupdate",function(event,ui){ 
				//alert(var_dump(event,'html',1,1)); 
				var data = $("#listItem").sortable('toArray');
				$.post('/suararadio_api/reorder_playlist/',{'list': data});
				$("#listItem li").each(function(index){
					$(this).attr('id',index);
					//$(this).child().attr('id',index);
				});
				suararadioPlayer.makePlaylist();
			});
			//refreshDelItem();
			$('#dialog-modal').dialog("destroy");
			$('#dialog-modal').dialog({
				height: 80,
				modal: true,
				autoOpen: false,
			});
			$('#savePlaylist').bind('click',function(event){
				var vdata = {};
				vdata.listid = $('#listid').val();
				savePlaylist(vdata);
			});
			$('#saveAsPlaylist').bind('click',function(event){
				$('#dialog-modal').dialog('open');
			});
			$('#clearPlaylist').bind('click',function(event){
				suararadio_playlist_clear();
               
				
			});
			$('#doSaveAsPlaylist').bind('click',function(event){
				var vdata = {};
				vdata.name = $('#playlist-name').val();
				if (vdata.name.length>0) {
					savePlaylist(vdata);
				} else {
					alert("empty string.");
				}
			});
			
			$('span.showPlaylist').bind('click',function(event){
				loadPlaylistItem($(this).attr('listid'));
			});
			$('#doPlay').bind('click',function(event){
				suararadioPlayer.playPlaylist();
				$('#doPlay').hide();
				$('#doStop').show();
			});
			$('#doStop').bind('click',function(event){
				suararadioPlayer.stopPlaylist();
				$('#doPlay').show();
				$('#doStop').hide();
			});
			$('#doPrev').bind('click',function(event){
				suararadioPlayer.prevPlaylist();
			});
			$('#doNext').bind('click',function(event){
				suararadioPlayer.nextPlaylist();
			});
            
            
            
			
			suararadioPlayer.makePlaylist();
			suararadioPlayer.events.melLoadCallback = loadCallback;
		});
		
		///// list of function /////
			function savePlaylist(vdata) {
			     
                //$('#form_saveas').html('loading...');
				$.ajax({
					type: "POST",
					url:  "/suararadio_api/save_playlist/",
					dataType: "json",
					data: vdata,
					success: function (data) {
                    
							if (data!=null) {
								$('#dialog-modal').dialog('close');
								str = "Playlist: "+data+", telah disimpan.";
								suararadio_flash(str);
                                $('#playlistname').val('');
                                //$('#form_saveas').hide();
                                
                                loadListPlayist();
							}else{
							     str = "Playlist: data gagal disimpan.";
								 suararadio_flash(str);
							}
						},
					error: function (req, stat, err) {
							str = "Playlist: data gagal disimpan.";
							suararadio_flash(str);
						}
				});
			}
			function delItemClick(idx){
					//idx = $(this).parent().parent().attr('id');
					$.ajax({
						type: "GET",
						url:  "/suararadio_api/del_playlist_item/"+idx,
						dataType: "json",
						success: function (data) {
								if (data!=null) {
									str = "File: "+data.title+", telah dihapus.";
									suararadio_flash(str);
									$("#listItem li[id="+idx+"]").remove();
									$("#listItem li").each(function(index){$(this).attr('id',index)});
									suararadioPlayer.makePlaylist();
                                    json_playlist_refresh();
                                    json_load_playlist();
								}
							}
					});
			} // endfunc delItemClick
			function delListClick(listid){
					if (confirm("Yakin menghapus list?")) {
						$.ajax({
							type: "POST",
							url:  "/suararadio_api/del_playlist/"+listid,
							dataType: "json",
							success: function (data) {
									if (data!=null) {
										str = "List: "+data.list_name+", telah dihapus.";
										suararadio_flash(str);
										$("#listPlay li[id="+listid+"]").remove();
                                        loadListPlayist();
									}
								}
						});
  				}
			} // endfunc delListClick
/*				
			function refreshDelItem() {
				//alert('do');
				$("a.delItem").bind("click",function(event){
					idx = $(this).parent().parent().attr('id');
					//alert(idx);
					//$("#listItem li[id="+idx+"]").remove();
					$.ajax({
						type: "GET",
						url:  "/suararadio_api/del_playlist_item/"+idx,
						dataType: "json",
						success: function (data) {
								if (data!=null) {
									str = "File: "+data.title+", telah dihapus.";
									suararadio_flash(str);
									$("#listItem li[id="+idx+"]").remove();
									$("#listItem li").each(function(index){$(this).attr('id',index)});
									
								}
							}
					});
				});
			}
*/
			function loadPlaylist(page) {
				$.ajax({
					type: "POST",
					url:  "/suararadio_api/load_playlist/",
					dataType: "json",
					success: function (data) {
							if (data!=null) {
								str = "Playlist: "+data.playlist.list_name+", telah dimuat.";
								suararadio_flash(str);
							}
						},
					error: function (req, stat, err) {
							str = "Playlist: data gagal dimuat.";
							suararadio_flash(str);
						}
				});
			} // loadPlaylist
			
			function loadPlaylistItem(listid) {
			     $(".soundplayer").unbind();
				// show on progress
				$.ajax({
					type: "POST",
					url:  "/suararadio_api/load_playlist_item/"+listid,
					dataType: "html",
					success: function (data) {
							if (data!=null) {
								str = "Playlist: telah dimuat.";
								suararadio_flash(str);
								$("#listActive").text($("#playlist_"+listid).text());
								$("#listItem").html(data);
								$("#listid").val(listid);
                                $("#listid2").val(listid);
								suararadioPlayer.makeSoundPlayer();
								suararadioPlayer.makePlaylist();	
                                
                                json_playlist_refresh(listid);
                                $('#playlist-bottom').show();
                                $('#savePlaylist2').show('fast');	
							}
						},
					error: function (req, stat, err) {
							str = "Playlist: data gagal dimuat.";
							suararadio_flash(str);
						}
				});
			}
               
            
            function loadListPlayist(){
                $('#loading').html('loading...');
                $('.add-playlist').empty();    
                
                $.ajax({
					type: "POST",
					url:  "http://"+window.location.host+"/apis/data/listplaylist?stat=1",
					dataType: "html",
					success: function (data) {
							if (data !=null) {
							     
                                 
                                take_data = JSON.parse(data);  
                                                                                                
                                if(!take_data || take_data ==''){
                                    $('.add-playlist').html('<div style="font-size:12px; text-align:center;">- List playlist kosong -</div>')  					       
                                	return false; 
                                }
                                    var len = take_data.length;
                                                                                     
                                    var newContent = "";
                                	for(i=0; i<len; ++i){
                                	   
                                	    var list_id = take_data[i].list_id;                               
                                        var user_id = take_data[i].user_id;
                                        var list_name = take_data[i].list_name;   
                                        
                                       /* newContent = newContent 
                                        +"<table class='table table-hover'>"
                                        +"<thead>"
                                        +"<tr>"
                                        //+"<th>Playlist Name</th>"
                                        +"</tr>"
                                        +"</thead>"
                                        +"<tbody>"
                                        +"<tr>"
                                        +"<td>"+list_name+"</td>"              
                                        +"<td style='text-align:right;'><button class='btn btn-mini btn-primary' onclick='loadPlaylistItem("+list_id+");'><i class='icon-play-sign'></i>&nbsp;Play All</button>&nbsp;&nbsp;<a listid='"+list_id+"' href='javascript:void(0);'><button class='btn btn-mini btn-warning btn-add'><i class='icon-eye-open'></i>&nbsp;View</button></a>&nbsp;&nbsp;<a onclick='delListClick("+list_id+");' listid='"+list_id+"' href='javascript:void(0);'><button class='btn btn-mini btn-danger btn-add'>x</button></a></td>"                        
                                        +"</tr>"
                                        +"</tbody>"
                                        +"</table>"; 
                                          
                                        */
                                        
                                        newContent = newContent 
                                        +"<li>"
                                        +"<div class='list-kr span12' id='info-lyric' style='width:98%;'>"
                                        +"<h4 class='playlist-title left' style='font-size:16px;'><i class='icon-list'></i>&nbsp;"+list_name+"</h4>"
                                        +"<div class='btn-kr'>"
                                        +"<button onclick='delListClick("+list_id+");' class='btn btn-small marker pull-right' data-original-title='delete' title='delete'><i class='icon-remove'></i></button>"
                                        +"<button onclick='tab("+list_id+");' class='btn btn-small marker pull-right' data-original-title='view' title='view'><i class='icon-eye-open'></i></button>"                                                    
                                        +"<button onclick='loadPlaylistItem("+list_id+");' class='btn btn-small marker pull-right' data-original-title='add to player' title='add to player'><i class='icon-play-circle'></i></button>"												                                                       
                                        +"</div>"
                                        +"</div>"
                                        +"<div id='box-kr_"+list_id+"' class='span12 box-kr' style='width:98%;'>"
										+"<span id='loading_playlist_"+list_id+"'></span>"
                                        +"<ul class='list-content-choose list-play-songs_"+list_id+"'>" 	 
                                        +"</ul>"	
                                        +"</div>"
                                        +"</li>";                             
                                    }  
                                    
                                    $('.add-playlist').prepend(newContent);
                                    $('#loading').empty(); 
                                        //$('.add-playlist').empty();                                                                                                                        
							}
                            
                            //alert('xxx');
						},
					error: function (req, stat, err) {
							str = "Playlist: data gagal dimuat.";
							suararadio_flash(str);
						}
				});
            }  
        
   
    
            
            
            
            
            
