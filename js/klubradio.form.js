(function( $ ) {
   
 /***
  * custom submit function
  *
  * */
/*
  $.fn.submitRegistration = function(){
		
		var submitUrl = 'http://api.suararadio.com/klubradio/API/apply';
           	var krrreg = {};
		var myID = this.selector;

		$.each($(myID + ' :input[name^="data["]').serializeArray(),function(){
			var vv = this.name.replace(/data/,'').replace(/(\[[0-9]\])$/,'');
			krrreg[vv] = this.value;
			
		});
	     $.ajax({ 
              type: "POST",
              url:  submitUrl,
              data: {internalform: "submit", 
		KR_Registarion:krrreg
		},
              dataType : "jsonp",

           success: function(returndata){
 		console.log(returndata);
            if(returndata.status == 0) 
             { return false;
             } else {
		console.log("clicked 1 " + returndata);
                }
                 } 
                 });    
            return false;

  }; */ // end of submit registration

  /***
   *
   * fieldsetLink to behave like tab! 
   *
   ***/
  $.fn.fieldsetlink = function() {
	var myID = this.selector;
	//$(document).on('click', myID+' ul li a', function(){
	$(myID+' ol li:not(:last-child) a').live('click',function(){
		var ftoShow = $(this).attr('data');
		var prevShow = $(myID+' ol#navigation li a.selected').attr('data');
		
		$(prevShow).validateF();
		$(myID+' fieldset').hide();
		$(myID+' fieldset'+ftoShow).fadeIn(1000).children().show();
		
		$(myID+' ol li a').removeClass('selected');
		$(this).attr('class','selected');
		return false;
	});

	$(myID+' ol li:last-child a').live('click',function(){
		$(myID+ ' fieldset').has('legend').each(function(){
			$('#'+$(this).attr('id')).validateF();
		});

		if($(myID).has('.notFilled').length > 0)
		  $("#messages").html('Masukan bertanda <span class="required">*</span> wajib diisi.');
		else{
			$("#messages").empty();
			$(myID).submit();
		}
	});



	  

	// initialize tabs 
	$(document).ready(function(){
		$(myID+' fieldset:gt(0)').hide();
		$(myID+' ol#navigation li a:nth(0)').addClass('selected visited');
	  	
		$(':input[required]').each(function(){
			$('label[for="'+$(this).attr('id')+'"]').append($('<span>').addClass('required').html('*'));	
		});
			

	});


  }; // end fieldsetLink
  $('#regform').fieldsetlink();	  





  $('#membership_type').live('change',function(){
		$('#fieldset-wrapper').attr('class',$(this).val());
  });

  $(':input[required]').live('change',function(){
	$(this).validateType();
  });

  $(document).ready(function(){
	$('#membership_type').change();
  });
 
 
})( jQuery );
