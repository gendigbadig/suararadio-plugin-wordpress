(function( $ ) {
   


  /***
   * 
   * validate functions 
   *
   ***/

// fieldset
  $.fn.validateF = function(){
	var myID = this.selector;
	
	$(myID +' :input[required]').each(function(){	
		$(this).validateType(); 
	});

	if($(myID).find('.err').length > 0)
		$('a[data="'+myID+'"]').addClass('notFilled').removeClass('filled');
	else 	
		$('a[data="'+myID+'"]').addClass('filled').removeClass('notFilled');
	return true;
  } // end validateF
  



  $.fn.validateType = function(){
	
	  var mySelector = '#'+$(this).attr('id');
	  $(this).removeClass('err');
	  $(this).parent().find(mySelector+'-warning').remove();

  // if there's no need to validate type
	  if (($(this).val()=='') || (parseInt($(this).val())<=0)){
		$(this).addClass('err');
		return false;
	  }

	  if(!$(this).is("[data-type]")) return true;
	  var regEx;
	  switch ($(this).attr('data-type'))
	  {
	  case 'number':
		  regEx = new RegExp(/[0-9]/);
	  break;
	  case 'email':
		  regEx = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
	break;
	case 'image':
		regEx = new RegExp(/^.*\.(jpg|jpeg|png)$/i );	
	break;
	  
	  }
	  var myWarning = $('<span id="'+$(this).attr('id')+'-warning">');
	  if($(this).val().match(regEx)){
		  myWarning.addClass('valid')
			  .html('&radic;')
			  .appendTo($(this).parent());
		  return true;
	  }
 
	  $(this).addClass('err');
	  myWarning.addClass('invalid')
		  .html('x')
		  .appendTo($(this).parent());
	  return false;
  } // end validateType
  



 
})( jQuery );
