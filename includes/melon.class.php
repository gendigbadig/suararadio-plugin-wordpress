<?php


/**
 * MelOn Exception
 * @author izhur2001
 * error list:
 * E001 : Unkown mode on search integration
 * E002 : Unkown get property
 */
class MelOnException extends Exception { 
	
}


/**
 * Enter description here ...
 * @author izhur2001
 *
 */

if (!defined(MELON_CLIENT)) define('MELON_CLIENT','SuaraRadio Client');
if (!defined(MELON_PASSWORD)) define('MELON_PASSWORD','FDC6AE04C07612604790007F01002015');
if (!defined(MELON_API_URL)) define('MELON_API_URL','http://118.98.31.135:8000/mapi/');

class MelOnAPI{

    protected $curl;
    protected $mapiUrl;
    protected $chcode;
    protected $userID;
    protected $userMail;
    protected $userPasswd;
    protected $isInit = 0;
    
    private $cookname;
    private $cname = "SuaraRadio Client";
    private $cpass = "FDC6AE04C07612604790007F01002015";

    function __construct($mapiUrl='',$clientName='',$clientPasswd='') {
        $this->curl = curl_init();
        $this->mapiUrl = "http://118.98.31.135:8000/mapi/";
        if ($mapiUrl!='') $this->mapiUrl = $mapiUrl;
        if ($clientName!='') $this->cname = $clientName;
        //if ($clientPasswd!='') $this->cpass = $clientPasswd;
        $this->cookname = md5($_SERVER['HTTP_HOST']);
    }
    
    // setter & getter
    public function __get($name) {
    	if (array_key_exists($name, get_class_vars(__CLASS__))) {
    		return $this->name;
    	} else {
    		throw new MelOnException("Unkown get property", 'E002');
    	}
    }

    protected function startMapi(){
    	if ($this->isInit>2) return false;
        try {
            curl_setopt($this->curl, CURLOPT_POST, 1);
            curl_setopt($this->curl, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
            curl_setopt($this->curl, CURLOPT_POSTFIELDS, 'clientName=' . urlencode($this->cname) . '&password=' . urlencode($this->cpass));
            curl_setopt($this->curl, CURLOPT_COOKIEJAR, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
            curl_setopt($this->curl, CURLOPT_URL, $this->mapiUrl . 'authentication/client');
            curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
            $result = curl_exec($this->curl);
						$this->chcode = @json_decode($result)->channelCd;
						$this->isInit+=1;
						//curl_close($this->curl);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    
	public function loginUser($uid, $upass){
		curl_setopt($this->curl, CURLOPT_POST, 1);
		curl_setopt($this->curl, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
		curl_setopt($this->curl, CURLOPT_POSTFIELDS, 'userEmail='. $uid .'&password='. base64_encode($upass) );
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($this->curl, CURLOPT_COOKIEJAR, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
		#echo md5($uid)."login<br/>";
		curl_setopt($this->curl, CURLOPT_URL, $this->mapiUrl . 'authentication/user');
		$result = curl_exec($this->curl);
		#var_dump($result);
		$result = json_decode($result,true);
		if(@$result['code']=="EC100" && $this->isInit<2){
			$this->startMapi();
			return $this->loginUser($uid, $upass);
		}else{
			$this->userID = $result['userId'];
			$this->userMail = $result['email'];
			return $result;
		}
	}
	
	public function serviceList($userid){
		curl_setopt($this->curl, CURLOPT_POST, 0);
		curl_setopt($this->curl, CURLOPT_URL, $this->mapiUrl . 'users/'. $userid .'/onservice'); # this is where you are requesting POST-method form results (working with secure connection using cookies after auth)
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($this->curl, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
		
		$xxx = curl_exec($this->curl);
		#var_dump($xxx);
		$xxx = json_decode($xxx,true);
		return;
		if(@$xxx['code']=="EC100" && $this->isInit<2){
			$this->startMapi();
			return $this->serviceList($userid);
		}else{
			return $xxx;
		}
	}
	
	public function getCertificationMusic($userid, $songid, $type){
		#curl_setopt($this->curl, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . "/data/". md5($this->userMail) .".log");
		curl_setopt($this->curl, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
		curl_setopt($this->curl, CURLOPT_POST, 1);
		curl_setopt($this->curl, CURLOPT_POSTFIELDS, 'userId='. $userid .'&songId='. $songid .'&channelCd='. $this->chcode .'&contentType='.$type);
		curl_setopt($this->curl, CURLOPT_URL, $this->mapiUrl . 'streaming/certification');
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
		#var_dump('userId='. $userid .'&songId='. $songid .'&channelCd='. $this->chcode .'&contentType='.$type);
		#echo  md5($this->userMail);
		$xxx = curl_exec($this->curl);
		$xxx = json_decode($xxx,true);
		if(@$xxx['code']=="EC100" && $this->isInit<2){
			#echo "xxxxxcccc";
			$this->startMapi();
			return $this->getCertificationMusic($userid, $songid, $type);
		}else{
			return $xxx;
		}
	}
	
	// add by MZR
	/**
	 * Get album data from mode
	 * @param mixed $params
	 * @return mixed
	 */
	public function getAlbums($params=array()) {
		$armodes = array('artist','genre','new','hot');
		$mode = "/new";
		if (isset($params['mode']) && $params['mode']!='' && in_array($params['mode'],$armodes)) {
			switch ($params['mode']) {
				case 'artist': $mode = "/by/artist"; break;
				case 'genre': $mode = "/by/genre"; break;
				case 'new': $mode = "/new"; break;
				case 'hot': $mode = "/hot"; break;
			}
		}
		if (isset($params['albumId']) && preg_match('/\d+/',$params['albumId'],$matches)) {
			$mode = "/".$params['albumId'];
		}
		$var = "";
		if (isset($params['offset'])) {
			$var .= ($var? "&":"")."offset=".$params['offset'];
		}
		if (isset($params['limit'])) {
			$var .= ($var? "&":"")."limit=".$params['limit'];
		}
		if (isset($params['dir'])) {
			$var .= ($var? "&":"")."dir=".$params['dir'];
		}
		if (isset($params['albumId'])) {
			$var .= ($var? "&":"")."albumId=".$params['albumId'];
		}
		if (isset($params['artistId'])) {
			$var .= ($var? "&":"")."artistId=".$params['artistId'];
		}
		if (isset($params['genreId'])) {
			$var .= ($var? "&":"")."genreId=".$params['genreId'];
		}
		$var = ($var? "?":"").$var;
		
		curl_setopt($this->curl, CURLOPT_HTTPGET, 1);
		curl_setopt($this->curl, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
		curl_setopt($this->curl, CURLOPT_URL, $this->mapiUrl . 'albums'.$mode.$var);
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec($this->curl);
		#var_dump('albums'.$mode.$var,$result);
		$result = json_decode($result,true);
		#var_dump($result);
		if(@$result['code']=="EC100" && $this->isInit<2){
			$this->startMapi();
			return $this->getAlbums($params);
		}else{
			return $result;
		}
	}

	/**
	 * Get Chart data according mode 
	 * @param string $mode
	 * @param string $startDate
	 * @throws MelOnException
	 * @return mixed
	 */
	public function getChart($mode='daily',$startDate='',$params=array()) {
		$armodes = array('daily','weekly','monthly');
		$vars = array();
		// yyyyMMdd
		if (!in_array($mode,$armodes)) {
			throw new MelOnException("Unkown mode", 'E001');
		}
		if ($mode=='daily') {
			$startDate = "";
		}
		if (isset($params['offset'])) {
			#$var .= ($var? "&":"")."offset=".$params['offset'];
			$vars['offset'] = $params['offset'];
		}
		if (isset($params['limit'])) {
			#$var .= ($var? "&":"")."limit=".$params['limit'];
			$vars['limit'] = $params['limit'];
		}
		$var = "?".http_build_query($vars);
		curl_setopt($this->curl, CURLOPT_HTTPGET, 1);
		curl_setopt($this->curl, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
		curl_setopt($this->curl, CURLOPT_URL, $this->mapiUrl . "chart/$mode".(($startDate)? "/$startDate":"").$var);
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec($this->curl);
		#var_dump($this->mapiUrl . "chart/$mode".(($startDate)? "/$startDate":""),$result);
		$result = json_decode($result,true);
		if(@$result['code']=="EC100" && $this->isInit<2){
			$this->startMapi();
			return $this->getChart($mode, $startDate);
		}else{
			return $result;
		}
	}
	
	/**
	 * get keywords search hit
	 * @return mixed
	 */
	public function getKeywordsHit() {
		curl_setopt($this->curl, CURLOPT_HTTPGET, 1);
		curl_setopt($this->curl, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
		curl_setopt($this->curl, CURLOPT_URL, $this->mapiUrl . 'keywords/hit');
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec($this->curl);
		$result = json_decode($result,true);
		if(@$result['code']=="EC100" && $this->isInit<2){
			$this->startMapi();
			return $this->getKeywordsHit();
		}else{
			return $result;
		}
	}
	
	public function search() {
		curl_setopt($this->curl, CURLOPT_HTTPGET, 1);
		curl_setopt($this->curl, CURLOPT_URL, $this->mapiUrl . "search/index/artist?index=A&channelCd=". $this->chcode);
		curl_setopt($this->curl, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
		#echo md5($this->userMail)."search<br/>";
		$result = curl_exec($this->curl);
		$result = json_decode($result,true);
		if(@$result['code']=="EC100" && $this->isInit<2){
			$this->startMapi();
			return $this->search();
		}else{
			return $result;
		}
	}
	
	/**
	 * Search integration function 
	 * @param string $mode
	 * @param string $keyword
	 * @throws MelOnException
	 * @return mixed
	 */
	public function searchIntegration($params=array()) {
		// $mode='artist',$keyword=''
		$mode = "/artist";
		$armodes = array('artist','song','album');
		if (isset($params['mode']) && $params['mode']!='' && in_array($params['mode'],$armodes)) {
			$mode = "/".$params['mode'];
		}
		$var = "";
		$vars = array();
		if (isset($params['offset'])) {
			#$var .= ($var? "&":"")."offset=".$params['offset'];
			$vars['offset'] = $params['offset'];
		}
		if (isset($params['limit'])) {
			#$var .= ($var? "&":"")."limit=".$params['limit'];
			$vars['limit'] = $params['limit'];
		}
		if (isset($params['dir'])) {
			#$var .= ($var? "&":"")."dir=".$params['dir'];
			$vars['dir'] = $params['dir'];
		}
		if (isset($params['keyword'])) {
			#$var .= ($var? "&":"")."keyword=".$params['keyword'];
			$vars['keyword'] = $params['keyword'];
		}
		#$var = ($var? "?":"").$var;
		$var = "?".http_build_query($vars);
		
		curl_setopt($this->curl, CURLOPT_HTTPGET, 1);
		curl_setopt($this->curl, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
		curl_setopt($this->curl, CURLOPT_URL, $this->mapiUrl . "search/integration".$mode.$var);
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
		#curl_setopt($this->curl, CURLINFO_HEADER_OUT, true);
		
		#echo md5($this->userMail)."search<br/>";
		$result = curl_exec($this->curl);
		$result = json_decode($result,true);
		#var_dump($this->mapiUrl . "search/integration".$mode.$var,$result);
		if(@$result['code']=="EC100" && $this->isInit<2){
			$this->startMapi();
			return $this->searchIntegration($mode, $keyword);
		}else{
			return $result;
		}
	}
	
	
	/**
	 * getSongs data from MelOn 
	 * @param mixed $params
	 * 	mode		mode songs data: album,artist,genre,new 
	 * 	offset  	default 0
	 * 	limit		default 10
	 * 	albumId		need for mode album
	 * 	artistId	need for mode artist
	 * 	genreId		need for mode genre & new, must be lowest level of genre code in categories
	 *  songId		
	 *  dir			direction sorting, data : asc,desc
	 * @return mixed
	 */
	public function getSongs($params=array()) {
		$armodes = array('album','artist','genre','new','hot');
		$mode = "/new";
		if (isset($params['mode']) && $params['mode']!='' && in_array($params['mode'],$armodes)) {
			$mode = "/".$params['mode'];
		}
		if (isset($params['songId']) && preg_match('/\d+/',$params['songId'],$matches)) {
			$mode = "/".$params['songId'];
		}
		$var = "";
		if (isset($params['offset'])) {
			$var .= ($var? "&":"")."offset=".$params['offset'];
		}
		if (isset($params['limit'])) {
			$var .= ($var? "&":"")."limit=".$params['limit'];
		}
		if (isset($params['albumId'])) {
			$var .= ($var? "&":"")."albumId=".$params['albumId'];
		}
		if (isset($params['artistId'])) {
			$var .= ($var? "&":"")."artistId=".$params['artistId'];
		}
		if (isset($params['genreId'])) {
			$var .= ($var? "&":"")."genreId=".$params['genreId'];
		}
		if (isset($params['dir'])) {
			$var .= ($var? "&":"")."dir=".$params['dir'];
		}
		$var = ($var? "?":"").$var;
		curl_setopt($this->curl, CURLOPT_HTTPGET, 1);
		curl_setopt($this->curl, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
		curl_setopt($this->curl, CURLOPT_URL, $this->mapiUrl . "songs".$mode.$var);
		#echo "\n".$this->mapiUrl . "songs".$mode.$var;
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
		
		$result = curl_exec($this->curl);
		$result = json_decode($result,true);
		if(@$result['code']=="EC100" && $this->isInit<2){
			$this->startMapi();
			return $this->getSongs($params);
		}else{
			return $result;
		}
	}
	
	
	/**
	 * Get Categories and genres
	 * @param array assoc $params
	 * 	genId is genreId
	 * @return mixed false
	 */
	public function getCategories($params=array()) {
		$armodes = array("all");
		$mode = "";
		if (isset($params['mode']) && $params['mode']!='' && in_array($params['mode'],$armodes)) {
			$mode = "/".$params['mode'];
		}
		if (isset($params['catId']) && $params['catId']!='') {
			if (preg_match('/\d+/',$params['catId'],$matches)) {
				$mode = "/".$params['catId']."/genres";	
			}
		}
		curl_setopt($this->curl, CURLOPT_HTTPGET, 1);
		curl_setopt($this->curl, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
		curl_setopt($this->curl, CURLOPT_URL, $this->mapiUrl . "categories".$mode);
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
		
		$result = curl_exec($this->curl);
		#var_dump($result,$this->mapiUrl . "categories".$mode);
		$result = json_decode($result,true);
		
		if(@$result['code']=="EC100" && $this->isInit<2){
			$this->startMapi();
			return $this->getCategories($params);
		}else{
			return $result;
		}
	}
	
	/**
	 * getArtist data
	 * @param mixed $params
	 * @return mixed
	 */
	public function getArtists($params=array()) {
		$armodes = array('hot');
		$mode = "";
		if (isset($params['mode']) && $params['mode']!='' && in_array($params['mode'],$armodes)) {
			$mode = "/".$params['mode'];
		}
		if (isset($params['artistId']) && preg_match('/\d+/',$params['artistId'],$matches)) {
			$mode = "/".$params['artistId'];
		}
		$var = "";
		if (isset($params['offset'])) {
			$var .= ($var? "&":"")."offset=".$params['offset'];
		}
		if (isset($params['limit'])) {
			$var .= ($var? "&":"")."limit=".$params['limit'];
		}
		if (isset($params['albumId'])) {
			$var .= ($var? "&":"")."albumId=".$params['albumId'];
		}
		if (isset($params['genreId'])) {
			$var .= ($var? "&":"")."genreId=".$params['genreId'];
		}
		if (isset($params['dir'])) {
			$var .= ($var? "&":"")."dir=".$params['dir'];
		}
		$var = ($var? "?":"").$var;
		curl_setopt($this->curl, CURLOPT_HTTPGET, 1);
		curl_setopt($this->curl, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
		curl_setopt($this->curl, CURLOPT_URL, $this->mapiUrl . "artists".$mode.$var);
		#echo "\n".$this->mapiUrl . "artists".$mode.$var;
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
				
		$result = curl_exec($this->curl);
		$result = json_decode($result,true);
		
		if(@$result['code']=="EC100" && $this->isInit<2){
			$this->startMapi();
			return $this->getArtists($params);
		} else {
			return $result;
		}
	} //endfunc getArtist

	/**
	* Enter description here ...
	* @param mixed $params
	* 	email*
	*  webPassword*
	*  nickname*
	*  gender
	*  birthday
	*  msisdn
	*  provinceId
	*  quizId
	*  quizAnswer-O-
	*  eventSmsYN
	*  newsMailing
	*/
	public function createUser($params) {
		if (trim($params['email'])!='' && trim($params['webPassword'])!='' && trim($params['nickname'])!='') {
			$params['webPassword'] = base64_encode($params['webPassword']);
			$params['quizId'] = '001';
			$params['quizAnswer'] = 'xxx';
			$vars = http_build_query($params);
			curl_setopt($this->curl, CURLOPT_POST, 1);
			curl_setopt($this->curl, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
			curl_setopt($this->curl, CURLOPT_POSTFIELDS, $vars);
			curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($this->curl, CURLOPT_COOKIEJAR, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
			curl_setopt($this->curl, CURLOPT_URL, $this->mapiUrl . 'users/suara/radio');
				
			$result = curl_exec($this->curl);
			$result = json_decode($result,true);
				
			#var_dump($result);
			if(@$result['code']=="EC100" && $this->isInit<2){
				$this->startMapi();
				return $this->createUser($params);
			} else {
				return $result;
			}
		}
	} //endfunc createUser
	
	public function getSongLyric($songId){
		curl_setopt($this->curl, CURLOPT_HTTPGET, 1);
		curl_setopt($this->curl, CURLOPT_URL, $this->mapiUrl . '/songs/$songId/lyric');
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($this->curl, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
		curl_setopt($this->curl, CURLOPT_COOKIEJAR, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
	
		$result = curl_exec($this->curl);
		$result = json_decode($result,true);
		if(@$result['code']=="EC100" && $this->isInit<2){
			$this->startMapi();
			return $this->getUser($userid);
		}else{
			return $result;
		}
	} // getSongLyric
} //endclass
