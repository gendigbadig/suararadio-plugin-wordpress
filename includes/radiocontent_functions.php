<?php
if (!defined('META_PODPRESSMEDIA')) define('META_PODPRESSMEDIA','podPressMedia');
if (!defined('META_RADIO_NAME')) define('META_RADIO_NAME','');
$keyword = '';



function suararadio_radiocontent_process($path){
	
	$radioname_meta = array(
		"klite-bandung" => array("klite-Bandung","k-lite"),
		"trijaya-palembang" => array("trijaya-palembang"),
		"topfm-bumiayu" => array("topfm951","topfm-brebes"),
		"rdi-lubuklinggau" => array("rdi-lubuklinggau"),
		"rdi-palembang" => array("rdi-palembang"),
		"rdi-prabumulih" => array("rdi-prabumulih"),
		"sgp-lampung" => array("sgp-lampung"),
	);
	// determine slugs to be included in wp_query
	list($radio, $params) = explode("?",strtolower($path[3]));
	

	//$channel_map in uangel_channels.php
	//include SUARARADIO_PLUGIN_DIR.'/includes/uangel_channels.php';
	//var_dump($channel_map);
	if(!$radio) {
		echo json_encode(
			array('klite','trijaya')
		);
		return;
	}
	
	
	// if subchannel is specified, go straight to sluq
	$limit = ($_REQUEST['limit'])? $_REQUEST['limit']:20;
	$offset = 0;
	$paged = ($_REQUEST['page'])? $_REQUEST['page']:1;
	
	//$slugs = $radioname_meta[$radio];

	// get subchannels 
	//$subchannels = $channel_map[$channel];
	//$slugs = flatten($subchannels);	
    

	// exclude by term_id? 
	//$excludes = get_term_ids($excluded_slug);
	//$includes = get_term_ids($slugs);
	
    //$includes = get_categ_ids($slugs);
    //var_dump($includesB);
    //echo "\n\n==========\n\n";
    //var_dump($includes);
	
	//if(isset($_REQUEST['dump'])) {var_dump($excludes); exit; }	
	//if(!$slugs){ 
	//	echo json_encode(
	//		array(
	//		'total_count' => 0,
	//		'posts_per_page' => $limit,
	//		'current_page' => $paged,
	//		'data' => array()
	//	));
	//	return;
	//}
		
	global $suararadio;
	global $current_user;
	global $wpdb,$post;
	
	//$slugs = array('music','berita');
	
	//var_dump($radioname_meta[$radio]);
	
	$args = array(
		//'s' => $keyword,
		//	'category-name' => $slug,
		//'category__in' => $includes,
    //'tag__in' => $includes,
    'meta_query' => array(
       array(
           'key' => 'nama_radio',
           'value' => $radioname_meta[$radio],
           'compare' => 'in',
       )
   	),
		//'tag__not_in' => $excludes,
		'offset' => $offset,
		'post_type' => 'post',
		'post_status'=>array('publish'),
		'posts_per_page'=>$limit,
		'paged' => $paged,
		'orderby' => 'post_date',
		'order' => 'desc'
	);
	
	$temp = array();
	$the_query = new WP_Query( $args );
	/*include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	if(is_plugin_active('relevanssi/relevanssi.php')){
		relevanssi_do_query($the_query);
	}*/
	$found_post_count = intval($the_query->found_posts);
	if ($the_query->have_posts()):
		while ( $the_query->have_posts() ) : $the_query->the_post();
			$vtemp = array();
			$vtemp['id'] = $post->ID;
			$vtemp['post_date'] = $post->post_date;
			$vtemp['title'] =  $post->post_title;
			//$vtemp['tags'] = wp_get_post_tags($post->ID);
			$vtemp['file'] = suararadio_getPodcastUrl($post->ID);
			$media = get_post_meta($post->ID, META_PODPRESSMEDIA, true);
			
			for($i=0; $i<$media.length; ++$i) {
				$vmedia = $media[$i];
				$vparse = parse_url($vmedia['URI']);
				if ($vparse['host']!=$_SERVER['HTTP_HOST']) {
					$vtemp['file_redirect'] = $vmedia['URI'];
				}
			}
        	/*foreach ($media as $vmedia) {
            	$vparse = parse_url($vmedia['URI']);
            	if ($vparse['host']!=$_SERVER['HTTP_HOST']) {
             		$vtemp['file_redirect'] = $vmedia['URI'];
            	}
        	}*/

		/*
		$_url = get_permalink($post->ID);
		$_parsed_url = parse_url($_url);
		if(isset($_parsed_url['host'])){
			$vtemp['url'] = $_url;
		}else{
			$vtemp['url'] = "http://".$_SERVER['HTTP_HOST'].get_permalink($post->ID) ;
		}*/



		//	$vtemp['url'] = "http://".$_SERVER['HTTP_HOST'].get_permalink($post->ID);
			$vtemp['url'] = getPermalink($post->ID);//"http://".$_SERVER['HTTP_HOST'].get_permalink($post->ID);
			if (META_RADIO_NAME!='') {
				$vtemp['radio'] = get_post_meta($post->ID, META_RADIO_NAME, true);
			} else {
				$vtemp['radio'] = $radio;
			}
			//$post_tags = get_the_tags($post->ID);
            $post_category = get_the_category($post->ID);
			if(isset($_REQUEST['dump'])){ echo"<pre>" ;var_dump($post_category); echo "</pre>";}
			foreach ($post_category as $key => $value){	
				$subchannel = array_search_recursive(strtolower($value->slug),$subchannels);
				if($subchannel) {
					$vtemp['subchannel'] = $subchannel[0];
					break;
				}

				//$my_slugs[] = array_search_recursive(strtolower($value->name),$subchannels);
			}


			

	
			//$vtemp['thumb'] = get_post_meta($post->ID, $post->meta_key, $single = true);
			//$vtemp['play_count'] = podPress_playCount($post->ID);
			
						

			$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  			$first_img = $matches[1][0];

  			if(empty($first_img)) {
    				$first_img = "http://www.suararadio.com/wp-content/themes/suararadio/images/ad_2.png";
  			}	
			$vtemp['attachment'] = $first_img;	
			$temp[] = $vtemp;
		endwhile;
	endif;
	echo json_encode( array(
		"total_count" => $found_post_count,
		"posts_per_page" => $limit,
		"current_page" => $paged, 
		"data" => $temp
	));
	return true;
 
	return;
	 

}

function get_channel_slugs($channel){

	switch($channel){
	case "entertainment":
		return array("music", "musik");
		break;
	case "news":
		return array("berita");
		break;
	}


}

function get_term_ids($slugs){
	
	$result = array();
	foreach($slugs as $slug){
		$term = get_term_by('slug',$slug,'post_tag');
		if($term) $result[] = $term->term_id;
	}
	return $result;

} // end of get_term_ids

function get_categ_ids($slugs){
	
	$result = array();
	foreach($slugs as $slug){
		$term = get_term_by('slug',$slug,'category');        
		if($term) $result[] = $term->term_id;
        
        $childs = get_categories('child_of='.$term->term_id);
        if($childs) $result[] = $childs->term_id;
	}
	return $result;

} // end of get_categ_ids

function flatten(array $array) {
    $return = array();
    	array_walk_recursive($array, function($a) use (&$return) { if($a) $return[] = $a; });
    return $return;
}

function array_search_recursive( $needle, $haystack, $strict=false, $path=array() )
{
    if( !is_array($haystack) ) {
        return false;
    }
 
    foreach( $haystack as $key => $val ) {
        if( is_array($val) && $subPath = array_search_recursive($needle, $val, $strict, $path) ) {
            $path = array_merge($path, array($key), $subPath);
            return $path;
        } elseif( (!$strict && $val == $needle) || ($strict && $val === $needle) ) {
            $path[] = $key;
            return $path;
        }
    }
    return false;
}


?>
