<?php
/* 
License:
 ==============================================================================

    Copyright 2012  M Zhuhriansyah R  (email : izhur2001@yahoo.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-107  USA
*/

if (!defined('META_RADIO_NAME')) define('META_RADIO_NAME','nama_radio');
if (!defined('META_THUMB_MOST_PLAYED')) define('META_THUMB_MOST_PLAYED','mostplayed');
if (!defined('META_THUMB_KONTEN_PILIHAN')) define('META_THUMB_KONTEN_PILIHAN','advimage');
if (!defined('META_PODPRESSMEDIA')) define('META_PODPRESSMEDIA','podPressMedia');


function slug_to_category($slug){
	if(in_array($slug,array('berita','news')))
		return get_category_by_slug('berita');

	if(in_array($slug,array('musik','music')))
		return get_category_by_slug('music');


	if(in_array($slug,array('talk','serba-serbi','variety')))
		return get_category_by_slug('variety');

	if(in_array($slug,array('travel','wisata')))
		return get_category_by_slug('travel');

	if(in_array($slug,array('advertising','ad')))
		return get_category_by_slug('advertising');

	return get_category_by_slug($slug);
}

function suararadio_data_process($paths) {
	$params = array();
	switch ($paths[3]) {
		case "weekly_chart":
		      return suararadio_data_most_played(array(
					'mode' => 'lastweek',
					'catid' => 54	
			)); 
		      break;
		case "mostplayed":
			$params['mode'] = $paths[4]; 
			$params['catid'] = $paths[5];
			return suararadio_data_most_played($params);
			break;
		case "kontenpilihan":
			return suararadio_data_konten_pilihan();
			break;
        case "advertising":
			return suararadio_data_advertising();
			break;              
		case "category":
			return suararadio_data_category();
			break;
		case "lists":
			$catid = $paths[4];
			if($catid == "kontenpilihan") 
				return suararadio_data_konten_pilihan();
			$offset = $paths[5];
			return suararadio_data_lists($catid, $offset);
			break;
		case "dummy":
			$catid = $paths[4];
			return suararadio_data_lists_dummy($catid);
			break;
		case "detail":
			$postid = $paths[4];
			return suararadio_data_detail($postid);
			break;

		case "detailed":
			$postid = $paths[4];
			return suararadio_data_detailed($postid);
			break;

		
		
		case "member_playlist":
			return suararadio_data_member_playlist();
			break;
		case "member_account_view":
			return suararadio_data_member_account_view();
			break;
		case "member_content":
			return suararadio_data_member_content();
			break;
		case "member_account":
			return suararadio_data_member_account();
			break;
		case "member_music":
			return suararadio_data_member_music();
			break;
		case "member_virtualcard":
			return suararadio_data_virtualcard();
			break;
		case "member_musicinfo":
			return suararadio_musicinfo();
			break;
		case "request":
			$params['mode'] = $paths[4]; // list,post
			return suararadio_data_request_3($paths[4]);
			break;
        	case "message":
			return suararadio_data_message2();
			break;
		case "rundown":
			if($paths[4]=="archive"){
				$arr = array_slice($paths,6);
				return suararadio_data_rundown_archive($paths[5], implode("/",$arr));
			}
			return suararadio_data_rundown_3();
			break;
		case "podcast":
			// $slug = $paths[4];
			// $offset = $paths[5];
			$slug = $_REQUEST['slug'];
			$offset = $_REQUEST['offset'];
			return suararadio_data_podcast($slug,$offset);
			break;
		case "programme":
			switch($paths[4]){
				case "archive": return suararadio_data_programme_archive();
					break;
				default:
					break;
				
			}
			return suararadio_data_programme_2();
			break;
		case "search":
			$keyword = $paths[6];
			$offset = $paths[4];
			$cat_id = $paths[5];
			//var_dump($paths, $cat_id);exit;
			return suararadio_data_search($keyword,$cat_id,$offset); 
			break;
        case "user":
            return suararadio_data_user();
            break;
        case "listplaylist":
			return suararadio_data_listplaylist();
			break;
	    case "pl":
            return suararadio_data_pl();
		    break;
        case "logo_radio":
			return suararadio_logoradio();
			break;
        case "playlist_save":
            return suararadio_data_playlist_save();
		default:
			status_header('404');
	}
} //endfunc suararadio_melon_process


function suararadio_data_most_played($params) {
	global $suararadio;
	global $current_user;
	global $wpdb;
	global $radio;
	
	$tmp = array();
	$data = array();
	$week = '';
	$tahun = '';
	$bulan = '';
	$catid = 0;
	if($params['catid']!=''){
		if(intval($params['catid'])){
			// it's a number, and therefore ... leave it as it is
			$catid = $params['catid'];
		} else{
			// it's a slug... get it via get_category_by_slug
			$category = get_category_by_slug($params['catid']);
			$catid = $category->term_id;
		}
		//$catid = ($params['catid']!='' && $params['catid']>0) ? $params['catid']:0;
	}
	switch ($params['mode']) {
		case "lastweek":
			if(date("W") == "01") {
				$week = 52; $tahun = date("Y");
			} else { 
				$week = date("W"); $tahun = date("Y"); 
			}
			$data = getMostly('play','W',$week,0,$tahun,$catid, 10);
			break;
		case "lastmonth":
			if(date("n") == 1){
				$bulan = 12; $tahun = date("Y");
			} else { 
				$bulan = date("n"); $tahun = date("Y");
			}
			$data = getMostly('play','M',0,$bulan,$tahun,$catid, 10);
			break;
		case "alltime":
			$data = getMostly('play','A',0,0,0,$catid, 10);
			break;
		default:
	}
	// thumbkontenpremiumradio ---> mostplayed
	foreach($data as $item){
		$vtmp = array();
		$vtmp['title'] = $item->post_title;
		$vtmp['id'] = $item->postid;
		$ditem = get_post( $item->postid );
		$vtmp['post_date'] = $ditem->post_date;
		$vtmp['tags'] = wp_get_post_tags($item->postid);
		$vtmp['file'] = suararadio_getPodcastUrl($item->postid);
        $vtmp['radio'] = get_post_meta($item->postid, "nama_radio", $single = true); 			
			  				                
        $media = get_post_meta($item->postid, META_PODPRESSMEDIA, true);
        foreach ($media as $vmedia) {
            $vparse = parse_url($vmedia['URI']);
            if ($vparse['host']!=$_SERVER['HTTP_HOST']) {
                $vtmp['file_redirect'] = $vmedia['URI'];
            }
        }
        
		//$vtmp['url'] = "http://".$_SERVER['HTTP_HOST'].get_permalink($item->postid);
		$vtmp['url'] = getPermalink($item->postid);//"http://".$_SERVER['HTTP_HOST'].get_permalink($item->postid);
		if (META_RADIO_NAME!='') {
			$vtmp['radio'] = get_post_meta($item->postid, META_RADIO_NAME, true);
		} else {
			$vtmp['radio'] = $radio;
		}
		$vtmp['thumb'] = get_post_meta($item->postid, META_THUMB_MOST_PLAYED, true);
		$vtmp['played'] = $item->jmlplay;
		$vtmp['play_count'] = podPress_playCount($item->postid);

		/*
		$att = array();
		$att_args = array( 'post_type' => 'attachment',
			 'post_mime_type' => 'image/jpeg',
			 'posts_per_page' => -1, 'post_status' =>'any', 'post_parent' => $item->postid ); 
		$attachments = get_posts($att_args);
		if ($attachments) {
			foreach ( $attachments as $attachment ) {
				//	echo apply_filters( 'the_title' , $attachment->post_title );
				$att[] = $attachment->guid;
				//var_dump($attachment);
			}	
		}*/


  		$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $ditem->post_content, $matches);
  		$first_img = $matches[1][0];

  		if(empty($first_img)) {
    			$first_img = "http://www.suararadio.com/wp-content/themes/suararadio/images/ad_2.png";
  		}

		$vtmp['attachment'] = $first_img;



		$tmp[] = $vtmp;
	}
	
	echo json_encode($tmp);
	return true;
} //endfunc suararadio_most_played

function suararadio_data_konten_pilihan() {
	global $suararadio;
	global $current_user;
	global $wpdb;
    global $radio;        

	$tmp = array();
	// kontenpilihanutama --> advimage
	if(isset($_REQUEST['dump'])) var_dump(META_THUMB_KONTEN_PILIHAN);
    
    if($_SERVER['HTTP_HOST'] ==  'www.suararadio.com' || $_SERVER['HTTP_HOST'] ==  'www.diradio.net'
    || $_SERVER['HTTP_HOST'] ==  'www.speedyinstanradio.net' || $_SERVER['HTTP_HOST'] ==  'www.langitmusikradio.com'
    || $_SERVER['HTTP_HOST'] =='www.melonradio.co.id' || $_SERVER['HTTP_HOST'] ==  'speedyinstanradio.net'){
       /* $sql = 
		"SELECT wposts.ID,wposts.post_title,wpostmeta.meta_key,wposts.post_date,wpostmeta.meta_value ".
		"FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta ".
		"WHERE wposts.ID = wpostmeta.post_id ". 
		"AND wpostmeta.meta_key = 'kontenpil_image' ".
		"AND wposts.post_type='post' ".
		"AND wposts.post_status = 'publish' ".
        "AND wposts.post_date < NOW()".																			    
		"ORDER BY wposts.post_date DESC limit 0,8";  */
        $sql ="
		select * from (
	    select post_id,
        group_concat(case meta_key when 'nama_radio' then meta_value else null end ) as nama_radio,
        group_concat(case meta_key when 'kontenpil_image' then meta_value else null end ) as kontenpil_image
        from wp_postmeta 
        where meta_key in ('nama_radio','kontenpil_image')
        group by post_id
        order by post_id
        ) tmp
        left join wp_posts p on p.ID=tmp.post_id 
        where post_type='post' 
		AND post_status = 'publish'
		AND post_date < NOW()		
		AND kontenpil_image !=''
		ORDER BY post_date DESC limit 0,8
";  


    $pageposts = $wpdb->get_results($sql, OBJECT);
	foreach($pageposts as $post){
		$vtmp = array();
		$vtmp['title'] = $post->post_title;
		$vtmp['id'] = $post->ID;
		$vtmp['post_date'] = $post->post_date;
		$vtmp['tags'] = wp_get_post_tags($post->ID); 
		$vtmp['file'] = suararadio_getPodcastUrl($post->ID);
        $media = get_post_meta($post->ID, META_PODPRESSMEDIA, true);
        $nmradio = $post->meta_value;
        foreach ($media as $vmedia) {
            $vparse = parse_url($vmedia['URI']);
            if ($vparse['host']!=$_SERVER['HTTP_HOST']) {
                $vtmp['file_redirect'] = $vmedia['URI'];
            }
        }
		
		$vtmp['url'] = getPermalink($post->ID);
        $vtmp['radio'] = $post->nama_radio;
		
		$vtmp['thumb'] = $post->kontenpil_image;
		$vtmp['attachment'] = $post->kontenpil_image;
		$vtmp['play_count'] = podPress_playCount($post->ID); 
		$tmp[] = $vtmp;
	}

    }else{
	$sql = 
		"SELECT * FROM wp_posts, wp_postmeta 
		WHERE wp_posts.ID = wp_postmeta.post_id 
		AND wp_postmeta.meta_key in ('advimage') 
        AND wp_posts.post_type='post' 
		AND wp_posts.post_status = 'publish' 
        ORDER BY wp_posts.post_modified DESC limit 8";
        
        $pageposts = $wpdb->get_results($sql, OBJECT);
	foreach($pageposts as $post){
		$vtmp = array();
		$vtmp['title'] = $post->post_title;
		$vtmp['id'] = $post->ID;
		$vtmp['post_date'] = $post->post_date;
		$vtmp['tags'] = wp_get_post_tags($post->ID); 
		$vtmp['file'] = suararadio_getPodcastUrl($post->ID);
        $media = get_post_meta($post->ID, META_PODPRESSMEDIA, true);
        $nmradio = $post->meta_value;
        foreach ($media as $vmedia) {
            $vparse = parse_url($vmedia['URI']);
            if ($vparse['host']!=$_SERVER['HTTP_HOST']) {
                $vtmp['file_redirect'] = $vmedia['URI'];
            }
        }
		
		$vtmp['url'] = getPermalink($post->ID);
        $vtmp['radio'] = $radio;
		
		$vtmp['thumb'] = get_post_meta($post->ID, $post->meta_key, $single = true);
		$vtmp['attachment'] = get_post_meta($post->ID, $post->meta_key, $single = true);
		$vtmp['play_count'] = podPress_playCount($post->ID); 
		$tmp[] = $vtmp;
	}                
    }
    
	
	//$tmp['debug'] = get_post_meta($post->ID);
	echo json_encode($tmp);
	return true;
} //endfunc suararadio_konten_pilihan


function suararadio_data_advertising() {
	global $suararadio;
	global $current_user;
	global $wpdb;

	$tmp = array();
	// advertising
    
    $type = $_GET['type'];
    $method = $_GET['method'];
    
    if($type =='1'){
        $where_type = "and advertising ='1'";
    }else if($type =='2'){
        $where_type = "and advertising ='2'";
    }
    
    if($method =='0.1'){
        $where_method = "and (nama_radio is null OR nama_radio ='diRadio')";
    }else{
        $where_method = "";
    }
    
    
    
     $sql = 
     " select * from (
     select post_id,
     group_concat(case meta_key when 'advertising' then meta_value else null end ) as advertising,
     group_concat(case meta_key when 'advertising_image' then meta_value else null end ) as advertising_image,
     group_concat(case meta_key when 'nama_radio' then meta_value else null end ) as nama_radio
     from wp_postmeta 
     where meta_key in ('advertising','advertising_image','nama_radio')
     group by post_id
     order by post_id
     ) tmp
     inner join wp_posts p on p.ID=tmp.post_id 
     and post_status = 'publish' 
	 and post_modified < NOW()
	 ".$where_type."
     ".$where_method."
     order by post_modified desc ";   
 
    
	$pageposts = $wpdb->get_results($sql, OBJECT);
	foreach($pageposts as $post){
        $vtmp = array();
	 
	    $vtmp['title'] = $post->post_title;
		$vtmp['id'] = $post->ID;
		$vtmp['post_date'] = $post->post_date;
		$vtmp['file'] = suararadio_getPodcastUrl($post->ID);
        $vtmp['url'] = getPermalink($post->ID);
        $vtmp['thumb'] = $post->advertising_image;
        $vtmp['type'] = $post->advertising;
                    
       
		$tmp[] = $vtmp;
       																																																																																		
    }; 
	
	echo json_encode($tmp);
	return true;
} //endfunc suararadio_konten_pilihan

function suararadio_data_kp_suararadio(){
	global $wpkontenpil;
	
	/*
	*
	*/
	
} //suararadio_data_kp_suararadio


function suararadio_data_category() {
	global $suararadio;
	global $current_user;
	global $wpdb;
	
	$params = $_REQUEST;

	$tmp = array();
	$params['type'] = ($params['type']) ? $params['type']:'post';
	$params['child_of'] = ($params['child_of']) ? $params['child_of']:'1';
	$params['orderby'] = ($params['orderby']) ? $params['orderby']:'count';
	$params['parent'] = ($params['parent']) ? $params['parent']:'0';
	$params['order'] = ($params['order']) ? $params['order']:'desc';
	$params['hide_empty'] = ($params['hide_empty']) ? $params['hide_empty']:'1';
	$params['hierarchical'] = ($params['hierarchical']) ? $params['hierarchical']:'1';
	$params['taxonomy'] = ($params['taxonomy']) ? $params['taxonomy']:'category';
	
	$tmp = get_categories($params);
	

	echo json_encode($tmp);
	return true;
} //endfunc suararadio_category

function suararadio_data_lists($catid,$offset=0) {
	

	global $suararadio;
	global $current_user;
	global $wpdb,$post; //,$radio;

	//global $_REQUEST;


	$catObj = null;
	if(in_array($catid, array("berita","news"))){
		$catObj = get_category_by_slug('berita');
	}else if($catid=="music"){
		$catObj = get_category_by_slug('music');
	}else if($catid=="talk"){
		$catObj = get_category_by_slug('variety');
	}else if($catid=="travel"){
		$catObj = get_category_by_slug('travel');
	}else if($catid=="advertising"){
		$catObj = get_category_by_slug('advertising');
	}else{
		//by default
		$catObj = get_category_by_slug($catid);
	}
	
	//$catObj = get_category_by_slug('music'); 																
	$category_id = $catObj->cat_ID;				
	//var_dump(get_category_by_slug('musik'));												
	$param = array(
				"cat"=>$category_id,
				//"category_name" => $catid,
				"post_type"=>"post",
				"post_status"=>"publish",			 
				"order"=>"DESC",
				"posts_per_page"=>10,
				"tag__not_in" => array('12999', '164', '14492','15477'),
				//"year" => 2013,
				"date_query"=>array(array("before"=>'now')),
				"offset" => $offset);
	$res = array();	
	$the_query = new WP_Query( $param );
	if($the_query->have_posts()) :
		while ($the_query->have_posts()) : $the_query->the_post();
			$tmp = array();
			$tmp['id'] = $post->ID;
			//$tmp['post_author'] = $post->post_author;
			$tmp['post_date'] = $post->post_date;
			$tmp['title'] = $post->post_title;
			//$tmp['post_title'] = get_the_title();
			//$tmp['post_category'] = $post->post_category;
			//$tmp['post_name'] = $post->post_name;
			//$tmp['post_parent'] = $post->post_parent;
			//$tmp['post_type'] = $post->post_type;
			//$tmp['comment_count'] = $post->comment_count;
			//$tmp['post_content'] = $post->post_content;
		
		$tmp['tags'] = wp_get_post_tags($post->ID);
		$tmp['file'] = suararadio_getPodcastUrl($post->ID);
        	$media = get_post_meta($post->ID, META_PODPRESSMEDIA, true);
        	foreach ($media as $vmedia) {
            		$vparse = parse_url($vmedia['URI']);
            		if ($vparse['host']!=$_SERVER['HTTP_HOST']) {
                		$tmp['file_redirect'] = $vmedia['URI'];
            		}
        	}
		
		// url problem!! some get no HTTP_HOST from get_permalink
		$tmp['url'] = getPermalink($post->ID);
		//$tmp['url'] = get_permalink($post->ID);
		/*if (META_RADIO_NAME!='') {
			$tmp['radio'] = get_post_meta($item->postid, META_RADIO_NAME, true);
		} else {
			$tmp['radio'] = $radio;
		}*/
		
		$tmp['radio'] = get_post_meta($post->ID, 'nama_radio', true);

		//var_dump('meta_key_thumb',$post->meta_key);
		$tmp['thumb'] = get_post_meta($post->ID, $post->meta_key, $single = true);
		$tmp['play_count'] = podPress_playCount($post->ID);
		//   get_attachment_image($post->ID); ///get_post_meta($post->ID, '_wp_attachment_image_alt', true);
		/*	$att = array();
		$att_args = array( 'post_type' => 'attachment',
			 'post_mime_type' => 'image/jpeg',
			 'posts_per_page' => -1, 'post_status' =>'any', 'post_parent' => $post->ID ); 
		$attachments = get_posts($att_args);
		if ($attachments) {
			foreach ( $attachments as $attachment ) {
				//	echo apply_filters( 'the_title' , $attachment->post_title );
				$att[] = $attachment->guid;
				//var_dump($attachment);
			}	
		}
*/
  		$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  		$first_img = $matches[1][0];

  		if(empty($first_img)) {
    			$first_img = "http://www.suararadio.com/wp-content/themes/suararadio/images/ad_2.png";
  		}	


		$tmp['attachment'] = $first_img;
			$res[] = $tmp;
		endwhile;
	
	else:
		$res['data'] = "post masih kosong";
	endif;

	//$tmp['id'] = 1;
	//$tmp['nama'] = "fikri";
	echo json_encode($res);

}

function suararadio_data_listsOld($catid) {
	global $suararadio;
	global $current_user;
	global $wpdb,$post;

	global $_REQUEST;
	
	$tmp = array();
	$metakey = ($_POST['meta_key']) ? $_POST['meta_key']:META_THUMB_MOST_PLAYED;
	$metavalue = ($_POST['meta_value']) ? $_POST['meta_value']:'__NULL__';
	$metacompare = ($_POST['meta_compare']) ? $_POST['meta_compare']:'!=';
	$orderby = ($_POST['orderby']) ? $_POST['orderby']:'date'; 
	$order = ($_POST['order']) ? $_POST['order']:'DESC';
	$posts_per_page = ($_POST['posts_per_page']) ? $_POST['posts_per_page']:'10';

	
	$param = array("cat"=>$catid,
				 "post_type"=>"post",
					 "post_status"=>"publish",			 
					 "meta_key"=>$metakey,
					 "meta_value"=>$metavalue,
					 "meta_compare"=>$metacompare,
 					 "orderby"=>$orderby,
					 "order"=>"DESC",
					 "posts_per_page"=>$posts_per_page
	);
	
	$the_query = new WP_Query( $param );
	$the_query->have_posts();
	$the_query->the_post();
	$flag = 0;

	
	if(count($post) > 0){
		$flag = 1;
		
		$vtmp = array();
		$vtmp['title'] = $post->post_title;
		$vtmp['id'] = $post->ID;
		$vtmp['post_date'] = $post->post_date;
		$vtmp['tags'] = wp_get_post_tags($post->ID);
		$vtmp['file'] = suararadio_getPodcastUrl($post->ID);
        $media = get_post_meta($post->ID, META_PODPRESSMEDIA, true);
        foreach ($media as $vmedia) {
            $vparse = parse_url($vmedia['URI']);
            if ($vparse['host']!=$_SERVER['HTTP_HOST']) {
                $vtmp['file_redirect'] = $vmedia['URI'];
            }
        }
		//$vtmp['url'] = "http://".$_SERVER['HTTP_HOST'].get_permalink($post->ID);
		$vtmp['url'] = getPermalink($post->ID);
		if (META_RADIO_NAME!='') {
			$vtmp['radio'] = get_post_meta($item->postid, META_RADIO_NAME, true);
		} else {
			$vtmp['radio'] = $radio;
		}
		$vtmp['thumb'] = get_post_meta($post->ID, $post->meta_key, $single = true);
		$vtmp['play_count'] = podPress_playCount($post->ID);
		$tmp[] = $vtmp;
		
	}
	echo json_encode($tmp);
	return true;
} //endfunc suararadio_list_contents


function suararadio_data_lists_dummy($slug){
	global $wpdb,$post;
	//include('lists_dummy.php');
	$cat = get_category_by_slug($slug);
	$category_id = $cat->cat_ID;

	$param = array(
		 "cat"=>$category_id,
		 "post_type"=>"post",
		 "post_status"=>"publish",			 
		// "meta_key"=>"thumbkontenpremiumradio",
		// "meta_value"=>"__NULL__",
		// "meta_compare"=>"!=",
		 "orderby"=>"modified",
		 "order"=>"DESC",
		 "posts_per_page"=>15
												
	);
	

	$res = array();
	$query = new WP_Query($param);
	while($query->have_posts()) :  $query->the_post();
		//$post_id = get_the_ID();
		//$tags = wp_get_post_tags($ID);
		//$file = suararadio_getPodcastUrl($ID);
        	//$media = get_post_meta($ID, META_PODPRESSMEDIA, true);
		
		/*	
       		foreach ($media as $vmedia) {
            		$vparse = parse_url($vmedia['URI']);
            		if ($vparse['host']!=$_SERVER['HTTP_HOST']) {
                		$vtmp['file_redirect'] = $vmedia['URI'];
            		}			
        	}*/

		//var_dump($post);

		$res[] = array(
			'title' => $post->post_title,
			'id' => $post->id,
			'post_date' =>$ $post->post_date,
			//'tags' => $tags, 
			'file' => suararadio_getPodcastUrl($post->id),
			//'url' => "http://".$_SERVER['HTTP_HOST'].get_the_permalink(),
			//'radio' => '', 
			//'thumb' => get_post_meta($ID, get_the_meta(), $single = true),
		//	'play_count'=> podPress_playCount($ID)
		);

	endwhile;
	echo json_encode($res);
	return true;

} // endfunc suararadio_data_lists_dummy

function suararadio_data_detail($postid) {
	global $wpdb;
	
	$tmp = array();
	$tmp = (array) get_post( $postid );
	$tmp['post_image'] = catch_that_image($tmp['post_content']);
	echo json_encode($tmp);
	return true;


} //endfunc suararadio_detail

function suararadio_data_detailed($postid) {
	global $wpdb;
	
	$tmp = array();
	$tmp = (array) get_post( $postid );
	$tmp["file"] = suararadio_getPodcastUrl($tmp["ID"]);
	echo json_encode(array($tmp));
	return true;


} //endfunc suararadio_detail




// fungsi klub radio mobile
function suararadio_data_member_playlist(){
	global $suararadio;
	global $current_user;
	global $wpdb;
	
	echo '<div class="playlistTitle">PLAYLIST PLAYER</div><br>';
	include $inc_playlist = SUARARADIO_PLUGIN_DIR.'/includes_mobile/member_playlist.php';
}

function suararadio_data_member_account_view(){
	global $suararadio;
	global $current_user;
	global $wpdb;

	include $inc_playlist = SUARARADIO_PLUGIN_DIR.'/includes_mobile/member_account_view.php'; 
}

function suararadio_data_member_music(){
	global $suararadio;
	global $current_user;
	global $wpdb;

	
		$inc_music = SUARARADIO_PLUGIN_DIR.'/includes_mobile/member_music.php';
		if (file_exists(TEMPLATEPATH.$DS.'member_music.php')) {
			$inc_music = TEMPLATEPATH.$DS.'member_music.php';
		}
		
		include $inc_music;
				
}

function suararadio_data_member_content(){
	global $suararadio;
	global $current_user;
	global $wpdb;
    global $post;

	
	$inc_content = SUARARADIO_PLUGIN_DIR.'/includes_mobile/member_content.php';
	if (file_exists(TEMPLATEPATH.$DS.'member_content.php')) {
		$inc_content = TEMPLATEPATH.$DS.'member_content.php';
	}
	
	include $inc_content;
				
}

function suararadio_data_member_account(){
	global $suararadio;
	global $current_user;
	global $wpdb;

	
		$inc_account = SUARARADIO_PLUGIN_DIR.'/includes_mobile/member_account.php';
		if (file_exists(TEMPLATEPATH.$DS.'member_account.php')) {
			$inc_account = TEMPLATEPATH.$DS.'member_account.php';
		}
		
		include $inc_account;				
}

function suararadio_data_virtualcard(){
	global $suararadio;
	global $current_user;
	global $wpdb;

	
		$inc_virtualcard = SUARARADIO_PLUGIN_DIR.'/includes_mobile/member_virtualcard.php';
		if (file_exists(TEMPLATEPATH.$DS.'member_virtualcard.php')) {
			$inc_virtualcard = TEMPLATEPATH.$DS.'member_virtualcard.php';
		}
		include $inc_virtualcard ;				
}

function suararadio_musicinfo(){
	global $suararadio;
	global $current_user;
	global $wpdb;

	
		$inc_musicinfo = SUARARADIO_PLUGIN_DIR.'/includes_mobile/member_music_info.php';
		if (file_exists(TEMPLATEPATH.$DS.'member_music_info.php')) {
			$inc_musicinfo = TEMPLATEPATH.$DS.'member_music_info.php';
		}
		
		include $inc_musicinfo ;			
}

function suararadio_data_request($params) {
	// mengeluarkan data request
	switch ($params['mode']) {
		case "list":
			$data = array(
				array("id"=>"39","waktu"=>"2013-01-17 11:59:11","jenis"=>"s","pengirim","foto"=>"http://placehold.it/50x50","pesan"=>"isi pesan!!!!"),
				array("id"=>"38","waktu"=>"2013-01-17 11:49:41","jenis"=>"s","pengirim","foto"=>"http://placehold.it/50x50","pesan"=>"isi pesan!!!!"),
				array("id"=>"37","waktu"=>"2013-01-17 11:49:31","jenis"=>"t","pengirim","foto"=>"http://placehold.it/50x50","pesan"=>"isi pesan!!!!"),
				array("id"=>"36","waktu"=>"2013-01-17 11:49:21","jenis"=>"t","pengirim","foto"=>"http://placehold.it/50x50","pesan"=>"isi pesan!!!!"),
				array("id"=>"35","waktu"=>"2013-01-17 11:47:41","jenis"=>"t","pengirim","foto"=>"http://placehold.it/50x50","pesan"=>"isi pesan!!!!"),
				array("id"=>"34","waktu"=>"2013-01-17 11:47:31","jenis"=>"e","pengirim","foto"=>"http://placehold.it/50x50","pesan"=>"isi pesan!!!!"),
				array("id"=>"33","waktu"=>"2013-01-17 11:47:21","jenis"=>"i","pengirim","foto"=>"http://placehold.it/50x50","pesan"=>"isi pesan!!!!"),
				array("id"=>"32","waktu"=>"2013-01-17 11:47:11","jenis"=>"f","pengirim","foto"=>"http://placehold.it/50x50","pesan"=>"isi pesan!!!!",
					"sub"=>array(
						array("id"=>"21","waktu"=>"2013-01-17 11:47:11","jenis"=>"f","pengirim","foto"=>"http://placehold.it/50x50","pesan"=>"isi pesan!!!!"),
						array("id"=>"22","waktu"=>"2013-01-17 11:47:11","jenis"=>"f","pengirim","foto"=>"http://placehold.it/50x50","pesan"=>"isi pesan!!!!"),
						array("id"=>"23","waktu"=>"2013-01-17 11:47:11","jenis"=>"f","pengirim","foto"=>"http://placehold.it/50x50","pesan"=>"isi pesan!!!!"),
						array("id"=>"24","waktu"=>"2013-01-17 11:47:11","jenis"=>"f","pengirim","foto"=>"http://placehold.it/50x50","pesan"=>"isi pesan!!!!"),
						array("id"=>"25","waktu"=>"2013-01-17 11:47:11","jenis"=>"f","pengirim","foto"=>"http://placehold.it/50x50","pesan"=>"isi pesan!!!!"),
						array("id"=>"26","waktu"=>"2013-01-17 11:47:11","jenis"=>"f","pengirim","foto"=>"http://placehold.it/50x50","pesan"=>"isi pesan!!!!"),
						),
				),
				array("id"=>"19","waktu"=>"2013-01-17 11:47:11","jenis"=>"p","pengirim","foto"=>"http://placehold.it/50x50","pesan"=>"isi pesan!!!!"),
				array("id"=>"18","waktu"=>"2013-01-17 11:47:11","jenis"=>"b","pengirim","foto"=>"http://placehold.it/50x50","pesan"=>"isi pesan!!!!"),
				array("id"=>"17","waktu"=>"2013-01-17 11:47:11","jenis"=>"a","pengirim","foto"=>"http://placehold.it/50x50","pesan"=>"isi pesan!!!!"),
				array("id"=>"16","waktu"=>"2013-01-17 11:47:11","jenis"=>"s","pengirim","foto"=>"http://placehold.it/50x50","pesan"=>"isi pesan!!!!"),
				array("id"=>"15","waktu"=>"2013-01-17 11:47:11","jenis"=>"s","pengirim","foto"=>"http://placehold.it/50x50","pesan"=>"isi pesan!!!!"),
				array("id"=>"14","waktu"=>"2013-01-17 11:47:11","jenis"=>"s","pengirim","foto"=>"http://placehold.it/50x50","pesan"=>"isi pesan!!!!"),
				array("id"=>"13","waktu"=>"2013-01-17 11:47:11","jenis"=>"a","pengirim","foto"=>"http://placehold.it/50x50","pesan"=>"isi pesan!!!!"),
				array("id"=>"12","waktu"=>"2013-01-17 11:47:11","jenis"=>"i","pengirim","foto"=>"http://placehold.it/50x50","pesan"=>"isi pesan!!!!"),
				array("id"=>"11","waktu"=>"2013-01-17 11:47:11","jenis"=>"b","pengirim","foto"=>"http://placehold.it/50x50","pesan"=>"isi pesan!!!!"),
				array("id"=>"10","waktu"=>"2013-01-17 11:47:11","jenis"=>"s","pengirim","foto"=>"http://placehold.it/50x50","pesan"=>"isi pesan!!!!"),
				array("id"=>"9","waktu"=>"2013-01-17 11:47:11","jenis"=>"s","pengirim","foto"=>"http://placehold.it/50x50","pesan"=>"isi pesan!!!!"),
				array("id"=>"8","waktu"=>"2013-01-17 11:47:11","jenis"=>"s","pengirim","foto"=>"http://placehold.it/50x50","pesan"=>"isi pesan!!!!"),
			);

			$result = array();
			$result['code'] = "1";
			$result['message'] = "ok";
			$result['data'] = $data;
			
			echo json_encode($result);
			break;
		case "post":
			$data = array();
			$data['sender'] = $_POST['sender'];
			$data['message'] = $_POST['message'];
			
			$result = array();
			$result['code'] = "1";
			$result['message'] = "ok";
			echo json_encode($result);
			break;
	}
	return true;	
}
    
function suararadio_data_message() {
	global $suararadio;
	global $current_user;
	global $wpdb;
	global $wpdbsr;
	global $wprisedb2;
	global $wprisesmsdb2;
	
	if(isset($_POST['sender']) && isset($_POST['message'])){
	    $data = array();
		//$data['type'] = $_POST['type'];
		$data['type'] = 'a';
	    $data['sender'] = $_POST['sender'];
	    $data['message'] = $_POST['message'];
	    
		//connect to database
		$connection  = mysql_connect("db.suararadio.com", "pena", "p3n4_db4dm1n");
		if(!$connection) {
			die('Could not connect: ' . mysql_error());
		}
		mysql_select_db("atradio", $connection);
		
		$result = array();
		//$MESSAGE_TABLE = IDRADIO."_t_messages";
		$MESSAGE_TABLE = "8_t_messages";
		//sql statements
		$sql = "INSERT INTO ".$MESSAGE_TABLE." (jenis,tgl,pesan,nama) VALUES ('".$data['type']."', '".date("Y-m-d")."', '".$data['message']."', '".$data['sender']."')";
		$query_result = mysql_query($sql);
		if(mysql_affected_rows()==1){
			$result['code'] = "1";
			$result['message'] = "ok";
			echo json_encode($result);
			return true;
		}else{
			$result['code'] = "2";
			$result['message'] = "failed saved";
			echo json_encode($result);
			return true;
		}
	}else{
		$result['code'] = "3";
		$result['message'] = "HTTP-POST doesn't work";
		echo json_encode($result);
		return true;
	}
	
	mysql_close($connection);
}

function suararadio_data_message2() {
# 	fungsi untuk menyimpan request dari pendengar, baik dari facebook, twitter, atau sms

	global $suararadio;
	global $current_user;
	global $wpdb;
	global $wpdbsr;
	global $wprisedb2;
	//global $wprisesmsdb;
	global $wprisesmsdb2;
    	
	if(isset($_POST['sender']) && isset($_POST['message'])){
	//karena data nohp tdk dikrim, validasi melalui pengecekan id_member tdk bsa dilakukan ...
		$data = array();
		if(isset($_POST['type'])){
			if($_POST['type'] == 'a'){
				$data['type'] = '3';
				$data['creator'] = 'a';
			}else if ($_POST['type'] == '3'){
                $data['type'] = '3';
				$data['creator'] = 'c';
			}
		}else{
			$data['type'] = '3';
			$data['creator'] = 'a';
		}

		if(isset($_POST['id_creator'])){
			$data['id_creator'] = $_POST['id_creator'];	
		}else{
			$data['id_creator'] = '';
		}

		$data['sender'] = $_POST['sender'];
	    	$data['message'] = $_POST['message'];
		
		$sinc = date("w");
		$DateNow = date("Y-m-d");
		$TimeNow = date("H:i:s");
		
		//echo json_encode($data); return;	
		if (($wprisedb2)) {
		
			$temp = array();
			$MESSAGE_TABLE = "t_sms";
			
			//$sql = "INSERT INTO ".$MESSAGE_TABLE." (jenis,tgl,pesan,nama) VALUES ". 
			//		"('".$data['type']."', '".date("Y-m-d")."', '".$data['message']."', '".$data['sender']."')";
		 	
			$sql_pro = "select id_program from t_program2 where ('".$DateNow."' between awal and akhir) and aktif='1' and stat='1' and sinkron_waktu like '%".$sinc."%' ".
       				" and ('".$TimeNow."' between waktu1 and waktu2)";
			$idprogram = $wprisedb2->get_var($sql_pro);

			$sdata = array("id_msc"=>0,
					  "no_hp"=>$data['sender'],
					  "isi"=>$data['message'],
					  "tgl_msk"=>date("Y-m-d"),
					  "waktu_msk"=> $TimeNow, //date("H:i:s"),
					  "stat"=>'1',
					  "id_program"=>$idprogram,
					  "id_lagu"=>'',
					  "id_polling"=>'0',
					  "id_pilihan"=>'0',
					  "id_member"=>'0',
					  "req"=>$data['type'],
					  "id_udh"=>'',
					  "id_feed"=>'',
					  "id_comment"=>'',
					  "creator"=>$data['creator'],
					  "id_creator"=>$data['id_creator'],
					  "release"=>'',
					  "foto_profil_tweet"=>'');
			//$column = array("%d","%s","%s","","","%d","%d","%s","%d","%d","%d","%s","%s","%s","%s","%s","%s","%s","%s");
			//$column = array("%d","%s","%s","%s","%s","%d","%d","%s","%d","%d","%d","%s","%s","%s","%s","%s","%s","%s","%s");
			$column = array(
					"%d", 	//"id_msc"=>'0',
					"%s",	//"no_hp"=>$data['sender'],
					"%s", 	//  "isi"=>$data['message'],
					"%s",	//  "tgl_msk"=>date("Y-m-d"),
					"%s",	//  "waktu_msk"=>date("H:i:s"),
					"%s", 	//  "stat"=>'1',
					"%d", 	//  "id_program"=>$idprogram,
					"%s",	//  "id_lagu"=>'',
					"%d", 	//  "id_polling"=>'0',
					"%d",	//  "id_pilihan"=>'0',
					"%d", 	//  "id_member"=>'0',
					"%s", 	//  "req"=>$data['type'],
					"%s",	//  "id_udh"=>'',
					"%s",	//  "id_feed"=>'',
					"%s",	//  "id_comment"=>'',
					"%s",	//  "creator"=>$data['creator'],
					"%s",	//  "id_creator"=>$data['id_creator'],
					"%s",	//  "release"=>'',
					"%s");	//  "foto_profil_tweet"=>'');


			$wprisesmsdb2->insert('t_sms', $sdata, $column);

			$temp['code'] = "2";
			$temp['message'] = "Data has been saved!! $TimeNow";
			//$query_result = $wprisesmsdb2->get_results($sql);
		
		}

		else if(isset($wpdbsr)) {
			$temp = array();
			$MESSAGE_TABLE = IDRADIO."_t_messages";
			
			//inserting data to table
			//$sql = "INSERT INTO ".$MESSAGE_TABLE." (jenis,tgl,pesan,nama) VALUES ". 
			//		"('".$data['type']."', '".date("Y-m-d")."', '".$data['message']."', '".$data['sender']."')";

			$sdata = array("jenis"=>$data['type'],
					"id_member"=>0,
					"nohp"=>$data['sender'],
					"tgl"=>$DateNow,
					"pesan"=>$data['message'],
					"waktu"=> $TimeNow,
					"creator"=>$data['creator'],
					"id_comment"=>'',
					"id_creator"=>$data['id_creator'],
					"id_feed"=>'',
					"twitter_foto"=>'',
					"idsm"=>'',
					"nama"=>'',
					"cuid"=>'_sync',
					"stat" => '1',
					"cdate"=>$DateNow);
			//$column = array("%s","%d","%s","","%s","","%s","%s");
			$column = array(
					"%s", //"jenis"=>$data['type'],
					"%d", //"id_member"=>0,
					"%s",	//"nohp"=>$data['sender'],
					"%s", //"tgl"=>$DateNow,
					"%s", //"pesan"=>$data['message'],
					"%s", //"waktu"=>$TimeNow,
					"%s", //"creator"=>$data['creator'],
					"%s", //"id_comment"=>'',
					"%s", //"id_creator"=>$data['id_creator'],
					"%s", //"id_feed"=>'',
					"%s", //"twitter_foto"=>'',
					"%s", //"idsm"=>'',
					"%s", //"nama"=>'',
					"%s", //"cuid"=>'_sync',
					"%s", //"stat" => '1',
					"%s" //"cdate"=>$DateNow);
			);


			$wpdbsr->insert($MESSAGE_TABLE, $sdata, $column);
		
			$temp['code'] = "1";
			$temp['message'] = "Data has been saved!!";
			//$query_result = $wpdbsr->get_results($sql);
	
		} else{
			$temp['code'] = "4";
			$temp['message'] = "cannot be saved!!";	
		}

	}else{
		$temp['code'] = "3";
		$temp['message'] = "HTTP-POST doesn't work";
	}
	
	echo json_encode($temp);
	return true;
}

// new added
function suararadio_data_request_2($r_number, $mode, $limit) {
	global $suararadio;
	global $current_user;
	global $wpdb;
	
	$connection  = mysql_connect("db.suararadio.com", "pena", "p3n4_db4dm1n");
	if(!$connection) {
		die('Could not connect: ' . mysql_error());
	}
	mysql_select_db("atradio", $connection);
	$sql = "SELECT * FROM ar_radios";
	$query_result = mysql_query($sql);
	while($row = mysql_fetch_array($query_result)) {
		$fr_number =  $row['r_number'];
	}
	if($fr_number != null) {
		$MESSAGE_TABLE = $r_number."_t_messages";
		$PHONEBOOK_TABLE = $r_number."_t_phonebook";
		$PROGRAM_TABLE = $r_number."_t_program";
		$REALIZATION_TABLE = $r_number."_t_realisasi_log";
		
		if($mode != null) {
			if($mode == 't') {
			$sql = 	"SELECT * FROM ". $MESSAGE_TABLE.
					" WHERE tgl = '" .date("Y-m-d"). "'".
					" AND jenis = 't' ORDER BY tgl DESC";
			} else if ($mode == 'f') {
			$sql = 	"SELECT * FROM ". $MESSAGE_TABLE.
					" WHERE tgl = '" .date("Y-m-d"). "'".
					" AND jenis = 'f' ORDER BY tgl DESC";
			} else if ($mode == '2') {
			$sql = 	"SELECT * FROM ". $MESSAGE_TABLE.
					" WHERE tgl = '" .date("Y-m-d"). "'".
					" AND jenis = '2' ORDER BY tgl DESC";
			} else {
			$sql = 	"SELECT * FROM ". $MESSAGE_TABLE.
					" WHERE tgl = '" .date("Y-m-d"). "'".
					" ORDER BY tgl DESC";					
			}
		} else if ($mode == null) {
			$sql = 	"SELECT * FROM ". $MESSAGE_TABLE.
					" WHERE tgl = '" .date("Y-m-d"). "'".
					" ORDER BY tgl DESC";		
		} else {}
		
		if($limit != null) {
			$sql = $sql. " LIMIT ".$limit;
		} else {
			$sql = $sql. " LIMIT 20 ";
		}
		
		$query_result = mysql_query($sql);
		$temp = array();
		while($row = mysql_fetch_array($query_result)){
			$vtemp = array();
			$vtemp['jenis'] = $row['jenis'];
			$vtemp['id_member'] = $row['id_member'];
			$vtemp['nohp'] = $row['nohp'];
			$vtemp['pesan'] = $row['pesan'];
			$vtemp['waktu'] = $row['waktu'];
			$vtemp['creator'] = $row['creator'];
			$vtemp['id_comment'] = $row['id_comment'];
			$vtemp['id_creator'] = $row['id_creator'];
			$vtemp['id_feed'] = $row['id_feed'];
			$vtemp['twitter_foto'] = $row['twitter_foto'];
			$vtemp['idsm'] = $row['idsm'];
			$vtemp['nama'] = $row['nama'];
			$vtemp['cuid'] = $row['cuid'];
			$vtemp['cdate'] = $row['cdate'];
			$temp[] = $vtemp;
		}
		echo json_encode($temp);
	} else {
#		r_number tidak ditemukan berarti data ada di penyimpanan lokal untuk masing-masing
#		stasiun radio. silakan cara lagi nanti bagaimana caranya.	
	}
	
	mysql_close($connection);
	return true;
}

// new added
function suararadio_data_request_3($mode) {
# 	fungsi untuk mengambil semua request dari pendengar, baik dari facebook, twitter, atau sms

	global $suararadio;
	global $current_user;
	global $wpdb;
	global $wpdbsr;
	global $wprisedb2;
	global $wprisesmsdb;
	global $wprisesmsdb2;
	
	if(isset($wpdbsr)) {

		$MESSAGE_TABLE = IDRADIO."_t_messages";
		$sql = 	"SELECT * FROM ". $MESSAGE_TABLE.
				" WHERE tgl = '" .date("Y-m-d"). "'";
			
		if($mode != null) {
			$sql = $sql." AND jenis = '".$mode."'";
		} 
		$sql = $sql. " ORDER BY waktu DESC LIMIT 20 ";

		$query_result = $wpdbsr->get_results($sql);
		$temp = array();
		foreach ($query_result as $result){
			$vtemp = array();
			$vtemp['id'] = $result->idsm;
			$vtemp['waktu'] = $result->waktu;
			$vtemp['jenis'] = $result->jenis;
			$vtemp['creator'] = $result->creator;
			if (($result->jenis == '1')||($result->jenis == '2')||($result->jenis == '3')) { // sms biasa
				$sql = "SELECT * FROM ".IDRADIO."_t_phonebook WHERE no_hp = '".$result->nohp."'";
				$data_member = $wpdbsr->get_results($sql);
				if($data_member[0]->id_member){
					if($data_member[0]->profilFB) {
						$vtemp['pengirim'] = $data_member[0]->nama;
						$vtemp['foto'] = "https://graph.facebook.com/".$data_member[0]->profileFB."/picture";
					} else {
						$vtemp['pengirim'] = $data_member[0]->nama;
						$vtemp['foto'] = "";
					}
				} else {
					if($result->creator == 'c' || $result->creator == 'i' || $result->creator == 'w' || $result->creator == 'b' || $result->creator == 'a'){
						$vtemp['pengirim'] = $result->nohp;
						$vtemp['foto'] = "https://graph.facebook.com/".$result->id_creator."/picture";
					}else{
						$vtemp['pengirim'] = "";
						$vtemp['foto'] = "";
					}
				}
			} else if (($result->jenis == 'f') || ($result->jenis == 't') || ($result->jenis == '6')) {
				$fb_graph_address = "https://graph.facebook.com/".$result->id_creator."?fields=name";
				$fb_json_result = file_get_contents($fb_graph_address);
				$fb_decoded_json = json_decode($fb_json_result);
				$desired_name = $fb_decoded_json->name;
				
				$vtemp['pengirim'] = $desired_name;
				if($result->jenis == 'f') {
					$vtemp['foto'] = "https://graph.facebook.com/".$result->id_creator."/picture";
				} else if ($result->jenis == 't') {
					$vtemp['foto'] = $result->twitter_foto;
				}
			} else {
				$vtemp['pengirim'] = substr($result->nohp,0, -3)."XXX";
				$vtemp['foto'] = "";
			}
			
			$vtemp['pesan'] = $result->pesan;
			
			
#			$vtemp['id_member'] = $result->id_member;
#			$vtemp['nohp'] = $result->nohp;
#			$vtemp['pesan'] = $result->pesan;
#			$vtemp['creator'] = $result->creator;
#			$vtemp['id_comment'] = $result->id_comment;
#			$vtemp['id_creator'] = $result->id_creator;
#			$vtemp['id_feed'] = $result->id_feed;
#			$vtemp['idsm'] = $result->idsm;
#			$vtemp['nama'] = $result->nama;			
#			$vtemp['cdate'] = $result->cdate;

			$temp[] = $vtemp;
		}
//		echo json_encode($temp);
		
	//} else if (($wprisedb2)) {
	} else if (($wprisesmsdb2)) {
		$MESSAGE_TABLE = "t_sms";
		$sql = 	"SELECT * FROM ". $MESSAGE_TABLE.
				" WHERE tgl_msk = '" .date("Y-m-d"). "'";
		if($mode != null) {
			$sql = $sql." AND req = '".$mode."'";
		} 
		$sql = $sql. " ORDER BY waktu_msk DESC LIMIT 20 ";
		$query_result = $wprisesmsdb2->get_results($sql);
		$temp = array();
		foreach ($query_result as $result){
			$vtemp = array();
			$vtemp['id'] = $result->id_sms_masuk;
			$vtemp['jenis'] = $result->req;
			$vtemp['creator'] = $result->creator;
			$vtemp['waktu'] = $result->waktu_msk;
			if($result->req == '1' || $result->req == '2' || $result->req == '3') {
				$sql = "SELECT nama, profileFB FROM t_phonebook WHERE no_hp = '".$result->no_hp."'";
				$member = $wprisesmsdb2->get_results($sql);
				$tmp = explode(" ", $result->no_hp);
				if(count($member) < 1) {
					if($result->creator == 'c' || $result->creator == 'i' || $result->creator == 'w' || $result->creator == 'b' || $result->creator == 'a') {
						$vtemp['foto'] = "https://graph.facebook.com/".$result->id_creator."/picture";
						if(count($tmp) > 1) {
							$vtemp['pengirim'] = $result->no_hp;
						} else {
							$vtemp['pengirim'] = substr($result->no_hp, 0, -3). "XXX";
						}
					} else {
						$vtemp['foto'] = "";
						if(count($tmp) > 1) {
							$vtemp['pengirim'] = $result->no_hp;
						} else {
							$vtemp['pengirim'] = substr($result->no_hp, 0, -3). "XXX";
						}
					}					
				} else {
					$vtemp['pengirim'] = $member[0]->nama;
					if($member[0]->profileFB) {
						$vtemp['foto'] = "https://graph.facebook.com/".$result->id_creator."/picture";
					} else {
						$vtemp['foto'] = "";
					}
				}
			} else {
				if($result->req == 'f' || $result->req == 't' || $result->req == '6') {
					$vtemp['pengirim'] = $result->creator;
				} else {
					$vtemp['pengirim'] = substr($result->no_hp, 0, -3)."XXX";
				}
				
				if($result->req == 'f') {
					$vtemp['foto'] = "https://graph.facebook.com/".$result->id_creator."/picture";
				} else if ($result->req == 't') {
					$vtemp['foto'] = $result->foto_profil_tweet;
				}
			} 
		
			$vtemp['pesan'] = $result->isi;
			
#			$vtemp['id_sms_masuk'] = $result->id_sms_masuk;
#			$vtemp['id_msc'] = $result->id_msc;
#			$vtemp['tgl_msk'] = $result->tgl_msk;
#			$vtemp['waktu_msk'] = $result->waktu_msk;
#			$vtemp['stat'] = $result->stat;
#			$vtemp['id_program'] = $result->id_program;
#			$vtemp['id_lagu'] = $result->id_lagu;
#			$vtemp['id_polling'] = $result->id_polling;
#			$vtemp['id_pilihan'] = $result->id_pilihan;
#			$vtemp['req'] = $result->req;
#			$vtemp['id_udh'] = $result->id_udh;
#			$vtemp['id_feed'] = $result->id_feed;
#			$vtemp['id_comment'] = $result->id_comment;
#			$vtemp['creator'] = $result->creator;
#			$vtemp['id_creator'] = $result->id_creator;
#			$vtemp['release'] = $result->release;
#			$vtemp['foto_profil_tweet'] = $result->foto_profil_tweet;
			$temp[] = $vtemp;
		}
#//		echo json_encode($temp);
	}
	
	echo json_encode($temp);
	return true;
}

function suararadio_data_rundown() {
	echo 'hello';
	// mengeluarkan data rundown
	$result = array();
	$result['code'] = "1";
	$result['message'] = "ok";
	$result['data'] = array(
		array("tgl"=>"2013-01-17","waktu"=>"11:24:00","tipe"=>"lagu","text"=>"isian rundown"),
		array("tgl"=>"2013-01-17","waktu"=>"11:23:00","tipe"=>"iklan","text"=>"isian rundown"),
		array("tgl"=>"2013-01-17","waktu"=>"11:22:00","tipe"=>"jingle","text"=>"isian rundown"),
		array("tgl"=>"2013-01-17","waktu"=>"11:21:00","tipe"=>"lagu","text"=>"isian rundown"),
		array("tgl"=>"2013-01-17","waktu"=>"11:20:00","tipe"=>"lagu","text"=>"isian rundown"),
		array("tgl"=>"2013-01-17","waktu"=>"11:19:00","tipe"=>"iklan","text"=>"isian rundown"),
		array("tgl"=>"2013-01-17","waktu"=>"11:19:40","tipe"=>"lagu","text"=>"lagu 4"),
		array("tgl"=>"2013-01-17","waktu"=>"11:19:30","tipe"=>"lagu","text"=>"lagu 3"),
		array("tgl"=>"2013-01-17","waktu"=>"11:19:20","tipe"=>"lagu","text"=>"lagu 2"),
		array("tgl"=>"2013-01-17","waktu"=>"11:19:10","tipe"=>"lagu","text"=>"lagu 1"),
	);
	echo json_encode($result); 
	return true;
}

// new added
function suararadio_data_rundown_2($r_number) {
	global $suararadio;
	global $current_user;
	global $wpdb;
	
	$connection  = mysql_connect("db.suararadio.com", "pena", "p3n4_db4dm1n");
	if(!$connection) {
		die('Could not connect: ' . mysql_error());
	} else { 
#		echo "success <br>";
	}
	mysql_select_db("atradio", $connection);
	$sql = "SELECT * FROM ar_radios";
	$query_result = mysql_query($sql);
	while($row = mysql_fetch_array($query_result)) {
		$fr_number =  $row['r_number'];
	}
		
	if($fr_number != null) {
#		karena r_number yang diminta ternyata ada di dalam database
#		maka pencarian rundown dapat dilakukan berikutnya.
#		echo "found <br>";
		$MESSAGE_TABLE = $r_number."_t_messages";
		$PHONEBOOK_TABLE = $r_number."_t_phonebook";
		$PROGRAM_TABLE = $r_number."_t_program";
		$REALIZATION_TABLE = $r_number."_t_realisasi_log";
		
#		echo $MESSAGE_TABLE." ".$PHONEBOOK_TABLE." ".$PROGRAM_TABLE." ".$REALIZATION_TABLE;
		
#		cari acara apa yang sekarang sedang berlangsung dengan cara
#		mengambil waktu saat ini dan mencari kira-kira dimana acara
#		yang tepat.
#		ambil semua isi tabel dan kemudian bandingkan semua jam pada tabel
#		dan cari acara yang cocok.
		
		$sql = 	"SELECT * FROM ".$PROGRAM_TABLE;
		$query_result = mysql_query($sql);
#		while($row = mysql_fetch_array($query_result)) {
#			echo $row['int']."<br>";
#			echo $row['program_name']."<br>";
#			echo $row['nama']."<br>";
#			echo $row['awal']."<br>";
#			echo $row['akhir']."<br>";
#			echo $row['uid']."<br>";
#		}
		
		$time = time();
		$stop = false;
		while(($stop == false) && ($row = mysql_fetch_array($query_result))) {
			if($time >= strtotime($row['awal']) && 
				$time <= strtotime($row['akhir'])) {
				$stop = true;
				$prog_int = $row['int'];
				$prog_name = $row['program_name'];
				$prog_host = $row['nama'];
				$prog_uid = $row['uid'];
				$prog_id = $row['id_acara'];
				$prog_start = $row['awal'];
				$prog_end = $row['akhir'];
#				echo $prog_id."<br>";
#				echo 'ok';
			} else {}
		}
		
#		cari semua acara-acara dari realization log yang memiliki id yang sama
#		dengan prog_id yang dijelaskan di atas.

#		$sql = 	"SELECT * FROM ".$REALIZATION_TABLE. 
#				" WHERE id_acara = '".$prog_id."'";
		
		$sql = 
			" SELECT * FROM ".$REALIZATION_TABLE.
			" WHERE id_acara = '".$prog_id. "' AND ".
			" waktu BETWEEN '".$prog_start."' and '".$prog_end."' AND ".
			" tanggal = '".date("Y-m-d")."'".
			" ORDER BY waktu DESC";
			
#		echo $sql."<br>";
		$query_result = mysql_query($sql);
#		while($row = mysql_fetch_array($query_result)) {
#			echo $row['id_log']."<br>";
#			echo $row['log_group']."<br>";
#			echo $row['tanggal']."<br>";
#			echo $row['waktu']."<br>";
#			echo $row['id_log_asal']."<br>";
#			echo $row['id_acara']."<br>";
#			echo $row['cdate']."<br>";
#			echo "ok";
#		}

		$temp = array();
		while($row = mysql_fetch_array($query_result)) {
			$vtemp = array();
			$vtemp['id_log'] = $row['id_log'];
			$vtemp['log_group'] = $row['log_group'];
			$vtemp['tanggal'] = $row['tanggal'];
			$vtemp['waktu'] = $row['waktu'];
			$vtemp['remark'] = $row['remark'];
			$vtemp['no_kontrak'] = $row['no_kontak'];
			$vtemp['content'] = $row['content'];
			$vtemp['id_biro'] = $row['id_biro'];
			$vtemp['biro'] = $row['biro'];
			$vtemp['id_log_asal'] = $row['id_log_asal'];
			$vtemp['id_program'] = $row['id_program'];
			$vtemp['id_acara'] = $row['id_acara'];
			$vtemp['cdate'] = $row['cdate'];
			$vtemp['cuid'] = $row['cuid'];
			$temp[] = $vtemp;
		}
		
		echo json_encode($temp);
		
	} else {
#		stasiun radio yang dicari ga ketemu di data center, tolong cari lagi
#		ke tempat lain.
	}
	
	mysql_close($connection);
	return true;
}

// new added
function suararadio_data_rundown_3() {
	global $suararadio;
	global $current_user;
	global $wpdb;
	global $wpdbsr;
	global $wprisedb2;
	global $_REQUEST;
	
	//if(isset($_REQUEST['dump'])) { echo "<pre>" ;var_dump("wpdbsr",$wpdbsr); echo "</pre>";}
	if(isset($wpdbsr)) {
#		cari ID dari acara yang sedang berlangsung sekarang
		//if(isset($_REQUEST['dump'])) { echo "<pre>" ;var_dump("wp server"); echo "</pre>";}
		$sql = 	"SELECT id_acara FROM ".IDRADIO.
				"_t_realisasi_log ORDER BY tanggal DESC, waktu DESC LIMIT 1";
		$id_acara = $wpdbsr->get_results($sql);
		$id_acara = $id_acara[0]->id_acara;
		$sql = "SELECT log_group, content, waktu, remark 
				FROM ".IDRADIO."_t_realisasi_log 
				WHERE id_acara = '".$id_acara.
					"' AND tanggal = '" .date("Y-m-d"). 
					"' ORDER BY tanggal DESC, waktu DESC";
		$query_result = $wpdbsr->get_results($sql, OBJECT);
		
		// jika rundown hari ini kosong ambil hari kemarin
		if(empty($query_result)) {
			$sql = "SELECT log_group, content, waktu, remark 
					FROM ".IDRADIO."_t_realisasi_log 
					WHERE id_acara = '".$id_acara.
					"' 	AND tanggal = DATE_SUB(CURDATE(), INTERVAL 1 DAY) 
						ORDER BY tanggal DESC, waktu DESC";
			$query_result = $wpdbsr->get_results($sql, OBJECT);
		}
		
		$temp = array();
		foreach($query_result as $result) {
			$vtemp = array();
			$vtemp['log_group'] = $result->log_group;
			$vtemp['waktu'] = $result->waktu;
			$vtemp['remark'] = ($result->log_group == "ADVS_AUDIO") ? "Start: ".$result->content : $result->remark;
			$temp[] = $vtemp;
		}
		
		echo json_encode($temp);
		
	} else if (isset($wprisedb2)){

		$sql = "SELECT tp.program_name,tpy.nama,tf.awal,tf.akhir,tpy.uid,tf.id_final ".
				"FROM t_realisasi_log tl ".
				"LEFT JOIN t_program tp ON tl.id_acara = tp.id_acara ".
				"LEFT JOIN t_final tf ON tl.id_final = tf.id_final ".
				"LEFT JOIN t_penyiar tpy ON tf.penyiar_onair = tpy.id_penyiar ".																			 
				"WHERE tl.id_log = (SELECT MAX(id_log) FROM t_realisasi_log) AND tf.stat='1'";
		$dtprogram = $wprisedb2->get_row($sql, ARRAY_A);
		//if(isset($_REQUEST['dump'])) var_dump($wprisedb2->get_row($sql, ARRAY_A),$wprisedb2);
		//else  $dtprogram = $wprisedb2->get_row($sql, ARRAY_A);
		$sql = "SELECT log_group, waktu, remark 
				FROM t_realisasi_log 
				WHERE id_final='".$dtprogram['id_final'].
			"' 	AND log_type='start' ORDER BY waktu DESC";
		$dtprogram = $wprisedb2->get_results($sql);
		$temp = array();
		foreach($dtprogram as $program) {
			$vtemp = array();
			$vtemp['log_group'] = $program->log_group;
			$vtemp['waktu'] = $program->waktu;
			if($program->log_group == "ADVS_AUDIO") {
				$_remark =   explode(" - ",$program->remark);

				$vtemp['remark'] = $_remark[0];
			}
			else $vtemp['remark'] = $program->remark;
			$temp[] = $vtemp;
		}
		echo json_encode($temp);
	}
}


function suararadio_data_programme() {
	$result = array();
	$result['code'] = "1";
	$result['message'] = "ok";
	$result['data'] = array(
		"id"=>"1",
		"name"=>"Acara Saat Ini",
		"penyiar"=>"Nama penyiar",
		"foto"=>"http://placehold.it/50x50",
		"mulai"=>"11:30",
		"selesai"=>"14:00",
		"deskripsi"=>"Deskripsi program",
	);
	echo json_encode($result);
	return true;
}

// new added

// new added
function suararadio_data_programme_2() {
	global $suararadio;
	global $current_user;
	global $wpdb;
	global $wpdbsr;
	global $wprisedb2;
	global $wprisesmsdb2;
        
	
	if(isset($wpdbsr)) {
//		echo "hello <br>";
#		cari acara yang sedang berlangsung saat ini.		
         

		$sql = 	"SELECT id_acara FROM ".IDRADIO.
				"_t_realisasi_log ORDER BY tanggal DESC, waktu DESC LIMIT 1";
		$id_acara = $wpdbsr->get_results($sql);
		$id_acara = $id_acara[0]->id_acara;
		
		$sql = "SELECT * FROM ".IDRADIO."_t_program WHERE id_acara = '".$id_acara."'";
		$query_result = $wpdbsr->get_results($sql);
                $temp = array();
		foreach($query_result as $result){
			$vtemp = array();
			$vtemp['id'] = $result->int;
			$vtemp['nama'] = $result->program_name;
			$vtemp['penyiar'] = $result->nama;
			$vtemp['foto'] = $result->profileFB; // modified by HH  // $result->uid;
			$vtemp['mulai'] = $result->awal;
			$vtemp['selesai'] = $result->akhir;
			$vtemp['deskripsi'] = $result->program_name;
			$temp[] = $vtemp;
		}
		
		echo json_encode($temp);
		
	} else {
		if(isset($wprisedb2)){
			$sql = 	"SELECT tp.id_acara, tp.program_desc, tp.program_name,tpy.nama,tf.awal,tf.akhir,tpy.uid,tf.id_final, tu.profileFB  ".
					"FROM t_realisasi_log tl ".
						"LEFT JOIN t_program tp ON tl.id_acara = tp.id_acara ".
						"LEFT JOIN t_final tf ON tl.id_final = tf.id_final ".
						"LEFT JOIN t_penyiar tpy ON tf.penyiar_onair = tpy.id_penyiar ".	
						"LEFT JOIN rise_dev_admin.t_user tu on tu.nama = tpy.nama ".																		 
						"WHERE tl.id_log = (select max(id_log) from t_realisasi_log) AND tf.stat='1'";
			$query_result = $wprisedb2->get_results($sql);
                      
			$program_name = $query_result[0]->program_name;
			$penyiar = getNamaFBPenyiar($query_result[0]->profileFB);
                        if (trim($penyiar) == "") $penyiar = $query_result[0]->nama;
			$waktu_mulai = $query_result[0]->awal;
			$waktu_selesai = $query_result[0]->akhir;
			$id_final = $query_result[0]->id_final;
			$penyiar_uid = $query_result[0]->profileFB;
			/*	
			$sql = "SELECT id_program, deskripsi 
					FROM t_program2 
					WHERE nama = '".$program_name."'";
			$query_result = $wprisesmsdb2->get_results($sql);
			*/
			$program_id = $query_result[0]->id_acara;
			$program_desc = $query_result[0]->program_desc;
			
			$temp = array();
			$vtemp = array();
			$vtemp['id'] = $program_id;
			$vtemp['nama'] = $program_name;
			$vtemp['penyiar'] = $penyiar;;
			$vtemp['foto'] = $penyiar_uid;
			$vtemp['mulai'] = $waktu_mulai;
			$vtemp['selesai'] = $waktu_selesai;
			$vtemp['deskripsi'] = $program_desc;
			$temp[] = $vtemp;
			
			echo json_encode($temp);
		}
	}	
	return true;
}

function suararadio_data_laris() {
	$result = array();
	$result['code'] = "1";
	$result['message'] = "ok";
	$result['data'] = array();
	echo json_encode($result);
	return true;
}

function suararadio_data_modis() {
	$result = array();
	$result['code'] = "1";
	$result['message'] = "ok";
	$result['data'] = array();
	echo json_encode($result);
	return true;
}

function suararadio_data_search($keyword,$slug = '',$offset=0) {
	global $suararadio;
	global $current_user;
	global $wpdb,$post;
	global $_REQUEST;

	//var_dump($slug);exit;	
	$args = array(
		"tag__not_in" => array('12999' ),	// added by HH, karena uzone nih nurfmrembang 
		's' => $keyword,
		'posts_per_page' => 10,
		'offset' => ($offset=='')?0: intval($offset)
	);
	


	if( $slug != ''){	

		$catObj = get_category_by_slug($slug);
		if($catObj && $catObj->cat_ID)
		$args['cat'] = $catObj->cat_ID;
	}
	//var_dump($args);
	//exit;
	//$query = new WP_Query( 's='.$keyword, 'posts_per_page=5' );
	$query = new WP_Query( $args );
	$res = array();

	while($query->have_posts()) : $query->the_post();
		$tmp = array();
		$tmp['id'] = $post->ID;
		$tmp['post_date'] = $post->post_date;
		$tmp['post_name'] = $post->post_name;
		$tmp['post_content'] = $post->post_content;
		$tmp['title'] = $post->post_title;
		$tmp['tags'] = wp_get_post_tags($post->ID);
		$tmp['file'] = suararadio_getPodcastUrl($post->ID);
        	
		$media = get_post_meta($post->ID, META_PODPRESSMEDIA, true);
        	foreach ($media as $vmedia) {
            		$vparse = parse_url($vmedia['URI']);
            		if ($vparse['host']!=$_SERVER['HTTP_HOST']) {
                		$tmp['file_redirect'] = $vmedia['URI'];
            		}
        	}
		$tmp['url'] = getPermalink($post->ID);
		//$tmp['url'] = "http://".$_SERVER['HTTP_HOST'].get_permalink($post->ID);
		if (META_RADIO_NAME!='') {
			$tmp['radio'] = get_post_meta($item->postid, META_RADIO_NAME, true);
		} else {
			$tmp['radio'] = $radio;
		}

		$tmp['thumb'] = get_post_meta($post->ID, $post->meta_key, $single = true);
		$tmp['play_count'] = podPress_playCount($post->ID);	
	
	/*	$att = array();
		$att_args = array( 'post_type' => 'attachment',
			 'post_mime_type' => 'image/jpeg',
			 'posts_per_page' => -1, 'post_status' =>'any', 'post_parent' => $post->ID ); 
		$attachments = get_posts($att_args);
		if ($attachments) {
			foreach ( $attachments as $attachment ) {
				//	echo apply_filters( 'the_title' , $attachment->post_title );
				$att[] = $attachment->guid;
				//var_dump($attachment);
			}	
		}*/




		$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  		$first_img = $matches[1][0];

  		if(empty($first_img)) {
    			$first_img = "http://www.suararadio.com/wp-content/themes/suararadio/images/ad_2.png";
  		}
		$tmp['attachment'] = $first_img;


		$res[] = $tmp;
	endwhile;
	
	if((count($res)) < 1){
		$tmp['title'] = "Sorry, no results found!!";
		$res[] = $tmp;
	}

	echo json_encode($res);
	return true;

}

/**
 * depecreated user apis/user/detail instead
 */
function suararadio_data_user() {
    global $suararadio;
    global $current_user;
    var_dump($_REQUEST);

    $datauser = array();
    if ($current_user->ID!=0) {
        $datauser['Facebook'] = $current_user->Facebook;
        $datauser['name'] = $current_user->name;
        $datauser['expired'] = $current_user->expired;
        $datauser['ext_expired'] = $current_user->ext_expired;
        $datauser['melon_id'] = $current_user->melon_id;
        $datauser['melon_uid'] = $current_user->melon_uid;
        $datauser['melon_pwd'] = $current_user->melon_pwd;
        $datauser['member_type'] = $current_user->member_type;
        $datauser['Twitter'] = $current_user->Twitter;
        $datauser['Yahoo'] = $current_user->Yahoo;
        $datauser['Google'] = $current_user->Google;
        $datauser['registered'] = $current_user->registered;
    }
    echo json_encode($datauser);
    return true;
}
    
function suararadio_data_listplaylist() {
    global $suararadio;
    global $current_user;   
            
    
    if ($current_user->ID!=0 || $_GET['stat'] =='1') {
    	$params = array();    
    	$params['numrow']= ($_RESULT['numrow']) ? $_RESULT['numrow']:(($_POST['numrow']) ? $_POST['numrow']:15);
    	$params['keyword'] = ($_RESULT['keyword']) ? $_RESULT['keyword']:(($_POST['keyword']) ? $_POST['keyword']:"");
    	$params['page']= ($_RESULT['paged']) ? $_RESULT['paged']:(($_POST['paged']) ? $_POST['paged']:1);
    	$playlist = $suararadio->getListPlaylist($params);
    	//$playlist['debug'] = $_POST['numrow'];
    	echo json_encode($playlist);
    	return true;
    } else {	
        
        $response['code'] = "err";
        $response['message'] = "user not logedin.";
        echo json_encode($response);
        return false;
        
        }
}
    
/**
 * format data $_POST;
 *  - list_id: number
 *  - name: string
 *  - items: array
 *      - list_num: number, list item position (urutan)
 *      - item_type: string, // melon / post
 *      - item_loc: string, //kalo post url mp3.
 *      - item_meta: // struktur item di playlist.
 *  format id ???
 *  melon|1414928|1414928|326154|105701|200030
 *   'id'=>'melon|'.$songId.'|'.$songId.'|'.$lagu['albumId'].'|'.$lagu['artistId'].'|'.$lagu['genreId'],
     dan artis, judul lagu,durasi,
 *  post|
 *   'id'=>'post|'.$post->ID.'|0|'.$fnamehash,
 * example:
 * - ss
 */
function suararadio_data_playlist_save() {
    global $wpdb;
    global $current_user;
    
    $response = array();
    if ($current_user->ID==0) {
        $response['code'] = "err";
        $response['message'] = "User not logged in.";
        echo json_encode($response);
        return false;
    }
   
 
    $list_id = ($_POST['list_id'])? $_POST['list_id']:null;
    $items = ($_POST['items'])? $_POST['items']:null;
    $uid = $current_user->ID;
    if (preg_match("/[\\\]+/", $items)){
    	$items = (array)json_decode(preg_replace("/[\\\]+/", "", $items));
	} 

    
    if (isset($list_id)) {
        // update playlist
        $wpdb->query("delete from wp_suararadio_playlist_item where list_id='$list_id'");
        $i = 1;
	$debug = array();

        foreach ($items as $vitem) {
	    $vitem = (array)$vitem;
            $data = array(
                'list_id'=>$list_id,'list_num'=>$i,'item_type'=>$vitem['type'],
                'item_loc'=>$vitem['location'],
                'item_meta'=>serialize($vitem['item_meta']));
            $wpdb->insert('wp_suararadio_playlist_item',$data);
	    $debug[$i] = $data;
            $i++;
        }
    } else {
        // new playlist
        if ($_POST['name']=='') {
            $response['code'] = "err";
            $response['message'] = "name not defined!";
	    $response['request'] = $_POST;
            echo json_encode($response);
            return false;
        }
        $data = array('user_id'=>$uid,'list_type'=>'ul','list_name'=>$_POST['name'],'list_desc'=>$params['desc']);
        $wpdb->insert('wp_suararadio_playlist',$data);
	$debug = array();
        if ($wpdb->insert_id) {
  			// berhasil simpan, lanjutkan dengan data itemnya
  			$list_id = $wpdb->insert_id;
  			$i = 1;
  			foreach($items as $vitem) {
				$vitem = (array)$vitem;
			
  				$data = array(
                    		'list_id'=>$list_id,
				'list_num'=>$i,
				'item_type'=>$vitem['type'],
                    		'item_loc'=>$vitem['location'],
                    		'item_meta'=>serialize($vitem['item_meta'])
				);
  				$wpdb->insert('wp_suararadio_playlist_item',$data);


				$debug[$i] = $data;

  				$i++;
  			}

    		$response['debug'] = $debug;
		$response['list_id'] = $list_id;
		$response['list_name'] = $_POST['name'];
  		}
    }
    //$response['debug'] = $items;
    $response['code'] = "1";
    $response['message'] = "OK";
    echo json_encode($response);
    return true;
} //endfunc suararadio_data_playlist_save



function suararadio_data_programme_archive($start=0, $interval=24){
	//global $suararadio;
	//global $current_user;
	//global $wpdb;
	global $wpdbsr;
	//global $wprisedb2;
	//global $wprisesmsdb2;
	$res =  null;
	if($wpdbsr){
		$logTable = IDRADIO."_t_realisasi_log";
		$programmeTable = IDRADIO."_t_program";
		$sql =<<<EOF
SELECT DISTINCT l.id_program as id_program,  l.tanggal, l.id_acara,  l.tanggal, l.waktu, p.int, p.nama, p.profileFB, p.program_name, 
p.awal, p.akhir 
FROM $logTable l 
LEFT JOIN $programmeTable p ON p.id_acara = l.id_acara
WHERE l.cdate <= NOW() AND l.cdate >= DATE_SUB(NOW(), INTERVAL $interval HOUR) 
GROUP BY l.id_program, l.tanggal ORDER BY l.cdate DESC
EOF;
		$res = $wpdbsr->get_results($sql);
	}else{
		// sql query for non-synched data!
			echo json_encode(array());
			return true;
	
	}
	if(!$res) {
		echo json_encode(array());
		return true;
	}

			
	$programmes = array();
	foreach ($res as $programme){	
		$programmes[] = array(
			'nama' => $programme->program_name,
			'penyiar' => $programme->nama,
			'foto' => $programme->profileFB,
			'mulai' => $programme->awal,
			'akhir' => $programme->akhir,
			'deskripsi' => $programme->program_name,
			'id' => $programme->int,
			'id_program' => $programme->id_program,
			'id_acara' => $programme->id_acara,
			'tanggal' => $programme->tanggal
		);
	}
	echo json_encode($programmes);
	return true;
}// endfunc suararadio_data_programme_archive

function suararadio_data_rundown_archive($date, $id_acara){
	// apis/data/rundown/archive/$date/$id_acara
	global $wpdbsr;
	$res = null;
	if($wpdbsr){
		$logTable = IDRADIO."_t_realisasi_log";
		$sql =<<<EOF
		SELECT * FROM $logTable WHERE ID_ACARA = "$id_acara" AND TANGGAL = "$date"
		ORDER BY CDATE DESC
EOF;
		$res = $wpdbsr->get_results($sql);
	}else{
		// non-synched database
	}

	if(!$res){
		echo json_encode(array());
		return false;
	}

	
	$rundown = array();
	foreach($res as $rd){
		$rundown[] = array(
			"log_group" => $rd->log_group,
			"waktu" => $rd->waktu,
			"remark" => $rd->remark,
			"tanggal" => $rd->tanggal
		);
	}	


	echo json_encode($rundown);
	return true;

} // endfunc suararadio_data_rundown_archive




function suararadio_data_pl(){
	global $suararadio;
	echo json_encode($suararadio->getPlaylistDuration());
	return true;
}



function toListItem($data){


} // 


function suararadio_logoradio() {
	global $wp_query;
  global $wpdb;
    
  $id = $_GET['id'];
  $url = API_SUARARADIO_URL."streaming/lists?method=2.1&radio=".$id;
  $json = file_get_contents($url);
  $data = json_decode($json, TRUE);
  $vtmp = array();
  $vtmp['logo'] = $data['pathlogo'];
  $vtmp['name'] = $data['name'];
  $vtmp['kota'] = $data['kota'];
  $vtmp['frekuensi'] = $data['frekuensi'];
  $vtmp['genre'] = $data['kategori'];
  $vtmp['band'] = $data['band'];
  echo json_encode($vtmp);
  /*
  $querystr = "SELECT * FROM wp_tuning WHERE id ='".$id."'";
  $query_result = $wpdb->get_results($querystr, OBJECT);
       
	foreach($query_result as $post){
        $vtmp = array();
        $vtmp['logo'] = $post->logo;
        $vtmp['name'] = $post->name;
        $vtmp['kota'] = $post->kota;
        $vtmp['frekuensi'] = $post->frekuensi;
        $vtmp['genre'] = $post->genre;
        $vtmp['band'] = $post->band;
        $tmp[] = $vtmp;                
    }
        
  echo json_encode($vtmp);
	return true;  */
}
