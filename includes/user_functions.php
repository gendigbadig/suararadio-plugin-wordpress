<?php
/* 
 user functions
*/
add_filter('authenticate', 'suararadio_wp_authenticate', 11, 3);
add_action('phpmailer_init', 'suararadio_set_mailer' );

function suararadio_user_process($paths) {
	$params = array();
	switch ($paths[3]) {
		case "login":          
      return suararadio_user_login();
			break;
    case "logout":          
			return suararadio_user_logout();
			break;
    case "detail":          
			return suararadio_user_detail();
			break;
     	return suararadio_user_profile();
     	break;
    case "purchase":
     	return suararadio_user_purchase_init();
     	break;
		default:
			if (function_exists('suararadio_user_'.$paths[3])) {
				call_user_func('suararadio_user_'.$paths[3], $paths);
			} else {
				status_header('404');
			}
	}
} //endfunc suararadio_user_process

function suararadio_set_mailer($phpmailer) {
	
	$phpmailer->CharSet = 'UTF-8';
	$phpmailer->IsSMTP();
	$phpmailer->Port = 587;
	$phpmailer->Host = "mail.suararadio.com";
	$phpmailer->SetFrom('mailer@suararadio.com', 'KlubRadio');
	$phpmailer->Username = "mailer@suararadio.com";
	$phpmailer->Password = "m41l3r4dm1n";
	$phpmailer->SMTPAuth = true;
	$phpmailer->AltBody    = "To view the message, please use an HTML compatible email viewer!";
}

function suararadio_user_tes() {
	global $suararadio_connect;
	/*
	$headers = array("From"=>"mailer@suararadio.com");
	
	$res = wp_mail("izhur2001@gmail.com", "KlubRadio Tes", "Isi message",$headers);
	var_dump($res);
	*/
}

function suararadio_wp_authenticate($user, $username, $password) {
	$error = new \WP_Error();
	$activation_code = "";
	
	$user_info = get_user_by( 'login', $username);
	if (empty($user_info)) {
		$error->add( 'incorrect user', __( 'Username does not exist', 'user-activation-email' ) );
		return $error;
	}
	
	$activation_code = get_user_meta( $user_info->ID, 'activation_code', true );
	
	if (empty($activation_code) || $activation_code=='active') {
		return $user;
	}
	
	remove_filter( 'authenticate', 'wp_authenticate_username_password', 20 );
	$user = new WP_Error( 'user_not_activated', __( 'Sorry, user not activated yet. Please try again. You can find the activation code in your welcome email.', 'suararadio' ) );
	return $user;
}

/**
 * 
 */
function suararadio_user_profile() {
	
} //endfunc suararadio_user_profile

/**
 * echo json data user detail
 * @return boolean
 */
function suararadio_user_detail() {
	global $suararadio;
	global $current_user;
	

	$datauser = array();
	if ($current_user->ID!=0) {
		$suararadio->initAPI();

		$datauser['Facebook'] = $current_user->Facebook;
		$datauser['name'] = $current_user->nickname;
		$datauser['expired'] = $current_user->expired;
		$datauser['ext_expired'] = $current_user->ext_expired;
		$datauser['dis_expired'] = $current_user->dis_expired;
		$datauser['melon_id'] = $current_user->melon_id;
		$datauser['melon_uid'] = $current_user->melon_uid;
		$datauser['melon_pwd'] = $current_user->melon_pwd;
		$datauser['member_type'] = $current_user->member_type;
		$datauser['Twitter'] = $current_user->Twitter;
		$datauser['Yahoo'] = $current_user->Yahoo;
		$datauser['Google'] = $current_user->Google;
		$datauser['Spin'] = $current_user->Spin;
		$datauser['registered'] = $current_user->registered;
		$datauser['user_id'] = $current_user->id;
		$datauser['klubid'] = $current_user->klubid;
		//$datauser['klubid'] = $current_user->klubid;
		$date = DateTime::createFromFormat("Ymd",$current_user->ext_expired);
		$datauser['ext_expired_str'] = ($date)? $date->format("d-M-Y"):'00-00-0000';
		$datauser['expired_str'] = substr($current_user->expired,-2)."/20".substr($current_user->expired,0,2);
		// signature
		$strdata = "klubid:".$datauser['klubid'].":".$datauser['user_id'].";expired:".$datauser['expired'].";ext_expired:".$datauser['ext_expired'].";member_type:".$datauser['member_type'].";dis_expired:".$datauser['dis_expired'].";";
		$res = $suararadio->api->signCreate($strdata);
		if ($res['code']==1) {
			$datauser['signature'] = $res['signature'];
		}
	}
	echo json_encode($datauser);
	return true;
} //endfunc suararadio user detail

/**
 * 
 */
function suararadio_check_username_pattern($username) {
	if (is_email($username)) {
		$_POST['Email'] = $username;
	} else if (preg_match('/^9\d{10,14}$/',$username)) {
		$_POST['Spin'] = $username;
	} else {
		$_POST['Userid'] = $username;
	}
} //endfunc suararadio_check_username_pattern

function suararadio_check_wp_user($field,$value,$password) {
	global $suararadio;
	
	if ($field=='spin') {
		$suararadio->initAPI();
			
		// check on API spin
		$resp = $suararadio->api->spinLogin($value, $password);
		if ($resp['code']!=1) {
			return false;
		} else {
			$user_id = (int) wsl_get_user_by_meta('Spin',$value);
			$user = get_user_by('id',$user_id);
			return $user;
		}
	} else {
		
		$tmpusr = get_user_by($field,$value);
		if (!$tmpusr) return false;
		
		$user = wp_authenticate($tmpusr->data->user_login,$password);
		
		return $user;
	}
} //endfunc suararadio_check_wp_user

function suararadio_user_validate() {
	$key = sanitize_text_field($_GET['activation_code']);
	$username = sanitize_user($_GET['user_login']);
	$user = get_user_by('login', $username);
	if ($user) {
		$code = get_user_meta($user->ID,'activation_code',true);
		if ($code=='active' || empty($code)) {
			wp_die('Nothing, realy!!!','Email Validation');
		} else if ($code==$key) {
			delete_user_meta($user->ID, 'activation_code');
			wp_die('User validated!!, you can use your email to logged in <a href="'.wp_login_url().'">login</a>.','Email Validation',array('response'=>200));
		} else {
			wp_die('Not valid code!!, Open your welcome mail to get link validation.');
		}
	}
	wp_die("I'm doing nothing, thanks!!!",'Email Validation');
}

/**
 * @return boolean
 */
function suararadio_user_token() {
	$res = array();
	session_start();
	if ($_SESSION['token_count']>4 && $_SESSION['token_time']>time()) {
		$res = array("stat"=>"ERR","code"=>"ER009","message"=>"Too many request!");
		echo json_encode($res);
		return true;
	} else if ($_SESSION['token_time']<time()) {
		$_SESSION['token_count'] = 0;
	}
	$_SESSION['token'] = md5(uniqid());
	$_SESSION['token_time'] = time()+1800; // 30 menit
	$_SESSION['token_count'] += 1;

	$res = array("token"=>$_SESSION['token'],"code"=>"1","stat"=>"OK","message"=>"Token generated!");
	echo json_encode($res);
	return true;
}

function suararadio_user_register() {
	global $suararadio;
	global $wpdb;
	
	$display_name = sanitize_user($_POST['display_name']); // nama
	$req_user_login = sanitize_user($_POST['user_login'],true);
	$user_email = sanitize_email($_POST['user_email']); // user email
	$password = sanitize_text_field($_POST['password']); // password
	$password2 = sanitize_text_field($_POST['password_reconfirm']); // password reconfirm
	$birthday = sanitize_text_field($_POST['birthday']); // in format YYYY-mm-dd
	$gender = sanitize_text_field($_POST['gender']);
	$token = sanitize_text_field($_POST['token']);
	
	$res = array();
	session_start();
	if ($_SESSION['token']!=$token) {
		$res = array('code'=>'ERR012','stat'=>"ERR","message"=>"Token needed!");
		echo json_encode($res);
		return false;
	}
	
	if ($display_name!='' && $user_email!='' && $password!='' && $password2!='') {
		// check password
		if ($password!=$password2 || strlen($password)<6) {
			$res = array('code'=>'ERR006','stat'=>"ERR","message"=>"password != confirm password, password length must be greater than 6!");
			echo json_encode($res);
			return false;
		}
		// check email
		if (!is_email($user_email)) {
			$res = array('code'=>'ERR007','stat'=>"ERR","message"=>"email format is not valid!");
			echo json_encode($res);
			return false;
		}
		// check email ganda!
		if (email_exists($user_email)) {
			$res = array('code'=>'ERR008','stat'=>"ERR","message"=>"email already exist. Maybe with social media connect!");
			echo json_encode($res);
			return false;
		}
		// check user login
		if ($req_user_login && validate_username($req_user_login)) {
			if (username_exists ( $req_user_login )) {
				$res = array('code'=>'ERR009','stat'=>"ERR","message"=>"username already exist!");
				echo json_encode($res);
				return false;
			} else {
				$user_login = $req_user_login;
			}
		} else {
			$user_login = trim(str_replace(' ','_',strtolower( $display_name )));
		}
		
		if ( username_exists ( $user_login ) ){
			$i = 1;
			$user_login_tmp = $user_login;
			do
			{
				$user_login_tmp = $user_login . "_" . ($i++);
			} while (username_exists ($user_login_tmp));
		
			$user_login = $user_login_tmp;
		}
		
		if( ! validate_username( $user_login ) ){
			$user_login = "email_user_" . md5( $user_email );
		}
		
		// userdata
		$userdata = array(
				'user_login'    => $user_login,
				'user_nicename' => $user_login, 
				'user_email'    => $user_email,
				'display_name'  => $display_name,
				'description'   => $user_data['description'],
				'user_pass'     => $password
		);
		
		$userdata['user_activation_key'] = sha1(json_encode($userdata));
		
		// Bouncer :: Membership level
		if( get_option( 'wsl_settings_bouncer_new_users_membership_default_role' ) != "default" ){
			$userdata['role'] = get_option( 'wsl_settings_bouncer_new_users_membership_default_role' );
		}
		
		// Bouncer :: User Moderation : None
		if( get_option( 'wsl_settings_bouncer_new_users_moderation_level' ) == 1 ){
			// well do nothing..
		}
		
		// Bouncer :: User Moderation : Yield to Theme My Login plugin
		if( get_option( 'wsl_settings_bouncer_new_users_moderation_level' ) > 100 ){
			$userdata['role'] = "pending";
		}

		// Create a new user
		$user_id = wp_insert_user( $userdata );
				
		// update user metadata
		if( $user_id && is_integer( $user_id ) ){
			update_user_meta( $user_id, "email", $user_email);
			update_user_meta( $user_id, "activation_code", wp_generate_password(32,false,false));
		}
		// do not continue without user_id or we'll edit god knows what
		else {
			if( is_wp_error( $user_id ) ){
				$resp = array('stat'=>'ERR','code'=>'ERR005','message'=>_wsl__("An error occurred while creating a new user!" . $user_id->get_error_message(), 'wordpress-social-login'));
				echo json_encode($resp);
				return false;
			}
			$resp = array('stat'=>'ERR','code'=>'ERR006','message'=>_wsl__("An error occurred while creating a new user!", 'wordpress-social-login'));
			return false;
		}
		
		// send email notications
		suararadio_user_mail_activation($user_id);
		
		// Send notifications
		if ( get_option( 'wsl_settings_users_notification' ) == 1 ){
			suararadio_admin_notification( $user_id, $provider );
		}
		
		// HOOKABLE: any action to fire right after a user created on database
		do_action( 'suararadio_hook_process_login_after_create_wp_user', $user_id, $provider, $user_data );
		
		$res['user_id'] = $user_id;
		$res['code'] = "1";
		$res['stat'] = "OK";
		$res['user_email'] = $user_email;
		$res['user_login'] = $user_login;
		$res['message'] = "User created! open your email to activate login";
		$_SESSION['token'] = md5(uniqid());
	} else {
		$res = array('code'=>'ER005','stat'=>"ERR","message"=>"display_name, user_email, password, confirm password are mandatory!");
	}
	echo json_encode($res);
	return true;
} //endfunc

/**
 * process login to website
 * dependencies: wordpress_social_login
 */
function suararadio_user_login() {
	global $suararadio;
	global $suararadio_connect;
	global $wpdb;
	$start = time();
	
	$provider = null;
	if ($_POST['username']!='' && $_POST['password']!='') {
		suararadio_check_username_pattern($_POST['username']);
	}

	if ($_POST['Facebook']!='') {
		$provider = "Facebook";
		$id = $_POST['Facebook'];
	} else if ($_POST['Twitter']!='') {
		$provider = "Twitter";
		$id = $_POST['Twitter'];
	} else if ($_POST['Yahoo']!='') {
		$provider = "Yahoo";
		$id = $_POST['Yahoo'];
	} else if ($_POST['Google']!='') {
		$provider = "Google";
		$id = $_POST['Google'];
	} else if ($_POST['MSN']!='') {
		$provider = "MSN";
		$id = $_POST['MSN'];
	} else if ($_POST['Spin']!='') {
		$provider = "Spin";
		$id = $_POST['Spin'];
		$passwd = $_POST['password'];
	} else if ($_POST['Email']!='') {
		$provider = "Email";
		$id = $_POST['Email'];
		$passwd = $_POST['password'];
	} else if ($_POST['Userid']!='') {
		$provider = "Userid";
		$id = $_POST['Userid'];
		$passwd = $_POST['password'];
	} else {
		$res = array('stat'=>"ERR","message"=>"Login not supported!");
		echo json_encode($res);
		return false;
	}
	
	//$sql = "SELECT user_id FROM $wpdb->usermeta WHERE meta_key = '%s' AND meta_value = '%s'";
	//$user_id = (int) $wpdb->get_var( $wpdb->prepare( $sql, $provider, $id) );
	if ($provider=='Email' || $provider=='Userid' || $provider=='Spin') {
		// local check
		$mapProvider = array('Email'=>'email','Userid'=>'login','Spin'=>'spin');
		$user = suararadio_check_wp_user($mapProvider[$provider], $id, $passwd);
		
		if (is_wp_error($user)) {
			$errAr = array('user_not_activated'=>"ER010");
			$errCode = $errAr[$user->get_error_code()];
			$res = array('code'=>$errCode,'stat'=>"ERR","message"=>$user->get_error_message());
			echo json_encode($res);
			return false;
		}
		$user_id = $user->ID;
	} else {
		$user_id = (int) wsl_get_user_by_meta($provider,$id);
	}
	$log = "";
	$request_user_login = null;
	$request_user_email = null;
	$user_data = null;
	
	if ($user_id>0) {
		// user already exists
		// check data ke api.suararadio.com, kalo ada data baru update data di website lokal
		$armeta = array("Facebook","Twitter","Yahoo","Google","MSN","Spin",
						"member_type","expired","member_pack",
                        "member_deposit","Live",
                        "ext_expired","radio_id","melon_uid","melon_id","melon_pwd","melon_exp",
                        "melon_pack","melon_stat","no_hp","other_radios");
		
		// need check membership ? from api suararadio.com for automatic load membership
		if (SUARARADIO_MEMBERSHIP_CHECK) {
			$log .= "CHEck!!";
			$suararadio->initAPI();
			$end = time();
			$log .= " -".($end-$start);
			
			$vars = array();
           	$user_data = get_userdata( $user_id );
			$end = time();
            $log .= " -".($end-$start);
            
			$vars['klubid'] = $user_data->klubid;
			$member = $suararadio->api->getMember($vars);
			$end = time();
			$log .= " ::".$member['log'];
            $log .= " -".($end-$start);
            $log .= var_export($vars,true);
            
			
			$new_update = false;
			if (function_exists('update_user_meta')) {
				$new_update = true;
			}
			
			if ($member['id']) {
				foreach ($armeta as $vk) {
					if ($member[$vk]!='') {
						if ($new_update) {
							update_user_meta( $user_id, $vk, $member[$vk]);
						} else {
							update_usermeta( $user_id, $vk, $member[$vk]);
						}
					}
				}
			} // endif
			if ($member['username']!='') {
				if ($new_update) {
					update_user_meta($user_id,'klubid',$member['username']);
				} else {
					update_usermeta($user_id,'klubid',$member['username']);
				}
			} //endif
		}
		wp_set_auth_cookie( $user_id );
		$end = time();
		$log .= "--".($end-$start);

		$res = array('stat'=>"OK","message"=>"Success","code"=>1); //,"log"=>$log
		echo json_encode($res);
		return true;
	} else {
		// create new user based on provider.
		if (defined('WORDPRESS_SOCIAL_LOGIN_PLUGIN_URL')) {
			$suararadio->initAPI();
			
			if ($provider=="Spin") {
				// check on API spin
				$resp = $suararadio->api->spinLogin($id, $passwd);
				if ($resp['code']!=1) {
					$resp['stat'] = 'ERR';
					echo json_encode($resp);
					return false;
				}
				// verified spin user
				$request_user_login = $resp['name_person'];
				$request_user_email = $resp['email']; 
				$user_data = $resp;
			} else if ($provider="Facebook") {
				//$suararadio_connect['facebook']['obj'];
				include_once(WORDPRESS_SOCIAL_LOGIN_ABS_PATH.'/hybridauth/Hybrid/thirdparty/Facebook/facebook.php');
				$appId = get_option( "wsl_settings_Facebook".$wsl_concat."_app_id" );
				$secret = get_option( "wsl_settings_Facebook".$wsl_concat."_app_secret" );
				$facebook = new Facebook(array(
					'appId'  => $appId,
					'secret' => $secret,
					'cookie' => true,
				));
				try {
					$user_data = $facebook->api("/".$id);
					$user_data['identifier'] = $id;
					$user_data['display_name'] = $user_data['name'];
				} catch (FacebookApiException $e) {
					$user_data = array();
					$resp = array('code'=>"ER002","message"=>"failed get data from facebook!",'stat'=>"ERR");
					echo json_encode($resp);
					return false;
				}				
			} else {
				// using social login
				$user_data = $_POST['user_data'];
			}
			// must exists: identifier,email,display_name
			if ($user_data['identifier']!='' && $user_data['email']!='' && $user_data['display_name']!='') {
				$request_user_login = $user_data['login'];
				$request_user_email = $user_data['email'];
			} else {
				$resp = array('code'=>"ER001","message"=>"data not complete!",'stat'=>"ERR");
				echo json_encode($resp);
				return false;
			}
			// needed params:
			// $provider --> ok
			// $user_data, ---> ok
			// $request_user_login ---> ok
			// $request_user_email ---> ok
			list(
				$user_id    , // ..
				$user_login , // ..
				$user_email , // ..
			)
			= suararadio_create_wp_user( $provider, $user_data, $request_user_login, $request_user_email );
			if ($user_id>0) {
				suararadio_store_user_data( $user_id, $provider, $user_data );
				wp_set_auth_cookie( $user_id );
				$res = array('code'=>1,"message"=>"user created",'stat'=>"OK",'user_id'=>$user_id,'user_login'=>$user_login,'user_email'=>$user_email);
				echo json_encode($res);
				return true;
			}
		}
		$res = array('stat'=>"ERR","message"=>"User not found!","code"=>"ER004");
		echo json_encode($res);
		return false;
	}
} //endfunc suararadio user login

function suararadio_user_logout() {
	wp_logout();

	$res = array('stat'=>"OK","message"=>"Success");
	echo json_encode($res);
	return true;
}

function suararadio_create_wp_user( $provider, $user_data, $request_user_login, $request_user_email )
{
	// HOOKABLE: any action to fire right before a user created on database
	do_action( 'suararadio_hook_process_login_before_create_wp_user' );

	$user_login = null;
	$user_email = null;

	// if coming from "complete registration form"
	if( $request_user_email && $request_user_login ){
		$user_login = $request_user_login;
		$user_email = $request_user_email;
	}

	# else, validate/generate the login and user email
	else{
		// generate a valid user login
		$user_login = trim( str_replace( ' ', '_', strtolower( $user_data['display_name'] ) ) );
		$user_email = $user_data['email'];

		if( empty( $user_login ) ){
			$user_login = trim( $user_data['last_name'] . " " . $user_data['first_name'] );
		}

		if( empty( $user_login ) ){
			$user_login = strtolower( $provider ) . "_user_" . $user_data['identifier'];
		}

		// user name should be unique
		if ( username_exists ( $user_login ) ){
			$i = 1;
			$user_login_tmp = $user_login;

			do
			{
				$user_login_tmp = $user_login . "_" . ($i++);
			} while (username_exists ($user_login_tmp));

			$user_login = $user_login_tmp;
		}

		if (empty($user_email)) {
			$user_email = $request_user_email;
		}

		// generate an email if none
		if ( ! isset ( $user_email ) OR ! is_email( $user_email ) ){
			$user_email = strtolower( $provider . "_" . $user_login ) . "@klubradio.co.id";
		}

		// email should be unique
		if ( email_exists ( $user_email ) ){
			do
			{
				$user_email = md5(uniqid(wp_rand(10000,99000)))."@klubradio.co.id";
			} while( email_exists( $user_email ) );
		}

		$user_login = sanitize_user ($user_login, true);

		if( ! validate_username( $user_login ) ){
			$user_login = strtolower( $provider ) . "_user_" . md5( $user_data['identifier'] );
		}
	}

	$display_name = $user_data['display_name'];

	if( $request_user_login || empty ( $display_name ) ){
		$display_name = $user_login;
	}

	$userdata = array(
			'user_login'    => $user_login,
			'user_email'    => $user_email,
			'display_name'  => $display_name,
			'first_name'    => $user_data['first_name'],
			'last_name'     => $user_data['last_name'],
			'user_url'      => $user_data['profile_url'],
			'description'   => $user_data['description'],
			'user_pass'     => wp_generate_password()
	);

	// Bouncer :: Membership level
	if( get_option( 'wsl_settings_bouncer_new_users_membership_default_role' ) != "default" ){
		$userdata['role'] = get_option( 'wsl_settings_bouncer_new_users_membership_default_role' );
	}

	// Bouncer :: User Moderation : None
	if( get_option( 'wsl_settings_bouncer_new_users_moderation_level' ) == 1 ){
		// well do nothing..
	}

	// Bouncer :: User Moderation : Yield to Theme My Login plugin
	if( get_option( 'wsl_settings_bouncer_new_users_moderation_level' ) > 100 ){
		$userdata['role'] = "pending";
	}

/*
	// HOOKABLE: change the user data
	$userdata = apply_filters( 'wsl_hook_process_login_alter_userdata', $userdata, $provider, $hybridauth_user_profile );

	// HOOKABLE: any action to fire right before a user created on database
	do_action( 'wsl_hook_process_login_before_insert_user', $userdata, $provider, $hybridauth_user_profile );

	// HOOKABLE: delegate user insert to a custom function
	$user_id = apply_filters( 'wsl_hook_process_login_after_insert_user', $userdata, $provider, $hybridauth_user_profile );
*/
	// Create a new user
	if( ! $user_id || ! is_integer( $user_id ) ){
		$user_id = wp_insert_user( $userdata );
	}

	// update user metadata
	if( $user_id && is_integer( $user_id ) ){
		update_user_meta( $user_id, $provider, $user_data['identifier']);
	}

	// do not continue without user_id or we'll edit god knows what
	else {
		if( is_wp_error( $user_id ) ){
			$resp = array('stat'=>'ERR','code'=>'ERR005','message'=>_wsl__("An error occurred while creating a new user!" . $user_id->get_error_message(), 'wordpress-social-login'));
			echo json_encode($resp);
			return false;
		}
		$resp = array('stat'=>'ERR','code'=>'ERR006','message'=>_wsl__("An error occurred while creating a new user!", 'wordpress-social-login'));
		return false;
	}

	// Send notifications
	if ( get_option( 'wsl_settings_users_notification' ) == 1 ){
		suararadio_admin_notification( $user_id, $provider );
	}

	// HOOKABLE: any action to fire right after a user created on database
	do_action( 'suararadio_hook_process_login_after_create_wp_user', $user_id, $provider, $user_data );

	return array(
			$user_id,
			$user_login,
			$user_email
	);
} //endfunc suararadio_create_wp_user

function suararadio_created_user($user_id, $provider, $user_data) {
	global $suararadio;
	global $arr_var_upd;

	// check apakah spin provider
	if ($provider=='Spin') {
		// tambahkan rutin untuk aktifkan melonnya.
		$suararadio->initAPI();

		$user = get_userdata( $user_id );
		$vars = (array) $user->data;
		$vars['tipe'] = "20";
		$vars['vcode'] = 1; // 30 hari
		$vars['provider'] = "Spin";
		$vars['identity'] = $user_data['identifier'];

		$vars['home_url'] = defined('API_CLIENT_HOME_URL')? API_CLIENT_HOME_URL:get_option("siteurl");
		if (defined('RADIO_ID')) $vars['radio_id'] = RADIO_ID;
		//if (defined('IDRADIO')) $vars['radio_id'] = IDRADIO; //ini maksudnya r_number
		if (defined('IDRADIO')) $vars['r_number'] = IDRADIO;

		$result = $suararadio->api->activate($vars);

		if ($result[code]=='1' || $result[code]=='2' || $result[code]=='3') {
			// update data local
			if (function_exists('update_user_meta')) {
				foreach ($arr_var_upd as $ky) {
					update_user_meta($user_id,$ky,$result[$ky]);
				}
			} else {
				foreach ($arr_var_upd as $ky) {
					update_usermeta($user_id,$ky,$result[$ky]);
				}
			}
		}
	}

	return $user_id;
} //endfunc suararadio_created_user

/**
 * Store user data
 * @param unknown $user_id
 * @param unknown $provider
 * @param unknown $user_data
 */
function suararadio_store_user_data( $user_id, $provider, $user_data )
{
	global $wpdb;
	
	if (defined('WORDPRESS_SOCIAL_LOGIN_PLUGIN_URL')) {
		$sql = "SELECT id, object_sha FROM `{$wpdb->prefix}wslusersprofiles` where user_id = '$user_id' and provider = '$provider'";
		$rs  = $wpdb->get_results( $sql );
	
		$profile_sha = sha1( serialize( $user_data ) );
	
		// if $profile didnt change, no need for update
		if( $rs && $rs[0]->object_sha == $profile_sha ){
			return;
		}
	
		$table_data = array(
				"user_id"    => $user_id,
				"provider"   => $provider,
				"object_sha" => $profile_sha 
		);
	
		foreach( $user_data as $key => $value ) {
			$table_data[ strtolower($key) ] = (string) $value;
		}
	
		// if $profile updated we re/store on database
		if( $rs && $rs[0]->object_sha != $profile_sha ){
			$wpdb->update( "{$wpdb->prefix}wslusersprofiles", $table_data, array( "id" => $rs[0]->id ) );
		}
		else{
			$wpdb->insert( "{$wpdb->prefix}wslusersprofiles", $table_data );
		}
	}
} //endfunc suararadio_store_user_data

function suararadio_user_mail_activation($user_id) {
	$user = new WP_User($user_id);
	$user_login = stripslashes($user->data->user_login);
	
	$headers = array('From'=>'KlubRadio <klubradio@klubradio.co.id>');
	
	$blogname = bloginfo('name');
	$code = get_user_meta($user->ID,'activation_code',true);
	$url = home_url()."/apis/user/validate?activation_code=".$code."&user_login=".$user->data->user_login;
	
	$message  = sprintf(__('Hello %s. You have registered, with: ' , 'suararadio'), $user->data->display_name      ) . "<br/><br/>";
	$message .= sprintf(__('Username: %s'                          , 'suararadio'), $user->data->user_login      ) . "<br/>";
	$message .= sprintf(__('Email: %s'                             , 'suararadio'), $user->data->user_email) . "<br/>";
	$message .= "\r\n\r\n";
	$message .= __('Your KlubRadio account is ready for activation. If you did not make this request, just ignore this email. Otherwise, please visit the link below to activate your account:','suararadio')."<br/><br/>";
	$message .= sprintf(__('<a href="%s">%s</a>', 'suararadio'),$url,$url)."<br/><br/>";
	$message .= __('Enjoy your millions of songs, radio contents, and merchant discount. Anywhare you go','suararadio')."<br/><br/>";
	$message .= __('Sincerely,','suararadio')."<br/><br/>";
	$message .= __('KlubRadio Team','suararadio')."<br/><br/>";
	
	wp_mail($user->data->user_email, 'KlubRadio '.sprintf(__('[%s] New User Activation', 'suararadio'), $blogname), $message,$headers);
}

function suararadio_admin_notification( $user_id, $provider )
{
	//Get the user details
	$user = new WP_User($user_id);
	$user_login = stripslashes($user->data->user_login);

	// The blogname option is escaped with esc_html on the way into the database
	// in sanitize_option we want to reverse this for the plain text arena of emails.
	$blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

	$message  = sprintf(__('New user registration on your site: %s', 'suararadio'), $blogname        ) . "\r\n\r\n";
	$message .= sprintf(__('Username: %s'                          , 'suararadio'), $user_login      ) . "\r\n";
	$message .= sprintf(__('Provider: %s'                          , 'suararadio'), $provider        ) . "\r\n";
	$message .= sprintf(__('Profile: %s'                           , 'suararadio'), $user->user_url  ) . "\r\n";
	$message .= sprintf(__('Email: %s'                             , 'suararadio'), $user->user_email) . "\r\n";
	$message .= "\r\n--\r\n";
	$message .= "WordPress Social Login\r\n";
	$message .= "http://wordpress.org/extend/plugins/wordpress-social-login/\r\n";

	@wp_mail(get_option('admin_email'), '[WordPress Social Login] '.sprintf(__('[%s] New User Registration', 'wordpress-social-login'), $blogname), $message);
} //endfunc suararadio_admin_notification

/**
 * Purchase init untuk
 * @param mixed $data
 */
function suararadio_user_purchase_init($data) {
        global $suararadio;
        global $current_user;

        $vars = array();
        if ($current_user->ID>0) {
                $suararadio->initAPI();

                $vars['r_number'] = IDRADIO; // nomor punggung,tes
                $vars['email'] = ($current_user->user_email!='')? $current_user->user_email:'';
                $vars['web_uid'] = $current_user->ID;
                $vars['klubid'] = ($current_user->klubid!='')? $current_user->klubid:'0';
                $vars['target_id'] = '0';
                $vars['name'] = $current_user->display_name;
                $vars['user_registered'] = $current_user->user_registered;
                $armeta = array(
                                "Facebook"=>"Facebook","Twitter"=>"Twitter","Spin"=>"Spin",
                                "Yahoo"=>"Yahoo","Google"=>"Google","Live"=>"Live");
                foreach ($armeta as $ky=>$vl) {
                        if ($current_user->{$vl}!='') $vars[$ky] = $current_user->{$vl};
                }
                $vars['url'] = $_SERVER['HTTP_HOST'];
                //var_dump($current_user);
                 
                $token = $suararadio->api->purchaseInit($vars);
                echo json_encode($token);
                return true;
        }
        return false;
}// endfunc suararadio_user_purchase_init;


add_action('suararadio_hook_process_login_after_create_wp_user','suararadio_created_user',10,3);