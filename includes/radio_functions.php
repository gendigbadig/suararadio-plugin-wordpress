<?php

if (!defined('META_PODPRESSMEDIA')) define('META_PODPRESSMEDIA','podPressMedia');
if (!defined('META_RADIO_NAME')) define('META_RADIO_NAME','');
$keyword = '';

function suararadio_data_process($path){
	$params = array();
	switch ($path[3]) {
		case "request": 
				return suararadio_data_request_1($path[4]); 
				break;
		case "search": 
				$slug = $_REQUEST['slug'];
				$keyword = $_REQUEST['keyword'];
				$offset = $_REQUEST['offset'];
				$limit = 20;
				if(isset($_REQUEST['limit']) && (intval($_REQUEST['limit']) >= 5 ))
					$limit = $_REQUEST['limit'];
				return suararadio_title_search($slug, $offset, $keyword,$limit);
				break;
		case "rs":
			return suararadio_relevanssi_search($_REQUEST['keyword']);
			break;
		case "podcast":
			$slug = $_REQUEST['slug'];
			$offset = $_REQUEST['offset'];
			$limit = $_REQUEST['limit'];
			return suararadio_data_podcast($slug,$offset,$limit);
			break;
		case "content":
			$slug = $_REQUEST['slug'];
			$offset = $_REQUEST['offset'];
			return suararadio_data_content_x($slug,$offset);
			break;
		case "streaming":
			$radio_id = ($path[4]!='')?$path[4]:IDRADIO;
			return suararadio_radio_streaming($radio_id);
			break;
		default:
			status_header('404');
	}
}

function suararadio_radio_streaming($radio_id) {
	global $data_meta_radio;	
        
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	$attr = '';
	if ($radio_id != ''){
	   	$attr .= (($attr != '')?'&radio='.$radio_id:'&radio='.$radio_id);
	}
	curl_setopt($ch, CURLOPT_URL, "http://api.suararadio.com/streaming/lists/?method=2.1".$attr);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$output = curl_exec($ch);
	curl_close($ch);
	$arr = json_decode($output, true);
	$data_meta_radio = $arr;
	
	add_action(wp_head,suararadio_meta_radio);
	
	get_header();
	echo "<div class='main-content-box'><div class='container container-wrap'><div class='row'><div class='span12' style='text-align:center;'>";
	echo "<img src = '".$arr['pathlogo']."' width='150' />";
	echo "<div>Name : ".$arr['name']."</div>";
	echo "<div>Lokasi : ".$arr['kota'].", ".$arr['namapropinsi']."</div>";
	echo "<div>Frekuensi : ".$arr['frekuensi']." ".$arr['band']."</div>";
	echo "<div>Website : ".$arr['sitea_dd']."</div>";
	echo "<div>Stereo : ".$arr['url_stream_stereo']."</div>";
	echo "<div>Mono : ".$arr['url_stream_mono']."</div>";
	echo "<div id='fb-root'></div>";            
	$nmFileFb 	= $arr['pathlogo'];
	
	$mediaUFb 	= "image";
	$dtSite	= explode("/",$arr['sitea_dd']);
	$optUrl     	= $arr['sitea_dd'];
    $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	$loc = $arr['url_stream_stereo'];
	
       $params = array('message'=>$dtSite[2], 'url'=>$url, 'nmfilefb'=>$nmFileFb, 'opturl'=>$optUrl, 'mediafb'=>$mediaUFb,'loc'=>$loc,'image'=>$arr['pathlogo']);
	
	$dtFb 	= explode ("&",$_REQUEST["fbs_144389048967182"]);
	$dtFb1 		= explode ("=", $dtFb[5]);
	$uidFb		=  ereg_replace("\"","",$dtFb1[1]);
	echo showIconCheckInResult2('', $params, "", $uidFb);
    
    if($arr['url_stream_mono'] !='http://stream.suararadio.com:8000/-' && 
    $arr['url_stream_mono'] !=''){
    $class_stream = 'streamplayer';
    $type ='0';       
    $href = $arr['url_stream_mono'];
                                                                                                   
    }else{
    $class_stream = 'apply-nolazy';
    $type ='1';
    $href = 'http://www.diradio.net/play-streaming/?idRadio='.$arr['id'].'&type=.pls';
                                                                       
    } 
    
    echo '<a href="'.$href.'" class="'.$class_stream.'" stream_diradio="'.$arr['id'].'">                                                                                                                    
    <button class="btn btn-inverse btn-mini" type="button" style="width:50px;"><i class="icon-play-sign"></i> Play</button>
    </a>';
	
	//echo tweet_button($url);
	echo "<ul class='social-media'>";
	echo "</div></div></div></div>";
	get_footer();
	return true;
}

function showIconCheckInResult2($postId, $params=array(), $CHECKINFBPLUGIN_URL, $uidFb){
	global $wpdb, $wp_query, $suararadio_connect;
	global $meta_radio;
	global $data_meta_radio;
        
	$Tempuid = $suararadio_connect['facebook']['session']['uid'];			
	/*$querystr = "
		SELECT wposts.* 
		FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta
		WHERE wposts.ID = wpostmeta.post_id 
		AND wposts.ID = ".$postId."
		ORDER BY wposts.post_date DESC
 	";

 	$pageposts = $wpdb->get_results($querystr, OBJECT);
    $konten = $pageposts[0]->post_content;
    $url_staytune = getPermalink($pageposts[0]->ID);//$_SERVER['HTTP_HOST'].$link;
	$isi = preg_replace('/((\\\\n)+)/'," ",preg_replace("/(\r\n|\n)/",'\\n',stripslashes($pageposts[0]->post_content)));  
	$jdl = preg_replace('/\"/'," ",preg_replace("/(\r\n|\n)/",'\\n',stripslashes($pageposts[0]->post_title)));  
	
  	if($_tmp= explode(",",$params['nmfilefb'])){
		$_file_counts = count($_tmp);
		$i=0;
		while(!eregi("(.+)\.mp3$",$_tmp[$i],$_regs) && ($i<$_file_counts))
			$i++;
		
		$list=explode("/",$_tmp[$i]);
	}else
	 
 		$list = explode("/", $params['nmfilefb']);

	$isi = strip_tags($isi);	
	$quotes = array('/"/',"/'/");
	$replacements = array('%22','%27');

	$isi = preg_replace($quotes,$replacements,$isi);
	$isi = substr($isi,0,150)." ...";*/
    $meta_radio = $data_meta_radio;
    $jdl = $meta_radio['name']." ".$meta_radio['frekuensi']." ".$meta_radio['band']." ".$meta_radio['kota'];    
    $isi = "Listen to ".$jdl." streaming online! You can listen directly on Facebook. Click the image to play.";
	
	$url = $_SERVER['HTTP_HOST'];
	$loc = $params['loc'];
    
    $url_staytune = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $api_staytune_streaming = $url_staytune;
    
	$imageLink = $params['image'];
    
		if($url=='www.diradio.net'){
		  	//$out = "<a class='aStay' onClick='streamPublish(\"Stay-tune on ".$_SERVER['HTTP_HOST']."\", \"".htmlentities($jdl, ENT_QUOTES)."\", \"".$isi."\", \"".htmlentities($jdl, ENT_QUOTES)."\", \"".$params['url']."\", \"".$loc."\", \"".$_SERVER['HTTP_HOST']."\", \"".$params['mediafb']."\", \"".'www.diradio.net'."\", \"".$CHECKINFBPLUGIN_URL."\", \"".$uidFb."\", \"".$postId."\", \"".$imageLink."\");'><img src='".get_bloginfo('template_url')."/images/f-tuned-c.png' border='0'></a>";		
    	    $out = "<a class='aStay' onClick='streamPublish_streaming(\"Stay-tune on ".$_SERVER['HTTP_HOST']."\", \"".htmlentities($jdl, ENT_QUOTES)."\", \"".$isi."\", \"".htmlentities($jdl, ENT_QUOTES)."\", \"".$params['url']."\", \"".$loc."\", \"".$_SERVER['HTTP_HOST']."\", \"".$params['mediafb']."\", \"".'www.diradio.net'."\", \"".$CHECKINFBPLUGIN_URL."\", \"".$uidFb."\", \"".$postId."\", \"".$imageLink."\");'>
            <button class='btn btn-primary btn-mini' type='button'><i class='icon-facebook'></i> stay tune</button>
            </a>";
        }else{
            //$out = "<a class='aStay' onClick='streamPublish(\"Stay-tune on ".$_SERVER['HTTP_HOST']."\", \"".htmlentities($jdl, ENT_QUOTES)."\", \"".$isi."\", \"".htmlentities($jdl, ENT_QUOTES)."\", \"".$params['url']."\", \"".$loc."\", \"".$params['HTTP_HOST']."\", \"".$params['mediafb']."\", \"".$params['message']."\", \"".$CHECKINFBPLUGIN_URL."\", \"".$uidFb."\", \"".$postId."\", \"".$imageLink."\");'><img src='".get_bloginfo('template_url')."/images/f-tuned-c.png' border='0'></a>";		
		    $out = "<a class='aStay' onClick='streamPublish_streaming(\"Stay-tune on ".$_SERVER['HTTP_HOST']."\", \"".htmlentities($jdl, ENT_QUOTES)."\", \"".$isi."\", \"".htmlentities($jdl, ENT_QUOTES)."\", \"".$params['url']."\", \"".$loc."\", \"".$params['HTTP_HOST']."\", \"".$params['mediafb']."\", \"".$params['message']."\", \"".$CHECKINFBPLUGIN_URL."\", \"".$uidFb."\", \"".$postId."\", \"".$imageLink."\");'>
            <button class='btn btn-primary btn-mini' type='button'><i class='icon-facebook'></i> stay tune</button>
            </a>";
        }
	return $out;		
}



function suararadio_meta_radio() {
	//global $is_mobile_status;
	global $suararadio;
	global $current_user;
	global $wpdb;
    global $suararadio_connect;
	global $meta_radio;
	global $data_meta_radio;
	
	$meta_radio = $data_meta_radio;
	$wsl_concat = (defined('WSL_CONCAT')) ? WSL_CONCAT:'';
	
	#if (!$is_mobile_status) {
	#	echo '<link rel="stylesheet" href="'.plugins_url('suararadio/suararadio.css').'" type="text/css" />'."\n";
    #	} else {
	#	echo '<link rel="stylesheet" href="'.plugins_url('suararadio/suararadio_mobile.css').'" type="text/css" />'."\n";
	#}
	#echo '<link rel="stylesheet" type="text/css" href="'.plugins_url('suararadio/flashblock.css').'" />'."\n";
	#echo '<link rel="stylesheet" href="'.plugins_url('suararadio/css/jquery-ui-1.8.1.css').'" type="text/css" />'."\n";
    
    $whatINeed = explode('/', $_SERVER['REQUEST_URI']);
    //echo 
    $whatINeed = $whatINeed[0];
	   
       
	$appId = get_option( "wsl_settings_Facebook".$wsl_concat."_app_id" );
	$jdl = $meta_radio['name']." ".$meta_radio['frekuensi']." ".$meta_radio['band']." ".$meta_radio['kota'];
		
	$purl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	//$stream ="http://stream.suararadio.com:8000/$meta_radio[url_stream_mono]";
    $stream_stereo = $meta_radio['url_stream_stereo'];
    
                                                                	
    if($stream_stereo !='' && $stream_stereo !='-'){
        $loc = $meta_radio['url_stream_stereo'];
        $stat = '.flv';
    }else{
        $loc = $meta_radio['url_stream_mono'];
        $stat = '.mp3';
    }	
		
	//$stream = "http://api.suararadio.com/streaming/playlist?radio=".$meta_radio['id'];
	//$srcsec = "https://api.suararadio.com/assets/player.swf?file=$stream&autostart=true&image=$meta_radio[pathlogo]&stretching=uniform&logo.hide=false&logo.file=https://api.suararadio.com/stayTuned/logo";
	//$src = "http://api.suararadio.com/assets/player.swf?file=$stream&autostart=true&image=$meta_radio[pathlogo]&stretching=uniform&logo.hide=false&logo.file=http://api.suararadio.com/stayTuned/logo";
	$srcsec = "https://api.suararadio.com/player/player_stream.swf?file=$loc?type=$stat&autostart=true&image=$meta_radio[pathlogo]&stretching=uniform&logo.hide=false&logo.file=https://api.suararadio.com/stayTuned/logo&backcolor=0xe6e6e6";
	$src = "http://api.suararadio.com/player/player_stream.swf?file=$loc?type=$stat&autostart=true&image=$meta_radio[pathlogo]&stretching=uniform&logo.hide=false&logo.file=http://api.suararadio.com/stayTuned/logo&backcolor=0xe6e6e6";
	echo '<img src="'.$meta_radio['pathlogo'].'" width="200" height="200" style="display:none;" />';	
	  	
	echo '<meta property="fb:app_id" content="'.$appId.'" />
		  <meta property="og:url" content="'.$purl.'" />
          <meta property="og:type" content="website" />
	  	  <meta property="og:site_name" content="'.$jdl.' - on diradio.net" />
          <meta property="og:title" content="'.$jdl.'" />
          <meta property="og:description" content="Listen to '.$jdl.' streaming online! You can listen directly on Facebook. Click the image to play." />
          <meta property="og:image" content="'.$meta_radio['pathlogo'].'" />
          
	      <meta property="og:video" content="'.$src.'"/>
          <meta property="og:video:secure_url" content="'.$srcsec.'"/>
          <meta property="og:video:height" content="250"/>
          <meta property="og:video:width" content="500"/>
          <meta property="og:video:type" content="application/x-shockwave-flash"/>
	   
	   	  <meta name="description" content="'.$jdl.'" />

	   	  <meta name="twitter:card" content="player">
          <meta name="twitter:site" content="@diradiodotnet">
          <meta name="twitter:creator" content="@diradiodotnet">
          <meta name="twitter:title" content="'.$jdl.'">
          <meta name="twitter:url" content="'.$meta_radio['sitea_dd'].'">
          <meta name="twitter:description" content="Listen to '.$jdl.' online!">
          <meta name="twitter:image" content="'.$meta_radio['pathlogo'].'"> 
          <meta name="twitter:player" content="'.$src.'">
          <meta name="twitter:player:width" content="640">
          <meta name="twitter:player:height" content="500">
          <meta name="twitter:domain" content="'.$meta_radio['sitea_dd'].'">   
          ';
} #endfunc suararadio_wp_head


function suararadio_data_request_1($mode) {
# 	fungsi untuk mengambil semua request dari pendengar, baik dari facebook, twitter, atau sms

	global $suararadio;
	global $current_user;
	global $wpdb;
	global $wpdbsr;
	global $wprisedb2;
	global $wprisesmsdb;
	global $wprisesmsdb2;
	
	if(isset($wpdbsr)) {
		$MESSAGE_TABLE = IDRADIO."_t_messages";
		$sql = 	"SELECT * FROM ". $MESSAGE_TABLE.
				" WHERE id_comment = '' and tgl = '" .date("Y-m-d"). "'";
			
		if($mode != null) {
			$sql = $sql." AND jenis = '".$mode."'";
		} 
		$sql = $sql. " ORDER BY waktu DESC LIMIT 30 ";

		$query_result = $wpdbsr->get_results($sql);
		$temp = array();
		foreach ($query_result as $result){
			$vtemp = array();
			$vtemp['id'] = $result->idsm;
			$vtemp['waktu'] = $result->waktu;
			$vtemp['jenis'] = $result->jenis;
			$vtemp['creator'] = $result->creator;
			if (($result->jenis == '1')||($result->jenis == '2')||($result->jenis == '3')) { // sms biasa
				$sql = "SELECT * FROM ".IDRADIO."_t_phonebook WHERE no_hp = '".$result->nohp."'";
				$data_member = $wpdbsr->get_results($sql);
				if($data_member[0]->id_member){
					if($data_member[0]->profilFB) {
						$vtemp['pengirim'] = $data_member[0]->nama;
						$vtemp['foto'] = "https://graph.facebook.com/".$data_member[0]->profileFB."/picture";
					} else {
						$vtemp['pengirim'] = $data_member[0]->nama;
						$vtemp['foto'] = "";
					}
				} else {
					if($result->creator == 'c' || $result->creator == 'i' || $result->creator == 'w' || $result->creator == 'b' || $result->creator == 'a'){
						$vtemp['pengirim'] = $result->nohp;
						$vtemp['foto'] = "https://graph.facebook.com/".$result->id_creator."/picture";
					}else{
						$vtemp['pengirim'] = "";
						$vtemp['foto'] = "";
					}
				}
			} else if (($result->jenis == 'f') || ($result->jenis == 't') || ($result->jenis == '6')) {
				$fb_graph_address = "https://graph.facebook.com/".$result->id_creator."?fields=name";
				$fb_json_result = file_get_contents($fb_graph_address);
				$fb_decoded_json = json_decode($fb_json_result);
				$desired_name = $fb_decoded_json->name;
				
				$vtemp['pengirim'] = $desired_name;
				if($result->jenis == 'f') {
					$vtemp['foto'] = "https://graph.facebook.com/".$result->id_creator."/picture";
				} else if ($result->jenis == 't') {
					$vtemp['foto'] = $result->twitter_foto;
				}
			} else {
				$vtemp['pengirim'] = substr($result->nohp,0, -3)."XXX";
				$vtemp['foto'] = "";
			}
			
			$vtemp['pesan'] = stripslashes($result->pesan);
			if ($result->jenis == 'f'){
				$sqlcomment = 	"SELECT * FROM ". $MESSAGE_TABLE.
						" WHERE id_comment != '' and id_feed like '".$result->id_feed."%' order by id_comment asc";
				$dtcomment = $wpdbsr->get_results($sqlcomment);
						$jmlcomment = count($dtcomment);
						$j = 0;
						foreach($dtcomment as $item){
							$vtemp['comment'][$j]['idcomment'] = $item->idsm;
							$vtemp['comment'][$j]['waktu'] = $item->waktu;
							$vtemp['comment'][$j]['jenis'] = $item->jenis;
							$vtemp['comment'][$j]['creator'] = $item->creator;
							$vtemp['comment'][$j]['pesan'] = stripslashes($item->pesan);
							
							$fb_graph_address = "https://graph.facebook.com/".$item->id_creator."?fields=name";
							$fb_json_result = file_get_contents($fb_graph_address);
							$fb_decoded_json = json_decode($fb_json_result);
							$desired_name = $fb_decoded_json->name;
				
							$vtemp['comment'][$j]['pengirim'] = $desired_name;
							$vtemp['comment'][$j]['foto'] = "https://graph.facebook.com/".$item->id_creator."/picture";
							$j++;
						}
						
			}
			
#			$vtemp['id_member'] = $result->id_member;
#			$vtemp['nohp'] = $result->nohp;
#			$vtemp['pesan'] = $result->pesan;
#			$vtemp['creator'] = $result->creator;
#			$vtemp['id_comment'] = $result->id_comment;
#			$vtemp['id_creator'] = $result->id_creator;
#			$vtemp['id_feed'] = $result->id_feed;
#			$vtemp['idsm'] = $result->idsm;
#			$vtemp['nama'] = $result->nama;			
#			$vtemp['cdate'] = $result->cdate;

			$temp[] = $vtemp;
		}
//		echo json_encode($temp);
		
	} else if (($wprisedb2)) {
		$MESSAGE_TABLE = "t_sms";
		$sql = 	"SELECT * FROM ". $MESSAGE_TABLE.
				" WHERE id_comment = '' and tgl_msk = '" .date("Y-m-d"). "'";
		if($mode != null) {
			$sql = $sql." AND req = '".$mode."'";
		} 
		$sql = $sql. " ORDER BY waktu_msk DESC LIMIT 30 ";
		$query_result = $wprisesmsdb2->get_results($sql);
		$temp = array();

		
		foreach ($query_result as $result){
			$vtemp = array();
			$vtemp['id'] = $result->id_sms_masuk;
			$vtemp['jenis'] = $result->req;
			$vtemp['creator'] = $result->creator;
			$vtemp['waktu'] = $result->waktu_msk;
			if($result->req == '1' || $result->req == '2' || $result->req == '3') {
				$sql = "SELECT nama, profileFB FROM t_phonebook WHERE no_hp = '".$result->no_hp."'";
				$member = $wprisesmsdb2->get_results($sql);
				$tmp = explode(" ", $result->no_hp);
				if(count($member) < 1) {
					if($result->creator == 'c' || $result->creator == 'i' || $result->creator == 'w' || $result->creator == 'b' || $result->creator == 'a') {
						$vtemp['foto'] = "https://graph.facebook.com/".$result->id_creator."/picture";
						if(count($tmp) > 1) {
							$vtemp['pengirim'] = $result->no_hp;
						} else {
							$vtemp['pengirim'] = substr($result->no_hp, 0, -3). "XXX";
						}
					} else {
						$vtemp['foto'] = "";
						if(count($tmp) > 1) {
							$vtemp['pengirim'] = $result->no_hp;
						} else {
							$vtemp['pengirim'] = substr($result->no_hp, 0, -3). "XXX";
						}
					}					
				} else {
					$vtemp['pengirim'] = $member[0]->nama;
					if($member[0]->profileFB) {
						$vtemp['foto'] = "https://graph.facebook.com/".$result->id_creator."/picture";
					} else {
						$vtemp['foto'] = "";
					}
				}
			} else {
				if($result->req == 'f' || $result->req == 't' || $result->req == '6') {
					$vtemp['pengirim'] = $result->creator;
				} else {
					$vtemp['pengirim'] = substr($result->no_hp, 0, -3)."XXX";
				}
				
				if($result->req == 'f') {
					$vtemp['foto'] = "https://graph.facebook.com/".$result->id_creator."/picture";
				} else if ($result->req == 't') {
					$vtemp['foto'] = $result->foto_profil_tweet;
				}
			} 
		
			$vtemp['pesan'] = stripslashes($result->isi);
			if ($result->jenis == 'f'){
				$sqlcomment = 	"SELECT * FROM ". $MESSAGE_TABLE.
						" WHERE id_comment != '' and id_feed like '".$result->id_feed."%' order by id_comment asc";
				$dtcomment = $wprisedb2->get_results($sqlcomment);
						$jmlcomment = count($dtcomment);
						$j = 0;
						foreach($dtcomment as $item){
							$vtemp['comment'][$j]['idcomment'] = $item->id_sms_masuk;
							$vtemp['comment'][$j]['waktu'] = $item->waktu_msk;
							$vtemp['comment'][$j]['jenis'] = $item->req;
							$vtemp['comment'][$j]['creator'] = $item->creator;
							$vtemp['comment'][$j]['pesan'] = stripslashes($item->isi);
							
							$fb_graph_address = "https://graph.facebook.com/".$item->id_creator."?fields=name";
							$fb_json_result = file_get_contents($fb_graph_address);
							$fb_decoded_json = json_decode($fb_json_result);
							$desired_name = $fb_decoded_json->name;
				
							$vtemp['comment'][$j]['pengirim'] = $desired_name;
							$vtemp['comment'][$j]['foto'] = "https://graph.facebook.com/".$item->id_creator."/picture";
							$j++;
						}
			}
#			$vtemp['id_sms_masuk'] = $result->id_sms_masuk;
#			$vtemp['id_msc'] = $result->id_msc;
#			$vtemp['tgl_msk'] = $result->tgl_msk;
#			$vtemp['waktu_msk'] = $result->waktu_msk;
#			$vtemp['stat'] = $result->stat;
#			$vtemp['id_program'] = $result->id_program;
#			$vtemp['id_lagu'] = $result->id_lagu;
#			$vtemp['id_polling'] = $result->id_polling;
#			$vtemp['id_pilihan'] = $result->id_pilihan;
#			$vtemp['req'] = $result->req;
#			$vtemp['id_udh'] = $result->id_udh;
#			$vtemp['id_feed'] = $result->id_feed;
#			$vtemp['id_comment'] = $result->id_comment;
#			$vtemp['creator'] = $result->creator;
#			$vtemp['id_creator'] = $result->id_creator;
#			$vtemp['release'] = $result->release;
#			$vtemp['foto_profil_tweet'] = $result->foto_profil_tweet;
			$temp[] = $vtemp;
		}
#//		echo json_encode($temp);
	}

	echo json_encode($temp);
	return true;
}


function filter_where( $where = '' ) {
	global $wpdb;

	$where .= " AND $wpdb->posts.post_title LIKE '%"
			.mysql_real_escape_string(  
				//trim(preg_replace( "/\([^)]+\)/"  ,"//",str_replace(array("(",")"," ","-"),"%",$GLOBALS['keyword'])))
				trim(str_replace(array(" ","-"),"%",preg_replace( "/\s*\([^)]*\)/"  ,"",$GLOBALS['keyword'])))
			)."%'";
	//var_dump($where);
	/*
	$arr = explode($GLOBALS['keyword']," ");
	$string = array();
	foreach($arr as $str){
		$string[] = "$wpdb->posts.post_title LIKE '%".$str."%'";
	}
	//preg_replace(\(\w+\));*/	
	//$where .= " AND (".implode($string," OR ").")";
  	return $where;
 }

function suararadio_data_podcast($slug="", $offset=0,$limit=20) {
	global $suararadio;
	global $current_user;
	global $wpdb,$post;

	$args = array(
		//'category_name' => $slug,
		'post_type' 	=> 'post',
		'offset'		=> $offset,
		'posts_per_page'	=> $limit,
	);
	if( $slug != "" ){
		$args['category_name'] = $slug;
	}

	$temp = array();
	$query = new WP_Query($args);
	while ( $query->have_posts() ) {
		$query->the_post();
		$vtemp = array();
		$vtemp['id'] = $post->ID;
		$vtemp['date'] = $post->post_date;
		$vtemp['post_date'] = $post->post_date;
		$vtemp['title'] =  $post->post_title;
		$vtemp['tags'] = wp_get_post_tags($post->ID);
		$vtemp['file'] = suararadio_getPodcastUrl($post->ID);
		$media = get_post_meta($post->ID, META_PODPRESSMEDIA, true);
		for($i=0; $i<$media.length; ++$i) {
			$vmedia = $media[$i];
			$vparse = parse_url($vmedia['URI']);
			if ($vparse['host']!=$_SERVER['HTTP_HOST']) {
				$vtemp['file_redirect'] = $vmedia['URI'];
        	}
		}
		//$vtemp['url'] = "http://".$_SERVER['HTTP_HOST'].get_permalink($post->ID);
		$vtemp['url'] = getPermalink($post->ID);//"http://".$_SERVER['HTTP_HOST'].get_permalink($post->ID);
		if (META_RADIO_NAME!='') {
			$vtemp['radio'] = get_post_meta($post->ID, META_RADIO_NAME, true);
		} else {
			$vtemp['radio'] = $radio;
		}
		$vtemp['thumb'] = get_post_meta($post->ID, $post->meta_key, $single = true);
		$vtemp['play_count'] = podPress_playCount($post->ID);
		$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  		$first_img = $matches[1][0];

  		if(empty($first_img)) {
    			$first_img = "http://www.suararadio.com/wp-content/themes/suararadio/images/ad_2.png";
  		}	
		$vtemp['attachment'] = $first_img;	
		$temp[] = $vtemp;
		}
	echo json_encode($temp);
	return $temp;
}

function suararadio_title_search($slug, $offset, $keyword, $limit = 20){
	global $suararadio;
	global $current_user;
	global $wpdb,$post;

	$args = array(
		's' => $keyword,
		'category-name' => $slug,
		'offset' => $offset,
		'post_type' => 'post',
		'post_status'=>array('publish'),
		'posts_per_page'=>$limit,
		'orderby' => 'relevance, post_date',
		'order' => 'desc'
	);
	
	$temp = array();
	$the_query = new WP_Query( $args );
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	if(is_plugin_active('relevanssi/relevanssi.php')){
		relevanssi_do_query($the_query);
	}
	if ($the_query->have_posts()):
		while ( $the_query->have_posts() ) : $the_query->the_post();
			$vtemp = array();
			$vtemp['id'] = $post->ID;
			$vtemp['post_date'] = $post->post_date;
			$vtemp['title'] =  $post->post_title;
			$vtemp['tags'] = wp_get_post_tags($post->ID);
			$vtemp['file'] = suararadio_getPodcastUrl($post->ID);
			$media = get_post_meta($post->ID, META_PODPRESSMEDIA, true);
			
			for($i=0; $i<$media.length; ++$i) {
				$vmedia = $media[$i];
				$vparse = parse_url($vmedia['URI']);
				if ($vparse['host']!=$_SERVER['HTTP_HOST']) {
					$vtemp['file_redirect'] = $vmedia['URI'];
				}
			}
        	/*foreach ($media as $vmedia) {
            	$vparse = parse_url($vmedia['URI']);
            	if ($vparse['host']!=$_SERVER['HTTP_HOST']) {
             		$vtemp['file_redirect'] = $vmedia['URI'];
            	}
        	}*/

		/*
		$_url = get_permalink($post->ID);
		$_parsed_url = parse_url($_url);
		if(isset($_parsed_url['host'])){
			$vtemp['url'] = $_url;
		}else{
			$vtemp['url'] = "http://".$_SERVER['HTTP_HOST'].get_permalink($post->ID) ;
		}*/



		//	$vtemp['url'] = "http://".$_SERVER['HTTP_HOST'].get_permalink($post->ID);
			$vtemp['url'] = getPermalink($post->ID);//"http://".$_SERVER['HTTP_HOST'].get_permalink($post->ID);
			if (META_RADIO_NAME!='') {
				$vtemp['radio'] = get_post_meta($post->ID, META_RADIO_NAME, true);
			} else {
				$vtemp['radio'] = $radio;
			}
			$vtemp['thumb'] = get_post_meta($post->ID, $post->meta_key, $single = true);
			$vtemp['play_count'] = podPress_playCount($post->ID);
			$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  			$first_img = $matches[1][0];

  			if(empty($first_img)) {
    				$first_img = "http://www.suararadio.com/wp-content/themes/suararadio/images/ad_2.png";
  			}	
			$vtemp['attachment'] = $first_img;	
			$temp[] = $vtemp;
		endwhile;
	endif;
	echo json_encode($temp);
	return true;
}

function suararadio_relevanssi_search($keyword){
	global $wpdb;
	global $post;

	$result = array();
	//$args = array();

        $args = array(
                'post_type' => 'post',
                'post_status'=>array('publish'),
                'posts_per_page'=>10,
                'orderby' => 'date',
                'order'=>'DESC'
        );

	$_GLOBAL['keyword'] = "a";
	$query = new WP_Query();
	$query->query_vars['posts_per_page'] = 10;
	$query->query_vars['s'] = 'a';
	relevanssi_do_query($query);
	var_dump($query);
	foreach ($query->posts as $r_post) {
		$result[] = array(

			'post_title' => $r_post->post_title,
			'post_date' => $r_post->post_date

		);
	
	}
	echo json_encode($result);
	return true;
}


function suararadio_data_content_x($catid="", $offset=0){


	

	global $suararadio;
	global $current_user;
	global $wpdb,$post;

	global $_REQUEST;


	$catObj = null;
	if(in_array($catid, array("berita","news"))){
		$catObj = get_category_by_slug('berita');
	}else if($catid=="music"){
		$catObj = get_category_by_slug('music');
	}else if($catid=="talk"){
		$catObj = get_category_by_slug('variety');
	}else if($catid=="travel"){
		$catObj = get_category_by_slug('travel');
	}else if($catid=="advertising"){
		$catObj = get_category_by_slug('advertising');
	}else{
		//by default
		$catObj = get_category_by_slug($catid);
	}
	
	//$catObj = get_category_by_slug('music'); 																
	$category_id = $catObj->cat_ID;				
	//var_dump(get_category_by_slug('musik'));												
	$param = array(
				"cat"=>$category_id,
				//"category_name" => $catid,
				"post_type"=>"post",
				"post_status"=>"publish",			 
				"order"=>"DESC",
				"posts_per_page"=>10,
				"tag__not_in" => array('12999', '164', '14492','15477'),
				//"year" => 2013,
				"offset" => $offset);
	$res = array();	
	$the_query = new WP_Query( $param );
	if($the_query->have_posts()) :
		while ($the_query->have_posts()) : $the_query->the_post();
			$tmp = array();
			$tmp['id'] = $post->ID;
			//$tmp['post_author'] = $post->post_author;
			$tmp['post_date'] = $post->post_date;
			$tmp['title'] = $post->post_title;
			//$tmp['post_title'] = get_the_title();
			//$tmp['post_category'] = $post->post_category;
			//$tmp['post_name'] = $post->post_name;
			//$tmp['post_parent'] = $post->post_parent;
			//$tmp['post_type'] = $post->post_type;
			//$tmp['comment_count'] = $post->comment_count;
			$tmp['post_content'] = $post->post_content;
		
		$tmp['tags'] = wp_get_post_tags($post->ID);
		$tmp['file'] = suararadio_getPodcastUrl($post->ID);
        	$media = get_post_meta($post->ID, META_PODPRESSMEDIA, true);
        	foreach ($media as $vmedia) {
            		$vparse = parse_url($vmedia['URI']);
            		if ($vparse['host']!=$_SERVER['HTTP_HOST']) {
                		$tmp['file_redirect'] = $vmedia['URI'];
            		}
        	}
		
		// url problem!! some get no HTTP_HOST from get_permalink
		$tmp['url'] = getPermalink($post->ID);
		//$tmp['url'] = get_permalink($post->ID);
		if (META_RADIO_NAME!='') {
			$tmp['radio'] = get_post_meta($item->postid, META_RADIO_NAME, true);
		} else {
			$tmp['radio'] = $radio;
		}
		//var_dump('meta_key_thumb',$post->meta_key);
		$tmp['thumb'] = get_post_meta($post->ID, $post->meta_key, $single = true);
		$tmp['play_count'] = podPress_playCount($post->ID);
		//   get_attachment_image($post->ID); ///get_post_meta($post->ID, '_wp_attachment_image_alt', true);
		/*	$att = array();
		$att_args = array( 'post_type' => 'attachment',
			 'post_mime_type' => 'image/jpeg',
			 'posts_per_page' => -1, 'post_status' =>'any', 'post_parent' => $post->ID ); 
		$attachments = get_posts($att_args);
		if ($attachments) {
			foreach ( $attachments as $attachment ) {
				//	echo apply_filters( 'the_title' , $attachment->post_title );
				$att[] = $attachment->guid;
				//var_dump($attachment);
			}	
		}
*/
  		$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  		$first_img = $matches[1][0];

  		if(empty($first_img)) {
    			$first_img = "http://www.suararadio.com/wp-content/themes/suararadio/images/ad_2.png";
  		}	


		$tmp['attachment'] = $first_img;
			$res[] = $tmp;
		endwhile;
	
	else:
		$res['data'] = "post masih kosong";
	endif;

	//$tmp['id'] = 1;
	//$tmp['nama'] = "fikri";
	echo json_encode($res);



}

function suararadio_radio_latest_podcasts($slug="", $offset=0, $limit=5){

	$param="post_type=post&posts_per_page=$limit&orderby=date&order=DESC";

	

	
	

}

?>
