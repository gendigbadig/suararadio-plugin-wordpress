<?php
/*
  Member Music Info
*/

$dsong = $suararadio->getLagu($songId);
$dalbum = $suararadio->getAlbumsByArtist($dsong['artistId']);
$dalbumsong = $suararadio->getSongsByAlbum($dsong['albumId']);
$ddalbum = $suararadio->getAlbumMelon($dsong['albumId']);
#var_dump($suararadio->formatDate($dsong['issueDate']));
?>
	<!-- main content -->
	<div class="main-content-box">
		<div class="container container-wrap"><!-- main container -->
			<div class="row"><!--row -->			
	    	  <div class="span12" style="margin-bottom: 50px;">
              
                <div class="utamaIsi">
                    <ul>
                        <li class="logoKlubRadio"></li>
                    </ul>
                </div>
                
                        <ul class="nav nav-kr">
							<li <?php echo ($foundpath=='/member/') ? 'class="active"':''; ?>><a href="/member/">featured</a></li>                          
                            <li <?php echo ($foundpath=='/member/content/') ? 'class="active"':''; ?> ><a href="/member/content/"><?= __('content','suararadio')?></a></li>
	    		            <li <?php echo ($foundpath=='/member/music/') ? 'class="active"':''; ?> ><a href="/member/music/"><?= __('music','suararadio')?></a></li>
                            <li <?php echo ($foundpath=='/member/playlists/') ? 'class="active"':''; ?> ><a href="/member/playlist/"><?= __('playlist','suararadio')?></a></li>				
						</ul>
                    
                    <div>					
						<div class="box-profil">
							<div class="row">
								<img class="span2 profil-small center" src="http://www.melon.co.id/image.do?fileuid=<?php echo $ddalbum['albumLImgPath'] ?>">
								<div class="span6">
                                
										<p>Artis : <?php echo $dsong['artistName'] ?>
                                        <p>Judul : <?php echo $dsong['songName'] ?>
                                        <p>Album : <?php echo $dsong['albumName'] ?> 
                                        <p>Durasi : <?php echo $suararadio->getDurasi($dsong['playtime'])?>
                                        <p>Label : <?php echo $ddalbum['sellCompany']." ".$ddalbum['agency']; ?>
                                        <p>Tanggal Release : <?php echo $suararadio->formatDate($dsong['issueDate']); ?>  
								</div>
							</div>
						</div>
						<hr/>
					</div>
              
              	
                <div class="tab-content">
                    <div id="tab_featured" class="tab-pane active">			
		          <div class="row">	
                        					
<div class="tab-content">
                        
    <div class="tab-pane active" id="music1"><!-- berita3-->

		<div class="span10">	
            <h4 class="middle-title">Daftar Album</h4>
        </div>
       
        <div id="chart-mb1" class="carousel konten-mb">
        <div class="carousel-inner">        			            	    			         	
        <ul class="list-content-choose-box">
			<?php foreach ($dalbum['dataList'] as $valbum) { ?>	
			<li class="span3 box-content-list view view-first">
                <a href="javascript:void(0);" class="albumLink" albumId="<?php echo $valbum['albumId']; ?>">
                    <img class="cover-artist" src="http://www.melon.co.id/image.do?fileuid=<?php echo $valbum['albumMImgPath'] ?>">
                </a>
	  			<div class="info-artist">
                       		<div class="title_content">
                                <!--<a class="linkMember" href="/member/music/<?=$vlagu['songId']?>">-->
                                    <?php echo $valbum['albumName'] ?>
                                <!--</a>-->
                            </div>
							<p class="nm_radio">release : <?php echo $suararadio->formatDate($valbum['issueDate']) ?></p>
					
					</div>
			</li>
				<?php } ?>
		</ul>
        
        </div>
        </div>
		<div style="clear:both;"></div>
		
	</div>
    
    <div class="span10">	
        <h4 class="middle-title">Daftar Lagu di Album</h4>
    </div>
   	<div style="clear:both;"></div>
    
    <section id="listLaguView">    
    <div class="tab-pane active listData" id="chart-dl">
									<ul id="listItem2" class="playlist-ml ui-sortable">
	                                   <li class="head-playlist ">
                                            <p class="span7 first-tk">artis - judul</p>
                                            <p class="span1 ">info</p>
                                            <p class="span2">action</p>
                                        </li>
                                       <?php
                                    		$params = array();
                                    		$params['style'] = 'width: 40px; height: 34px;';
                                    		foreach ($dalbumsong['dataList'] as $vsong) { ?>
                                          
                                          <li id="listlagu">
										    
											<p class="span7">
                                                	<?php $nmfile = suararadio_showMiniMelonPlay($vsong['songId']);?>
                                                    <a class="linkMember" href="/member/music/<?php echo $vsong['songId']; ?>"><?php echo $vsong['artistName']." - ".$vsong['songName']?></a>
											</p>
											<p class="span1 "><?php echo $suararadio->getDurasi($vsong['playtime']) ?></p>
											
                                            <p class="span2 ">
												<?php if (!empty($nmfile)) suararadio_show_addlink('melon',$vsong['songId'],$params); ?>
											     <?php 
                                             $dtFb = explode ("&",$_REQUEST["fbs_144389048967182"]);
                                             $dtFb1 = explode ("=", $dtFb[5]);
                                             $uidFb	=  ereg_replace("\"","",$dtFb1[1]);	
                                            
                                             $url = "http://".$_SERVER['HTTP_HOST']."/member/music/".$vsong['songId'];
                                             $dsong = $suararadio->getLagu($vsong['songId']);
                                            											
                                             $params = array('songName'=>$dsong['songName'], 'url'=>$url, 'artistName'=> $dsong['artistName'], 'image'=>$ddalbum['albumLImgPath'], 'albumname'=>$dsong['albumName'], 'label'=>$ddalbum['sellCompany']." ".$ddalbum['agency'],'release'=>$suararadio->formatDate($dsong['issueDate']));																		
                                             
                                            ?> 
                                            <?= share_melon($vsong['songId'], $params,"",$uidFb); ?>         
                
                                            </p>
											<p class="clear"></p>
										</li>
                                    	<?php } ?>   
									</ul>
								
								</div>
                                </section>
                                
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
	       </div><!--row -->
		</div><!-- main container -->
	</div>
    
<script type="text/javascript">
$(document).ready(function() {
	$('.albumLink').bind('click',function(event){
		var aId = $(this).attr("albumId");
		$.get('/suararadio_api/melon/listsongs/'+aId,function(rdata){
			$("#listItem2").empty();
			$("#listItem2").append(rdata);
			suararadioPlayer.makeSoundPlayer();
		});
	});
});
</script>    