<?php
if (!defined('META_PODPRESSMEDIA')) define('META_PODPRESSMEDIA','podPressMedia');
if (!defined('META_RADIO_NAME')) define('META_RADIO_NAME','');

$keyword = '';

function suararadio_channel_process($path){
    
    
	// determine slugs to be included in wp_query
	$channel = $path[3];
	if(!$channel) {
		echo json_encode(
			array('entertainment','religion','music','news')
		);
		return;
	}
	
	
	// if subchannel is specified, go straight to sluq
	$limit = ($_REQUEST['limit'])? $_REQUEST['limit']:20;
	$offset = 0;
	$paged = ($_REQUEST['page'])? $_REQUEST['page']:1;
	$slugs = array();
	if(isset($_REQUEST['subchannel'])){
		$slugs[] = $_REQUEST['subchannel'];			
	}else{
		$slugs = get_channel_slugs($channel);
	}
	global $suararadio;
	global $current_user;
	global $wpdb,$post;


	//$slugs = array('music','berita');
	$args = array(
		//'s' => $keyword,
		//	'category-name' => $slug,
		'tag_slug__in' => $slugs,
		'offset' => $offset,
		'post_type' => 'post',
		'post_status'=>array('publish'),
		'posts_per_page'=>$limit,
		'paged' => $paged,
		'orderby' => 'post_date',
		'order' => 'desc'
	);
	
	$temp = array();
	$the_query = new WP_Query( $args );
	/*include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	if(is_plugin_active('relevanssi/relevanssi.php')){
		relevanssi_do_query($the_query);
	}*/
	$found_post_count = $the_query->found_posts;
	if ($the_query->have_posts()):
		while ( $the_query->have_posts() ) : $the_query->the_post();
			$vtemp = array();
			$vtemp['id'] = $post->ID;
			$vtemp['post_date'] = $post->post_date;
			$vtemp['title'] =  $post->post_title;
			//$vtemp['tags'] = wp_get_post_tags($post->ID);
			$vtemp['file'] = suararadio_getPodcastUrl($post->ID);
			$media = get_post_meta($post->ID, META_PODPRESSMEDIA, true);
			
			for($i=0; $i<$media.length; ++$i) {
				$vmedia = $media[$i];
				$vparse = parse_url($vmedia['URI']);
				if ($vparse['host']!=$_SERVER['HTTP_HOST']) {
					$vtemp['file_redirect'] = $vmedia['URI'];
				}
			}
        	/*foreach ($media as $vmedia) {
            	$vparse = parse_url($vmedia['URI']);
            	if ($vparse['host']!=$_SERVER['HTTP_HOST']) {
             		$vtemp['file_redirect'] = $vmedia['URI'];
            	}
        	}*/

		/*
		$_url = get_permalink($post->ID);
		$_parsed_url = parse_url($_url);
		if(isset($_parsed_url['host'])){
			$vtemp['url'] = $_url;
		}else{
			$vtemp['url'] = "http://".$_SERVER['HTTP_HOST'].get_permalink($post->ID) ;
		}*/



		//	$vtemp['url'] = "http://".$_SERVER['HTTP_HOST'].get_permalink($post->ID);
			$vtemp['url'] = getPermalink($post->ID);//"http://".$_SERVER['HTTP_HOST'].get_permalink($post->ID);
			if (META_RADIO_NAME!='') {
				$vtemp['radio'] = get_post_meta($post->ID, META_RADIO_NAME, true);
			} else {
				$vtemp['radio'] = $radio;
			}
			//$vtemp['thumb'] = get_post_meta($post->ID, $post->meta_key, $single = true);
			//$vtemp['play_count'] = podPress_playCount($post->ID);
			$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  			$first_img = $matches[1][0];

  			if(empty($first_img)) {
    				$first_img = "http://www.suararadio.com/wp-content/themes/suararadio/images/ad_2.png";
  			}	
			$vtemp['attachment'] = $first_img;	
			$temp[] = $vtemp;
		endwhile;
	endif;
	echo json_encode( array(
		"total_count" => $found_post_count,
		"posts_per_page" => $limit,
		"current_page" => $paged, 
		"data" => $temp
	));
	return true;
 
	return;
	 

}

function get_channel_slugs($channel){

	switch($channel){
	case "entertainment":
		return array("music", "musik");
		break;
	case "news":
		return array("berita");
		break;
	}


}


?>
