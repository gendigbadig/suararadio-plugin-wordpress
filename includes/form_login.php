<?php 
  global $current_user;
  global $wp_roles;
  global $namaradio;
  
  $img_profile_url = suararadio_get_profile_photo();
  $type = $current_user->member_type?$current_user->member_type:"Buddy";
  $type = $current_user->member_type?$current_user->member_type:"Buddy";
  $imgA = SUARARADIO_PLUGIN_URL."/images/transparent-bg.png";
  if ($current_user->expired) {
  	preg_match('/(\d{2})(\d{2})/',$current_user->expired,$exps);
  	$strexp = $exps[2]."/".$exps[1];
  } else {
  	$strexp = "00/00";
  }
  
  $strextexp = "";
	if ($current_user->ext_expired) {
		preg_match('/(\d{4})(\d{2})(\d{2})/',$current_user->ext_expired,$exps);
		$strextexp = $exps[3]."/".$exps[2]."/".$exps[1];
	}
	
  if (is_user_logged_in()) { ?>
    <div>
		  <div class="toggle4 live-content"><!-- main container -->
			  <div class="container">
				  <div class="row">
					  <div class="span12">
						  <ul class="nav nav-user">
							  <li class="user-menu-avatar span2" style="margin-left: 0px;"><img src="<?php echo $img_profile_url; ?>" style="width:30px; height:30px;">
								  <p><?php echo $current_user->display_name; ?></p>
								</li>
								<li class="user-exp" ><p>Expired: <?php echo $strextexp; ?></p></li>
								<li class="user-exp"><p>Membership : <?php echo $type; ?> </p></li>
								<li class="pull-right"><a class="label apply-nolazy" href="<?php echo wp_logout_url("/"); ?>" title="logout">Log Out</a></li>
								<!--<li class="pull-right"><a  href="#" class="label">Vcard</a></li>-->	
                <!-- li class="pull-right"><a  href="/member/account/" class="label">Account</a></li-->		
                <li class="pull-right"><a  data-toggle="collapse" href="#accPanel" class="label">Account</a></li>		
								<li> 
								  <div class="collapse" id="accPanel">								  
								   <?php
								   	 include SUARARADIO_PLUGIN_DIR.'/includes/member_account.php';
								   ?>						  
									</div> 
								</li>
							</ul>
						</div>						
					</div>
				</div>
			</div>	
      <!--<hr/>-->
		</div>
  <?php } else { // not logged in ?>
    <div class="toggle_login" style=" display:none; width:100%;height:100%;"><!-- main container -->
		  <div class="container">
			  <div class="row ">
				  <div class="span12">
					  <ul class="nav nav-user">
              <article class="loginFrm" style="width: 100%; text-align: center; padding-bottom: 10px;">
                <?php wsl_render_login_form(); ?>
                <section style="width: 50%; margin: auto;">
                </section>
              </article>
            </ul>
          </div>
				</div>
			</div>
		</div>	
  <?php } ?> 