<?php
/*
License:
 ==============================================================================

    Copyright 2011  M Zhuhriansyah R  (email : izhur2001@yahoo.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-107  USA
*/


function suararadio_paypal_cancel() {
	var_dump($_REQUEST,$_COOKIE);
}

function suararadio_paypal_success() {
	var_dump($_REQUEST,$_COOKIE);
}
/**
 * memproses IPN yang dikirim dari paypal
 **/
function suararadio_paypal_notify() {
	global $wpdb,$suararadio,$user_ID;

	$req = $_POST;
	error_log("paypal ".var_export($req,true)."\n ", 3, "paypal.log");

	// check validitas
	$res = suararadio_paypal_verify($req);

	if ($res==0) {
		echo "INVALID";
		$data = array(
			"trx_type"=>"ipn","trx_method"=>"paypal","trx_time"=>date("Y-m-d H:i:s"),"trx_data"=>json_encode($req),
			"trx_status"=>"invalid","trx_id"=>$req['txn_id'],"trx_msg"=>$req['txn_type']
		);
		$wpdb->insert("wp_suararadio_trx",$data);
		return 0;
	} else if ($res!=1) {
		// unable to verified
		$data = array(
			"trx_type"=>"ipn","trx_method"=>"paypal","trx_time"=>date("Y-m-d H:i:s"),"trx_data"=>json_encode($req),
			"trx_status"=>"unverified","trx_id"=>$req['txn_id'],"trx_msg"=>$req['txn_type']
		);
		$wpdb->insert("wp_suararadio_trx",$data);
		return 0;
	}
	
	// verified
	// check payment status
	switch ($req['payment_status']) {
		case "Completed":
			break;
		case "Pending":
			$data = array(
				"trx_type"=>"ipn","trx_method"=>"paypal","trx_time"=>date("Y-m-d H:i:s"),"trx_data"=>json_encode($req),
				"trx_status"=>"pending","trx_id"=>$req['txn_id'],"trx_msg"=>$req['txn_type']
			);
			$wpdb->insert("wp_suararadio_trx",$data);
			return 0;
			break;
		default:
			$data = array(
				"trx_type"=>"ipn","trx_method"=>"paypal","trx_time"=>date("Y-m-d H:i:s"),"trx_data"=>json_encode($req),
				"trx_status"=>"unknown","trx_id"=>$req['txn_id'],"trx_msg"=>$req['txn_type']
			);
			$wpdb->insert("wp_suararadio_trx",$data);
			return 0;
			break;
	}
	
	// check trx id, is exist ?
	$cnt = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM wp_suararadio_trx where trx_type=%s and trx_method=%s and trx_id=%s","add_poin","paypal",$req['txn_id']));
	if ($cnt>0) return 0;
	// insert add poin
	$vardata = unserialize($req['custom']);

	if ($vardata['user_ID']) {
		$user_ID = $vardata['user_ID'];
		$balance = $suararadio->getBalance();
		$suararadio->setBalance($balance+$vardata['poin']);
		
		// update
		$data = array(
			"trx_type"=>"add_poin","trx_method"=>"paypal","trx_time"=>date("Y-m-d H:i:s"),"trx_data"=>json_encode($req),
			"trx_status"=>"ok","trx_id"=>$req['txn_id'],"trx_msg"=>$req['txn_type']
		);
		$wpdb->insert("wp_suararadio_trx",$data);
	}
} // endfunc suararadio_paypal_notify

function suararadio_paypal_get_env($mode='ver') {
	if ($mode=='api') {
		if (SUARARADIO_PAYMENT=='live') {
			return "api-3t.paypal.com/nvp";
		}
		return "api-3t.sandbox.paypal.com/nvp";
	} else {
		if (SUARARADIO_PAYMENT=='live') {
			return "www.paypal.com";
		}
		return "www.sandbox.paypal.com";
	}
}

function suararadio_paypal_verify($data) {
	$postdata = "";
  	$response = array();

  	if (is_array($data)) foreach($data as $var=>$val)
  	{
    		$postdata .= $var . "=" . urlencode($val) . "&";
  	}
  	$postdata.="cmd=_notify-validate";
	$host = suararadio_paypal_get_env();

	$fp=@fsockopen("ssl://$host" ,"443",$errnum,$errstr,30);
	if(!$fp)
	{
		return "$errnum: $errstr";
  	} else
  	{
		fputs($fp, "POST /cgi-bin/webscr HTTP/1.1\r\n");
		fputs($fp, "Host: $host\r\n");
		fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
		fputs($fp, "Content-length: ".strlen($postdata)."\r\n");
		fputs($fp, "Connection: close\r\n\r\n");
		fputs($fp, $postdata . "\r\n\r\n");
		while(!feof($fp)) { $response[]=@fgets($fp, 1024); }
		fclose($fp);
	}
	$response = implode("\n", $response);
var_dump($response);
	if(eregi("VERIFIED",$response))
	{	echo "OK";
		return true;
	}else
	{
		return false;
	}
} //endfunc suararadio_paypal_verify
