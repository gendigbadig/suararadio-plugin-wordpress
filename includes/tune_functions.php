<?php
/* 

*/

if (!defined('META_RADIO_NAME')) define('META_RADIO_NAME','');
if (!defined('META_THUMB_MOST_PLAYED')) define('META_THUMB_MOST_PLAYED','mostplayed');
if (!defined('META_THUMB_KONTEN_PILIHAN')) define('META_THUMB_KONTEN_PILIHAN','advimage');
if (!defined('META_PODPRESSMEDIA')) define('META_PODPRESSMEDIA','podPressMedia');


function suararadio_tune_process($paths) {
	$params = array();
	switch ($paths[3]) {
		case "staytuned":          
            $song_id = $paths[4];     
			return staytuned($song_id);
			break;
        case "staytuned_kontenpil":          
            $song_id = $paths[4];     
			return staytuned_kontenpil($song_id);
			break;
        case "tweets":          
            $song_id = $paths[4];     
			return tweets($song_id);
			break;
		default:
        case "staytuned_melon":          
            $song_id = $paths[4];     
			return staytuned_melon($song_id);
			break;
		default:        

			status_header('404');
	}
} //endfunc suararadio_melon_process

function staytuned($song_id){
	global $suararadio;
	global $current_user;
	global $wpdb;
    global $suararadio_connect;
    
    $wsl_concat = (defined('WSL_CONCAT')) ? WSL_CONCAT:'';
    
    if (!get_option('wsl_settings_Facebook'.$wsl_concat.'_enabled')) return false;
    
   // echo '<script src="http://connect.facebook.net/en_US/all.js"></script>'."\n";
    
    echo $appId = get_option( "wsl_settings_Facebook".$wsl_concat."_app_id" );

    
    $querystr = "
		SELECT wposts.* 
		FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta
		WHERE wposts.ID = wpostmeta.post_id 
		AND wposts.ID = '".$song_id."'
		ORDER BY wposts.post_date DESC
 	";
 	$pageposts = $wpdb->get_results($querystr, OBJECT);
    $konten = $pageposts[0]->post_content;
	$isi = preg_replace('/((\\\\n)+)/'," ",preg_replace("/(\r\n|\n)/",'\\n',stripslashes($pageposts[0]->post_content)));  
    $jdl = preg_replace('/\"/'," ",preg_replace("/(\r\n|\n)/",'\\n',stripslashes($pageposts[0]->post_title)));  

    $isi = remove_HTML($isi);	
	$quotes = array('/"/',"/'/");
	//$replacements = array('%22','%27');
	$replacements = array('%22','%27');
    $link = get_permalink($pageposts[0]->ID);
    $isi = preg_replace($quotes,$replacements,$isi);
    $url = "http://".$_SERVER['HTTP_HOST'].$link;
        
                    
    
    $loc_audio = podPress_getAvailablePodcast($pageposts[0]->ID);
    $loc = $loc_audio['free'][0];
	
	$imageLink = getImagePostStayTune($postId, $pageposts,$konten);
    $src = "http://api.suararadio.com/assets/player.swf?file=$loc&autostart=true&image=https://api.suararadio.com/stayTuned/image&stretching=exactfit&logo.hide=false&logo.file=https://api.suararadio.com/stayTuned/logo";
    $src2 = "https://api.suararadio.com/assets/player.swf?file=$loc&autostart=true&image=$imageLink";
    echo '
        <html>
        <head>
          <title>'.$jdl.'</title>
          <meta property="fb:app_id" content="'.$appId.'" />
          <meta property="og:url" content="'.$url.'" />
          <meta property="og:type" content="website" />
          <meta property="og:title" content="'.$jdl.'" />
          <meta property="og:description" content="'.$isi.'" />
          <meta property="og:image" content="'.$imageLink.'" />
          <meta property="og:video" content="'.$src.'"/>
          <meta property="og:video:secure_url" content="'.$src2.'"/>
          <meta property="og:video:height" content="250"/>
          <meta property="og:video:width" content="403"/>
          <meta property="og:video:type" content="application/x-shockwave-flash"/>
        </head>
        <body> 
        <p>You should have been redirected to <a href="'.$url.'" class="redirect_url">'.$jdl.'</a>.</p>
  
          <script type="application/javascript">
            window.location.href = "http://'.$url.'?utm_campaign=opengraph&utm_content=song&utm_medium=link&utm_source=facebook";
          </script>
        </body>
        </html>';
			
}

function staytuned_kontenpil($song_id){
	global $suararadio;
	global $current_user;
	global $wpdb;
    global $wpkontenpil;

    
    $querystr = "
		SELECT wposts.* 
		FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta
		WHERE wposts.ID = wpostmeta.post_id 
		AND wposts.ID = '".$song_id."'
		ORDER BY wposts.post_date DESC
 	";
 	$pageposts = $wpkontenpil->get_results($querystr, OBJECT);
    $konten = $pageposts[0]->post_content;
	$isi = preg_replace('/((\\\\n)+)/'," ",preg_replace("/(\r\n|\n)/",'\\n',stripslashes($pageposts[0]->post_content)));  
    $jdl = preg_replace('/\"/'," ",preg_replace("/(\r\n|\n)/",'\\n',stripslashes($pageposts[0]->post_title)));  

    $wsl_concat = (defined('WSL_CONCAT')) ? WSL_CONCAT:'';
    
    if (!get_option('wsl_settings_Facebook'.$wsl_concat.'_enabled')) return false;
    
   // echo '<script src="http://connect.facebook.net/en_US/all.js"></script>'."\n";
     $appId = get_option( "wsl_settings_Facebook".$wsl_concat."_app_id" ); 
    
    
 	/* $geturl = $_SERVER['HTTP_HOST'];
    if($geturl=='www.diradio.net'){
        $appId = '281043441969930';    
    }else{
           
    } */
    
    
    $isi = remove_HTML($isi);	
	$quotes = array('/"/',"/'/");
	//$replacements = array('%22','%27');
	$replacements = array('%22','%27');
    $link = get_permalink($pageposts[0]->ID);
    //$isi = preg_replace($quotes,$replacements,$isi);
    
    $isi_iklan = template_iklan();
    //$url = $_SERVER['HTTP_HOST'].$link;
    $url = "http://".$_SERVER['HTTP_HOST']."/single_kp/".$pageposts[0]->ID;                
    $url_direct = "http://".$_SERVER['HTTP_HOST']."/single_kp/".$pageposts[0]->ID;                
    
    $loc_audio = podPress_getAvailablePodcast_kp($pageposts[0]->ID);
    $loc = $loc_audio['free'][0];
	
	$imageLink = getImagePostStayTune($postId, $pageposts,$konten);
    //$src = "https://api.suararadio.com/assets/player.swf?file=$loc&autostart=true&image=https://api.suararadio.com/stayTuned/image&stretching=exactfit&logo.hide=false&logo.file=https://api.suararadio.com/stayTuned/logo";
    $src = "http://api.suararadio.com/assets/player.swf?file=$loc&autostart=true&image=$imageLink&stretching=exactfit&logo.hide=false";
    $src_secure = "https://api.suararadio.com/assets/player.swf?file=$loc&autostart=true&image=$imageLink&stretching=exactfit&logo.hide=false";
    
    
    echo '
        <!doctype html>
        <html>
        <head>
          <title>'.$jdl.'</title>
          <meta property="fb:app_id" content="'.$appId.'" />
          <meta property="og:url" content="'.$url.'" />
          <meta property="og:type" content="website" />
          <meta property="og:title" content="'.$jdl.'" />
          <meta property="og:description" content="'.$isi_iklan.'" />
          <meta property="og:image" content="'.$imageLink.'" />
          <meta property="og:video" content="'.$src.'"/>
          <meta property="og:video:secure_url" content="'.$src_secure.'"/>
          <meta property="og:video:height" content="250"/>
          <meta property="og:video:width" content="500"/>
          <meta property="og:video:type" content="application/x-shockwave-flash"/>
        </head>
        <body> 
        <p>You should have been redirected to <a href="'.$url.'" class="redirect_url">'.$jdl.'</a>.</p>
  
          <script type="application/javascript">
            window.location.href = "'.$url_direct.'?utm_campaign=opengraph&utm_content=song&utm_medium=link&utm_source=facebook";
          </script>
        </body>
        </html>';
			
}

function tweets(){
	   echo '
        <html>
        <head>
          <title>tes</title>
           <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@nytimes">
    <meta name="twitter:creator" content="@SarahMaslinNir">
    <meta name="twitter:url" content="http://www.nytimes.com/2012/02/19/arts/music/amid-police-presence-fans-congregate-for-whitney-houstons-funeral-in-newark.html">
    <meta name="twitter:title" content="Parade of Fans for Houstonís Funeral">
    <meta name="twitter:description" content="NEWARK - The guest list and parade of limousines with celebrities emerging from them seemed more suited to a red carpet event in Hollywood or New York than than a gritty stretch of Sussex Avenue near the former site of the James M. Baxter Terrace public housing project here.">
    <meta name="twitter:image" content="http://graphics8.nytimes.com/images/2012/02/19/us/19whitney-span/19whitney-span-articleLarge.jpg">
        </head>
        <body> 
        
        </body>
        </html>';
			
}

function staytuned_melon($song_id){
	global $suararadio;
	global $current_user;
	global $wpdb;
    global $wpkontenpil;

    
   
    $wsl_concat = (defined('WSL_CONCAT')) ? WSL_CONCAT:''; 
    if (!get_option('wsl_settings_Facebook'.$wsl_concat.'_enabled')) return false;
    $appId = get_option( "wsl_settings_Facebook".$wsl_concat."_app_id" ); 
    
    $dsong = $suararadio->getLagu($song_id);
    $ddalbum = $suararadio->getAlbumMelon($dsong['albumId']);
    
    $jdl = ' [PREVIEW] '.$dsong['songName'].'-'.$dsong['artistName'];
    $isi = ' Judul : '.$dsong['songName'].' | Artis : '.$dsong['artistName'].' | Album : '.$dsong['albumName'].' | Label : '.$ddalbum['sellCompany']." ".$ddalbum['agency'].' | Tanggal release : '.$suararadio->formatDate($dsong['issueDate']);   	
    $imageLink = "http://www.melon.co.id/image.do?fileuid=".$ddalbum['albumLImgPath'].""; 
        
    $url = $suararadio->convertMelonToPodcastWebPath($song_id);
    $url_direct = "http://".$_SERVER["HTTP_HOST"]."/member/music/".$song_id;        
    
    $src = "http://api.suararadio.com/assets/player.swf?songId=$song_id&contentType=M&channelCd=CH0023&streamer=rtmp://stm.melon.co.id/prelisten&file=melon&autostart=true&icons=false&image=$imageLink&logo.hide=false&logo.file=https://api.suararadio.com/stayTuned/logo";
    $src_secure = "https://api.suararadio.com/assets/player.swf?songId=$song_id&contentType=M&channelCd=CH0023&streamer=rtmp://stm.melon.co.id/prelisten&file=melon&autostart=true&icons=false&image=$imageLink&logo.hide=false&logo.file=https://api.suararadio.com/stayTuned/logo";
             
    
    echo '
        <!doctype html>
        <html>
        <head>
          <title>'.$jdl.'</title>
          <meta property="fb:app_id" content="'.$appId.'" />
          <meta property="og:url" content="'.$url.'" />
          <meta property="og:type" content="website" />
          <meta property="og:title" content="'.$jdl.'" />
          <meta property="og:description" content="'.$isi.'" />
          <meta property="og:image" content="'.$imageLink.'" />
          <meta property="og:video" content="'.$src.'"/>
          <meta property="og:video:secure_url" content="'.$src_secure.'"/>
          <meta property="og:video:height" content="250"/>
          <meta property="og:video:width" content="500"/>
          <meta property="og:video:type" content="application/x-shockwave-flash"/>
        </head>
        <body> 
        <p>You should have been redirected to <a href="'.$url_direct.'" class="redirect_url">'.$jdl.'</a>.</p>
  
          <script type="application/javascript">
            window.location.href = "'.$url_direct.'?utm_campaign=opengraph&utm_content=song&utm_medium=link&utm_source=facebook";
          </script>
        </body>
        </html>';
			
}

