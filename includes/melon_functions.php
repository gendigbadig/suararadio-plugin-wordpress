<?php
/*
License:
 ==============================================================================

    Copyright 2012  M Zhuhriansyah R  (email : izhur2001@yahoo.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-107  USA
*/

function suararadio_melon_process($paths) {
	switch ($paths[3]) {
		case "certification":
			$songId = $paths[4];
			return suararadio_melon_get_certification($songId);
			break;
		case "album":
			$songId = $paths[4];
			return suararadio_melon_get_album($songId);
			break;
		case "listsongs":
			$albumId = $paths[4];
			return suararadio_melon_get_list_album($albumId);
			break;
		case "category":
			$mode = $paths[4];
			return suararadio_melon_get_category($mode);
			break;
		default:
			status_header('404');
	}
} //endfunc suararadio_melon_process

function suararadio_melon_get_certification($songId) {
	global $suararadio;
	global $current_user;
	
	$tmp = array();
	$tmp['songId'] = $songId;
	
	$cert = $suararadio->getCertification($songId);
	if ($cert['code']=='1' && $cert['supportStream']=='Y') {
		$song = $suararadio->getLaguMelon($songId);
		$tmp['cert'] = $cert['message'];
		$tmp['payed'] = 'Y';
		$tmp['playtime'] = $song['playtime'];
	} else {
		$tmp['payed'] = 'N';
		$tmp['playtime'] = 60;
	}
	echo json_encode($tmp);
	return true;
} //endfunc suararadio_melon_get_certification

function suararadio_melon_get_album($songId) {
	global $suararadio;
	global $current_user;
	
	$song = $suararadio->getLagu($songId);
	$album = $suararadio->getAlbumMelon($song['albumId']);
	$album['songName'] = $song['songName'];
	$album['artistName'] = $song['artistName'];
	echo json_encode($album);
	return true;
} //endfunc suararadio_melon_get_album

function suararadio_melon_get_list_album($albumId) {
	global $suararadio;
	global $current_user;
	
	$list = $suararadio->getSongsByAlbum($albumId);
	if (strpos($_SERVER['HTTP_ACCEPT'],'application/json')!==false) {
		echo json_encode($list);
	} else {
		$params = array();
		$params['style'] = 'width: 40px; height: 34px;';
		foreach ($list['dataList'] as $vsong) { ?>
		<li>
			<span class="player">
			<?php $nmfile = suararadio_showMiniMelonPlay($vsong['songId']); ?>
			</span>
			<span class="title" style="width:500px"><a class="linkMember" href="/member/music/<?php echo $vsong['songId']; ?>"><?php echo $vsong['artistName']." - ".$vsong['songName']?></a></span>
			<span class="info"><?php echo $suararadio->getDurasi($vsong['playtime']) ?></span>
			<span class="action">
				<?php if (!empty($nmfile)) suararadio_show_addlink('melon',$vsong['songId'],$params); ?>
			     <?php 
                                             $dtFb = explode ("&",$_REQUEST["fbs_144389048967182"]);
                                             $dtFb1 = explode ("=", $dtFb[5]);
                                             $uidFb	=  ereg_replace("\"","",$dtFb1[1]);	
                                            
                                             $url = "http://".$_SERVER['HTTP_HOST']."/member/music/".$vsong['songId'];
                                             $dsong = $suararadio->getLagu($vsong['songId']);
                                            											
                                             $params = array('songName'=>$dsong['songName'], 'url'=>$url, 'artistName'=> $dsong['artistName'], 'image'=>$ddalbum['albumLImgPath'], 'albumname'=>$dsong['albumName'], 'label'=>$ddalbum['sellCompany']." ".$ddalbum['agency'],'release'=>$suararadio->formatDate($dsong['issueDate']));																		
                                             
                                            ?> 
                                            <?= share_melon($vsong['songId'], $params,"",$uidFb); ?> 
            </span>
		</li><?php 
		}
	}
	return true;
} //endfunc suararadio_melon_get_list_album

function suararadio_melon_get_category($mode) {
	global $suararadio;

	$mode = ($mode=='')? "all":$mode; 
	$result = $suararadio->mapi->getCategories(array('mode'=>$mode));
   	$cats = array();
   	foreach($result as $obres) {
   		$cats[$obres['genreId']] = array($obres['orderNum'],					$obres['genreName']);
   	}
   		
   	ksort($cats,SORT_STRING);
   	return $cats;
} //endfunc suararadio_melon_get_category
