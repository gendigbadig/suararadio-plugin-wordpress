<?php
/*
  Member Account
*/
global $current_user;

$type = $current_user->member_type?$current_user->member_type:"Legend";
$imgA = SUARARADIO_PLUGIN_URL."/images/transparent-bg.png";
switch($type) {
	case "Listener":
		$imgA = SUARARADIO_PLUGIN_URL."/images/star1.png";
		break;
	case "Fans":
		$imgA = SUARARADIO_PLUGIN_URL."/images/star2.png";
		break;
	case "Lover":
		$imgA = SUARARADIO_PLUGIN_URL."/images/star3.png";
		break;
	case "Mania":
		$imgA = SUARARADIO_PLUGIN_URL."/images/star4.png";
		break;
	case "Legend":
		$imgA = SUARARADIO_PLUGIN_URL."/images/star5.png";
		break;
}

$img_profile_url = suararadio_get_profile_photo(null,"large");
?>
<div class="main-content-box">
		<div class="container container-wrap"><!-- main container -->
			<div class="row"><!--row -->
<div id='klubradio'>
	<div id="klubradioIsi">    
		<section class="utamaIsi">		
			<figure class="logoAccount lg<?php echo $type; ?>"></figure>
			<legend class="krStar"><img src="<?php echo $imgA;?>"/></legend>
			<header class="krMemberType krmt<?php echo $type; ?>">The Radio <?php echo $type; ?></header>	
		</section>
		<div class="krAccount krbg<?php echo $type; ?>">
		<figure id="accountFoto"><img src="<?php echo $img_profile_url; ?>"></figure>
		<section id="accountDetail">
		  <fieldset>
			<div class="row">
				<label class="txt<?php echo $type; ?>">Nama</label>
				<span class="txt<?php echo $type; ?>"><?php echo $current_user->display_name; ?></span>
			</div>
			<div class="row">
				<label class="txt<?php echo $type; ?>">Email</label>
				<span class="txt<?php echo $type; ?>"><?php echo $current_user->email; ?>&nbsp;</span>
			</div>
			<div class="row">
				<label class="txt<?php echo $type; ?>">Membership</label>
				<span class="txt<?php echo $type; ?>"><?php echo $type; ?></span>
				<input type="button" class="button-primary" name="connect" value="upgrade">
			</div>
			<div class="row">
				<label>&nbsp;</label>
				<span class="txt<?php echo $type; ?>">expired: <?php echo ($current_user->member_expired ? $current_user->member_expired:"0000-00-00"); ?></span>
			</div>
			<div class="row">
				<label class="txt<?php echo $type; ?>">KlubID</label>
				<span class="txt<?php echo $type; ?>"><?php echo $current_user->suararadio_id; ?></span>
			</div>
			<div class="row">
				<label class="txt<?php echo $type; ?>">Member since</label>
				<span class="txt<?php echo $type; ?>"><?php echo $current_user->user_registered; ?></span>
			</div>
		  </fieldset>
		  <fieldset>
		  	<div class="row">
				<label class="txt<?php echo $type; ?>">Facebook ID</label>
				<span class="txt<?php echo $type; ?>"><?php echo $current_user->fbuid; ?></span>
				<?php if ($current_user->fbuid!='') { ?>
				<?php } else { ?>
				<input type="button" class="button-primary" name="connect" value="connect">
				<?php } ?>
			</div>
			<div class="row">
				<label class="txt<?php echo $type; ?>">Twitter ID</label>
				<span class="txt<?php echo $type; ?>"><?php echo $current_user->twitterid; ?></span>
				<input type="button" class="button-primary" name="connect" value="connect">
			</div>
			<div class="row">
				<label class="txt<?php echo $type; ?>">Google ID</label>
				<span class="txt<?php echo $type; ?>"><?php echo $current_user->googleid; ?></span>
				<input type="button" class="button-primary" name="connect" value="connect">
			</div>
		  </fieldset>
		  <fieldset>
		  	<!--   div class="row">
				<label>Delima UserID</label>
				<span>izhur2001@yahoo.com</span>
				<input type="button" class="button-primary" name="connect" value="connect">
			</div -->
			<!--  div class="row">
				<label>Melon UserID</label>
				<span><?php echo $current_user->melon_id ?></span>
				<?php if ($current_user->melon_uid!='') { ?>
				<?php } else { ?>
				<input type="button" class="button-primary" name="connect" id="melConnect" value="connect">
				<input type="button" class="button-primary" name="create" id="melCreate" value="create">
				<?php } ?>
			</div -->
		  <fieldset>
		</section>
		<div class="clear"></div>
		</div>
	<div class="clear" style="height: 20px;"></div>
	</div> <!---- containersingleIsi-->
</div> <!---- containersingle-->
	</div> 
</div>
</div>
<script type="text/javascript">
$(function() {
	$('#melConnect').click(function(){
		$( "#dialog-form" ).attr("tittle","Connect MelOn ID");
		$( "#dialog-form" ).dialog( "open" );
	});
	$('#melCreate').click(function(){
		$.ajax({
			type: "POST",
			url: "/wp-admin/admin-ajax.php",
			data:{ 
				action: "suararadio_melon_create"
			},
			success: function (data) {
				
			}
		});
	});
	$( "#dialog-form" ).dialog({
		autoOpen: false,
		height: 300,
		width: 350,
		modal: true,
		buttons: {
			Proses: function() {
				$.ajax({
					type: "POST",
					url: "/wp-admin/admin-ajax.php",
					data:{ 
						action: "suararadio_melon_connect",
						username: $('#username').val(),
						password: $('#password').val()
					},
					success: function (data) {
						if (data=="OK") {
							window.location.reload();
						} else {
							alert(data);
						}
					}
				});
				$('#username').val('');
				$('#password').val('');
				$( this ).dialog( "close" );
			},
			Batal: function() {
				$( this ).dialog( "close" );
			}
		}
	});
});

</script>