<?php

$channel_map = array(
	"entertainment" => array(
		"infotainment" => array('musik-hiburan'),
    "lifestyle" => array('lifestyle-hobby'),
    "comedy" => array('humor'),
    "story" => array('cerita'),
	),
	"news" => array(
		//"national" => array('ekonomi-bisnis','nusantara','politik-peristiwa'),
    "economy-business" => array('ekonomi-bisnis'),
    "national" => array('nusantara','politik-peristiwa'),
		"international" => array('internasional'),
		"sports" => array('olahraga'),
    "tips"=>array('tips'),
    "talkshow"=>array('talk-show'),
	),
	"religion" => array(
		"islam" => array('mutiara-hikmah','islam', 'hikmah-ramadhan','pengajian-rasika','ramadhan', 'tausyiah','rohani-islam', 'stairway-to-heaven','pengajian','tausyiah-hikmah-ramadhan-ramadhan-special'),
		"christianity" => array( 'kristen','katolik'),
		"buddhism" => array('budha','buddha'),
		"hinduism" => array('hindu'),
		//"others" => array('rohani')
	),
	"tourism" => array(
		"culinary" => array('oleh-oleh-fasilitas-travel-travel','kuliner'),	
		"travel" => array('wisata-pilihan','hotel-fasilitas-travel-travel','wisata-travel'),  
    //"destination" => array(),
    
	),
		
);

$excluded_slug = array(
    "music", "musik"
);



?>
