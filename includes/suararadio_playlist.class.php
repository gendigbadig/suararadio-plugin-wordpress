<?
/*
License:
 ==============================================================================

    Copyright 2010  M Zhuhriansyah R  (email : izhur2001@yahoo.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-107  USA
    
    5/5/2010 4:05:13 PM MZR
*/

class suararadioPlaylist_class extends suararadio_class {

	//ctor
	function suararadioPlaylist_class() {
	}
	
	function getGenres() {
		global $wprisedb;
		
		$genres = $wprisedb->get_results(
			$wprisedb->prepare("select genre from t_genre where stat='1' and id_branch= %s ",RISE_ID_BRANCH),
			ARRAY_A);
		return $genres;
	}
	
	function getTypes() {
		global $wprisedb;
		
		$types = $wprisedb->get_results(
			$wprisedb->prepare("select type from t_type where stat='1' and id_branch= %s ",RISE_ID_BRANCH),
			ARRAY_A);
		return $types;
	}
	
	function getBeats() {
		global $wprisedb;
		
		$beats = $wprisedb->get_results(
			$wprisedb->prepare("select beat from t_type where stat='1' and id_branch= %s ",RISE_ID_BRANCH),
			ARRAY_A);
		return $beats;
	}
	
	function getLanguages() {
		global $wprisedb;
		
		$langs = $wprisedb->get_results(
			$wprisedb->prepare("select bahasa from t_bahasa where stat='1' and id_branch= %s ",RISE_ID_BRANCH),
			ARRAY_A);
		return $langs;
	}
	
	function getStyles() {
		global $wprisedb;
		
		$langs = $wprisedb->get_results(
			$wprisedb->prepare("select bahasa from t_bahasa where stat='1' and id_branch= %s ",RISE_ID_BRANCH),
			ARRAY_A);
		return $langs;
	}
	
	function getGender() {
		global $wprisedb;
		
		$langs = $wprisedb->get_results(
			$wprisedb->prepare("select bahasa from t_bahasa where stat='1' and id_branch= %s ",RISE_ID_BRANCH),
			ARRAY_A);
		return $langs;
	}
	
	function getLagu($lagu_id) {
		global $wprisedb;
		
		$lagu = $wprisedb->get_row(
			$wprisedb->prepare("select * from t_lagu where id_branch= %s and id_lagu = %s ",RISE_ID_BRANCH,$lagu_id),
			ARRAY_A);
		return $lagu;
	}
	
	function getListLagu($params=array()){
		global $wprisedb;
		global $max_page;
		global $paged;
		
		//$params['start'] = ;
		$limitrow = ($params['numrow'])? $params['numrow']:$this->num_per_page;
		$page = ($params['page'])? $params['page']:1;

		$select = "select * ";
		$query = " from t_lagu where id_branch= '".RISE_ID_BRANCH."' and stat='1' ";
		$limit = "";
		$order = "";
		
		if ($params['genre']!='') {
			$query .= " and genre= '".$params['genre']."' ";
		}
		if ($params['keyword']!='') {
			$query .= " and (judul like '%".$params['keyword']."%' or penyanyi like '%".$params['keyword']."%') ";
		}
		
		// paged limit
		if ($limitrow!='') {
			$start = ($params['start'])? $params['start']:0;
			if ($page>1) {
				$start = $limitrow*($page-1);
			}
			$limit = " limit  ".$start.",".$limitrow; // mysql specific.
		}
		
#echo $select.$query.$order.$limit;

		$lagus = $wprisedb->get_results($select.$query.$order.$limit,ARRAY_A);
		$select = "select count(*) as num ";

		$nums = $wprisedb->get_var($select.$query);
		#$nums_per_page = get_option('posts_per_page');
		$max_page = ceil($nums / $limitrow);
		
		return $lagus;
	}
	
	function convertFromLaguId($lagu_id) {
		return str_replace($this->rep_from,$this->rep_to,$lagu_id);
	}
	
	function convertToLaguId($path) {
		return str_replace($this->rep_to,$this->rep_from,$path);
	}
	
	/**
   * mengubah data rise lagu menjadi path url wordpress portal radio
   */
  function convertLaguToPodcastWebPath($lagu_id){
  		$link = $this->convertFromLaguId($lagu_id);
			$url = get_settings('siteurl').'/suararadio_api/play_lagu/'.$link;
			return $url;
  } ##endfunc convert to podcast web path	
  
  /**
   *
   */
  function convertPodcastWebPathToLagu($path) {
  	global $wprisedb;
  	
  	$lagu_id = $this->convertToLaguId($path);
  	$lagu = $this->getLagu($lagu_id);

  	//http://prod.rise-app.com/musicdirector/dir_music/KOTAKKOTAK_KEDUA%20-%20KEMBALI_UNTUKMU.MP3
  	$filename = '';
  	if ($lagu['nama_file']!='') {
	  	$filename = RISE_URL_DIR_MUSIC.rawurlencode($lagu['nama_file']);
  	} 
  	return $filename;
  } #endfunc convertPodcastWebPathToLagu
  
  
  /**
   * mengecek ukuran file di tempat remote.
   */
  function getRemoteFileHeader($url) {
        $sch = parse_url($url, PHP_URL_SCHEME);
        if (($sch != "http") && ($sch != "https") && ($sch != "ftp") && ($sch != "ftps")) {
            return false;
        }
        if (($sch == "http") || ($sch == "https")) {
            $headers = get_headers($url, 1);
            if ((!array_key_exists("Content-Length", $headers))) { return false; }
            return $headers["Content-Length"];
        }
        if (($sch == "ftp") || ($sch == "ftps")) {
            $server = parse_url($url, PHP_URL_HOST);
            $port = parse_url($url, PHP_URL_PORT);
            $path = parse_url($url, PHP_URL_PATH);
            $user = parse_url($url, PHP_URL_USER);
            $pass = parse_url($url, PHP_URL_PASS);
            if ((!$server) || (!$path)) { return false; }
            if (!$port) { $port = 21; }
            if (!$user) { $user = "anonymous"; }
            if (!$pass) { $pass = "phpos@"; }
            switch ($sch) {
                case "ftp":
                    $ftpid = ftp_connect($server, $port);
                    break;
                case "ftps":
                    $ftpid = ftp_ssl_connect($server, $port);
                    break;
            }
            if (!$ftpid) { return false; }
            $login = ftp_login($ftpid, $user, $pass);
            if (!$login) { return false; }
            $ftpsize = ftp_size($ftpid, $path);
            ftp_close($ftpid);
            if ($ftpsize == -1) { return false; }
            return $ftpsize;
        }
   } #getRemoteFileSize
   
  /**
   * mengecek ukuran file di tempat remote.
   */
  function getListPlaylist($params=array()){
		global $wpdb;
		global $max_page;
		global $paged;
		
		//$params['start'] = ;
		$limitrow = ($params['numrow'])? $params['numrow']:$this->num_per_page;
		$page = ($params['page'])? $params['page']:1;

		$select = "select * ";
		$query = " from wp_suararadio_playlist where id_branch= '".RISE_ID_BRANCH."' and stat='1' ";
		$limit = "";
		$order = "";

		
		if ($params['genre']!='') {
			$query .= " and genre= '".$params['genre']."' ";
		}
		if ($params['keyword']!='') {
			$query .= " and (judul like '%".$params['keyword']."%' or penyanyi like '%".$params['keyword']."%') ";
		}
		
		// paged limit
		if ($limitrow!='') {
			$start = ($params['start'])? $params['start']:0;
			if ($page>1) {
				$start = $limitrow*($page-1);
			}
			$limit = " limit  ".$start.",".$limitrow; // mysql specific.
		}
		
#echo $select.$query.$order.$limit;

		$list = $wpdb->get_results($select.$query.$order.$limit,ARRAY_A);
		$select = "select count(*) as num ";

		$nums = $wpdb->get_var($select.$query);
		#$nums_per_page = get_option('posts_per_page');
		$max_page = ceil($nums / $limitrow);
		
		return $list;
  }
  
  function getSessionList() {
  	return $_SESSION[SR_PLAYLIST_VAR];
  }
}

?>
