<?php


/**
 * Suararadio API Exception
 * @author izhur2001
 * 
 * error list:
 * E001 : Unkown mode on search integration
 * E002 : Unkown get property
 */
class SuararadioException extends Exception { 
}

if (!defined(DELIMA_MERCHANT_CODE)) define('DELIMA_MERCHANT_CODE','195106067343');
if (!defined(DELIMA_URL)) define('DELIMA_URL','https://em.telkomdelima.com/jets-delima/WebCollection.action');
if (!defined(API_SUARARADIO_URL)) define ('API_SUARARADIO_URL','http://api.suararadio.com/');

/**
 * Enter description here ...
 * @author izhur2001
 *
 */
class SuararadioAPI{

    protected $curl;
    protected $sapiUrl;
    protected $smapiUrl; // melon api cached
    protected $clcode;
    protected $userID;
    protected $userPasswd;
    
    private $cookname;
    private $cname = "webapi";
    private $cpass = "dzNiNHAxX2FjYw==";
    private $mapiPath = "melon/api/";

    function __construct($sapiUrl='',$clientName='',$clientPasswd='') {
        $this->curl = curl_init();
        $this->sapiUrl = API_SUARARADIO_URL;
        if ($sapiUrl!='') $this->sapiUrl = $sapiUrl;
        $this->smapiUrl = $this->sapiUrl.$this->mapiPath;
        if ($clientName!='') $this->cname = $clientName;
        if ($clientPasswd!='') $this->cpass = $clientPasswd;
        $this->cookname = "coo_".md5($_SERVER['HTTP_HOST']);
    }
    
    // setter & getter
    public function __get($name) {
    	if (array_key_exists($name, get_class_vars(__CLASS__))) {
    		return $this->name;
    	} else {
    		throw new SuararadioException("Unkown get property", 'E002');
    	}
    }

    protected function startSapi(){
        try {
            curl_setopt($this->curl, CURLOPT_POST, 1);
            curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($this->curl, CURLOPT_COOKIEFILE, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
            curl_setopt($this->curl, CURLOPT_POSTFIELDS, 'username=' . urlencode($this->cname) . '&password=' . urlencode($this->cpass));
            curl_setopt($this->curl, CURLOPT_COOKIEJAR, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
            curl_setopt($this->curl, CURLOPT_URL, $this->sapiUrl . 'site/login');
            curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
            $result = curl_exec($this->curl);
			$this->chcode = @json_decode($result)->channelCd;
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    
    public function init() {
    	return $this->startSapi();
    } //endfunc init
    
    
    public function klubVoucher($vars) {
    	$var = http_build_query($vars);
    	
    	curl_setopt($this->curl, CURLOPT_POST, 1);
    	curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
    	curl_setopt($this->curl, CURLOPT_COOKIEFILE, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
    	curl_setopt($this->curl, CURLOPT_URL, $this->sapiUrl . "klub/voucher");
    	curl_setopt($this->curl, CURLOPT_POSTFIELDS, $vars);
    	curl_setopt($this->curl, CURLOPT_COOKIEJAR, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
    	curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
    	
    	$result = curl_exec($this->curl);
    	#var_dump($vars);
    	$result = json_decode($result,true);
    	if(@$result['code']=="E001"){
    		$this->startSapi();
    		return $this->klubVoucher($vars);
    	}else{
    		return $result;
    	}
    }
    
    public function klubPrice($vars) {
    	$var = http_build_query($vars);
    	
    	$code = "";
    	if (isset($vars['code']) && $vars['code']!='') {
    		$code = "/code/".$vars['code'];
    	}
    	 
    	curl_setopt($this->curl, CURLOPT_POST, 1);
    	curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
    	curl_setopt($this->curl, CURLOPT_COOKIEFILE, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
    	curl_setopt($this->curl, CURLOPT_URL, $this->sapiUrl . "klub/price".$code);
    	curl_setopt($this->curl, CURLOPT_POSTFIELDS, $vars);
    	curl_setopt($this->curl, CURLOPT_COOKIEJAR, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
    	curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
    	 
    	$result = curl_exec($this->curl);
    	$result = json_decode($result,true);
    	if(@$result['code']=="E001"){
    		$this->startSapi();
    		return $this->klubPrice($vars);
    	}else{
    		return $result;
    	}
    } //endfunc klubPrice
    
    public function klubRegisterMelon($vars) {
    	$var = http_build_query($vars);
    	 
    	curl_setopt($this->curl, CURLOPT_POST, 1);
    	curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
    	curl_setopt($this->curl, CURLOPT_COOKIEFILE, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
    	curl_setopt($this->curl, CURLOPT_URL, $this->sapiUrl . "klub/registermelon");
    	curl_setopt($this->curl, CURLOPT_POSTFIELDS, $var);
    	curl_setopt($this->curl, CURLOPT_COOKIEJAR, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
    	curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
    
    	$result = curl_exec($this->curl);
    	#var_dump($result,$var);
    	$result = json_decode($result,true);
    	if(@$result['code']=="E001"){
    		$this->startSapi();
    		return $this->klubRegisterMelon($vars);
    	}else{
    		return $result;
    	}
    } //endfunc klubRegisterMelon
    
    public function klubPromo($vars) {
    	$var = http_build_query($vars);
    	 
    	curl_setopt($this->curl, CURLOPT_POST, 1);
    	curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
    	curl_setopt($this->curl, CURLOPT_COOKIEFILE, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
    	curl_setopt($this->curl, CURLOPT_URL, $this->sapiUrl . "klub/promo");
    	curl_setopt($this->curl, CURLOPT_POSTFIELDS, $var);
    	curl_setopt($this->curl, CURLOPT_COOKIEJAR, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
    	curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
    	 
    	$result = curl_exec($this->curl);
    	#var_dump($result);
    	$result = json_decode($result,true);
    	if(@$result['code']=="E001"){
    		$this->startSapi();
    		return $this->klubPromo($vars);
    	} else {
    		return $result;
    	}
    } //endfunc klubPromo
    
    public function delimaInvoice($vars) {
    	$var = http_build_query($vars);
    	
    	curl_setopt($this->curl, CURLOPT_POST, 1);
    	curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
    	curl_setopt($this->curl, CURLOPT_COOKIEFILE, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
    	curl_setopt($this->curl, CURLOPT_URL, $this->sapiUrl . "delima/invoice");
    	curl_setopt($this->curl, CURLOPT_POSTFIELDS, $vars);
    	curl_setopt($this->curl, CURLOPT_COOKIEJAR, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
    	curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
    	
    	$result = curl_exec($this->curl);
    	$result = json_decode($result,true);
    	if(@$result['code']=="E001"){
    		$this->startSapi();
    		return $this->delimaInvoice($vars);
    	}else{
    		return $result;
    	}
    } //endfunc delimaInvoice
    
    public function delimaMembership($vars) {
    	$var = http_build_query($vars);
    	 
    	curl_setopt($this->curl, CURLOPT_POST, 1);
    	curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
    	curl_setopt($this->curl, CURLOPT_COOKIEFILE, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
    	curl_setopt($this->curl, CURLOPT_URL, $this->sapiUrl . "delima/membership");
    	curl_setopt($this->curl, CURLOPT_POSTFIELDS, $vars);
    	curl_setopt($this->curl, CURLOPT_COOKIEJAR, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
    	curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
    	 
    	$result = curl_exec($this->curl);
    	$result = json_decode($result,true);
    	if(@$result['code']=="E001"){
    		$this->startSapi();
    		return $this->delimaInvoice($vars);
    	}else{
    		return $result;
    	}
    } //endfunc delimaMembership
    
    public function getRadioID($vars) {
    	$var = http_build_query($vars);
    	 
    	curl_setopt($this->curl, CURLOPT_POST, 1);
    	curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
    	curl_setopt($this->curl, CURLOPT_COOKIEFILE, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
    	curl_setopt($this->curl, CURLOPT_URL, $this->sapiUrl . "klub/radio");
    	curl_setopt($this->curl, CURLOPT_POSTFIELDS, $var);
    	curl_setopt($this->curl, CURLOPT_COOKIEJAR, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
    	curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
    	 
    	$result = curl_exec($this->curl);
    	$result = json_decode($result,true);
    	if(@$result['code']=="E001"){
    		$this->startSapi();
    		return $this->getRadioID($vars);
    	}else{
    		return $result;
    	}
    }
    
    public function getDelimaLink($vars) {
    	return DELIMA_URL."?MerchantCode=".DELIMA_MERCHANT_CODE."&invoiceNo=".$vars['invoice_no'];
    } //endfunc getDelimaLink
    
    public function getMember($vars) {
		$start = time();
    	$var = http_build_query($vars);
    	
    	curl_setopt($this->curl, CURLOPT_POST, 1);
    	curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
    	curl_setopt($this->curl, CURLOPT_COOKIEFILE, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
    	curl_setopt($this->curl, CURLOPT_URL, $this->sapiUrl . "klub/member");
    	curl_setopt($this->curl, CURLOPT_POSTFIELDS, $var);
    	curl_setopt($this->curl, CURLOPT_COOKIEJAR, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
    	curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
    	
    	$result = curl_exec($this->curl);
    	$result = json_decode($result,true);
    	if(@$result['code']=="E001"){
    		$this->startSapi();
    		return $this->getMember($vars);
    	} else {
			$end = time();
	        $result['log'] = $end-$start." ".$this->cookname; //$this->sapiUrl
    		return $result;
    	}
    } //endfunc getMember
    
    public function melonConnect($vars) {
    	$var = http_build_query($vars);
    	
    	curl_setopt($this->curl, CURLOPT_POST, 1);
    	curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
    	curl_setopt($this->curl, CURLOPT_COOKIEFILE, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
    	curl_setopt($this->curl, CURLOPT_URL, $this->sapiUrl . "klub/melonconnect");
    	curl_setopt($this->curl, CURLOPT_POSTFIELDS, $var);
    	curl_setopt($this->curl, CURLOPT_COOKIEJAR, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
    	curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
    	
    	$result = curl_exec($this->curl);
    	$result = json_decode($result,true);
    	if(@$result['code']=="E001"){
    		$this->startSapi();
    		return $this->melonConnect($vars);
    	} else {
    		return $result;
    	}
    } //endfunc melonConnect

    public function purchaseInit($vars) {
    	$var = http_build_query($vars);
    	 
    	curl_setopt($this->curl, CURLOPT_POST, 1);
    	curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
    	curl_setopt($this->curl, CURLOPT_COOKIEFILE, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
    	curl_setopt($this->curl, CURLOPT_URL, $this->sapiUrl . "klubradio/purchase/init");
    	curl_setopt($this->curl, CURLOPT_POSTFIELDS, $var);
    	curl_setopt($this->curl, CURLOPT_COOKIEJAR, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
    	curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
    	 
    	$result = curl_exec($this->curl);
    	#var_dump($result,$vars,$this->sapiUrl."klubradio/purchase/init");
    	$result = json_decode($result,true);
    	if(@$result['code']=="E001"){
    		$this->startSapi();
    		return $this->purchaseInit($vars);
    	}else{
    		return $result;
    	}
    } //endfunc purchaseInit
    
    public function activate($vars) {
    	
    	$var = http_build_query($vars);
    	
    	curl_setopt($this->curl, CURLOPT_POST, 1);
    	curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
    	curl_setopt($this->curl, CURLOPT_COOKIEFILE, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
    	curl_setopt($this->curl, CURLOPT_URL, $this->sapiUrl . "klub/activate");
    	curl_setopt($this->curl, CURLOPT_POSTFIELDS, $var);
    	curl_setopt($this->curl, CURLOPT_COOKIEJAR, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
    	curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
    	
    	$result = curl_exec($this->curl);
    	#var_dump($result,$vars,$this->sapiUrl."klub/activate");
    	$result = json_decode($result,true);
    	if(@$result['code']=="E001"){
    		$this->startSapi();
    		return $this->activate($vars);
    	}else{
    		return $result;
    	}
    } //endfunc activate
    
    public function signCreate($data) {
    	$var = array("data"=>$data);
    	$vars = http_build_query($var);
    	
    	curl_setopt($this->curl, CURLOPT_POST, 1);
    	curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
    	curl_setopt($this->curl, CURLOPT_COOKIEFILE, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
    	curl_setopt($this->curl, CURLOPT_URL, $this->sapiUrl . "key/signcreate");
    	curl_setopt($this->curl, CURLOPT_POSTFIELDS, $vars);
    	curl_setopt($this->curl, CURLOPT_COOKIEJAR, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
    	curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
    	
    	$result = curl_exec($this->curl);
    	$result = json_decode($result,true);
    	if(@$result['code']=="E001"){
    		$this->startSapi();
    		return $this->signCreate($data);
    	}else{
    		return $result;
    	}
    } //endfunc signCreate
    
    public function signVerify($data,$signature) {
    	$var = array("data"=>$data,"signature"=>$signature);
    	$vars = http_build_query($var);
    	 
    	curl_setopt($this->curl, CURLOPT_POST, 1);
    	curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
    	curl_setopt($this->curl, CURLOPT_COOKIEFILE, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
    	curl_setopt($this->curl, CURLOPT_URL, $this->sapiUrl . "key/signverify");
    	curl_setopt($this->curl, CURLOPT_POSTFIELDS, $vars);
    	curl_setopt($this->curl, CURLOPT_COOKIEJAR, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
    	curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
    	
    	$result = curl_exec($this->curl);
    	$result = json_decode($result,true);
    	if(@$result['code']=="E001"){
    		$this->startSapi();
    		return $this->signVerify($data,$signature);
    	}else{
    		return $result;
    	}
    } //endfunc signVerify
    
    public function signPublic($user,$passwd) {
    	
    	curl_setopt($this->curl, CURLOPT_HTTPGET, 1);
    	curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
    	curl_setopt($this->curl, CURLOPT_COOKIEFILE, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
    	curl_setopt($this->curl, CURLOPT_URL, $this->sapiUrl . "key/signpublic");
    	curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
    	
    	$result = curl_exec($this->curl);
    	$result = json_decode($result,true);
    	if(@$result['code']=="E001"){
    		$this->startSapi();
    		return $this->signPublic($user,$passwd);
    	}else{
    		return $result;
    	}
    } //endfunc signPublic
    
    
    /**
     * login to Speedy Instan 
     * @return mixed
     */
    public function spinLogin($username,$password) {
    	
    	$vars = http_build_query(array('username'=>$username,'password'=>$password));
    	curl_setopt($this->curl, CURLOPT_POST, 1);
    	curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
    	curl_setopt($this->curl, CURLOPT_COOKIEFILE, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
    	curl_setopt($this->curl, CURLOPT_URL, $this->sapiUrl . "spin/user/login");
    	curl_setopt($this->curl, CURLOPT_POSTFIELDS, $vars);
    	curl_setopt($this->curl, CURLOPT_COOKIEJAR, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
    	curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
    	 
    	$result = curl_exec($this->curl);
    	$result = json_decode($result,true);
    	if(@$result['code']=="E001"){
    		$this->startSapi();
    		return $this->spinLogin($username,$password);
    	}else{
    		return $result;
    	}
    } //endfunc spinLogin
    
    public function klubConnect($klubid,$provider,$identity) {
    	$vars = http_build_query(array('klubid'=>$klubid,'provider'=>$provider,'identity'=>$identity));
    	curl_setopt($this->curl, CURLOPT_POST, 1);
    	curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
    	curl_setopt($this->curl, CURLOPT_COOKIEFILE, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
    	curl_setopt($this->curl, CURLOPT_URL, $this->sapiUrl . "klub/socialconnect ");
    	curl_setopt($this->curl, CURLOPT_POSTFIELDS, $vars);
    	curl_setopt($this->curl, CURLOPT_COOKIEJAR, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
    	curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
    	
    	$result = curl_exec($this->curl);
    	$result = json_decode($result,true);
    	if(@$result['code']=="E001"){
    		$this->startSapi();
    		return $this->klubConnect($klubid,$provider,$identity);
    	}else{
    		return $result;
    	}
    } //endfunc klubConnect

    ///////////////////////////////////////////
    /////////////  melon functions ////////////
    ///////////////////////////////////////////
    /**
     * get album data
     * @param unknown $params
     * @return mixed
     */
    public function getAlbums($params) {
    	$armodes = array('artist','genre','new','hot');
    	$mode = "/new";
    	if (isset($params['mode']) && $params['mode']!='' && in_array($params['mode'],$armodes)) {
    		switch ($params['mode']) {
    			case 'artist': $mode = "/by/artist"; break;
    			case 'genre': $mode = "/by/genre"; break;
    			case 'new': $mode = "/new"; break;
    			case 'hot': $mode = "/hot"; break;
    		}
    	}
    	if (isset($params['albumId']) && preg_match('/\d+/',$params['albumId'],$matches)) {
    		$mode = "/".$params['albumId'];
    	}
    	$var = "";
    	if (isset($params['offset'])) {
    		$var .= ($var? "&":"")."offset=".$params['offset'];
    	}
    	if (isset($params['limit'])) {
    		$var .= ($var? "&":"")."limit=".$params['limit'];
    	}
    	if (isset($params['dir'])) {
    		$var .= ($var? "&":"")."dir=".$params['dir'];
    	}
    	if (isset($params['albumId'])) {
    		$var .= ($var? "&":"")."albumId=".$params['albumId'];
    	}
    	if (isset($params['artistId'])) {
    		$var .= ($var? "&":"")."artistId=".$params['artistId'];
    	}
    	if (isset($params['genreId'])) {
    		$var .= ($var? "&":"")."genreId=".$params['genreId'];
    	}
    	$var = ($var? "?":"").$var;
    	
    	$this->curl = curl_init();
    	curl_setopt($this->curl, CURLOPT_HTTPGET, 1);
    	curl_setopt($this->curl, CURLOPT_COOKIEFILE, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
    	curl_setopt($this->curl, CURLOPT_URL, $this->smapiUrl. 'albums'.$mode.$var);
    	curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
    	$result = curl_exec($this->curl);
    	#var_dump('albums'.$mode.$var,$result);
    	$result = json_decode($result,true);
    	
    	return $result;
    }
    
    /**
     * get songs data
     * @param unknown $params
     */
    public function getSongs($params) {
    	$armodes = array('album','artist','genre','new','hot');
    	$mode = "/new";
    	if (isset($params['mode']) && $params['mode']!='' && in_array($params['mode'],$armodes)) {
    		$mode = "/".$params['mode'];
    	}
    	if (isset($params['songId']) && preg_match('/\d+/',$params['songId'],$matches)) {
    		$mode = "/".$params['songId'];
    		if (isset($params['lyric'])) $mode .= "/".$params['lyric'];
    	}
    	$var = "";
    	if (isset($params['offset'])) {
    		$var .= ($var? "&":"")."offset=".$params['offset'];
    	}
    	if (isset($params['limit'])) {
    		$var .= ($var? "&":"")."limit=".$params['limit'];
    	}
    	if (isset($params['albumId'])) {
    		$var .= ($var? "&":"")."albumId=".$params['albumId'];
    	}
    	if (isset($params['artistId'])) {
    		$var .= ($var? "&":"")."artistId=".$params['artistId'];
    	}
    	if (isset($params['genreId'])) {
    		$var .= ($var? "&":"")."genreId=".$params['genreId'];
    	}
    	if (isset($params['dir'])) {
    		$var .= ($var? "&":"")."dir=".$params['dir'];
    	}
    	$var = ($var? "?":"").$var;
    	$this->curl = curl_init();
    	curl_setopt($this->curl, CURLOPT_HTTPGET, 1);
    	curl_setopt($this->curl, CURLOPT_COOKIEFILE, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
    	curl_setopt($this->curl, CURLOPT_URL, $this->smapiUrl . "songs".$mode.$var);
    	//echo "\n".$this->mapiUrl . "songs".$mode.$var;
    	curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
    	
    	$result = curl_exec($this->curl);
    	$result = json_decode($result,true);
    	
    	return $result;
    }
    
	public function getChart($mode='daily',$startDate='',$params=array()) {
		$armodes = array('daily','weekly','monthly');
		// yyyyMMdd
		if (!in_array($mode,$armodes)) {
			throw new Exception("Unkown chart mode", 1071);
		}
		if ($mode=='daily') {
			$startDate = "";
		}
		$vars = array();
		if (isset($params['offset'])) {
			#$var .= ($var? "&":"")."offset=".$params['offset'];
			$vars['offset'] = $params['offset'];
		}
		if (isset($params['limit'])) {
			#$var .= ($var? "&":"")."limit=".$params['limit'];
			$vars['limit'] = $params['limit'];
		}
		$var = "?".http_build_query($vars);
		$this->curl = curl_init();
		curl_setopt($this->curl, CURLOPT_HTTPGET, 1);
		curl_setopt($this->curl, CURLOPT_COOKIEFILE, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
		curl_setopt($this->curl, CURLOPT_URL, $this->smapiUrl . "chart/$mode".(($startDate)? "/$startDate":"").$var);
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec($this->curl);
		//var_dump($this->mapiUrl . "chart/$mode".(($startDate)? "/$startDate":""),$result);
		$result = json_decode($result,true);
		return $result;
	}
	
	public function searchIntegration($params=array()) {
		// $mode='artist',$keyword=''
		$mode = "/artist";
		$armodes = array('artist','song','album');
		if (isset($params['mode']) && $params['mode']!='' && in_array($params['mode'],$armodes)) {
			$mode = "/".$params['mode'];
		}
		$var = "";
		$vars = array();
		if (isset($params['offset'])) {
			#$var .= ($var? "&":"")."offset=".$params['offset'];
			$vars['offset'] = $params['offset'];
		}
		if (isset($params['limit'])) {
			#$var .= ($var? "&":"")."limit=".$params['limit'];
			$vars['limit'] = $params['limit'];
		}
		if (isset($params['dir'])) {
			#$var .= ($var? "&":"")."dir=".$params['dir'];
			$vars['dir'] = $params['dir'];
		}
		if (isset($params['keyword'])) {
			#$var .= ($var? "&":"")."keyword=".$params['keyword'];
			$vars['keyword'] = $params['keyword'];
		}
		#$var = ($var? "?":"").$var;
		$var = "?".http_build_query($vars);
	
		$this->curl = curl_init();
		curl_setopt($this->curl, CURLOPT_HTTPGET, 1);
		curl_setopt($this->curl, CURLOPT_COOKIEFILE, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
		curl_setopt($this->curl, CURLOPT_URL, $this->smapiUrl . "search/integration".$mode.$var);
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
		#curl_setopt($this->curl, CURLINFO_HEADER_OUT, true);
	
		#echo md5($this->userMail)."search<br/>";
		$result = curl_exec($this->curl);
		$result = json_decode($result,true);
		
		return $result;
	}
	
	public function getCategories($params=array()) {
		$armodes = array("all");
		$mode = "";
		if (isset($params['mode']) && $params['mode']!='' && in_array($params['mode'],$armodes)) {
			$mode = "/".$params['mode'];
		}
		if (isset($params['catId']) && $params['catId']!='') {
			if (preg_match('/\d+/',$params['catId'],$matches)) {
				$mode = "/".$params['catId']."/genres";
			}
		}
		$this->curl = curl_init();
		curl_setopt($this->curl, CURLOPT_HTTPGET, 1);
		curl_setopt($this->curl, CURLOPT_COOKIEFILE, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
		curl_setopt($this->curl, CURLOPT_URL, $this->smapiUrl . "categories".$mode);
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
	
		$result = curl_exec($this->curl);
		#var_dump($result,$this->mapiUrl . "categories".$mode);
		$result = json_decode($result,true);
		curl_close($this->curl);
		
		if(@$result['code']=="E001"){
			$this->startSapi();
			return $this->getCategories($params);
		}else{
			return $result;
		}
		
	}
	
	public function getCertificationMusic($userid, $songid, $type){
		
		$this->curl = curl_init();
		curl_setopt($this->curl, CURLOPT_COOKIEFILE, session_save_path().DIRECTORY_SEPARATOR.$this->cookname);
		curl_setopt($this->curl, CURLOPT_POST, 1);
		curl_setopt($this->curl, CURLOPT_POSTFIELDS, 'userId='. $userid .'&songId='. $songid .'&channelCd='. $this->chcode .'&contentType='.$type);
		curl_setopt($this->curl, CURLOPT_URL, $this->smapiUrl . 'streaming/certification');
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
		
		$result = curl_exec($this->curl);
		$result = json_decode($result,true);
		
		if(@$result['code']=="E001"){
			$this->startSapi();
			return $this->getCertificationMusic($userid, $songid, $type);
		}else{
			return $result;
		}
	}
	
} //endclass
