<?php
/*
Template Name: register Membership
*/

wp_enqueue_style( "wsl_login_css", WORDPRESS_SOCIAL_LOGIN_PLUGIN_URL . "/assets/css/login.css" );
?>
<?php get_header('membership'); ?>
		

		<section id="login-with" style="padding:10px;">
			<section id="login-with-row">
				<section id="login-with-cell" style="width: 10%;">
					<section class="line"></section>
				</section>
				<section id="login-with-cell" style="width: 40%;">
				Anda Adalah Seorang ...
				</section>
				<section id="login-with-cell" style="width: 10%;">
					<section class="line"></section>
				</section>
			</section>
		</section>
		<section id="login-with" style="width: 100%;" >
			<?php 
			$menu = $_GET['menu'];
			if($menu =="1"){ ?>
			<section id="menu_1"></section>
			<section id="info_listener" style="background: none repeat scroll 0 0 #d2d3d5;
    			margin: 10px 15px 5px 15px;">
				<section class="info">
					<section class="info_image"><img src="<?php echo bloginfo('template_directory') . '/images/klub_radio/klubmob_radlist__modis.png'; ?>"></section>
					<section class="info_text">Get discount on<br> All of the MODIS Outlets<br> around Indonesia</section>
				</section>
				<section class="info">
					<section class="info_image"><img src="<?php echo bloginfo('template_directory') . '/images/klub_radio/klubmob_radlist__ebroad.png'; ?>"></section>
					<section class="info_text">Unlimited Radio Station<br> Content Streaming</section>
				</section>
				<section class="info">
					<section class="info_image"><img src="<?php echo bloginfo('template_directory') . '/images/klub_radio/klubmob_radlist__onemin.png'; ?>"></section>
					<section class="info_text">Music Preview Provided<br>by Melon</section>
				</section>
				<section class="info">
					<section class="info_image"><img src="<?php echo bloginfo('template_directory') . '/images/klub_radio/klubmob_radlist__laris.png'; ?>"></section>
					<section class="info_text">Free Advertising for one<br>product/service on LARIS<br> for a day per months</section>
				</section>
			</section>
			<? }; if ($menu =="2"){ ?>
			<section id="menu_2"></section>
			<section id="info_listener" style="background: none repeat scroll 0 0 #d2d3d5;
    			margin: 10px 15px 5px 15px;">
				<section class="info">
					<section class="info_image"><img src="<?php echo bloginfo('template_directory') . '/images/klub_radio/klubmob_radfan_radlist_icon.png'; ?>"></section>
					<section class="info_text">All the Features of<br>Radio Listener</section>
				</section>
				<section class="info">
					<section class="info_image"><img src="<?php echo bloginfo('template_directory') . '/images/klub_radio/klubmob_radfan_unlimited_icon.png'; ?>"></section>
					<section class="info_text">Unlimited Music Streaming<br> Provided by melon</section>
				</section>
				<section class="info">
					<section class="info_image"><img src="<?php echo bloginfo('template_directory') . '/images/klub_radio/klubmob_radfan_laris_icon.png'; ?>"></section>
					<section class="info_text">Free Advertising for one<br>product/service on LARIS<br> for 2 days per months</section>
				</section>
				<section class="info">

				</section>
		
			</section>
			<? }; if ($menu =="3"){ ?>
			<section id="menu_3"></section>
			<section id="info_listener" style="background: none repeat scroll 0 0 #d2d3d5;
    			margin: 10px 15px 5px 15px;">
				<section class="info">
					<section class="info_image"><img src="<?php echo bloginfo('template_directory') . '/images/klub_radio/klubmob_radlove_radfan_icon.png'; ?>"></section>
					<section class="info_text">All the Features of<br>Radio Listener</section>
				</section>
				<section class="info">
					<section class="info_image"><img src="<?php echo bloginfo('template_directory') . '/images/klub_radio/klubmob_radlove_unlimited_icon.png'; ?>"></section>
					<section class="info_text">Unlimited Music Streaming<br> Provided by melon</section>
				</section>
				<section class="info">
					<section class="info_image"><img src="<?php echo bloginfo('template_directory') . '/images/klub_radio/klubmob_radlove_laris_icon.png'; ?>"></section>
					<section class="info_text">Free Advertising for one<br>product/service on LARIS<br> for 2 days per months</section>
				</section>
				<section class="info">

				</section>
		
			</section>

			<? }; if ($menu =="4"){ ?>
			<section id="menu_4"></section>
			<section id="info_listener" style="background: none repeat scroll 0 0 #d2d3d5;
    			margin: 10px 15px 5px 15px;">
				<section class="info">
					<section class="info_image"><img src="<?php echo bloginfo('template_directory') . '/images/klub_radio/klubmob_radmania_radlov_icon.png'; ?>"></section>
					<section class="info_text">All the Features of<br>Radio Listener</section>
				</section>
				<section class="info">
					<section class="info_image"><img src="<?php echo bloginfo('template_directory') . '/images/klub_radio/klubmob_radmania_download_icon.png'; ?>"></section>
					<section class="info_text">Unlimited Music Streaming<br> Provided by melon</section>
				</section>
				<section class="info">
					<section class="info_image"><img src="<?php echo bloginfo('template_directory') . '/images/klub_radio/klubmob_radmania_laris_icon.png'; ?>"></section>
					<section class="info_text">Free Advertising for one<br>product/service on LARIS<br> for 2 days per months</section>
				</section>
				<section class="info">

				</section>
		
			</section>
			<? }; if ($menu =="5"){ ?>
			<section id="menu_5"></section>
			<section id="info_listener" style="background: none repeat scroll 0 0 #d2d3d5;
    			margin: 10px 15px 5px 15px;">
				<section class="info">
					<section class="info_image"><img src="<?php echo bloginfo('template_directory') . '/images/klub_radio/klubmob_radleg_radman_icon.png'; ?>"></section>
					<section class="info_text">All the Features of<br>Radio Listener</section>
				</section>
				<section class="info">
					<section class="info_image"><img src="<?php echo bloginfo('template_directory') . '/images/klub_radio/klubmob_radleg_RTM_fac_icon.png'; ?>"></section>
					<section class="info_text">Unlimited Music Streaming<br> Provided by melon</section>
				</section>
				<section class="info">
					<section class="info_image"><img src="<?php echo bloginfo('template_directory') . '/images/klub_radio/klubmob_radleg_laris_icon.png'; ?>"></section>
					<section class="info_text">Free Advertising for one<br>product/service on LARIS<br> for 2 days per months</section>
				</section>
				<section class="info">

				</section>
		
			</section>
			<? } ?>
			

		</section>


<?php get_footer('memberpolos'); ?>