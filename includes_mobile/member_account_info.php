<?php
/*
  Member Account Info
*/
$logs = $suararadio->getLastLog();
$balance = $suararadio->getBalance();
$varcustom = array("user_ID"=>$user_ID,"PHPSESSID"=>session_id(),"poin"=>10000);
?>
<br/>
<div class="title"><?php echo __('Last Access','suararadio'); ?></div>
<div class="content">
	<? if ($logs) { ?>
	<span class="time"><?php echo date("Y-m-d H:i:s",$logs['dt']); ?></span>
	<span class="method"><?php echo $logs['method']; ?></span>
	<span class="link"><?php suararadio_showMiniUrlPlay('podpress_trac/play/'.$logs['postID'].'/'.$logs['mediaNum'].'/'.$logs['media']); ?></span>
	<span class="media"><?php echo (strlen($logs['media'])<50)? $logs['media']:substr($logs['media'],0,50)." ..."; ?></span>
	<? } else { ?>
	<span>Tidak ada data.</span>
	<? } ?>
</div>
<br/>
<div class="title"><?php echo __('Balance','suararadio'); ?></div>
<div class="content">
	<span class="label"><?php echo __('Poin','suararadio'); ?>:</span>
	<span class="value"><?php echo number_format($balance,0,",","."); ?></span>
</div>
<br/>
<div class="title"><?php echo __('Storage','suararadio'); ?></div>
<div class="content">
	<span class="label">Storage:</span>
	<span class="value">10 MB</span>
</div>
<br/>
<div class="title"><?php echo __('Deposit','suararadio'); ?></div>
<div class="content">
  Beli poin member sebanyak 10.000

  <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
  <input type="hidden" name="cmd" value="_xclick">
  <input type="hidden" name="custom" value="<?= htmlspecialchars(serialize($varcustom)) ?>">
  <input type="hidden" name="email" value="<?= $current_user->user_email ?>">
  <input type="hidden" name="first_name" value="<?= $current_user->user_firstname ?>">
  <input type="hidden" name="last_name" value="<?= $current_user->user_lastname ?>">
  <input type="hidden" name="notify_url" value="<?= site_url("/suararadio_api/paypal/notify") ?>">
  <input type="hidden" name="business" value="WCN82QF5DF8KJ">
  <input type="hidden" name="item_name" value="Poin Member (10.000 poin)">
  <input type="hidden" name="item_number" value="0128176211291">
  <input type="hidden" name="amount" value="10">
  <input type="hidden" name="currency_code" value="USD">
  <input type="hidden" name="no_shipping" value="1">
  <input type="hidden" name="return" value="<?= site_url("/member/account/?paypal=success");?>">
  <input type="hidden" name="cancel_ return" value="<?= site_url("/member/account/?paypal=cancel");?>">


  <input type="image" src="https://www.sandbox.paypal.com/WEBSCR-640-20110429-1/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
  <img alt="" border="0" src="https://www.sandbox.paypal.com/WEBSCR-640-20110429-1/en_US/i/scr/pixel.gif" width="1" height="1">
  </form>

</div>