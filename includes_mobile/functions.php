<?php
/*
License:
 ==============================================================================

    Copyright 2010  M Zhuhriansyah R  (email : izhur2001@yahoo.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-107  USA
*/
$arr_var_upd = array(
	'radio_id','klubid','member_type','member_pack','expired','ext_expired','melon_id',
	'melon_uid','melon_pwd','melon_exp','melon_stat','member_deposit','other_radios','no_hp'
);

function playlist_add_item($post_id,$pos=0,$index='last') {	
	global $post;
	global $suararadio;
	
	// category playlist
	if ($post_id=='category') {
		$category_id = get_cat_id($pos); 
		$no_of_posts = get_option('suararadio_post_rows');
		if (!function_exists('is_user_logged_in')) {
			require_once(ABSPATH . WPINC . '/pluggable.php');
		}
		
		#var_dump('cat_name='.$pos.'&order=DESC&posts_per_page=' . $no_of_posts);
		query_posts('category_name='.$pos.'&order=DESC&posts_per_page=' . $no_of_posts);
		while (have_posts()) : the_post();
				#echo $post->ID."\n";
				#var_dump($post);
				
				$meta = get_post_meta($post->ID, 'podPressMedia', true);
				if (!$meta) continue;

				#$fname = basename($meta[0]["URI"]);
				$finfo = pathinfo($meta[0]["URI"]);
				$fnamehash = md5($finfo['basename'].AUTH_KEY).".".$finfo['extension'];
				$fimg = $meta[0]["previewImage"]; 
				
				$loc = "http://".$_SERVER["HTTP_HOST"]."/podpress_trac/play/".$post->ID."/0/".$fname;
				
				if (!empty($loc)) {
					$fimg = (trim($fimg)!=""? $fimg:$imgpath = "http://".$_SERVER["HTTP_HOST"]."/wp-content/plugins/mandala/images/vpreview_center.png");
					$content = array(
						'title'=> $post->post_title,
						'location'=>$loc,
						'duration'=>$meta[$pos]['duration'],
						'annotation'=> $post->post_title,
						'image'=>$fimg,
						'id'=>'post|'.$post->ID.'|0|'.$fnamehash,
						'type'=>'post',
					);
					
					if ($index!='last' && is_number($index)) {
						array_fill($index,1,$content);
					} else {
						$_SESSION[SR_PLAYLIST_VAR][] = $content;
					}
				}
		endwhile;
		wp_reset_query();
		print json_encode(array('title'=>'Kategori '.$pos));
		#var_dump($_SESSION[SR_PLAYLIST_VAR]);
		return true;
	} else if ($post_id=='rise') {
		// lagu dari rise
		$lagu_id = $suararadio->convertToLaguId($pos);
		$lagu = $suararadio->getLaguRise($lagu_id);
		// http://k-lite.suararadio.lan/suararadio_api/play_lagu/2010_01_04.lagu.8156
		$loc = "http://".$_SERVER["HTTP_HOST"]."/suararadio_api/play_lagu/".$pos;
		$fimg = "http://".$_SERVER["HTTP_HOST"]."/wp-content/plugins/mandala/images/vpreview_center.png";
		
		$content = array(
				'title'=> $lagu['penyanyi'].' - '.$lagu['judul'],
				'location'=>$loc,
				'annotation'=>$lagu['penyanyi'].' - '.$lagu['judul'],
				'duration'=>$lagu['durasi'],
				'id'=>'lagu|'.$pos.'|'.$lagu_id.'|'.$lagu['nama_file'],
				'type'=>'lagu',
				'image'=>$fimg,
			);
		if ($index!='last' && is_number($index)) {
			array_fill($index,1,$content);
		} else {
			$_SESSION[SR_PLAYLIST_VAR][] = $content;
		}
		print json_encode($content);
		return true;
	} else if ($post_id=='melon') {
		$songId = $pos;
		$lagu = $suararadio->getLaguMelon($songId);
		//$album = $suararadio->getAlbumMelon($lagu['albumId']);
		$fimg = "http://www.melon.co.id/imageSong.do?songId=".$songId;
		$min = floor($lagu['playtime'] / 60);
		$sec = $lagu['playtime']-($min*60);
		$content = array(
				'title'=> $lagu['artistName'].' - '.$lagu['songName'],
				'location'=>$loc,
				'annotation'=>$lagu['artistName'].' - '.$lagu['songName'],
				'duration'=>$min.":".sprintf('%02d',$sec),
				'id'=>'melon|'.$pos.'|'.$songId.'|'.$lagu['albumId'].'|'.$lagu['artistId'].'|'.$lagu['genreId'],
				'songid'=>$songId,
				'type'=>'melon',
				'image'=>$fimg,
			);
		if ($index!='last' && is_number($index)) {
			array_fill($index,1,$content);
		} else {
			$_SESSION[SR_PLAYLIST_VAR][] = $content;
		}
		print json_encode($content);
		return true;
	}
	// lagu playlist
	
	$postdata = get_post($post_id);
	$meta = get_post_meta($post_id, 'podPressMedia', true);
	
	if (!$meta) return false;
	
	$finfo = pathinfo($meta[0]["URI"]);
	$fname = $finfo['basename'];
	$fnamehash = md5($finfo['basename'].AUTH_KEY).".".$finfo['extension'];
	$fimg = $meta[$pos]["previewImage"]; 

	if ($postdata && $fname) {
		$loc = "http://".$_SERVER["HTTP_HOST"]."/podpress_trac/play/".$post_id."/".$pos."/".$fnamehash;
		$img = (trim($fimg)!=""? $fimg:$imgpath = "http://".$_SERVER["HTTP_HOST"]."/wp-content/plugins/mandala/images/vpreview_center.png");
		$content = array(
				'title'=> $postdata->post_title,
				'location'=>$loc,
				'annotation'=> $postdata->post_title,
				'duration'=>$meta[$pos]['duration'],
				'id'=>'post|'.$postdata->ID.'|0|'.$fname,
				'type'=>'post',
				'image'=>$fimg,
			);
			
		if ($index!='last' && is_number($index)) {
			array_fill($index,1,$content);
		} else {
			$_SESSION[SR_PLAYLIST_VAR][] = $content;
		}
		print json_encode($content);
		return true;
	}
	return false;
} //endplaylist_end

function playlist_show() {
}

function playlist_del_item($index) {
	if (is_numeric($index)) {
		$playlist = (isset($_SESSION[SR_PLAYLIST_VAR])? $_SESSION[SR_PLAYLIST_VAR]:array());
		$content = $playlist[$index];
		unset($playlist[$index]);
		$_SESSION[SR_PLAYLIST_VAR] = array_merge($playlist, array());
		print json_encode($content);
		return true;
	}
	return false;
}

function playlist_del($listid) {
	global $suararadio;
	
	if (is_numeric($listid)) {
		$result = $suararadio->delPlaylist($listid);
		print json_encode($result);
		return true;
	}
	return false;
}

function playlist_load($page) {
	global $suararadio;
	$params['page'] = $page;
	$playlist = $suararadio->getListPlaylist($params);
	print json_encode($playlist);
}

function playlist_item_load($listid=0) {
	global $suararadio;
	if ($listid==0) {
		$items = $_SESSION[SR_PLAYLIST_VAR];
		for($i=0;$i<count($items);$i++) {
			$valitem = $items[$i];
			echo '<li id="'.$i.'"><span class="player">';
			if ($valitem['type']=='melon') {
				$nmfile = suararadio_showMiniMelonPlay($valitem['songid']);
			} else {
				$nmfile = suararadio_showMiniUrlPlay($valitem['location']);
			}
			echo '</span>
	  				<span class="title">'.$valitem['title'].'</span>
	  				<span class="info">'.(($valitem['duration'])? $valitem['duration']:'--:--:--').'</span>
	  				<span class="action"><a href="javascript:void(0);" class="delItem" num="'.$i.'" onClick="delItemClick($(this).parent().parent().attr(\'id\'));"></a></span>
	  			</li>'."\n";
		}
	} else {
		$data = $suararadio->loadPlaylist($listid);
		if (count($data['items'])>0) {
			$_SESSION[SR_PLAYLIST_ID] = $data['playlist']['list_id'];
			$_SESSION[SR_PLAYLIST_NAME] = $data['playlist']['list_name'];
			$_SESSION[SR_PLAYLIST_VAR] = array();
			for($i=0;$i<count($data['items']);$i++) {
				$tmpd = $data['items'][$i];
				echo '<li id="'.$i.'"><span class="player">';
				if ($tmpd['item_meta']['type']=='melon') {
					$nmfile = suararadio_showMiniMelonPlay($tmpd['item_meta']['songid']);
				} else {
					$nmfile = suararadio_showMiniUrlPlay($tmpd['item_meta']['location']);
				}
				echo '</span>
	  				<span class="title">'.$tmpd['item_meta']['title'].'</span>
	  				<span class="info">'.(($tmpd['item_meta']['duration'])? $tmpd['item_meta']['duration']:'--:--:--').'</span>
	  				<span class="action"><a href="javascript:void(0);" class="delItem" num="'.$i.'" onClick="delItemClick($(this).parent().parent().attr(\'id\'));"></a></span>
	  			</li>'."\n";
	  		$_SESSION[SR_PLAYLIST_VAR][] = $tmpd['item_meta'];
			}
		}
	}
} //endfunc playlist_item_load

function playlist_reorder() {
	$playlist = $_SESSION[SR_PLAYLIST_VAR];
	$newplaylist = array();
	if ($_POST['list']) foreach ($_POST['list'] as $vi) {
		$newplaylist[] = $playlist[intval($vi)];
	}
	$_SESSION[SR_PLAYLIST_VAR] = $newplaylist;
	return true;
}

function playlist_clear() {
	$_SESSION[SR_PLAYLIST_VAR]= array();
	$arres = array('status'=>"ok");
	print json_encode($arres);
	return true;
}

function playlist_save() {
	global $suararadio;
	
	$params = array();
	if ($_POST['listid']!='') {
		$params['list_id'] = $_POST['listid'];
	} else {
		$params['name'] = $_POST['name'];
	}
	$arres = $suararadio->savePlaylist($params);
	print json_encode($arres);
	return true;
}

function suararadio_play_lagu($path) {
	global $wprisedb, $current_user;
	global $suararadio;
	
	// check login user, check juga hak akses untuk mengambil lagu
	if ($current_user->ID==null) {
		status_header('401');
		return false;
	}	
	
	$filename = $suararadio->convertPodcastWebPathToLagu($path[3]);
	#echo "xxxx".$filename."yyy";
	if ($filename!='') {
		// perlu dicek koneksi ke servernya bisa tidak.
		$copts = array(
			CURLOPT_CONNECTTIMEOUT => 10,
    	CURLOPT_RETURNTRANSFER => true,
    	CURLOPT_TIMEOUT        => 60,
    	CURLOPT_URL => $filename,
    	CURLOPT_CAINFO => SUARARADIO_PLUGIN_DIR.'/suararadio.crt',
    );
    
    $ch = curl_init();
    curl_setopt_array($ch, $copts);
    $result = curl_exec($ch);
    curl_close($ch);
    
		#var_dump($result);
		
		#$hfile = fopen ($filename, "r");
		if (!$result) {
			status_header('404');
			return false;
		} else {
			status_header('200');
			$headers = get_headers($filename, 1);
			$content_type = $headers['Content-Type'];
			$filesize = $headers['Content-Length'];
			/*
			header("Pragma: ");
			header("Cache-Control: ");
			header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
			header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
			header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1
			header("Cache-Control: post-check=0, pre-check=0", false);
			header('Content-Disposition: attachment; filename="'.$filename.'"');
			header("Content-Description: ".trim(htmlentities($filename)));
			header("Connection: close");
			header("Content-Transfer-Encoding: binary");
			*/
			
			header("Connection: Keep-Alive");
			header("X-ForcedBy: suararadio");
			header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
			header('Content-type: '.$content_type);
			
			header('Content-Length: '.$filesize);
			set_time_limit(0);
			echo $result;
/*
			$chunksize = 1*(1024*1024); // how many bytes per chunk
			while (!feof($hfile) && connection_status()==0) {
				echo fread($hfile, $chunksize);
				ob_flush();
				flush();
			}
			fclose($hfile);
*/
		}
	} else {
		status_header('404');
	}
} #endfunc suararadio_play_lagu

function isMobileAgent() {
	static $mua = null;
	if (!isset($mua)) {
		include_once(WP_PLUGIN_DIR.'/suararadio/includes/mobileagent/MobileUserAgent.php');
		$mua = new MoblieUserAgent();
	}
	return $mua->success();
} #endfunc isMobileAgent

function suararadio_show_player($params=array()) {
	$playlist = "http://".$_SERVER["HTTP_HOST"]."/suararadio_api/show_playlist/playlist.xspf";
	#$playlist = "http://".$_SERVER["HTTP_HOST"]."/category/news/feed/xspf/";

$id = (isset($params["nofloat"]) && $params["nofloat"]==true)? "suararadio_player_block":"suararadio_player_container";
$width = (isset($params["width"]) && $params["width"]==true)? $params["width"]:"180";
$height = (isset($params["height"]) && $params["height"]==true)? $params["height"]:"100";
#var_dump($params,$width,$height);
	print "<div id=\"$id\">\n";
	print "<div align=\"right\"><a href=\"#\" onclick=\"suararadio_show_player();\"><image src=\"".plugins_url('suararadio/images/ipod.png')."\" width=\"12px\" height=\"12px\"/></a></div>";
	print "<div id=\"suararadio_player\">\n";
	print "<p>Suara Radio Player</p>\n";
	print "</div>\n";
	print "<div id=\"suararadio_player_menu\">\n";
	print "<a href=\"#\" onclick=\"suararadio_playlist_clear();suararadio_create_player('suararadio_player','$playlist','$width','$height');\"><span id=\"suararadio_action\">Clear</span></a><span id=\"suararadio_separator\"> | </span>\n";
	print "<a href=\"#\" onclick=\"suararadio_create_player('suararadio_player','$playlist','$width','$height');\"><span id=\"suararadio_action\">Refresh</span></a><span id=\"suararadio_separator\"> | </span>\n";
	print "<a href=\"#\" onclick=\"suararadio_hide_player();\"><span id=\"suararadio_action\">Hide</span></a>\n";
	print "</div>\n";
	print "</div>\n";
	print "<script type=\"text/javascript\">\n";
	#print "swfobject.embedSWF(\"/wp-content/plugins/suararadio/xspf_player.swf?autoload=true&repeat_playlist=true&player_title=e-Broadcasting Institute&playlist_url=$playlist\", \"suararadio_player\", \"$width\", \"$height\", \"9.0.0\",\"wp-content/plugins/suararadio/js/swfobject/expressInstall.swf\");\n";
	print "suararadio_create_player('suararadio_player','$playlist','$width','$height');\n";
	print "</script>\n";
	
	#print "<object type=\"application/x-shockwave-flash\" width=\"180\" height=\"100\" data=\"http://".$_SERVER["HTTP_HOST"]."/wp-content/plugins/xspf_player/xspf_player.swf?autoload=true&player_title=e-Broadcasting Institute&playlist_url=http://".$_SERVER["HTTP_HOST"]."/suararadio_api/show_playlist/playlist.xspf\">";
	#print "<param name=\"movie\" value=\"http://".$_SERVER["HTTP_HOST"]."/wp-content/plugins/xspf_player/xspf_player.swf?autoload=true&player_title=e-Broadcasting Institute&playlist_url=http://".$_SERVER["HTTP_HOST"]."/suararadio_api/show_playlist/playlist.xspf\" />";
	#print "<param name=\"player_title\" value=\"e-Broadcasting Institute\" />";
	#print "</object>";
}

function suararadio_show_player2($params=array()) {
	$playlist = "http://".$_SERVER["HTTP_HOST"]."/suararadio_api/show_playlist/playlist.xspf";
	#$playlist = "http://".$_SERVER["HTTP_HOST"]."/category/news/feed/xspf/";

$id = (isset($params["nofloat"]) && $params["nofloat"]==true)? "suararadio_player_block":"suararadio_player_container";
$width = (isset($params["width"]) && $params["width"]==true)? $params["width"]:"180";
$height = (isset($params["height"]) && $params["height"]==true)? $params["height"]:"100";
	print "<div id=\"$id\" style=\"display: none;\">\n";
	//print "<div align=\"right\"><a href=\"#\" onclick=\"suararadio_show_player();\"><image src=\"".plugins_url('suararadio/images/ipod.png')."\" width=\"12px\" height=\"12px\"/></a></div>";
	print "<div id=\"suararadio_player\">\n";
	print "<p>Suara Radio Player</p>\n";
	print "</div>\n";
	print "<div id=\"suararadio_player_menu\">\n";
	print "<a href=\"#\" onclick=\"suararadio_playlist_clear();suararadio_create_player('suararadio_player','$playlist','$width','$height');\"><span id=\"suararadio_action\">Clear</span></a><span id=\"suararadio_separator\"> | </span>\n";
	print "<a href=\"#\" onclick=\"suararadio_create_player('suararadio_player','$playlist','$width','$height');\"><span id=\"suararadio_action\">Refresh</span></a><span id=\"suararadio_separator\"> | </span>\n";
	print "<a href=\"#\" id=\"mnuPlaylistHide\"><span id=\"suararadio_action\">Hide</span></a>\n";
	print "</div>\n";
	print "</div>\n";
	print "<script type=\"text/javascript\">\n";
	print "suararadio_create_player('suararadio_player','$playlist','$width','$height');\n";
	print "
		var offset = jQuery('#mnuPlaylist').offset();
		var height = jQuery('#mnuPlaylist').height();

		jQuery('#suararadio_player_container').css('position','absolute');
		jQuery('#suararadio_player_container').css('top',offset.top+height+6);
		jQuery('#suararadio_player_container').css('left',offset.left);
		jQuery('#mnuPlaylistHide').click(function(){
			jQuery('#suararadio_player_container').hide();
		});
		jQuery('#mnuPlaylist').click(function(){
			jQuery('#suararadio_player_container').toggle();
		});
	";
	print "</script>\n";
}

function suararadio_play_streaming_onair() {  
	echo "<a href=\"/streaming.php\" class=\"streamplayer\"><img src=\"".plugins_url('suararadio/images/ebi.png')."\" align=\"absmiddle\"></a>\n";
	echo "<a href=\"/streaming.php?type=.pls\"><img src=\"".plugins_url('suararadio/images/pls4.png')."\" align=\"absmiddle\"></a>\n";
	echo "<a href=\"/streaming.php?type=.m3u\"><img src=\"".plugins_url('suararadio/images/m3u5.png')."\" align=\"absmiddle\"></a>\n";
	echo "<a href=\"/streaming.php?type=.asx\"><img src=\"".plugins_url('suararadio/images/wmp.png')."\" align=\"absmiddle\"></a>\n";
}

function suararadio_play_streaming_onair_mobile_bb() {
	echo "<p class=\"playlist-icon-streaming hide\" id=\"id-playlist-streaming1\">";
	echo "<a href=\"/streaming.php\" class=\"streamplayer\"><img src=\"".plugins_url('suararadio/images/ebi.png')."\" align=\"absmiddle\"></a>&nbsp;";
	echo "<a href=\"\" onClick=\"stream1();\"><img src=\"".plugins_url('suararadio/images/pls4.png')."\" align=\"absmiddle\"></a>&nbsp;";
	echo "</p>";
	echo "<p class=\"icon_streaming\">";
	echo "<a class=\"icon\"><img src=\"".plugins_url('suararadio/images/live_black.png')."\"/></a>";
	echo "</p>";
	echo "<p class=\"playlist-icon-streaming\" id=\"id-playlist-streaming2\">";
	echo "<a href=\"\" onClick=\"stream2();\"><img src=\"".plugins_url('suararadio/images/m3u5.png')."\" align=\"absmiddle\"></a>&nbsp;";
	echo "<a href=\"\" onClick=\"stream3();\"><img src=\"".plugins_url('suararadio/images/wmp.png')."\" align=\"absmiddle\"></a>&nbsp;";
	echo "</p>";
}

function suararadio_play_streaming_onair_mobile($kodeUserAgent, $urlstreaming) {
	$kode = $kodeUserAgent;
	echo "<p class=\"playlist-icon-streaming hide\" id=\"id-playlist-streaming1\">";
	if ($kode != 'i'){
		echo "<a href=\"/streaming.php\" class=\"streamplayer\"><img src=\"".plugins_url('suararadio/images/ebi.png')."\" align=\"absmiddle\"></a>&nbsp;";
	}else{
		echo "<a><img style = \"cursor: pointer;\" src=\"".plugins_url('suararadio/images/ebi.png')."\" class=\"liveStream_button\" audiofile=\"".$urlstreaming."\" align=\"absmiddle\"/></a>&nbsp;";
	}
	echo "<a href=\"/streaming.php?type=.pls\"><img src=\"".plugins_url('suararadio/images/pls4.png')."\" align=\"absmiddle\"></a>&nbsp;";
	echo "</p>";
	echo "<p class=\"icon_streaming\">";
	echo "<a class=\"icon\"><img src=\"".plugins_url('suararadio/images/live_black.png')."\"/></a>";
	echo "</p>";
	echo "<p class=\"playlist-icon-streaming\" id=\"id-playlist-streaming2\">";
	echo "<a href=\"/streaming.php?type=.m3u\"><img src=\"".plugins_url('suararadio/images/m3u5.png')."\" align=\"absmiddle\"></a>&nbsp;";
	echo "<a href=\"/streaming.php?type=.asx\"><img src=\"".plugins_url('suararadio/images/wmp.png')."\" align=\"absmiddle\"></a>&nbsp;";
	echo "</p>";
}

function suararadio_show_links($feedpath,$params=array()) {
    // $params['imgWidth']
	// $params['imgHeight']
	$ih = ($params['imgHeight'])?$params['imgHeight']:'25px';
	$iw = ($params['imgWidth'])?$params['imgWidth']:'25px';
	$style = ($params['style'])? 'style="'.$params['style'].'"':'';
	echo "<a href=\"/".$feedpath."/feed/playlist.pls\"><img src=\"".plugins_url('suararadio/images/pls4.png')."\" align=\"absmiddle\" width=\"$iw\" height=\"$ih\" $style></a>\n";
	echo "<a href=\"/".$feedpath."/feed/playlist.m3u\"><img src=\"".plugins_url('suararadio/images/m3u5.png')."\" align=\"absmiddle\" width=\"$iw\" height=\"$ih\" $style></a>\n";
	echo "<a href=\"/".$feedpath."/feed/playlist.asx\"><img src=\"".plugins_url('suararadio/images/wmp.png')."\" align=\"absmiddle\" width=\"$iw\" height=\"$ih\" $style></a>\n";
	echo "<a href=\"/".$feedpath."/feed/podcast\"><img src=\"".plugins_url('suararadio/images/ipod_cast.png')."\" align=\"absmiddle\" width=\"$iw\" height=\"$ih\" $style></a>\n";
}

/*function suararadio_show_addlink($post_id,$pos=false,$params=array()) {
	$ih = ($params['imgHeight'])?$params['imgHeight']:'14px';
	$iw = ($params['imgWidth'])?$params['imgWidth']:'14px';
	$style = ($params['style'])? 'style="'.$params['style'].'"':'';
	if (!SUARARADIO_MEMBER) {
		echo " <a href=\"javascript:suararadio_playlist_add('$post_id','$pos');\"><img src=\"".plugins_url('suararadio/images/add.png')."\" align=\"absmiddle\" width=\"$iw\" height=\"$ih\" $style></a>";
	} else if (SUARARADIO_MEMBER && is_user_logged_in()) {
		echo " <a href=\"javascript:suararadio_playlist_add('$post_id','$pos');\"><img src=\"".plugins_url('suararadio/images/add.png')."\" align=\"absmiddle\" width=\"$iw\" height=\"$ih\" $style></a>";
	}
}*/
function suararadio_show_addlink($post_id,$pos=false,$params=array(), $out = false) {
	$ih = ($params['imgHeight'])?$params['imgHeight']:'25px';
	$iw = ($params['imgWidth'])?$params['imgWidth']:'25px';
	$style = ($params['style'])? 'style="'.$params['style'].'"':'style="width: 25px; height: 25px;"';
	
	$icon = " <a class=\"addPlaylist\" $style href=\"javascript:suararadio_playlist_add('$post_id','$pos');\"></a>";
	if($out){		
		if (!SUARARADIO_MEMBER) {
			return $icon;
		} else if (SUARARADIO_MEMBER && is_user_logged_in()) {
			return $icon;
		}
	}else{
		if (!SUARARADIO_MEMBER) {
			//echo " <a href=\"javascript:suararadio_playlist_add('$post_id','$pos');\"><img src=\"".plugins_url('suararadio/images/add.png')."\" align=\"absmiddle\" width=\"$iw\" height=\"$ih\" $style></a>";
			echo $icon;
		} else if (SUARARADIO_MEMBER && is_user_logged_in()) {
			//echo " <a href=\"javascript:suararadio_playlist_add('$post_id','$pos');\"><img src=\"".plugins_url('suararadio/images/add.png')."\" align=\"absmiddle\" width=\"$iw\" height=\"$ih\" $style></a>";
			echo $icon;
		}
	}	
}


function suararadio_show_addlink_membership($post_id,$pos=false,$params=array(), $out = false) {
	$ih = ($params['imgHeight'])?$params['imgHeight']:'25px';
	$iw = ($params['imgWidth'])?$params['imgWidth']:'25px';
	$style = ($params['style'])? 'style="'.$params['style'].'"':'';
	
	$icon = " <a href=\"javascript:suararadio_playlist_add('$post_id','$pos');\"><img src=\"".plugins_url('suararadio/images/add.png')."\" align=\"absmiddle\" width=\"$iw\" height=\"$ih\" $style></a>";
	if($out){		
		if (!SUARARADIO_MEMBER) {
			return $icon;
		} else if (SUARARADIO_MEMBER && is_user_logged_in()) {
			return $icon;
		}
	}else{
		if (!SUARARADIO_MEMBER) {
			//echo " <a href=\"javascript:suararadio_playlist_add('$post_id','$pos');\"><img src=\"".plugins_url('suararadio/images/add.png')."\" align=\"absmiddle\" width=\"$iw\" height=\"$ih\" $style></a>";
			echo $icon;
		} else if (SUARARADIO_MEMBER && is_user_logged_in()) {
			//echo " <a href=\"javascript:suararadio_playlist_add('$post_id','$pos');\"><img src=\"".plugins_url('suararadio/images/add.png')."\" align=\"absmiddle\" width=\"$iw\" height=\"$ih\" $style></a>";
			echo $icon;
		}
	}	
}


function suararadio_get_xspf() {
	global $suararadio;
	$suararadio->setSessData();

	header("HTTP/1.0 200 OK");
	header('Content-type: application/xspf+xml; charset=' . get_settings('blog_charset'), true);
	
	$more = 1;
	
	echo '<?xml version="1.0" encoding="'.get_settings('blog_charset').'" ?'.">\n";
	echo '<playlist version="0" xmlns="http://xspf.org/ns/0/">'."\n";
	echo "  <title>"; bloginfo_rss('name'); echo "</title>\n";
	echo "  <annotation></annotation>\n";
	echo "  <creator>"; the_author(); echo "</creator>\n";
	echo "  <location>"; bloginfo_rss('url'); echo "</location>\n";
	echo "  <license>http://creativecommons.org/licenses/by-sa/1.0/</license>\n";
	echo "  <trackList>\n";
	$playlist = $suararadio->getSessionList();
	if ($playlist) {
		foreach ($playlist as $vlist) {
			echo "    <track>\n";
			echo "      <location>".$vlist['location']."</location>\n";
			echo "      <annotation>".$vlist['annotation']."</annotation>\n";
			echo "      <title>".$vlist['title']."</title>\n";
			echo "      <image>".$vlist['image']."</image>\n";
			echo "    </track>\n";
		}
	}
	echo "  </trackList>\n";
	echo "</playlist>\n";
#	return;
} #endfunc suararadio_get_xspf

function suararadio_get_asx() {
	global $suararadio;
	
	$suararadio->setSessData();
	
	header("HTTP/1.0 200 OK");
	#header('Content-Type: text/xml; charset=' . get_option('blog_charset'), true);
	#header('Content-type: application/xspf+xml; charset=' . get_settings('blog_charset'), true);
	header('Content-type: video/x-ms-asf', true);
	header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	
	$more = 1;
	
	echo '<ASX version = "3.0">'."\n";
	echo '<TITLE>'.bloginfo_rss('name').'</TITLE>'."\n";
	$playlist = $suararadio->getSessionList();
	if ($playlist) {
		foreach ($playlist as $vlist) {
			echo '  <ENTRY>'."\n";
			echo '    <TITLE>'.$vlist['title'].'</TITLE>'."\n";
			echo '    <REF HREF="'.$vlist['location'].'" />'."\n";
			echo '    <AUTHOR>e-Broadcasting Institute</AUTHOR>'."\n";
			echo '  </ENTRY>'."\n";
		}
	}
	echo '</ASX>'."\n";
	#	return;
} #endfunc suararadio_get_asx

function suararadio_get_m3u() {
	global $suararadio;
	
	$suararadio->setSessData();
	
	header("HTTP/1.0 200 OK");
	header('Content-type: audio/x-mpegurl; charset=' . get_settings('blog_charset'), true);
	header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

	echo '#EXTM3U'."\n";
	$playlist = $suararadio->getSessionList();
	if ($playlist) {
		foreach ($playlist as $vlist) {
			echo "#EXTINF:-1,".$vlist['title']."\n";
			echo $vlist['location']."\n";
		}
	}
#	return;
} #endfunc suararadio_get_m3u

function suararadio_get_pls() {
	global $suararadio;
       
    $suararadio->setSessData();
	
	header("HTTP/1.0 200 OK");
	header('Content-type: audio/x-scpls', true);
	header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header('Connection: keep-alive');

		
	$playlist = $suararadio->getSessionList();
	echo "[playlist]\n";
	echo "NumberOfEntries=".count($playlist)."\n\n";
	if ($playlist) {
		$i = 1;	
		foreach ($playlist as $vlist) {
			echo "File".$i."=".$vlist['location']."\n";
			echo "Title".$i."=".$vlist['title']."\n";
			echo "Length".$i."=-1"."\n\n";
			$i++;
		}
	}
	echo "Version=2"."\n";
#	return;
} #endfunc suararadio_get_pls

function suararadio_do_feed_xspf() {
  header("HTTP/1.0 200 OK");
  header('Content-type: application/xspf+xml; charset=' . get_settings('blog_charset'), true);

  if ( file_exists(TEMPLATEPATH . '/feed-xspf.php') ) {
  	load_template( TEMPLATEPATH . '/feed-xspf.php' );
  } else {
	load_template( SUARARADIO_PLUGIN_DIR. '/feed-xspf.php' );
  }
} #endfunc suararadio_do_feed_xspf

function suararadio_do_feed_asx() {
  header('Content-type: video/x-ms-asf', true);
  header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
  header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

  if ( file_exists(TEMPLATEPATH . '/feed-asx.php') ) {
  	load_template( TEMPLATEPATH . '/feed-asx.php' );
  } else {
	load_template( SUARARADIO_PLUGIN_DIR. '/feed-asx.php' );
  }
} #endfunc suararadio_do_feed_xspf

function suararadio_do_feed_m3u() {
  header('Content-type: audio/x-mpegurl; charset=' . get_option('blog_charset'), true);
  header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
  header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

  if ( file_exists(TEMPLATEPATH . '/feed-m3u.php') ) {
  	load_template( TEMPLATEPATH . '/feed-m3u.php' );
  } else {
	load_template( SUARARADIO_PLUGIN_DIR. '/feed-m3u.php' );
  }
} #endfunc suararadio_do_feed_m3u

function suararadio_do_feed_pls() {
  header('Content-type: audio/x-scpls', true);
  header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
  header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

  if ( file_exists(TEMPLATEPATH . '/feed-pls.php') ) {
  	load_template( TEMPLATEPATH . '/feed-pls.php' );
  } else {
	load_template( SUARARADIO_PLUGIN_DIR. '/feed-pls.php' );
  }
} #endfunc suararadio_do_feed_pls

function suararadio_encode($str) {
  return $str;
}

function suararadio_decode($str) {
  return $str;
}

/**
 * dependecy: podpress suararadio.
 */
function suararadio_showMiniPlay($postId=false) {
		global $post,$current_user,$podPress;
		
		$postId = ($postId)? $postId: $post->ID;
		$list = podPress_getAvailablePodcast($postId);
		$cuid = $current_user->ID;
		$caps = array("free"=>true);
		if ($cuid!=0) $caps = array_merge($caps,$current_user->allcaps);
		
		$tmp = array();
		foreach ($caps as $key=>$val) {
			$tmp = is_array($list[$key])? array_merge($tmp,$list[$key]):$tmp;
		}
		$str = implode(",",$tmp);		
		if ($str!="") {
			#echo '<a href="#" onclick="javascript: podPressPopupPlayer(\'0\',\''.$str.'\',300,10); return false;"><img width="12" height="12" src="'.SUARARADIO_PLUGIN_URL.'/images/spk.gif"></a>'."\n";
			//echo '<a href="'.$str.'" class="soundplayer"><img width="12" height="12" src="'.SUARARADIO_PLUGIN_URL.'/images/spk.gif"></a>'."\n";
			echo '<a href="'.$str.'" class="soundplayer"><img src="'.SUARARADIO_PLUGIN_URL.'/images/spk.png"></a>'."\n"; //
		} else {
			$keys = array_keys($podPress->getContentLevel());
			$n = 2; $cek = false;
			foreach ($keys as $vk) {
				if (!empty($list[$vk])) {
					echo '<img src="'.SUARARADIO_PLUGIN_URL.'/images/spk'.$n.'.png">'."\n";
					$cek = true;
					break;
				}
				$n++;
			}
			if (!$cek) echo '<img width="38" height="38" src="'.SUARARADIO_PLUGIN_URL.'/images/spk0.png">'."\n";		
		}
		return $str;
}

function suararadio_showMiniPlayResult($postId=false) {
		global $post,$current_user,$podPress;
		
		$postId = ($postId)? $postId: $post->ID;
		$list = podPress_getAvailablePodcast($postId);

		$cuid = $current_user->ID;
		$caps = array("free"=>true);
		if ($cuid!=0) $caps = array_merge($caps,$current_user->allcaps);
		
		$tmp = array();
		foreach ($caps as $key=>$val) {
			$tmp = is_array($list[$key])? array_merge($tmp,$list[$key]):$tmp;
		}
		$str = implode(",",$tmp);		
		if ($str!="") {
			#echo '<a href="#" onclick="javascript: podPressPopupPlayer(\'0\',\''.$str.'\',300,10); return false;"><img width="12" height="12" src="'.SUARARADIO_PLUGIN_URL.'/images/spk.gif"></a>'."\n";
			//echo '<a href="'.$str.'" class="soundplayer"><img width="12" height="12" src="'.SUARARADIO_PLUGIN_URL.'/images/spk.gif"></a>'."\n";
			$out = '<a href="'.$str.'" class="soundplayer"><img border="0" src="'.SUARARADIO_PLUGIN_URL.'/images/spk.png"></a>'."\n"; //
		} else {
			$keys = array_keys($podPress->getContentLevel());
			$n = 2; $cek = false;
			foreach ($keys as $vk) {
				if (!empty($list[$vk])) {
					$out = '<img border=\"0\" src="'.SUARARADIO_PLUGIN_URL.'/images/spk'.$n.'.png">'."\n";
					$cek = true;
					break;
				}
				$n++;
			}
			if (!$cek) $out = '<img border=\"0\" src="'.SUARARADIO_PLUGIN_URL.'/images/spk0.png">'."\n";		
		}
		return $out;
}

/**
 * suararadio lagu player
 * untuk dapat play lagu yang ada di database radio tersebut, maka perlu menjadi
 * member premium aja kali ya.
 */
 function suararadio_showMiniLaguPlay($lagurec) {
		global $current_user,$podPress;
		global $suararadio;
		
		$cuid = $current_user->ID;
		$caps = array("free"=>true);
		if ($cuid!=0) $caps = array_merge($caps,$current_user->allcaps);
		
		//check lagu id dengan hak akses user;
		
		$str = $suararadio->convertLaguToPodcastWebPath($lagurec['id_lagu']);
		if ($str!="") {
			//echo '<a href="#" onclick="javascript: podPresages/spk.gif"></a>'."\n";
			echo '<a href="'.$str.'" class="soundplayer"><img src="'.SUARARADIO_PLUGIN_URL.'/images/spk.png"></a>'."\n";
		} else {
			echo '<img src="'.SUARARADIO_PLUGIN_URL.'/images/spk3.png">'."\n";		
		}
		return $str;
}

function suararadio_showMiniMelonPlay($songId) {
	global $current_user,$podpress;
	global $suararadio;
	
	$cuid = $current_user->ID;
	
	$caps = array("free"=>true);
	if ($cuid!=0) $caps = array_merge($caps,$current_user->allcaps);
	
	//check lagu id dengan hak akses user;
	
	$str = $suararadio->convertMelonToPodcastWebPath($songId);
	if ($str!="") {
		//echo '<a href="#" onclick="javascript: podPresages/spk.gif"></a>'."\n";
		echo '<a href="'.$str.'" songid="'.$songId.'" class="soundplayer melon"><img src="'.SUARARADIO_PLUGIN_URL.'/images/spk.png"></a>'."\n";
	} else {
		echo '<img src="'.SUARARADIO_PLUGIN_URL.'/images/spk3.png">'."\n";
	}
	return $str;
}

/**
 * suararadio file player player
 * unsPopupPlayer(\'0\',\''.$str.'\',300,10); return false;"><img width="12" height="12" src="'.SUARARADIO_PLUGIN_URL.'/images/spk.png"></a>'."\n";
			echo '<a href="'.$str.'" class="soundplayer"><img width="12" height="12" src="'.SUARARADIO_PLUGIN_URL.'/imtuk dapat play lagu yang ada di database radio tersebut, maka perlu menjadi
 * member premium aja kali ya.
 */
 function suararadio_showMiniUrlPlay($url) {
		global $current_user,$podPress;
		global $suararadio;
		
		$cuid = $current_user->ID;
		$caps = array("free"=>true);
		if ($cuid!=0) $caps = array_merge($caps,$current_user->allcaps);
		
		// check url dengan hak akses
		## 
		
		if ($url!="") {
			echo '<a href="'.$url.'" class="soundplayer"><img src="'.SUARARADIO_PLUGIN_URL.'/images/spk.png"></a>'."\n"; //
		} else {
			echo '<img src="'.SUARARADIO_PLUGIN_URL.'/images/spk.png">'."\n";		
		}
		return $url;
}

function suararadio_tes() {
	global $current_user;
	
	#var_dump($current_user);
	exit;
}

function suararadio_melon_connect() {
	global $current_user;
	global $suararadio;
	global $arr_var_upd;
	
	include_once 'suararadio.api.class.php';
	$api = new SuararadioAPI();
	$api->init();
	
	$result = array();
	
	if ($_POST['username']!='' && $_POST['password']!='') {
		#$muser = $suararadio->mapi->loginUser($_POST['username'], $_POST['password']);
		$vars = (array)$current_user->data;
		$vars['username'] = $_POST['username'];
		$vars['password'] = $_POST['password'];
		
		$muser = $api->melonConnect($vars);
		if ($muser['code']=='1') {
			if (function_exists('update_user_meta')) {
				update_user_meta($current_user->ID,'melon_id',$muser['melon_id']);
				update_user_meta($current_user->ID,'melon_uid',$muser['melon_uid']);
				update_user_meta($current_user->ID,'melon_pwd',$muser['melon_pwd']);
				update_user_meta($current_user->ID,'melon_exp',$muser['melon_exp']);
				update_user_meta($current_user->ID,'melon_stat',$muser['melon_stat']);
				update_user_meta($current_user->ID,'ext_expired',$muser['ext_expired']);
			} else {
				update_usermeta($current_user->ID,'melon_id',$muser['melon_id']);
				update_usermeta($current_user->ID,'melon_uid',$muser['melon_uid']);
				update_usermeta($current_user->ID,'melon_pwd',$muser['melon_pwd']);
				update_usermeta($current_user->ID,'melon_exp',$muser['melon_exp']);
				update_usermeta($current_user->ID,'melon_stat',$muser['melon_stat']);
				update_usermeta($current_user->ID,'ext_expired',$muser['ext_expired']);
			}
		}
		$result = $muser;
		echo json_encode($result);
	}
	exit;
}

function suararadio_promo_process() {
	global $current_user,$suararadio;
	global $arr_var_upd;
	
	include_once 'suararadio.api.class.php';
	$api = new SuararadioAPI();
	
	$vars = (array)$current_user->data;
	$vars['home_url'] = defined('API_CLIENT_HOME_URL')? API_CLIENT_HOME_URL:get_option("siteurl");
	
	$result = $api->klubPromo($vars);
	if ($result[code]=='1') {
		if (function_exists('update_user_meta')) {
			foreach ($arr_var_upd as $ky) {
				update_user_meta($current_user->ID,$ky,$result[$ky]);
			}
		} else {
			foreach ($arr_var_upd as $ky) {
				update_usermeta($current_user->ID,$ky,$result[$ky]);
			}
		}	
	}
	echo json_encode($result);
	exit;
}

function suararadio_voucher_process() {
	global $current_user,$suararadio;
	global $arr_var_upd;
	
	include_once 'suararadio.api.class.php';
	$api = new SuararadioAPI();
	$api->init();
	
	$vars = (array)$current_user->data;
	$vars['voucher'] = $_POST['voucher'];
	$vars['nokartu'] = $_POST['nokartu'];
	$vars['cvc'] = $_POST['cvc'];
	$vars['expkartu_bln'] = $_POST['expkartu_bln'];
	$vars['expkartu_thn'] = $_POST['expkartu_thn'];
	$vars['home_url'] = defined('API_CLIENT_HOME_URL')? API_CLIENT_HOME_URL:get_option("siteurl");
	#$vars['home_url'] = "http://www.1071klitefm.com";
	
	$result = $api->klubVoucher($vars);
	#var_dump($result);
	if ($result[code]=='1' || $result[code]=='2' || $result[code]=='3') {
		// update data local
		if (function_exists('update_user_meta')) {
			foreach ($arr_var_upd as $ky) {
				update_user_meta($current_user->ID,$ky,$result[$ky]);
			}
		} else {
			foreach ($arr_var_upd as $ky) {
				update_usermeta($current_user->ID,$ky,$result[$ky]);
			}
		}
	}
	echo json_encode($result);
	exit;
} //endfunc suararadio_voucher_process

function suararadio_melon_create() {
	global $current_user;
	global $suararadio;
	
	if (trim($current_user->email)=='') {
		echo "Email belum disetting";
	} else {
		$suararadio->mapi->createUser();
	}
	exit;
}

function suararadio_delima_process() {
	global $current_user;
	global $suararadio;
	
	include_once 'suararadio.api.class.php';
	$api = new SuararadioAPI();
	
	$vars = (array)$current_user->data;
	$vars['home_url'] = defined('API_CLIENT_HOME_URL')? API_CLIENT_HOME_URL:get_option("siteurl");
	#$vars['home_url'] = "http://www.1071klitefm.com";
	$vars['paket'] = $_POST['paket'];

	$result = $api->delimaMembership($vars);
	if ($result[code]=='1') {
		// tambahkan link pembayaran
		$result['link'] = $api->getDelimaLink($result);
	}
	echo json_encode($result);
	exit;
}

function suararadio_callback_process() {
	include_once 'suararadio.api.class.php';
	$api = new SuararadioAPI();
	
	
	exit;
} //endfunc suararadio_callback_process

function suararadio_get_profile_photo($user_id=null,$mode='normal') {
	global $current_user;
	
	$user = $current_user;
	if ($user->Facebook!='') {
		$img_profile_url = "https://graph.facebook.com/".$user->Facebook."/picture?type=".$mode;
	} else if ($user->wsl_user_image) {
		$img_profile_url = $user->wsl_user_image;
	} else {
		$img_profile_url = plugins_url('suararadio/images/profile.png');
	}
	return $img_profile_url;
} 

/**
 * buat button like fb :p
 * $pSiteUrl : site u like, layout : (box_count, standard, 
 */
/*function button_like($pSiteUrl, $widt='200'){	
	echo "<iframe src=\"http://www.facebook.com/plugins/like.php?href=".$pSiteUrl."&layout=standard&show_faces=false&width=250&action=like&colorscheme=light\" scrolling=\"no\" frameborder=\"0\" style=\"border:none; overflow:hidden; width:".$widt."px; height:35px;\" allowTransparency=\"true\"></iframe>";
}*/

function button_like($url = ''){
	echo "<div class=\"fb-like\" data-href=\"$url\" data-send=\"false\" data-width=\"300\" data-show-faces=\"false\"></div>";
}

function tweet_button($url = ''){
	echo "<a href=\"".$url."\" class=\"twitter-share-button\" data-related=\"jasoncosta\" data-lang=\"en\" data-size=\"small\" data-count=\"none\">Tweet</a>";
}

function gp_button($url = ''){
	echo "<g:plusone size='medium' annotation='none'></g:plusone>";
}

function TuneInLog(){
	global $suararadio_connect;
	global $user_ID,$current_user;
	
	$options = &$suararadio_connect['options'];
	?>
	<div id="loginconnect">
	<?php
	#print_r($suararadio_connect);
	#echo "uid >> ".$suararadio_connect['facebook']['session']['uid'];
	$Tempuid = $suararadio_connect['facebook']['session']['uid'];
	
	if ($options['facebook']['active']) {
		$fbbutton = '';
		suararadio_get_fbuser();
		$fbuser = $suararadio_connect['facebook']['user'];
		//if (isset($user_ID) && $current_user->fbuid==$fbuser['id']) { 
		if ((isset($user_ID) && $current_user->fbuid==$fbuser['id']) || $Tempuid ) { 
			// lagi login
			$logouturl = $suararadio_connect['facebook']['obj']->getLogoutUrl();
			$fbbutton = '<a href="'.$logouturl.'"><img src="http://static.ak.fbcdn.net/rsrc.php/z2Y31/hash/cxrz4k7j.gif"></a>';
		} else if ($suararadio_connect['facebook']['user']!=null && !isset($user_ID)) { 
			$fbbutton = '<div><fb:login-button v="2" show-faces="true" size="small" perms="status_update,publish_stream, read_stream"></fb:login-button></div>';
			//$fbbutton = '<div class="fb_button" id="fb-login"><a href="javascript:fblogin();" class="fb_button_medium"><span id="fb_login_text" class="fb_button_text">Login</span></a></div>';
		} else {
			$fbbutton = '<div><fb:login-button v="2" show-faces="true" size="small" perms="status_update,publish_stream, read_stream"></fb:login-button></div>';
		} 
		?>
		<div id="fbconnect">
			<? if ($fbuser['id']) {?><img class="fbuser" src="https://graph.facebook.com/<?php echo $fbuser['id']; ?>/picture"><? } ?>
			<a class="fbuser" href="<?php echo $fbuser['link']; ?>"><?php echo $fbuser['name']; ?></a><br>
			<?php echo $fbbutton; ?>
		</div>
<?} ?>
	</div>
	<?php
}

/***** support staytune bisa play mp3 di facebook *****/
function get_first_image() {
	$first_img = '';
	$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
	$first_img = $matches[1][0];

	if(empty($first_img)){
		//Defines a default image
		$first_img = get_template_directory_uri()."/images/ad_2.png";
	}
	return $first_img;
}

function getImagePostStayTune($postId, $post){
	//global $wpdb, $wp_query, $suararadio_connect;
	$thumb = get_post_meta($postId, 'thumbkontenpremiumradio', $single = true);
	if($thumb)
	$imageLink = $thumb; // ambil image ruas thumbkontenpremiumradio
	else{
		$thumb = get_post_meta($postId, 'mostplayed', $single = true);
		if($thumb) $imageLink = $thumb; /// ambil image ruas mostplayed
		else{
			$thumb = get_post_meta($postId, 'advimage', $single = true);
			if($thumb) $imageLink = $thumb; // ambil image ruas advimage
			else $imageLink = get_first_image($post); // ambil image embed post konten
		}
	}
	return $imageLink;
}


function showIconCheckIn($postId, $params=array(), $CHECKINFBPLUGIN_URL, $uidFb,$premium = 0){
	global $wpdb, $wp_query, $suararadio_connect;
	$Tempuid = $suararadio_connect['facebook']['session']['uid'];			
	#var_dump($suararadio_connect['facebook']);
	//echo $Tempuid;
	$querystr = "
		SELECT wposts.* 
		FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta
		WHERE wposts.ID = wpostmeta.post_id 
		AND wposts.ID = ".$postId."
		ORDER BY wposts.post_date DESC
 	";

 	$pageposts = $wpdb->get_results($querystr, OBJECT);

	$isi = preg_replace('/((\\\\n)+)/'," ",preg_replace("/(\r\n|\n)/",'\\n',stripslashes($pageposts[0]->post_content)));  
	$jdl = preg_replace('/\"/'," ",preg_replace("/(\r\n|\n)/",'\\n',stripslashes($pageposts[0]->post_title)));  
	
	//$list = explode("/", $params['nmfilefb']);

  	if($_tmp= explode(",",$params['nmfilefb'])){
		$_file_counts = count($_tmp);
		$i=0;
		while(!eregi("(.+)\.mp3$",$_tmp[$i],$_regs) && ($i<$_file_counts))
			$i++;
		
		$list=explode("/",$_tmp[$i]);
		//$list= explode("/",$params['nmfilefb']);	
	}else
	 
 		$list = explode("/", $params['nmfilefb']);


	$isi = remove_HTML($isi);	
	$quotes = array('/"/',"/'/");
	//$replacements = array('%22','%27');
	$replacements = array('%22','%27');

	$isi = preg_replace($quotes,$replacements,$isi);
	
	$loc = "http://".$_SERVER["HTTP_HOST"]."/podpress_trac/play/".$postId."/0/".$list[(count($list)-1)];
	//$podcast = "http://www.citrawanodyaangkasafm.com/podpress_trac/play/".$postId."/0/fitri-jangan-buru-buru-0111.mp3";	
	$imageLink = getImagePostStayTune($postId, $pageposts);
	if(empty($Tempuid))
		//echo '<fb:login-button v="2" show-faces="false" size="small" scope="status_update,publish_stream, read_stream"><font size="1">Tune In</font></fb:login-button>';
		echo "<a class='f-tuned' onclick='cekFB();'></a>";
	else{
		echo "<a class='aStay' onClick='streamPublish(\"Stay-tune on ".htmlentities($params['message'], ENT_QUOTES)."\", \"".htmlentities($jdl, ENT_QUOTES)."\", \"".$isi."\", \"".htmlentities($jdl, ENT_QUOTES)."\", \"".$params['url']."\", \"".$loc."\", \"".$params['opturl']."\", \"".$params['mediafb']."\", \"".$params['message']."\", \"".$CHECKINFBPLUGIN_URL."\", \"".$uidFb."\", \"".$postId."\", \"".$imageLink."\");'>";
		echo "<img src='".get_bloginfo('template_url')."/images/f-tuned-c.png' border='0'>";
		echo "</a>";
		if($premium == 0){		
			echo "<div class=\"overlayFTune\">".		
	        "<p class=\"AddToPlaylist\" onclick=\"javascript:suararadio_playlist_add('$postId',0);\"></p>".                
	    "</div>";
	  }else{
	  	echo "<div class=\"overlayFTunePremium\">".		
	        "<p class=\"AddToPlaylistPremium\" onclick=\"javascript:suararadio_playlist_add('$postId',0);\"></p>".                
	    "</div>";
	  }
	}
		
		//21,45,28,38,29,45,21,45
		//echo "<a class=\"aStay\" onClick= \"streamPublish('Stay-tune on ".htmlentities($params['message'], ENT_QUOTES)."', '".htmlentities($jdl, ENT_QUOTES)."', '".htmlentities($isi, ENT_QUOTES)."', '".htmlentities($jdl, ENT_QUOTES)."', '".$params['url']."', '".$loc."', '".$params['opturl']."', '".$params['mediafb']."', '".$params['message']."', '".$CHECKINFBPLUGIN_URL."', '".$uidFb."', '".$postId."');\"><img src=\"".get_option('siteurl')."/wp-content/themes/k-lite/images/fb-logo.png\" height=\"15\"></a>";
			
		//echo "Materi ini bisa di share ke teman-teman Anda dengan cara klik <a class='aStay' onClick= 'streamPublish(\"Stay-tune on ".htmlspecialchars($params['message'], ENT_QUOTES)."\", \"".htmlspecialchars($jdl, ENT_QUOTES)."\", \"".htmlspecialchars($isi, ENT_QUOTES)."\", \"".htmlspecialchars($jdl, ENT_QUOTES)."\", \"".$params['url']."\", \"".$params['nmfilefb']."\", \"".$params['opturl']."\", \"".$params['mediafb']."\", \"".$params['message']."\", \"".$CHECKINFBPLUGIN_URL."\", \"".$uidFb."\", \"".$postId."\");'><img src='".get_option('siteurl')."/wp-content/themes/k-lite/images/fb-logo.png' height='15'></a> kemudian klik Publish";
}


function showIconCheckInResult($postId, $params=array(), $CHECKINFBPLUGIN_URL, $uidFb){
	global $wpdb, $wp_query, $suararadio_connect;
	$Tempuid = $suararadio_connect['facebook']['session']['uid'];			
	#var_dump($suararadio_connect['facebook']);
	//echo $Tempuid;
	$querystr = "
		SELECT wposts.* 
		FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta
		WHERE wposts.ID = wpostmeta.post_id 
		AND wposts.ID = ".$postId."
		ORDER BY wposts.post_date DESC
 	";

 	$pageposts = $wpdb->get_results($querystr, OBJECT);

	$isi = preg_replace('/((\\\\n)+)/'," ",preg_replace("/(\r\n|\n)/",'\\n',stripslashes($pageposts[0]->post_content)));  
	$jdl = preg_replace('/\"/'," ",preg_replace("/(\r\n|\n)/",'\\n',stripslashes($pageposts[0]->post_title)));  
	
	//$list = explode("/", $params['nmfilefb']);

  	if($_tmp= explode(",",$params['nmfilefb'])){
		$_file_counts = count($_tmp);
		$i=0;
		while(!eregi("(.+)\.mp3$",$_tmp[$i],$_regs) && ($i<$_file_counts))
			$i++;
		
		$list=explode("/",$_tmp[$i]);
		//$list= explode("/",$params['nmfilefb']);	
	}else
	 
 		$list = explode("/", $params['nmfilefb']);


	$isi = remove_HTML($isi);	
	$quotes = array('/"/',"/'/");
	//$replacements = array('%22','%27');
	$replacements = array('%22','%27');

	$isi = preg_replace($quotes,$replacements,$isi);
	
	$loc = "http://".$_SERVER["HTTP_HOST"]."/podpress_trac/play/".$postId."/0/".$list[(count($list)-1)];
	//$podcast = "http://www.citrawanodyaangkasafm.com/podpress_trac/play/".$postId."/0/fitri-jangan-buru-buru-0111.mp3";	
	$imageLink = getImagePostStayTune($postId, $pageposts);

	if(empty($Tempuid))
		//echo '<fb:login-button v="2" show-faces="false" size="small" scope="status_update,publish_stream, read_stream"><font size="1">Tune In</font></fb:login-button>';
		$out = "<a class='f-tuned' onclick='cekFB();'></a>";
	else
		$out = "<a class='aStay' onClick='streamPublish(\"Stay-tune on ".htmlentities($params['message'], ENT_QUOTES)."\", \"".htmlentities($jdl, ENT_QUOTES)."\", \"".$isi."\", \"".htmlentities($jdl, ENT_QUOTES)."\", \"".$params['url']."\", \"".$loc."\", \"".$params['opturl']."\", \"".$params['mediafb']."\", \"".$params['message']."\", \"".$CHECKINFBPLUGIN_URL."\", \"".$uidFb."\", \"".$postId."\", \"".$imageLink."\");'><img src='".get_bloginfo('template_url')."/images/f-tuned-c.png' border='0'></a>";		
	return $out;		
}

function showIconCheckInResult_kp($postId, $params=array(), $CHECKINFBPLUGIN_URL, $uidFb){
	global $wpdb, $wp_query, $suararadio_connect, $wpkontenpil;
	$Tempuid = $suararadio_connect['facebook']['session']['uid'];
	#var_dump($suararadio_connect['facebook']);
	//echo $Tempuid;
	$querystr = "
	SELECT wposts.*
	FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta
	WHERE wposts.ID = wpostmeta.post_id
            AND wposts.ID = ".$postId."
            ORDER BY wposts.post_date DESC
            ";
            
            $pageposts = $wpkontenpil->get_results($querystr, OBJECT);

	$isi = preg_replace('/((\\\\n)+)/'," ",preg_replace("/(\r\n|\n)/",'\\n',stripslashes($pageposts[0]->post_content)));
            $jdl = preg_replace('/\"/'," ",preg_replace("/(\r\n|\n)/",'\\n',stripslashes($pageposts[0]->post_title)));  

	//$list = explode("/", $params['nmfilefb']);
            
	if($_tmp= explode(",",$params['nmfilefb'])){
	$_file_counts = count($_tmp);
	$i=0;
	while(!eregi("(.+)\.mp3$",$_tmp[$i],$_regs) && ($i<$_file_counts))
		$i++;

		$list=explode("/",$_tmp[$i]);
		//$list= explode("/",$params['nmfilefb']);
	}else

		$list = explode("/", $params['nmfilefb']);


		$isi = remove_HTML($isi);
		$quotes = array('/"/',"/'/");
		//$replacements = array('%22','%27');
		$replacements = array('%22','%27');

		$isi = preg_replace($quotes,$replacements,$isi);

		$url = $_SERVER['HTTP_HOST'];
		if($url=='www.diradio.net'){
		$loc = "http://www.suararadio.com/podpress_trac/play/".$postId."/0/".$list[(count($list)-1)];
}else{
		$loc = "http://kontenpilihan.suararadio.com/podpress_trac/play/".$postId."/0/".$list[(count($list)-1)];
}

		//$podcast = "http://www.citrawanodyaangkasafm.com/podpress_trac/play/".$postId."/0/fitri-jangan-buru-buru-0111.mp3";
		$imageLink = getImagePostStayTune($postId, $pageposts);
            
            if(empty($Tempuid))
		//echo '<fb:login-button v="2" show-faces="false" size="small" scope="status_update,publish_stream, read_stream"><font size="1">Tune In</font></fb:login-button>';
		$out = "<a class='f-tuned' onclick='cekFB();'></a>";
		else
		if($url=='www.diradio.net'){
		$out = "<a class='aStay' onClick='streamPublish(\"Stay-tune on ".$_SERVER['HTTP_HOST']."\", \"".htmlentities($jdl, ENT_QUOTES)."\", \"".$isi."\", \"".htmlentities($jdl, ENT_QUOTES)."\", \"".$params['url']."\", \"".$loc."\", \"".$_SERVER['HTTP_HOST']."\", \"".$params['mediafb']."\", \"".'www.diradio.net'."\", \"".$CHECKINFBPLUGIN_URL."\", \"".$uidFb."\", \"".$postId."\", \"".$imageLink."\");'><img src='".get_template_directory_uri()."/images/f-tuned-c.png' border='0'></a>";
}else{
		$out = "<a class='aStay' onClick='streamPublish(\"Stay-tune on ".htmlentities($params['message'], ENT_QUOTES)."\", \"".htmlentities($jdl, ENT_QUOTES)."\", \"".$isi."\", \"".htmlentities($jdl, ENT_QUOTES)."\", \"".$params['url']."\", \"".$loc."\", \"".$params['opturl']."\", \"".$params['mediafb']."\", \"".$params['message']."\", \"".$CHECKINFBPLUGIN_URL."\", \"".$uidFb."\", \"".$postId."\", \"".$imageLink."\");'><img src='".get_template_directory_uri()."/images/f-tuned-c.png' border='0'></a>";
                }
            return $out;		
}

function suararadio_showMiniPlayResult_kp($postId=false) {
		global $post,$current_user,$podPress;
		
		$postId = ($postId)? $postId: $post->ID;
		$list = podPress_getAvailablePodcast_kp($postId);


		$cuid = $current_user->ID;
		$caps = array("free"=>true);
		if ($cuid!=0) $caps = array_merge($caps,$current_user->allcaps);
		
		$tmp = array();
		foreach ($caps as $key=>$val) {
			$tmp = is_array($list[$key])? array_merge($tmp,$list[$key]):$tmp;
		}
		$str = implode(",",$tmp);
		if ($str!="") {
		#echo '<a href="#" onclick="javascript: podPressPopupPlayer(\'0\',\''.$str.'\',300,10); return false;"><img width="12" height="12" src="'.SUARARADIO_PLUGIN_URL.'/images/spk.gif"></a>'."\n";
		//echo '<a href="'.$str.'" class="soundplayer"><img width="12" height="12" src="'.SUARARADIO_PLUGIN_URL.'/images/spk.gif"></a>'."\n";
		$out = '<a href="'.$str.'" class="soundplayer"><img border="0" src="'.SUARARADIO_PLUGIN_URL.'/images/spk.png"></a>'."\n"; //
		} else {
		$keys = array_keys($podPress->getContentLevel());
		$n = 2; $cek = false;
		foreach ($keys as $vk) {
		if (!empty($list[$vk])) {
		$out = '<img border=\"0\" src="'.SUARARADIO_PLUGIN_URL.'/images/spk'.$n.'.png">'."\n";
		$cek = true;
		break;
		}
		$n++;
		}
		if (!$cek) $out = '<img border=\"0\" src="'.SUARARADIO_PLUGIN_URL.'/images/spk0.png">'."\n";
		}
		return $out;
}



function showButtonLogin(){
	global $wpdb, $wp_query, $suararadio_connect;
	$Tempuid = $suararadio_connect['facebook']['session']['uid'];				
	$out = "";
	if(empty($Tempuid))		
		$out = "<a class='f-login' onclick='cekFB();'></a>";	
	else
		$out = "<div class='divsubmit'><input type='submit' name='submit5' value='SEND' class='submitMessage'></div>";	
	return $out;		
}

function showLastAct ($uidfb="", $limit='10'){
	global $wpdb, $wp_query;
	$querystr = "
		SELECT wposts.post_title, wposts.post_content, DATE_FORMAT(lfb.cdate, '%d %b %Y %H:%i') as cdate, lfb.ket, lfb.uidfb, wposts.guid, lfb.postid    
		FROM $wpdb->posts wposts, logfb lfb
		WHERE wposts.ID = lfb.postid 
		AND wposts.post_type = 'post' 
		AND wposts.post_status = 'publish' 
		".(empty($uidfb) ? "" : "AND lfb.uidfb = '". $uidfb."'")."
		ORDER BY lfb.cdate DESC 
		LIMIT 0, $limit
	 ";

	$pageposts = $wpdb->get_results($querystr, OBJECT);
	return $pageposts;
}

function showLastActSideBar ($limit='10', $postid){
	global $wpdb, $wp_query;
	$querystr = "
		SELECT wposts.post_title, wposts.post_content, DATE_FORMAT(lfb.cdate, '%d %b %Y %H:%i') as cdate, lfb.ket, lfb.uidfb, wposts.guid    
		FROM $wpdb->posts wposts, logfb lfb
		WHERE wposts.ID = lfb.postid 
		AND wposts.post_type = 'post' 
		AND wposts.post_status = 'publish'
		AND wposts.ID = '".$postid."' 
		ORDER BY lfb.cdate DESC 
		LIMIT 0, $limit
	 ";

 $pageposts = $wpdb->get_results($querystr, OBJECT);
 return $pageposts;
}
function remove_HTML($s , $keep = '' , $expand = 'script|style|noframes|select|option'){
	/**///prep the string
	$s = ' ' . $s;
   
	/**///initialize keep tag logic
	if(strlen($keep) > 0){
		$k = explode('|',$keep);
		for($i=0;$i<count($k);$i++){
			$s = str_replace('<' . $k[$i],'[{(' . $k[$i],$s);
			$s = str_replace('</' . $k[$i],'[{(/' . $k[$i],$s);
		}
	}
   
	//begin removal
	/**///remove comment blocks
	while(stripos($s,'<!--') > 0){
		$pos[1] = stripos($s,'<!--');
		$pos[2] = stripos($s,'-->', $pos[1]);
		$len[1] = $pos[2] - $pos[1] + 3;
		$x = substr($s,$pos[1],$len[1]);
		$s = str_replace($x,'',$s);
	}
   
	/**///remove tags with content between them
	if(strlen($expand) > 0){
		$e = explode('|',$expand);
		for($i=0;$i<count($e);$i++){
			while(stripos($s,'<' . $e[$i]) > 0){
				$len[1] = strlen('<' . $e[$i]);
				$pos[1] = stripos($s,'<' . $e[$i]);
				$pos[2] = stripos($s,$e[$i] . '>', $pos[1] + $len[1]);
				$len[2] = $pos[2] - $pos[1] + $len[1];
				$x = substr($s,$pos[1],$len[2]);
				$s = str_replace($x,'',$s);
			}
		}
	}
   
	/**///remove remaining tags
	while(stripos($s,'<') > 0){
		$pos[1] = stripos($s,'<');
		$pos[2] = stripos($s,'>', $pos[1]);
		$len[1] = $pos[2] - $pos[1] + 1;
		$x = substr($s,$pos[1],$len[1]);
		$s = str_replace($x,'',$s);
	}
   
	/**///finalize keep tag
	for($i=0;$i<count($k);$i++){
		$s = str_replace('[{(' . $k[$i],'<' . $k[$i],$s);
		$s = str_replace('[{(/' . $k[$i],'</' . $k[$i],$s);
	}
   
	return trim($s);
}

function penggalKata($title, $postid = "", $param1 = 40, $param2 = 75, $param3 = 100){
	#echo "$param1, $param2, $param3";
	$tmp = explode(" ",$title);
	$judul1 = "";
	$judul2 = "";
	$judul3 = "";
	
	$lengtotal = strlen($title);
	for($i=0;$i<count($tmp);$i++){
		$countstr += strlen($tmp[$i]);
		if($countstr <= $param1){
			$judul1 .= $tmp[$i]." ";																						
			$par1 = 1;
		}elseif($countstr > $param1 && $countstr <= $param2) {
			$judul2 .= $tmp[$i]." ";																						
		 	$par1 = 2;
		}elseif($countstr > $param2 && $countstr <= $param3) {
			$judul3 .= $tmp[$i]." ";																							
			$par1 = 3;
		}																																												
	}

	if($postid){
		if($par1 == 1) $judul1 .= suararadio_show_addlink($postid,'0','',true);
		elseif($par1 == 2) $judul2 .= suararadio_show_addlink($postid,'0','',true);
		elseif($par1 == 3) $judul3 .= suararadio_show_addlink($postid,'0','',true);
	}
	
	$out = array("judul1"=>$judul1,
		      "judul2"=>$judul2,
		      "judul3"=>$judul3);
	return $out;
}

function penggalKata2($title, $postid, $param1 = 46, $param2 = 80){
	$tmp = explode(" ",$title);
	$judul1 = "";
	$judul2 = "";
	$lengtotal = strlen($title);
	for($i=0;$i<count($tmp);$i++){
		$countstr += strlen($tmp[$i]);								
		if($countstr <= $param1){
			$judul1 .= $tmp[$i]." ";																						
			$par1 = 1;
		}elseif($countstr > $param1 && $countstr <= $param2) {
			$judul2 .= $tmp[$i]." ";																						
		 	$par1 = 2;
		}
	}
	if($par1 == 1) $judul1 .= suararadio_show_addlink($postid,'0','',true);
	elseif($par1 == 2) $judul2 .= suararadio_show_addlink($postid,'0','',true);
	
	$out = array("judul1"=>$judul1,
		      "judul2"=>$judul2
			);
	return $out;
}


function penggalKata3($title, $postid, $param1 = 46){
	$tmp = explode(" ",$title);
	$judul1 = "";
	$judul2 = "";
	$lengtotal = strlen($title);
	$masuk = 0;
	for($i=0;$i<count($tmp);$i++){
		$countstr += strlen($tmp[$i]);								
		if($countstr <= $param1){
			$judul1 .= $tmp[$i]." ";																						
			$par1 = 1;
		}else {
			$concat = "..";
			$masuk = 1;
		}
	}
	
	if($masuk == 0 && $lengtotal < 25) $concat = "<BR><BR>";	
	if($par1 == 1) $judul1 = $judul1." ".$concat;		
	$out = array("judul1"=>$judul1);
	return $out;
}


function switchIconMessage($jenisMedia, $creator){
	switch($jenisMedia){
		case "f" : $res = "<img class='overlay-img' src=\"".SUARARADIO_PLUGIN_URL."/images/fb.png\">";break;
		case "t" : $res = "<img class='overlay-img' src=\"".SUARARADIO_PLUGIN_URL."/images/twitter.png\">";break;
		case "c" : $res = "<img class='overlay-img' height='20' width='20' src=\"".SUARARADIO_PLUGIN_URL."/images/ebi.png\">";break;
		case "1" : $res = "<img class='overlay-img' height='20' width='20' src=\"".SUARARADIO_PLUGIN_URL."/images/sms.png\">";break;
		case "2" : $res = "<img class='overlay-img' height='20' width='20' src=\"".SUARARADIO_PLUGIN_URL."/images/sms.png\">";break;
		case "3" : 
			switch($creator){
				case "a": 
					$res = "<img height='20' width='20' class='overlay-img' src=\"".SUARARADIO_PLUGIN_URL."/images/android.png\">";	
				break;
				case "b":
					$res = "<img height='20' width='20' class='overlay-img' src=\"".SUARARADIO_PLUGIN_URL."/images/BB.png\">";	
				break;
				case "w":
					$res = "<img height='20' width='20' class='overlay-img' src=\"".SUARARADIO_PLUGIN_URL."/images/windows.png\">";	
				break;
				case "i":
					$res = "<img height='20' width='20' class='overlay-img' src=\"".SUARARADIO_PLUGIN_URL."/images/apple.png\">";	
				break;
				case "c":
					$res = "<img height='20' width='20' class='overlay-img' src=\"".SUARARADIO_PLUGIN_URL."/images/ebi.png\">";	
				break;
				default: 
					$res = "<img height='20' width='20' class='overlay-img' src=\"".SUARARADIO_PLUGIN_URL."/images/sms.png\">";
				break;
			}
			#$res = ($creator == 'c')? "<img class='overlay-img' height='20' width='20' src=\"".SUARARADIO_PLUGIN_URL."/images/ebi.png\">" : "<img height='20' width='20' class='overlay-img' src=\"".SUARARADIO_PLUGIN_URL."/images/sms.png\">";
		break;
	}
	return $res;	
}

function getLikeFacebook($ID = ''){	
	$url = "https://graph.facebook.com/".$ID;
#echo $url;
  $ch = curl_init();

  curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);

  $result = curl_exec($ch);
  $json_result = json_decode($result);	
  $likes = ($json_result->likes)? $json_result->likes : $json_result->shares;
	return $likes;
}


function getFollowerTweet($screen_name){
	$url = "https://api.twitter.com/1/users/show.json?screen_name=".$screen_name."&include_entities=true";
  $ch = curl_init();

  curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);

  $result = curl_exec($ch);
  $json_result = json_decode($result);	
  return $json_result->followers_count;	
}

function getPlayedAllTime(){
	global $wpdb, $wp_query;
	$sql = "select distinct(a.postid) as postid, sum(a.total) as jmlplay, b.ID,b.post_title ".
				 "from wp_podpress_statcounts a ".
				 "left join wp_posts b on b.id = a.postid ".
				 "group by a.postid ".
				 "order by sum(a.total) desc ".
				 "limit 1";				 	
	$mostplayedalltime = $wpdb->get_results($sql, OBJECT);																																					
	return $mostplayedalltime;
}

function getPlayedMonthly(){
	global $wpdb, $wp_query;	
	
	if(date("n") == 1){ $bulan = 12; $tahun = date("Y") - 1; }
	else{ $bulan = date("n") - 1; $tahun = date("Y"); }	
	$sql = "select distinct(a.postid) as postid, count(a.id) as jmlplay, b.ID,b.post_title ".
				 "from wp_podpress_stats a ".
				 "left join wp_posts b on b.id = a.postid ".
				 "where month(FROM_UNIXTIME(dt)) = '".$bulan."' and year(FROM_UNIXTIME(dt)) = '".$tahun."' ".
				 "group by a.postid order by count(a.id) desc limit 1 ";
	$mostplayedmonthly = $wpdb->get_results($sql, OBJECT);	
		
	/*if(date("W") == "01"){ $week = 52; $tahun = date("Y") - 1; }
	else{ $week = date("W") - 1; $tahun = date("Y"); }		
	$sql = "select distinct(a.postid) as postid, count(a.id) as jmlplay, b.ID,b.post_title ".
				 "from wp_podpress_stats a ".
				 "left join wp_posts b on b.id = a.postid ".
				 "where year(FROM_UNIXTIME(dt)) = '".$tahun."' and week(FROM_UNIXTIME(dt)) = '".$week."'".
				 "group by a.postid order by count(a.id) desc limit 1 ";	
	$mostplayedweekly = $wpdb->get_results($sql, OBJECT);
		
	if($mostplayedmonthly[0]->postid == $mostplayedweekly[0]->postid){		
		$mostplayedmonthly[0]->postid = $mostplayedmonthly[1]->postid;
		$mostplayedmonthly[0]->jmlplay = $mostplayedmonthly[1]->jmlplay;
		$mostplayedmonthly[0]->post_title = $mostplayedmonthly[1]->post_title;
		$mostplayedmonthly[0]->ID = $mostplayedmonthly[1]->ID;
	}*/
	
	return $mostplayedmonthly;
}
function getPlayedWeekly(){
	global $wpdb, $wp_query;	
			
	if(date("W") == "01"){ $week = 52; $tahun = date("Y") - 1; }
	else{ $week = date("W") - 1; $tahun = date("Y"); }		
	$sql = "select distinct(a.postid) as postid, count(a.id) as jmlplay, b.ID,b.post_title ".
				 "from wp_podpress_stats a ".
				 "left join wp_posts b on b.id = a.postid ".
				 "where year(FROM_UNIXTIME(dt)) = '".$tahun."' and week(FROM_UNIXTIME(dt)) = '".$week."'".
				 "group by a.postid order by count(a.id) desc limit 2 ";	
	$mostplayedweekly = $wpdb->get_results($sql, OBJECT);
	
	if(date("n") == 1){ $bulan = 12; $tahun = date("Y") - 1; }
	else{ $bulan = date("n") - 1; $tahun = date("Y"); }	
	$sql = "select distinct(a.postid) as postid, count(a.id) as jmlplay, b.ID,b.post_title ".
				 "from wp_podpress_stats a ".
				 "left join wp_posts b on b.id = a.postid ".
				 "where month(FROM_UNIXTIME(dt)) = '".$bulan."' and year(FROM_UNIXTIME(dt)) = '".$tahun."' ".
				 "group by a.postid order by count(a.id) desc limit 1 ";
	$mostplayedmonthly = $wpdb->get_results($sql, OBJECT);	
		
	
	if($mostplayedmonthly[0]->postid == $mostplayedweekly[0]->postid){		
		$mostplayedweekly[0]->postid = $mostplayedweekly[1]->postid;
		$mostplayedweekly[0]->jmlplay = $mostplayedweekly[1]->jmlplay;
		$mostplayedweekly[0]->post_title = $mostplayedweekly[1]->post_title;
		$mostplayedweekly[0]->ID = $mostplayedweekly[1]->ID;
	}
	return $mostplayedweekly;
}

function getStayTuneAllTime(){
	global $wpdb, $wp_query;	
	$sql = "select distinct(a.postid) as postid, count(a.postid) as jmlstaytune, b.ID,b.post_title from logfb a ".
				 "left join wp_posts b on b.id = a.postid ".
				 "group by a.postid order by count(a.postid) desc limit 1";
	$moststaytunealltime = $wpdb->get_results($sql, OBJECT);
	return $moststaytunealltime;
}

function getStayTuneMonthly(){
	global $wpdb, $wp_query;	
	
	if(date("n") == 1){ $bulan = 12; $tahun = date("Y") - 1; }
	else{ $bulan = date("n") - 1; $tahun = date("Y"); }
	$sql = "select distinct(a.postid) as postid, count(a.postid) as jmlstaytune, b.ID,b.post_title from logfb a ".
				 "left join wp_posts b on b.id = a.postid ".
				 "where month(cdate) = '".$bulan."' and year(cdate) = '".$tahun."' ".
				 "group by a.postid order by count(a.postid) desc limit 2";
	$moststaytunemonthly = $wpdb->get_results($sql, OBJECT);	
	
	/*if(date("W") == "01"){ $week = 52; $tahun = date("Y") - 1; }
	else{ $week = date("W") - 1; $tahun = date("Y"); }		
	$sql = "select distinct(a.postid) as postid, count(a.postid) as jmlstaytune, b.ID,b.post_title from logfb a ".
				 "left join wp_posts b on b.id = a.postid ".
				 "where year(cdate) = '".$tahun."' and week(cdate) = '".$week."' ".
				 "group by a.postid order by count(a.postid) desc limit 1";	
	$moststaytuneweekly = $wpdb->get_results($sql, OBJECT);
	
	if($moststaytunemonthly[0]->postid == $moststaytuneweekly[0]->postid){		
		$moststaytunemonthly[0]->postid = $moststaytunemonthly[1]->postid;
		$moststaytunemonthly[0]->jmlplay = $moststaytunemonthly[1]->jmlplay;
		$moststaytunemonthly[0]->post_title = $moststaytunemonthly[1]->post_title;
		$moststaytunemonthly[0]->ID = $moststaytunemonthly[1]->ID;
	}*/
	
	return $moststaytunemonthly;
}

function getStayTuneWeekly(){
	global $wpdb, $wp_query;	
	
	if(date("W") == "01"){ $week = 52; $tahun = date("Y") - 1; }
	else{ $week = date("W") - 1; $tahun = date("Y"); }		
	$sql = "select distinct(a.postid) as postid, count(a.postid) as jmlstaytune, b.ID,b.post_title from logfb a ".
				 "left join wp_posts b on b.id = a.postid ".
				 "where year(cdate) = '".$tahun."' and week(cdate) = '".$week."' ".
				 "group by a.postid order by count(a.postid) desc limit 2";	
	$moststaytuneweekly = $wpdb->get_results($sql, OBJECT);
	
	
	if(date("n") == 1){ $bulan = 12; $tahun = date("Y") - 1; }
	else{ $bulan = date("n") - 1; $tahun = date("Y"); }
	$sql = "select distinct(a.postid) as postid, count(a.postid) as jmlstaytune, b.ID,b.post_title from logfb a ".
				 "left join wp_posts b on b.id = a.postid ".
				 "where month(cdate) = '".$bulan."' and year(cdate) = '".$tahun."' ".
				 "group by a.postid order by count(a.postid) desc limit 1";
	$moststaytunemonthly = $wpdb->get_results($sql, OBJECT);	
	
	if($moststaytunemonthly[0]->postid == $moststaytuneweekly[0]->postid){		
		$moststaytuneweekly[0]->postid = $moststaytuneweekly[1]->postid;
		$moststaytuneweekly[0]->jmlplay = $moststaytuneweekly[1]->jmlplay;
		$moststaytuneweekly[0]->post_title = $moststaytuneweekly[1]->post_title;
		$moststaytuneweekly[0]->ID = $moststaytuneweekly[1]->ID;
	}
	
	return $moststaytuneweekly;
}

function getStayTune($postID = ''){
	global $wpdb, $wp_query;	
	$sql = "select distinct(a.postid) as postid, count(a.postid) as jmlstaytune, b.ID,b.post_title from logfb a ".
				 "left join wp_posts b on b.id = a.postid ".
				 "where a.postid = '$postID' ".
				 "group by a.postid ";				 
	$stayTune = $wpdb->get_results($sql, OBJECT);
	return $stayTune[0]->jmlstaytune;
}

function getCountTweet($url){
    $url = urlencode($url);
    $twitterEndpoint = "http://urls.api.twitter.com/1/urls/count.json?url=%s";
    $fileData = file_get_contents(sprintf($twitterEndpoint, $url));
    $json = json_decode($fileData, true);
    unset($fileData); // free memory
    return $json['count'];
}


function getTopicPagesFB($ID = '', $access_token = ''){	
	$url = 'https://graph.facebook.com/'.$ID.'/feed?access_token='.$access_token;
  $ch = curl_init();

  curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);

  $result = curl_exec($ch);
  $json_result = json_decode($result);	  
    
	for($i=0;$i<count($json_result->data);$i++){		
		if($json_result->data[$i]->from->id == $ID){
			$data['pesan'] = $json_result->data[$i]->message;			
			$data['url'] = $json_result->data[$i]->actions[0]->link;			
			break;
		}
	}	
	return $data;
}

function getMostly($type = 'play', $typeperiode = 'W', $minggu = 0, $bulan = 0, $tahun = 0, $category_id = 0){
	global $wpdb, $wp_query;	
	
	switch($typeperiode){
		case 'W' : 
			$sql = "select postID as postid, jumlah as jmlplay, title as post_title from wp_mostly where type='$type' and typeperiode='$typeperiode' and minggu='$minggu' and bulan = '$bulan' and tahun='$tahun' ";
			$sql .= ($category_id)? "and category_id = '$category_id' " : "and category_id = '0' ";
			$sql .= "order by jumlah desc limit 3";
		break;	
		case 'M' : 
			$sql = "select postID as postid, jumlah as jmlplay, title as post_title from wp_mostly where type='$type' and typeperiode='$typeperiode' and bulan = '$bulan' and tahun='$tahun' ";
			$sql .= ($category_id)? "and category_id = '$category_id' " : "and category_id = '0' ";
			$sql .= "order by jumlah desc limit 3";
		break;
		case 'A' : 
			$sql = "select postID as postid, jumlah as jmlplay, title as post_title from wp_mostly where type='$type' and typeperiode='$typeperiode' ";
			$sql .= ($category_id)? "and category_id = '$category_id' " : "and category_id = '0' ";
			$sql .= "order by jumlah desc limit 3";
		break;
	}	

	$mostplayedalltime = $wpdb->get_results($sql, OBJECT);																																					
	return $mostplayedalltime;
}

function suararadio_getPodcastUrl($postId=false) {
		global $post,$current_user,$podPress;
		
		$postId = ($postId)? $postId: $post->ID;
		$list = podPress_getAvailablePodcast($postId);
		$cuid = $current_user->ID;
		$caps = array("free"=>true);
		if ($cuid!=0) $caps = array_merge($caps,$current_user->allcaps);
		
		$tmp = array();
		foreach ($caps as $key=>$val) {
			$tmp = is_array($list[$key])? array_merge($tmp,$list[$key]):$tmp;
		}
		$str = implode(",",$tmp);
		$posisi = strpos($str,"mp3");
		$str1 = substr($str,0,$posisi+3);
		return $str1;
}

function checkIn($postId, $params=array(), $CHECKINFBPLUGIN_URL, $uidFb,$premium = 0){
	global $wpdb, $wp_query, $suararadio_connect;
	$Tempuid = $suararadio_connect['facebook']['session']['uid'];			
	#var_dump($suararadio_connect['facebook']);
	//echo $Tempuid;
	$querystr = "
		SELECT wposts.* 
		FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta
		WHERE wposts.ID = wpostmeta.post_id 
		AND wposts.ID = ".$postId."
		ORDER BY wposts.post_date DESC
 	";

 	$pageposts = $wpdb->get_results($querystr, OBJECT);

	$isi = preg_replace('/((\\\\n)+)/'," ",preg_replace("/(\r\n|\n)/",'\\n',stripslashes($pageposts[0]->post_content)));  
	$jdl = preg_replace('/\"/'," ",preg_replace("/(\r\n|\n)/",'\\n',stripslashes($pageposts[0]->post_title)));  
	
	//$list = explode("/", $params['nmfilefb']);

  	if($_tmp= explode(",",$params['nmfilefb'])){
		$_file_counts = count($_tmp);
		$i=0;
		while(!eregi("(.+)\.mp3$",$_tmp[$i],$_regs) && ($i<$_file_counts))
			$i++;
		
		$list=explode("/",$_tmp[$i]);
		//$list= explode("/",$params['nmfilefb']);	
	}else
	 
 		$list = explode("/", $params['nmfilefb']);


	$isi = remove_HTML($isi);	
	$quotes = array('/"/',"/'/");
	//$replacements = array('%22','%27');
	$replacements = array('%22','%27');

	$isi = preg_replace($quotes,$replacements,$isi);
	
	$loc = "http://".$_SERVER["HTTP_HOST"]."/podpress_trac/play/".$postId."/0/".$list[(count($list)-1)];
	//$podcast = "http://www.citrawanodyaangkasafm.com/podpress_trac/play/".$postId."/0/fitri-jangan-buru-buru-0111.mp3";	
	if(empty($Tempuid)){
		$str = 'cekFB();';
	}else{
		$str = 'streamPublish(\'Stay-tune on '.htmlentities($params['message'], ENT_QUOTES).'\', \''.htmlentities($jdl, ENT_QUOTES).'\', \''.$isi.'\', \''.htmlentities($jdl, ENT_QUOTES).'\', \''.$params['url'].'\', \''.$loc.'\', \''.$params['opturl'].'\', \''.$params['mediafb'].'\', \''.$params['message'].'\', \''.$CHECKINFBPLUGIN_URL.'\', \''.$uidFb.'\', \''.$postId.'\');';
		//echo "<a class='aStay' onClick='streamPublish(\"Stay-tune on ".htmlentities($params['message'], ENT_QUOTES)."\", \"".htmlentities($jdl, ENT_QUOTES)."\", \"".$isi."\", \"".htmlentities($jdl, ENT_QUOTES)."\", \"".$params['url']."\", \"".$loc."\", \"".$params['opturl']."\", \"".$params['mediafb']."\", \"".$params['message']."\", \"".$CHECKINFBPLUGIN_URL."\", \"".$uidFb."\", \"".$postId."\");'>";
	}
	return $str;
}

function getPlayedAllTimeMobile(){
	global $wpdb, $wp_query;
	$sql = "select distinct(a.postid) as postid, sum(a.total) as jmlplay, b.ID,b.post_title ".
				 "from wp_podpress_statcounts a ".
				 "left join wp_posts b on b.id = a.postid ".
				 "group by a.postid ".
				 "order by sum(a.total) desc ".
				 "limit 1";				 	
	$mostplayedalltime = $wpdb->get_results($sql, OBJECT);
																																						
	return $mostplayedalltime;
}

function getPlayedMonthlyMobile(){
	global $wpdb, $wp_query;	
	
	if(date("n") == 1){ $bulan = 12; $tahun = date("Y") - 1; }
	else{ $bulan = date("n") - 1; $tahun = date("Y"); }	
	$sql = "select distinct(a.postid) as postid, count(a.id) as jmlplay, b.ID,b.post_title ".
				 "from wp_podpress_stats a ".
				 "left join wp_posts b on b.id = a.postid ".
				 "where month(FROM_UNIXTIME(dt)) = '".$bulan."' and year(FROM_UNIXTIME(dt)) = '".$tahun."' ".
				 "group by a.postid order by count(a.id) desc limit 1 ";
	$mostplayedmonthly = $wpdb->get_results($sql, OBJECT);	
		
	/*if(date("W") == "01"){ $week = 52; $tahun = date("Y") - 1; }
	else{ $week = date("W") - 1; $tahun = date("Y"); }		
	$sql = "select distinct(a.postid) as postid, count(a.id) as jmlplay, b.ID,b.post_title ".
				 "from wp_podpress_stats a ".
				 "left join wp_posts b on b.id = a.postid ".
				 "where year(FROM_UNIXTIME(dt)) = '".$tahun."' and week(FROM_UNIXTIME(dt)) = '".$week."'".
				 "group by a.postid order by count(a.id) desc limit 1 ";	
	$mostplayedweekly = $wpdb->get_results($sql, OBJECT);
		
	if($mostplayedmonthly[0]->postid == $mostplayedweekly[0]->postid){		
		$mostplayedmonthly[0]->postid = $mostplayedmonthly[1]->postid;
		$mostplayedmonthly[0]->jmlplay = $mostplayedmonthly[1]->jmlplay;
		$mostplayedmonthly[0]->post_title = $mostplayedmonthly[1]->post_title;
		$mostplayedmonthly[0]->ID = $mostplayedmonthly[1]->ID;
	}*/
	
	return $mostplayedmonthly;
}
function getPlayedWeeklyMobile(){
	global $wpdb, $wp_query;	
			
	if(date("W") == "01"){ $week = 52; $tahun = date("Y") - 1; }
	else{ $week = date("W") - 1; $tahun = date("Y"); }		
	$sql = "select distinct(a.postid) as postid, count(a.id) as jmlplay, b.ID,b.post_title ".
				 "from wp_podpress_stats a ".
				 "left join wp_posts b on b.id = a.postid ".
				 "where year(FROM_UNIXTIME(dt)) = '".$tahun."' and week(FROM_UNIXTIME(dt)) = '".$week."'".
				 "group by a.postid order by count(a.id) desc limit 2 ";	
	$mostplayedweekly = $wpdb->get_results($sql, OBJECT);
	
	if(date("n") == 1){ $bulan = 12; $tahun = date("Y") - 1; }
	else{ $bulan = date("n") - 1; $tahun = date("Y"); }	
	$sql = "select distinct(a.postid) as postid, count(a.id) as jmlplay, b.ID,b.post_title ".
				 "from wp_podpress_stats a ".
				 "left join wp_posts b on b.id = a.postid ".
				 "where month(FROM_UNIXTIME(dt)) = '".$bulan."' and year(FROM_UNIXTIME(dt)) = '".$tahun."' ".
				 "group by a.postid order by count(a.id) desc limit 1 ";
	$mostplayedmonthly = $wpdb->get_results($sql, OBJECT);	
		
	
	if($mostplayedmonthly[0]->postid == $mostplayedweekly[0]->postid){		
		$mostplayedweekly[0]->postid = $mostplayedweekly[1]->postid;
		$mostplayedweekly[0]->jmlplay = $mostplayedweekly[1]->jmlplay;
		$mostplayedweekly[0]->post_title = $mostplayedweekly[1]->post_title;
		$mostplayedweekly[0]->ID = $mostplayedweekly[1]->ID;
	}
	return $mostplayedweekly;
}

function getStayTuneAllTimeMobile(){
	global $wpdb, $wp_query;	
	$sql = "select distinct(a.postid) as postid, count(a.postid) as jmlstaytune, b.ID,b.post_title from logfb a ".
				 "left join wp_posts b on b.id = a.postid ".
				 "group by a.postid order by count(a.postid) desc limit 1";
	$moststaytunealltime = $wpdb->get_results($sql, OBJECT);
	return $moststaytunealltime;
}

function getStayTuneMonthlyMobile(){
	global $wpdb, $wp_query;	
	
	if(date("n") == 1){ $bulan = 12; $tahun = date("Y") - 1; }
	else{ $bulan = date("n") - 1; $tahun = date("Y"); }
	$sql = "select distinct(a.postid) as postid, count(a.postid) as jmlstaytune, b.ID,b.post_title from logfb a ".
				 "left join wp_posts b on b.id = a.postid ".
				 "where month(cdate) = '".$bulan."' and year(cdate) = '".$tahun."' ".
				 "group by a.postid order by count(a.postid) desc limit 2";
	$moststaytunemonthly = $wpdb->get_results($sql, OBJECT);	
	
	/*if(date("W") == "01"){ $week = 52; $tahun = date("Y") - 1; }
	else{ $week = date("W") - 1; $tahun = date("Y"); }		
	$sql = "select distinct(a.postid) as postid, count(a.postid) as jmlstaytune, b.ID,b.post_title from logfb a ".
				 "left join wp_posts b on b.id = a.postid ".
				 "where year(cdate) = '".$tahun."' and week(cdate) = '".$week."' ".
				 "group by a.postid order by count(a.postid) desc limit 1";	
	$moststaytuneweekly = $wpdb->get_results($sql, OBJECT);
	
	if($moststaytunemonthly[0]->postid == $moststaytuneweekly[0]->postid){		
		$moststaytunemonthly[0]->postid = $moststaytunemonthly[1]->postid;
		$moststaytunemonthly[0]->jmlplay = $moststaytunemonthly[1]->jmlplay;
		$moststaytunemonthly[0]->post_title = $moststaytunemonthly[1]->post_title;
		$moststaytunemonthly[0]->ID = $moststaytunemonthly[1]->ID;
	}*/
	
	return $moststaytunemonthly;
}

function getStayTuneWeeklyMobile(){
	global $wpdb, $wp_query;	
	
	if(date("W") == "01"){ $week = 52; $tahun = date("Y") - 1; }
	else{ $week = date("W") - 1; $tahun = date("Y"); }		
	$sql = "select distinct(a.postid) as postid, count(a.postid) as jmlstaytune, b.ID,b.post_title from logfb a ".
				 "left join wp_posts b on b.id = a.postid ".
				 "where year(cdate) = '".$tahun."' and week(cdate) = '".$week."' ".
				 "group by a.postid order by count(a.postid) desc limit 2";	
	$moststaytuneweekly = $wpdb->get_results($sql, OBJECT);
	
	
	if(date("n") == 1){ $bulan = 12; $tahun = date("Y") - 1; }
	else{ $bulan = date("n") - 1; $tahun = date("Y"); }
	$sql = "select distinct(a.postid) as postid, count(a.postid) as jmlstaytune, b.ID,b.post_title from logfb a ".
				 "left join wp_posts b on b.id = a.postid ".
				 "where month(cdate) = '".$bulan."' and year(cdate) = '".$tahun."' ".
				 "group by a.postid order by count(a.postid) desc limit 1";
	$moststaytunemonthly = $wpdb->get_results($sql, OBJECT);	
	
	if($moststaytunemonthly[0]->postid == $moststaytuneweekly[0]->postid){		
		$moststaytuneweekly[0]->postid = $moststaytuneweekly[1]->postid;
		$moststaytuneweekly[0]->jmlplay = $moststaytuneweekly[1]->jmlplay;
		$moststaytuneweekly[0]->post_title = $moststaytuneweekly[1]->post_title;
		$moststaytuneweekly[0]->ID = $moststaytuneweekly[1]->ID;
	}
	
	return $moststaytuneweekly;
}

function showButtonLoginMobile(){
	global $wpdb, $wp_query, $suararadio_connect;
	$Tempuid = $suararadio_connect['facebook']['session']['uid'];				
	$out = "";
	if(empty($Tempuid))		
		$out = "<a class='f_login_button' onclick='cekFB();'></a>";	
	else
		$out = "<a href='javascript: document.requestForm.submit();' class='send_button'></a>";	return $out;		
}

function switchIconMessageMobile($jenisMedia, $creator){
	switch($jenisMedia){
		case "f" : $res = "<img class='overlay-img' src=\"".SUARARADIO_PLUGIN_URL."/images/fb.png\">";break;
		case "t" : $res = "<img class='overlay-img' src=\"".SUARARADIO_PLUGIN_URL."/images/twitter.png\">";break;
		case "c" : $res = "<img class='overlay-img' height='20' width='20' src=\"".SUARARADIO_PLUGIN_URL."/images/ebi.png\">";break;
		case "1" : $res = "<img class='overlay-img' height='20' width='20' src=\"".SUARARADIO_PLUGIN_URL."/images/sms.png\">";break;
		case "2" : $res = "<img class='overlay-img' height='20' width='20' src=\"".SUARARADIO_PLUGIN_URL."/images/sms.png\">";break;
		case "3" : if ($creator == 'c')
					$res = "<img class='overlay-img' height='20' width='20' src=\"".SUARARADIO_PLUGIN_URL."/images/ebi.png\">";	
				else if ($creator =='i')
					$res = "<img class='overlay-img' height='20' width='20' src=\"".get_option('siteurl')."/wp-content/themes/klite_mobile/images/lologoan_apple.png\">";
				else if ($creator =='b')
					$res = "<img class='overlay-img' height='20' width='20' src=\"".get_option('siteurl')."/wp-content/themes/klite_mobile/images/lologoan_BB.png\">";
				else if ($creator =='a')
					$res = "<img class='overlay-img' height='20' width='20' src=\"".get_option('siteurl')."/wp-content/themes/klite_mobile/images/lologoan_android.png\">";
				else if ($creator =='w')
					$res = "<img class='overlay-img' height='20' width='20' src=\"".get_option('siteurl')."/wp-content/themes/klite_mobile/images/lologoan_windows.png\">";
				else
					$res = "<img height='20' width='20' class='overlay-img' src=\"".SUARARADIO_PLUGIN_URL."/images/sms.png\">";
				break;
	}
	return $res;	
}

if (!function_exists('getPodcastMeta_kp')) {
	function getPodcastMeta_kp($postId){
		global $_SERVER,$wpkontenpil;
	
		$arrMeta = array();
		//$arrMeta = get_post_meta($postId, 'podPressMedia', true);
		$sql = "SELECT wp_posts.ID,wp_posts.post_title,wp_postmeta.meta_key ,wp_postmeta.meta_value
				FROM wp_posts, wp_postmeta 
				WHERE wp_posts.ID = wp_postmeta.post_id and wp_posts.ID ='".$postId."'
				AND wp_postmeta.meta_key in ('_podPressMedia') 
				AND wp_posts.post_type='post' 
				AND wp_posts.post_status = 'publish' 
				ORDER BY wp_posts.post_date DESC";													        					
		$list_query = $wpkontenpil->get_results($sql, OBJECT);
	
		$get_list = $list_query[0]->meta_value;
		$arrMeta = unserialize($get_list);
	
		$pCount  = count($arrMeta);
		$dt		= '';
	
		if(!empty($arrMeta)){
			for($k=0;$k < $pCount;$k++){
				$namafile = empty($arrMeta[$k]["URI"]) ? array() : $arrMeta[$k]["URI"];
				$nmFile 	 = explode("/",$namafile);
				$dt 		.=  "http://kontenpilihan.suararadio.com/podpress_trac/web/".$postId."/".$k."/".$nmFile[(count($nmFile)-1)];
				$koma 	 = ($k != ($pCount-1) && $k != $pCount) ? "," : "";
				$dt 		.= $koma;
			}
		}
		return $dt;
	}
}
?>
