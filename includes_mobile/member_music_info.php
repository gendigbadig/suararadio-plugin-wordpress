<?php
/*
  Member Music Info
*/

$dsong = $suararadio->getLagu($songId);
$dalbum = $suararadio->getAlbumsByArtist($dsong['artistId']);
$dalbumsong = $suararadio->getSongsByAlbum($dsong['albumId']);
$ddalbum = $suararadio->getAlbumMelon($dsong['albumId']);
#var_dump($suararadio->formatDate($dsong['issueDate']));
?>
<div id='klubradio'>	
	<div class="utamaIsi">		
			<!--<ul>
			  <li class="logoKlubRadio"></li>
			</ul>-->						
		</div>
	<div id="klubradioIsi">    
		
<section id="songView">
	<figure class="songAlbumArt"><img src="http://www.melon.co.id/image.do?fileuid=<?php echo $ddalbum['albumLImgPath'] ?>"></figure>
	<div class="songDetail">
		<label>Artis</label><span><?php echo $dsong['artistName'] ?></span>
		<label>Judul</label><span><?php echo $dsong['songName'] ?></span>
		<label>Album</label><span><?php echo $dsong['albumName'] ?></span>
		<!--   label>Genre</label><span><?php echo $dsong['genreId'] ?></span -->
		<label>Durasi</label><span><?php echo $suararadio->getDurasi($dsong['playtime'])?></span>
		<label>Label</label><span><?php echo $ddalbum['sellCompany']." ".$ddalbum['agency']; ?></span>
		<label>Tanggal Release</label><span><?php echo $suararadio->formatDate($dsong['issueDate']); ?></span>
	</div>
</section>
<section id="albumView">
	<header>Daftar Album</header>
	<ul>
	<?php foreach ($dalbum['dataList'] as $valbum) { ?>
	  <li>
	  	<a href="javascript:void(0);" class="albumLink" albumId="<?php echo $valbum['albumId']; ?>"><img width="98" border="0" height="98" src="http://www.melon.co.id/image.do?fileuid=<?php echo $valbum['albumMImgPath'] ?>"></a>
	  	<label title="<?php echo $valbum['albumName'] ?>"><?php echo $valbum['albumName'] ?></label>
	  	<span><?php echo $suararadio->formatDate($valbum['issueDate']) ?></span>
	  </li>
	<?php } ?>
	</ul>
</section>
<section id="listLaguView">
	<header>Daftar Lagu Di Album</header>
<div>
	<div class="listData" style="max-height: 570px;">
	<ul id="listItem2" class="ui-sortable" style="margin-bottom: 80px;">
	<?php
		$params = array();
		$params['style'] = 'width: 40px; height: 34px;';
		foreach ($dalbumsong['dataList'] as $vsong) { ?>
	  <li>
		<span class="player">
			<?php $nmfile = suararadio_showMiniMelonPlay($vsong['songId']); ?>
		</span>
		<span class="title" style="width:55%;"><a class="linkMember" href="/member/music/<?php echo $vsong['songId']; ?>"><?php echo $vsong['artistName']." - ".$vsong['songName']?></a></span>
		<span class="info"><?php echo $suararadio->getDurasi($vsong['playtime']) ?></span>
		<span class="action">
			<?php if (!empty($nmfile)) suararadio_show_addlink('melon',$vsong['songId'],$params); ?>
		</span>
	  </li>
	<?php } ?>
	</ul>
	</div>
</div>
</section>

	</div> <!-- containersingleIsi-->

	

</div> <!-- containersingle-->
<script type="text/javascript">
$(document).ready(function() {
	$('.albumLink').bind('click',function(event){
		var aId = $(this).attr("albumId");
		$.get('/suararadio_api/melon/listsongs/'+aId,function(rdata){
			$("#listLaguView ul#listItem2").empty();
			$("#listLaguView ul#listItem2").append(rdata);
			suararadioPlayer.makeSoundPlayer();
		});
	});
});
</script>