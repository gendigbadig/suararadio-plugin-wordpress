<?php
/*
Template Name: Login Membership
*/

wp_enqueue_style( "wsl_login_css", WORDPRESS_SOCIAL_LOGIN_PLUGIN_URL . "/assets/css/login.css" );
?>
<!DOCTYPE html>
<html>
	<head profile="http://gmpg.org/xfn/11">
		<meta name="MobileOptimized" content="width"/> 
		<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
		<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" /> <!-- leave this for stats -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">

		<title><?php bloginfo('name'); ?> <?php if ( is_single() ) { ?> &raquo; Blog Archive <?php } ?> <?php wp_title(); ?></title>
		<?php //wp_head_mobile(); ?>
		<?php wp_head(); ?>

		<!--<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />-->
		<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_option('home'); ?>/wp-content/themes/mobile_new/style_membership.css" />
		<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
		<link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
		<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />        

	</head>
	<body>		
		              
<!--Begin canvas-->                    
    	<a href="<?=bloginfo('home');?>/login_membership/">
		<section id="header-content">
    		</section>
	</a>
       
       <section id="content">
 		<div id="scroller">             
            		<section id="isi_konten">
		
		<p class="p1">
			Bergabung dengan Klub Radio sekarang,<br/>
			dan dapatkan akses penuh ke semua musik legal<br/>
			dan konten premium radio 2.0 diseluruh Indonesia
		</p>

			<section id="login-with">
			<section id="login-with-row">
				<section id="login-with-cell">
					<section class="line"></section>
				</section>
				<section id="login-with-cell">
				<a style="text-decoration:none; color:red;" href="<?=bloginfo('home');?>/menu_membership/">Register</a>
				</section>
				<section id="login-with-cell">
					<section class="line"></section>
				</section>
			</section>
		</section>
		<article class="loginFrm" style="margin-bottom:20px; padding:0px; height:100px;">
		<?php wsl_render_login_form(); ?>
		<section style="width: 50%; margin: auto;">
		</section>
		</article>
		



<?php get_footer('memberpolos'); ?>