<?php
/*
  Member playlist
*/
?>
<?php
#$suararadio->getStatData();
?>
	    <form name="member" class="postform" method="post" action="<?php echo $requested; ?>">
	    <div class="" style="clear: left;" align="left">
	    	<span style="padding: 0 8px;">Pencarian</span>&nbsp;
	    	<input type="text" name="mkeyword" size="30" value="<?php echo $filter['mkeyword']?>">
		    <input type="hidden" name="morder" value="<?php echo $filter['morder']?>">
		    <input type="submit" class="button-primary" value="<?php _e('Search') ?>" />
	    </div>
	    <div class="" align="left">
	    </div>
	    <div class="line_4Center_sub">
	    	<ul>
	    		<li><a <?php echo ($morder=='newest') ? 'class="selected"':'';?> ><?= __('Newest','suararadio')?></a></li>
	    		<li><a <?php echo ($morder=='view') ? 'class="selected"':'';?> ><?= __('Most View','suararadio')?></a></li>
	    		<li><a <?php echo ($morder=='rating') ? 'class="selected"':'';?> ><?= __('Top Rating','suararadio')?></a></li>
	    	</ul>
	    </div>
	    </form>
	    <div class="" style="clear: left; padding: 0 8px;" >
<?
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$params = array();
	$params['numrow']= 15;
	$params['keyword']=$filter['mkeyword'];
	$params['page']= $paged;
	$playlist = $suararadio->getListPlaylist($params);
?> 
			<div class="listData">
				<ul id="listPlay">
<? if ( count($playlist)>0 ) foreach ( $playlist as $vpl) { ?>
          <li id="<?php echo $vpl['list_id']; ?>">
					  <span class="player"><?php //$nmfile = suararadio_showMiniLaguPlay($vpl);?></span>
					  <span class="title showPlaylist" listid="<?= $vpl['list_id']; ?>" id="playlist_<?= $vpl['list_id']; ?>"><?php echo $vpl['list_name'];?></span>
					  <span class="info"><?php echo $suararadio->getPlaylistCount($vpl['list_id'])?> item</span>
					  <span class="action">
					  	<a href="javascript:void(0);" class="sendPlaylist" listid="<?= $vpl['list_id']?>" onClick="sendListClick($(this).attr('listid'));">send</a>
					  	<a href="javascript:void(0);" class="delPlaylist" listid="<?= $vpl['list_id']?>" onClick="delListClick($(this).attr('listid'));">del</a>
					  </span>
			    </li> 
<? } else { ?>
					<li>
						tidak ada data.
					</li>
<? } ?>
			</div>
<? if(function_exists('wp_page_numbers')) { wp_page_numbers(); } ?>
<? wp_reset_query(); ?>
      </div>
      <div class="" style="clear: left; padding: 0 8px;" >
      	<div class="listTitle">-session list-</div>
	  		<div class="listData">
	  			<ul id="listItem">
	  				<?php
	  					$list = $suararadio->getSessionList();
	  					$i = 0;
	  					if (count($list)>0) foreach ($list as $vli) {
	  				?>
	  				<li id="<?php echo $i; ?>">
	  					<span class="player"><?php $nmfile = suararadio_showMiniUrlPlay($vli['location']); ?></span>
	  					<span class="title"><?php echo $vli['title'];?></span>
	  					<span class="info"><?php echo ($vli['duration'])?$vli['duration']:'--:--:--';?></span>
	  					<span class="action"><a href="javascript:void(0);" class="delItem" num="<?= $i?>" onClick="delItemClick($(this).parent().parent().attr('id'));">del</a></span>
	  				</li>
	  				<? $i++; } else { ?>
	  				<li>-kosong-</li>
	  				<? } ?>
	  			</ul>
	  		</div>
				<div class="listInfo">
	  			playlist dapat di urutkan dengan <i>drag & drop</i>
	  		</div>
	  		<div class="listButton" align="left">
	  			<div id="toolbar" class="toolbarButton" style="float: left;">
						<!-- input type="button" name="bu-clear" id="doFirst" class="button-primary sr-icon sr-icon-first" value=""/ -->
						<input type="button" name="bu-clear" id="doPrev" class="button-primary sr-icon sr-icon-prev" value=""/>
						<input type="button" name="bu-clear" id="doPlay" class="button-primary sr-icon sr-icon-play" value=""/>
						<input type="button" name="bu-clear" id="doStop" class="button-primary sr-icon sr-icon-stop" value=""/>
						<input type="button" name="bu-clear" id="doNext" class="button-primary sr-icon sr-icon-next" value=""/>
						<!-- input type="button" name="bu-clear" id="doLast" class="button-primary sr-icon sr-icon-last" value=""/ --->
					</div>
					&nbsp;<div id="listTimeLine" style="float: left; padding: 2px 4px 0 4px;">00:00:00</div>&nbsp;
					<div align="right" style="float: left;">
		  			<input type="hidden" name="listid" id="listid" value="<?=$_SESSION[SR_PLAYLIST_ID]?>">
		  			<input type="button" name="bu-clear" id="clearPlaylist" class="button-primary" value="<?php _e('Clear') ?>"/>
		  			<input type="button" name="bu-save" id="savePlaylist" class="button-primary" value="<?php _e('Save') ?>"/>
		  			<input type="button" name="bu-save-as" id="saveAsPlaylist" class="button-primary" value="<?php _e('Save As') ?>" />
	  			</div>
	  		</div>
	  		<div id="dialog-modal" title="Save Playlist As">
	  			<input type="text" name="playlist-name" id="playlist-name"/>
	  			<input type="button" name="button-save" id="doSaveAsPlaylist" class="button-primary" value="<?php _e('Save') ?>"/>
	  		</div>
      </div>

<script type="text/javascript">
		var playlist_page = 1;
		  
		$(document).ready(function() {
			$("#listItem").sortable();
			$("#listItem").disableSelection();
			$("#listItem").bind("sortupdate",function(event,ui){ 
				//alert(var_dump(event,'html',1,1)); 
				var data = $("#listItem").sortable('toArray');
				$.post('/suararadio_api/reorder_playlist/',{'list': data});
				$("#listItem li").each(function(index){
					$(this).attr('id',index);
					//$(this).child().attr('id',index);
				});
				suararadioPlayer.makePlaylist();
			});
			//refreshDelItem();
			$('#dialog-modal').dialog("destroy");
			$('#dialog-modal').dialog({
				height: 80,
				modal: true,
				autoOpen: false,
			});
			$('#savePlaylist').bind('click',function(event){
				var vdata = {};
				vdata.listid = $('#listid').val();
				savePlaylist(vdata);
			});
			$('#saveAsPlaylist').bind('click',function(event){
				$('#dialog-modal').dialog('open');
			});
			$('#clearPlaylist').bind('click',function(event){
				suararadio_playlist_clear();
				
			});
			$('#doSaveAsPlaylist').bind('click',function(event){
				var vdata = {};
				vdata.name = $('#playlist-name').val();
				savePlaylist(vdata);
			});
			
			$('span.showPlaylist').bind('click',function(event){
				loadPlaylistItem($(this).attr('listid'));
			});
			$('#doPlay').bind('click',function(event){
				suararadioPlayer.playPlaylist();
			});
			$('#doStop').bind('click',function(event){
				suararadioPlayer.stopPlaylist();
			});
			$('#doPrev').bind('click',function(event){
				suararadioPlayer.prevPlaylist();
			});
			$('#doNext').bind('click',function(event){
				suararadioPlayer.nextPlaylist();
			});
			
			suararadioPlayer.makePlaylist();
		});
		
		///// list of function /////
			function savePlaylist(vdata) {
				$.ajax({
					type: "POST",
					url:  "/suararadio_api/save_playlist/",
					dataType: "json",
					data: vdata,
					success: function (data) {
							if (data!=null) {
								$('#dialog-modal').dialog('close');
								str = "Playlist: "+data+", telah disimpan.";
								suararadio_flash(str);
							}
						},
					error: function (req, stat, err) {
							str = "Playlist: data gagal disimpan.";
							suararadio_flash(str);
						}
				});
			}
			function delItemClick(idx){
					//idx = $(this).parent().parent().attr('id');
					$.ajax({
						type: "GET",
						url:  "/suararadio_api/del_playlist_item/"+idx,
						dataType: "json",
						success: function (data) {
								if (data!=null) {
									str = "File: "+data.title+", telah dihapus.";
									suararadio_flash(str);
									$("#listItem li[id="+idx+"]").remove();
									$("#listItem li").each(function(index){$(this).attr('id',index)});
									suararadioPlayer.makePlaylist();
								}
							}
					});
			} // endfunc delItemClick
			function delListClick(listid){
					if (confirm("Yakin menghapus list?")) {
						$.ajax({
							type: "POST",
							url:  "/suararadio_api/del_playlist/"+listid,
							dataType: "json",
							success: function (data) {
									if (data!=null) {
										str = "List: "+data.list_name+", telah dihapus.";
										suararadio_flash(str);
										$("#listPlay li[id="+listid+"]").remove();
									}
								}
						});
  				}
			} // endfunc delListClick
/*				
			function refreshDelItem() {
				//alert('do');
				$("a.delItem").bind("click",function(event){
					idx = $(this).parent().parent().attr('id');
					//alert(idx);
					//$("#listItem li[id="+idx+"]").remove();
					$.ajax({
						type: "GET",
						url:  "/suararadio_api/del_playlist_item/"+idx,
						dataType: "json",
						success: function (data) {
								if (data!=null) {
									str = "File: "+data.title+", telah dihapus.";
									suararadio_flash(str);
									$("#listItem li[id="+idx+"]").remove();
									$("#listItem li").each(function(index){$(this).attr('id',index)});
									
								}
							}
					});
				});
			}
*/
			function loadPlaylist(page) {
				$.ajax({
					type: "POST",
					url:  "/suararadio_api/load_playlist/",
					dataType: "json",
					success: function (data) {
							if (data!=null) {
								str = "Playlist: "+data.playlist.list_name+", telah dimuat.";
								suararadio_flash(str);
							}
						},
					error: function (req, stat, err) {
							str = "Playlist: data gagal dimuat.";
							suararadio_flash(str);
						}
				});
			} // loadPlaylist
			
			function loadPlaylistItem(listid) {
				$.ajax({
					type: "POST",
					url:  "/suararadio_api/load_playlist_item/"+listid,
					dataType: "html",
					success: function (data) {
							if (data!=null) {
								str = "Playlist: telah dimuat.";
								suararadio_flash(str);
								$("#listItem").html(data);
								$("#listid").val(listid);
								suararadioPlayer.makeSoundPlayer();
								suararadioPlayer.makePlaylist();		
							}
						},
					error: function (req, stat, err) {
							str = "Playlist: data gagal dimuat.";
							suararadio_flash(str);
						}
				});
			}
</script>