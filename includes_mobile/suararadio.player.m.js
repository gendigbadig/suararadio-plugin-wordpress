/**
suararadio_player
dependency: jquery,soundManager
by: MZR
versi 1.5
last change:
- 5/4/2011 5:38:34 PM, whileloading, menampilkan track buffering
- 2012/03/14 penambahan untuk penanganan mobile website player di podcast
*/

function SuararadioPlayer() {
	var self = this;
	var pl = this;
	var sm = soundManager;
/*
	var _event;
	var _cleanup;
*/
	var _ui;
	
	var isIE = (navigator.userAgent.match(/msie/i));
  var isOpera = (navigator.userAgent.match(/opera/i));
  var isSafari = (navigator.userAgent.match(/safari/i));
  var isChrome = (navigator.userAgent.match(/chrome/i));
  var isFirefox = (navigator.userAgent.match(/firefox/i));
  var isTouchDevice = (navigator.userAgent.match(/ipad|ipod|iphone/i));
  var plugindir = '/wp-content/plugins/suararadio/';
  //var imgOn = plugindir+'images/spka.gif';
  var imgOn = plugindir+'images/spka.png'; // edit untuk themes baru
  var imgOff = plugindir+'images/spk.png';
  
  this.links = [];
  this.playlist = [];
  this.tmpImg = '';
  this.index = 0;
  this.idx = -1;
  this.srcname = 'href';
  this.playObj = null;
  this.lastSound = null;
  this.detIdx = 0;
  this.detCount = 0;
  this.dragActive = false;
  this.dragExec = new Date();
  this.dragTimer = null;
  this.lastWPExec = new Date();
  this.lastWLExec = new Date();
  
  this.config = {
  	playNext: true,
  	usePeakData: false,
  	mode: 0, // 0: links, 1: playlist, 2: streaming
  	useThrottling: false
  }

/*  
  _event = (function() {
    var old = (window.attachEvent && !window.addEventListener),
    _slice = Array.prototype.slice,
    evt = {
      add: (old?'attachEvent':'addEventListener'),
      remove: (old?'detachEvent':'removeEventListener')
    };
    function getArgs(oArgs) {
      var args = _slice.call(oArgs), len = args.length;
      if (old) {
        args[1] = 'on' + args[1]; // prefix
        if (len > 3) {
          args.pop(); // no capture
        }
      } else if (len === 3) {
        args.push(false);
      }
      return args;
    }
    function apply(args, sType) {
      var element = args.shift(),
          method = [evt[sType]];
      if (old) {
        element[method](args[0], args[1]);
      } else {
        element[method].apply(element, args);
      }
    }
    function add() {
      apply(getArgs(arguments), 'add');
    }
    function remove() {
      apply(getArgs(arguments), 'remove');
    }
    return {
      'add': add,
      'remove': remove
    };
  }());
*/

  this.events = {
  	play: function() {
  		self._ui.player.show();
  		self._ui.buPause.show();
		var atrcls = $(self.playObj).attr("class");
		if (atrcls!='streamplayer') {
  		  //self.tmpImg = $(self.playObj).children('img').attr('src');
  		  //$(self.playObj).children('img').attr('src',imgOn);
  		  //$(self.playObj).children('img').attr('played',"1");
		}
  		self._ui.buPlay.hide();
  	},
  	resume: function() {
  		if (pl.dragActive) {
        return false;
      }
  		self._ui.buPause.show();
  		self._ui.buPlay.hide();
  		self._ui.timeline.removeClass("paused");
  	},
  	stop: function() {
  		self.lastSound.destruct();
  		//sm.destroySound('sr_soundplay');
			self._ui.player.hide();
			var atrcls = $(self.playObj).attr("class");
			if (atrcls!='streamplayer') {
				//$(self.playObj).children('img').attr('src',imgOff);
				//$(self.playObj).children('img').removeAttr('played');
			}
			//self._ui.timeline.text('00:00:00');
			//self._ui.barPos.width('0%');
			if (self.config.mode==2) {
				self.hideStreamPlayer();
			}
			self.lastSound = null;
  	},
  	load: function(bSuccess) {
  		if (this.readyState==2) {
  		//if (!bSuccess) {
  			self.events.stop();
  			self.debug('Gagal loading file!');
  		}
  	},
  	pause: function() {
  		if (pl.dragActive) {
        return false;
      }
  		self._ui.buPause.hide();
  		self._ui.buPlay.show();
  		//self._ui.timeline.addClass("paused");
  	},
  	finish: function() {
  		self.lastSound.destruct();
			self._ui.player.hide();
			//self._ui.timeline.text('00:00:00');
			//self._ui.barPos.width('0%');
			//$('#listTimeLine').text('00:00:00');  // di playlist member
			//$(self.playObj).children('img').attr('src',imgOff);
			//$(self.playObj).children('img').removeAttr('played');
			self.lastSound = null;
			self.debug("playnext: "+self.config.playNext+" mode: "+self.config.mode);
			if (self.config.mode==1 && self.config.playNext) {
				self.nextPlaylist();
				//return true;
			}
			if (self.config.mode==0 && self.config.playNext) {
				self.nextPlay();
				//return true;
			}
  	},
  	whileloading: function() {
  		function doWork() {
  			//self._ui.barLoad.width((this.bytesLoaded/this.bytesTotal*100)+'%');
  		}
        //self._ui.barLoad.html("buff");
        //alert(self._ui.barLoad.html());
        /*
  		if (!pl.config.useThrottling) {
  			doWork.apply(this);
  		} else {
  			var d = new Date();
            if (d && d-self.lastWLExec>3 || this.bytesLoaded === this.bytesTotal)  {
                doWork.apply(this);
                self.lastWLExec = d;
            }
  		}
        */
  	},
  	whileplaying: function() {
  		var d = null;
      if (pl.dragActive || !pl.config.useThrottling) {
      } else {
      	d = new Date();
      	if (d-self.lastWPExec>60) {
	      	if (sm.flashVersion >= 9) {
	      		if (pl.config.usePeakData && this.instanceOptions.usePeakData) {
	            self.updatePeaks.apply(this);
	          }
	      	}
	  			self.updateTimer.apply(this);
	  			self.updateBar.apply(this);
	  			self.lastWPExec = d;
	  		}
	  	}
  	},
  	bufferchange: function() {
  	},
  	id3: function(){
  		var prop = null;
  		var data = '';
  		for (prop in this.id3) {
  			data += prop+': '+this.id3[prop]+','; // eg. title: Loser, artist: Beck
			}	
  		self.debug("id3: "+data);
  	}
  }
  
  this.debug = function(str) {
  	sm._writeDebug(str);
  } //endfunc debug
  this.updatePeaks = function() {
  	self._ui.peakLeft.width((this.peakData.left*100)+'%');
  	self._ui.peakRight.width((this.peakData.right*100)+'%');
  } //endunc updatePeaks
  this.updateBar = function () {
  	self._ui.barPos.width((this.position/this.durationEstimate*100)+'%');
  } //endfunc updateBar
  this.updateTimer = function () {
  	var secs = 0;
		var mins = 0;
		var hours = 0;
  		
  	secs = Math.round(this.position/1000);
		hours = Math.floor(secs/3600);
		secs = secs%3600;
		mins = Math.floor(secs/60);
		secs = secs%60;
		hours = hours.toString();
		mins = mins.toString();
		secs = secs.toString();
		self._ui.timeline.text((hours.length>1?'':'0')+hours+':'+(mins.length>1?'':'0')+mins+':'
			+(secs.length>1?'':'0')+secs);
		if (self.config.mode==1) $('#listTimeLine').text((hours.length>1?'':'0')+hours+':'+(mins.length>1?'':'0')+mins+':'
			+(secs.length>1?'':'0')+secs);
  } //endfunc updateTimer
  this.togglePlayNext = function() {
  	if (self.config.playNext) {
  		self.config.playNext=false;
  		self._ui.buList.removeClass('listactive').addClass('list');
  	} else {
  		self.config.playNext=true;
  		self._ui.buList.removeClass('list').addClass('listactive');
  	}
  } //endfunc togglePlayNext
  this.makeSoundPlayer = function(selector) {
  	if (selector==undefined) selector='a.soundplayer,a.streamplayer,a.playit';
  	self.links = $(selector);
  	for(i=0;i<self.links.length;i++) {
  		$(self.links[i]).attr('id','_srsnd_'+i).bind('click',this.handleClick);
  	}
  } //endfunc makeSoundPlayer
  
  this.makeMobileSoundPlayer = function(selector) {
  	if (selector==undefined) selector='div.kontenPodcast';
  	self.links = $(selector);
  	for(i=0;i<self.links.length;i++) {
  		$(self.links[i]).attr('id','_srsnd_'+i).bind('click',this.mobileClick);
  	}
  } //endfunc makeMobileSoundPlayer
  
  
/*
  this.makeStreamPlayer = function(selector) {
  	if (selector==undefined) selector='a.streamplayer';
  	var listlinks = $(selector);
  	for(i=0;i<listlinks.length;i++) {
  		$(listlinks[i]).attr('id','_srstr_'+i).bind('click',this.handleClick);
  	}
  } //endfunc makeStreamPlayer
*/
  this.makePlaylist = function(selector) {
  	if (selector==undefined) {
  		selector='#listItem a.soundplayer';
  	}
  	if (typeof selector=='string') {
  		self.playlist = $(selector);
  	} else if (typeof selector=='object') {
  		self.playlist = $(selector).children('a.soundplayer');
  	}
  } //endfunc makePlaylist
  
  this.init = function() {
  	// tambahkan div template, bisa diganti sesuai kebutuhan
  	var str = '<div class="flash-player" id="flash-player" style="display: none;">'+
							'<div class="controls">'+
							'<div class="buttonplayer play"><a href="javascript:void(0);"></a></div>'+
							'<div class="buttonplayer pause"><a href="javascript:void(0);"></a></div>'+
						  '<div class="buttonplayer stop"><a href="javascript:void(0);"></a></div>'+
						  '<div class="buttonplayer prev"><a href="javascript:void(0);"></a></div>'+
						  '<div class="buttonplayer next"><a href="javascript:void(0);"></a></div>'+
						  '<div class="buttonplayer listactive"><a href="javascript:void(0);"></a></div>'+
						  '</div>'+
/*
						  '<div class="slider">'+
							'<div class="timeline">00:00:00</div>'+
							'<div class="statbar"><div class="loading" style="width: 0%;"></div>'+
							'<div class="position" style="width: 0%;"></div></div>'+
							'</div>'+
							'<div class="vumeter">'+
							'<div class="left" style="width: 0%;"></div>'+
							'<div class="right" style="width: 0%;"></div>'+
							'</div>'+
*/
							'</div>';
		$('body').prepend(str);
		self._ui = {
			player: $('#flash-player'),
			buPause: $('#flash-player .pause'),
			buPlay: $('#flash-player .play'),
			buStop: $('#flash-player .stop'),
			buNext: $('#flash-player .next'),
			buPrev: $('#flash-player .prev'),
			buList: $('#flash-player .listactive'),
			controls: $('#flash-player .controls'),
            //,
			//statbar: $('#flash-player .statbar'),
			//barPos: $('#flash-player .statbar .position'),
			barLoad: $('#flash-player .statbar .mesg')
			//timeline: $('#flash-player .timeline'),
			//peakLeft: $('#flash-player .vumeter .left'),
			//peakRight: $('#flash-player .vumeter .right')
		}
		
		if (sm.flashVersion >= 9) {
      //sm.useMovieStar = this.config.useMovieStar; // enable playing FLV, MP4 etc.
      //sm.defaultOptions.usePeakData = this.config.usePeakData;
    }
		
    self.makeSoundPlayer();
    self.makeMobileSoundPlayer();
  	self._ui.buPause.bind('click',function(event){
  		self.togglePause();
  		return false;
  	});
  	self._ui.buPlay.bind('click',function(event){
  		self.togglePause();
  		return false;
  	});
  	self._ui.buStop.bind('click',function(event){
  		self.stop();
  		return false;
  	});
  	//self._ui.buNext.bind('click',function(event){
  	//	self.nextPlay();
  	//	return false;
  	//});
  	//self._ui.buPrev.bind('click',function(event){
  	//	self.prevPlay();
  	//	return false;
  	//});
  	//self._ui.statbar.bind('click',this.handleMouseDown);
  	//self._ui.buList.bind('click',function(event){
  	//	self.togglePlayNext();
  	//	return false;
  	//});
  	this.index = 0;
  }
  
  this.getLink = (isIE)?function(e) {
    // I really didn't want to have to do this.
    return (e && e.target?e.target:window.event.srcElement);
  }:function(e) {
    return e.target;
  } //end getLink
  
  this.handleClick = function(e) {
  	if (e.button > 1) {
	  	// only catch left-clicks
	  	return true;
    }
    var o = self.getLink(e);
    if (o.nodeName.toLowerCase() != 'a') {
    	o = $(o).parent('a');
    	if (!o) return true;
    }
    // multifile gimana ?
    var soundURL = $(o).attr('href');
    // alert(soundURL);
    if (self.lastSound!=null) {
  		self.stop();
        return false;
  	}
  	self.config.mode = 0;
  	if ($(o).hasClass('streamplayer')) {
        self.playStream(o);
    } else {
		self.play(o);
	}
    return false;
  } //end handleClick
  
  this.mobileClick = function(e) {
    if (e.button > 1) {
	  	// only catch left-clicks
	  	return true;
    }
    //var o = self.getLink(e);
    var o = $(this);
    
    self.config.mode = 0;
    
    var tmp = $(o).attr('id');
  	tmp = tmp.split('_');
  	var idx = parseInt(tmp[tmp.length-1]);
  	self.idx = idx;
    
    // multifile gimana ?
    if (self.lastSound!=null) {
  		self.stop();
        return false;
  	}

    return false;
  } //end mobileClick
  
  this.setPosition = function(e) {
/*
    x = parseInt(e.clientX,10);
    var bar = $('#flash-player .statbar');
    var ostat = bar.offset();
    var wdh = bar.width();
    self.debug("position: "+x+" bar "+ostat.left+","+wdh);
    nMsecOffset = Math.floor(((x-ostat.left)/wdh)*self.lastSound.durationEstimate);
    self.debug("position: "+x+" bar "+ostat.left+","+wdh+","+nMsecOffset);
    if (!isNaN(nMsecOffset)) {
      nMsecOffset = Math.min(nMsecOffset,self.lastSound.duration);
    }
    if (!isNaN(nMsecOffset)) {
      self.lastSound.setPosition(nMsecOffset);
    }
*/
  } //endfunc setPosition
  
  this.handleMouseDown = function(e) {
    // a sound link was clicked
    if (isTouchDevice && e.touches) {
      e = e.touches[0];
    }
    if (e.button === 2) {
      return false; // ignore right-clicks
    }
    if (self.config.mode!=2) {
	    //self.lastSound.pause();
	    self.setPosition(e);
  	}
    return true;
  } // endfunc handlemousedown
  
  this.updateUI = function() {}  
  this.stop = function() {
  	if (self.lastSound!=null) {
  		self.lastSound.stop();
  	}
  } //end stops
  
  this.togglePause = function() {
  	if (self.lastSound!=null) {

  		self.lastSound.togglePause();
  	}
  } //end pause
  
  this.play = function(soundURL) {
  	if (self.lastSound!=null) {
        //alert("last sound");
  		if (self.lastSound.playState==0) self.lastSound.play();
  	} else {
        //alert("new sound");
  		var sURL = "";
  		if (typeof soundURL=='object') {
  			var tmp = $(soundURL).attr('id');
  			tmp = tmp.split('_');
  			var idx = parseInt(tmp[tmp.length-1]);
  			self.idx = idx;
  			self.debug("play idx: "+idx);
  			sURL = $(soundURL).attr(self.srcname);
  			var tmp = sURL.split(',');
  			self.detCount = tmp.length;
  			self.playObj = soundURL;
  			if (self.detIdx>=0 && self.detIdx<self.detCount) {
  				sURL = tmp[self.detIdx];
  			} else {
  				// idx tidak valid
  				return false;
  			}
  		} else if (typeof soundURL=='string'){
  			self.detIdx = 0;
  			self.detCount = 0;
  			sURL = soundURL;
  		} else {
            return false;
  		}
        
  		//if (!sURL || !sm.canPlayURL(sURL)) {
	    //	return true; // pass-thru for non-MP3/non-links
	    //}
        
	    if (self.lastSound==null) {
            var opts = {
	    		id:'sr_soundplay',
	    		url:sURL,
	    		onplay:self.events.play,
	    		onstop:self.events.stop,
	    		onpause:self.events.pause,
	    		onresume:self.events.resume,
	    		onfinish:self.events.finish,
	    		onload:self.events.load,
	    		onid3:self.events.id3,
	    		onbufferchange:self.events.bufferchange,
	    		whileloading:self.events.whileloading,
	    		whileplaying:self.events.whileplaying
	    	};
            if (self.config.mode == 2) {
                //opts.type = 'audio/mpeg';
            } 
	    	self.lastSound = sm.createSound(opts);
            //alert("create");
	    	self.lastSound.play();
	    }
  	}
  	return false;
  } //end play
  
  this.playMobile = function() {
    self.srcname = 'audiofile';
    this.showStreamPlayer();
    
	var idx = self.idx;
    if (idx>-1) {
        self.updateUI($('#_srsnd_'+idx));
        return self.play($('#_srsnd_'+idx));
    }
    return false;
  } //end playMobile;
  
  this.showStreamPlayer = function() {
  	self._ui.buPrev.hide();
  	self._ui.buNext.hide();
  	self._ui.buList.hide();
  	self._ui.controls.width('40px');
  	self._ui.player.width('45px');
  }
  this.hideStreamPlayer = function() {
  	self._ui.buPrev.show();
  	self._ui.buNext.show();
  	self._ui.buList.show();
  	self._ui.controls.width('100px');
  	self._ui.player.width('210px');
  }
  
  this.playStream = function(soundURL) {
    //alert(soundURL);
  	self.config.mode = 2;
  	this.showStreamPlayer();
  	return self.play(soundURL);
  } //end play
  
  this.playPlaylist = function() {
  		if (self.playlist.length>0) {
  			self.config.playNext = true;
  			self.config.mode = 1; //playlist mode
  			//var firstURL = $(self.playlist[0]).attr('href');
  			//alert($(firstURL).attr('href'));
  			//self.playObj = self.playlist[0];
  			self.index = 0;
  			//self.play(firstURL);
  			self.play(self.playlist[0]);
  		}
  } //end playPlaylist
  this.stopPlaylist = function() {
  	self.stop();
  	//self.config.playNext = false;
  	self.config.mode = 0; //playlist mode
  	//self.playObj = null;
  }//end stopPlaylist
  
  this.nextPlaylist = function() {
  	if (self.lastSound!=null) {
  		self.stop();
  	}
	//alert(self.index);
	var idx = self.index+1;
	if (self.playlist.length>=idx) {
		self.index = idx;
		self.play(self.playlist[idx]);
	}
/*
  	var idx = $(self.playObj).parent().parent().attr('id');
		idx = parseInt(idx)+1;
		if (self.playlist.length>idx) {
			//var soundURL = $(self.playlist[idx]).attr('href');
			self.index = idx;
			//self.playObj = self.playlist[idx];
			//self.play(soundURL);
			self.play(self.playlist[idx]);
		}
*/
  }//end stopPlaylist
  
  this.prevPlaylist = function() {
  	if (self.lastSound!=null) {
  		self.stop();
  	}
	var idx = self.index-1;
	if (0<=idx) {
		self.index = idx;
		self.play(self.playlist[idx]);
	}
/*
  	var idx = $(self.playObj).parent().parent().attr('id');
		idx = parseInt(idx)-1;
		if (0<=idx) {
			//var soundURL = $(self.playlist[idx]).attr('href');
			self.index = idx;
			//self.playObj = self.playlist[idx];
			//self.play(soundURL);
			self.play(self.playlist[idx]);
		}
*/
  }//end stopPlaylist
  
  this.nextPlay = function() {
  	if (self.lastSound!=null) {
  		self.stop();
  	}
	self.idx = self.idx+1;
    self.playMobile();
  }//end nextPlay
  this.prevPlay = function() {
  	if (self.lastSound!=null) {
  		self.stop();
  	}
	self.idx = self.idx-1;
  	self.playMobile();
  } //end prevPlay
  this.getPlayState = function() {
    if (self.lastSound!=null) {
        return self.lastSound.playState;
    }
    return false;
  } //end getPlayState
}

suararadioPlayer = new SuararadioPlayer();
soundManager.useFlashBlock = true;
soundManager.debugMode = true;
soundManager.consoleOnly = true;
soundManager.url = '/wp-content/plugins/suararadio/sm/swf/'; //'./swf/';
soundManager.flashVersion = 9;
soundManager.useHTML5Audio = true;
soundManager.preferFlash = true;
//soundManager.html5Test = /^(probably|maybe)$/i
//soundManager.waitForWindowLoad = true;
soundManager.onready(function() {
  suararadioPlayer.init();
});