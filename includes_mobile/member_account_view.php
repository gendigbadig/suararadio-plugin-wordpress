<?php
/*
  Member Account View
*/
#$logs = $suararadio->getLastLog();
#$balance = $suararadio->getBalance();
#$varcustom = array("user_ID"=>$user_ID,"PHPSESSID"=>session_id(),"poin"=>10000);

global $current_user;
global $wp_roles;

#var_dump($current_user->roles);
#$options = get_option('wp_user_roles');
//var_dump($options);
$img_profile_url = suararadio_get_profile_photo();
$type = $current_user->member_type?$current_user->member_type:"Buddy";

?>
<section id="accountView">
	<header>
		<span align="left"><?php echo $current_user->nickname; ?> klub radio member page</span>
		<div id="accToolbar">
			<ul class="memClickable">
			<li style="margin-right: 5px;"><!--<a href="#" onclick="member_account();">account</a>--></li>
			<li style="margin-right: 0px;"><!--<a href="<?php echo wp_logout_url( "/" ); ?>" title="logout">logout</a>--></li>
			</ul>
		</div>
	</header>
	<section class="accDetail">
		<figure class="accFoto"><img src="<?php echo $img_profile_url; ?>"></figure>
		<div class="accInfo">
			<label>Nama</label><span> : <?php echo $current_user->display_name; ?></span>
			<label>KlubId</label><span> : <?php echo $current_user->klubid ?></span>
			<label>HP</label><span> : </span>
			<label>Radio</label><span> : <?php echo $type; ?></span>

		</div>
	</section>
	<section class="accSince">member since: <?php echo $current_user->user_registered; ?></section>
	<!-- div class="accButton">
		<ul class="memClickable">
		<li><a class="selected" href="/member/">edit</a></li>
		</ul>
	</div -->
</section>