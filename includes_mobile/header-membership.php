<!DOCTYPE html>
<html>
	<head profile="http://gmpg.org/xfn/11">
		<meta name="MobileOptimized" content="width"/> 
		<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
		<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" /> <!-- leave this for stats -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">

		<title><?php bloginfo('name'); ?> <?php if ( is_single() ) { ?> &raquo; Blog Archive <?php } ?> <?php wp_title(); ?></title>
		<?php //wp_head_mobile(); ?>
		<?php wp_head(); ?>

		<!--<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />-->
		<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_option('home'); ?>/wp-content/themes/mobile_new/style_membership.css" />
		<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
		<link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
		<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />        




		<!-- <script type="text/javascript" src="<?php echo get_option('home'); ?>/wp-includes/js/iscroll/iscroll.js"></script>    
		<script type="text/javascript">
			var myScroll;
			function loaded() {
				document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
				myScroll = new iScroll('content');
			}
			document.addEventListener('DOMContentLoaded', function () { setTimeout(loaded, 200); }, false);
		</script> -->
                      

			
<script type="text/javascript">

function member_playlist(){
$.ajax({
   type: "GET",
  url: '/apis/data/member_playlist/',
  success: function(data) {
  	$('#div_klubradioIsiRight').html(data);
	$('#div_dataView').hide();	
	$('.utamaIsi').show();
	$('#div_klubradioIsiRight').show();
	//member_account_view();
       $('#view_account').hide();
       suararadioPlayer.makeSoundPlayer();
    	suararadioPlayer.makePlaylist();

  }
});
}


function member_account_view(){
$.ajax({
   type: "GET",
  url: '/apis/data/member_account_view/',
  success: function(data) {
    $('#div_accountView').html(data);
    $('#view_account').hide();
  }
});
}


function search_content() {
		var mkeyword = $('#mkeyword').val();
		var mcat = $('#mcat').val();
		var params = 'mcat=' + mcat + '&mkeyword=' + mkeyword ;
		//alert(params);
		member_content(params);

	}

function member_content(params){
$.ajax({
   type: "POST",
  url: '/apis/data/member_content/',
  data: params,
  success: function(data) {
    $('#div_dataView').html(data);
    $('.utamaIsi').show();
    $('#div_klubradioIsiRight').hide();	
    $('#div_dataView').show();
    $('#view_account').hide();
    //member_account_view();
    suararadioPlayer.makeSoundPlayer();
    suararadioPlayer.makePlaylist();	

  }
});
}

function member_music(params){
//$('#loading').html('<div align="center" ><img src="<?php echo bloginfo('template_url'); ?>/images/loading-gif.gif" /></div>');
$.ajax({
   type: "POST",
  url: '/apis/data/member_music/',
  data: params,
  success: function(data) {
    $('#loading').html('');
    $('#div_dataView').html(data);
    $('.utamaIsi').show();
    $('#div_klubradioIsiRight').hide();
    $('#div_dataView').show();
    $('#view_account').hide(); 
    //member_account_view();
    suararadioPlayer.makeSoundPlayer();
    suararadioPlayer.makePlaylist();
    
  }
});
}

function search_music() {
		var mkeyword = $('#mkeyword').val();
		var mgenre = $('#mgenre').val();
		var params = 'mgenre=' + mgenre + '&mkeyword=' + mkeyword + '&morder=';
		//alert(params);
		member_music(params);

	}

function clickOrder(morder) {
		$('#frmMember input[name="morder"]').val(morder);
		$('#frmMember input[name="mkeyword"]').val('');
		$('#frmMember input[name="mgenre"]').val('');
		var params = $('#frmMember').serialize();
		//alert(params);
		member_music(params);
	}

function member_account(){

$.ajax({
   type: "GET",
  url: '/apis/data/member_account/',
  success: function(data) {
    $('#view_account').show();
    $('#view_account').html(data);
    //$('#detail_song').hide();
    $('.utamaIsi').hide();
     	
  }
});
}

function member_virtualcard(){

$.ajax({
   type: "GET",
  url: '/apis/data/member_virtualcard/',
  success: function(data) {
     $('#view_account').html(data);
    $('.utamaIsi').hide();
  }
});
}

$(document).ready(function(){
	//member_playlist();
	//member_account_view();
	//member_content();
	member_account();
	//member_virtualcard();
	
});


</script>



	</head>
	<body>		
		              
<!--Begin canvas-->                    
    	<a href="#" onclick="member_account();">
		<section  id="header-content">
    		</section>
	</a>
<section id="content">
 		<div id="scroller">             
            		<section id="isi_konten">
				<div id="loading" align="center" style="width:100%"></div>

       
       