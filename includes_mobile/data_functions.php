<?php
/*
License:
 ==============================================================================

    Copyright 2012  M Zhuhriansyah R  (email : izhur2001@yahoo.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-107  USA
*/

if (!defined('META_RADIO_NAME')) define('META_RADIO_NAME','');
if (!defined('META_THUMB_MOST_PLAYED')) define('META_THUMB_MOST_PLAYED','mostplayed');
if (!defined('META_THUMB_KONTEN_PILIHAN')) define('META_THUMB_KONTEN_PILIHAN','advimage');
if (!defined('META_PODPRESSMEDIA')) define('META_PODPRESSMEDIA','podPressMedia');


function suararadio_data_process($paths) {
	$params = array();
	switch ($paths[3]) {
		case "mostplayed":
			$params['mode'] = $paths[4]; 
			$params['catid'] = $paths[5];
			return suararadio_data_most_played($params);
			break;
		case "kontenpilihan":
			return suararadio_data_konten_pilihan();
			break;
		case "category":
			return suararadio_data_category();
			break;
		case "lists":
			$catid = $paths[4];
			return suararadio_data_lists($catid);
			break;
		case "detail":
			$postid = $paths[4];
			return suararadio_data_detail($postid);
			break;
		default:
			status_header('404');
	}
} //endfunc suararadio_melon_process

function suararadio_data_most_played($params) {
	global $suararadio;
	global $current_user;
	global $wpdb;
	global $radio;
	
	$tmp = array();
	$data = array();
	$week = '';
	$tahun = '';
	$bulan = '';
	$catid = ($params['catid']!='' && $params['catid']>0) ? $params['catid']:0;
	switch ($params['mode']) {
		case "lastweek":
			if(date("W") == "01") {
				$week = 52; $tahun = date("Y") - 1;
			} else { 
				$week = date("W") - 1; $tahun = date("Y"); 
			}
			$data = getMostly('play','W',$week,0,$tahun,$catid);
			break;
		case "lastmonth":
			if(date("n") == 1){
				$bulan = 12; $tahun = date("Y") - 1;
			} else { 
				$bulan = date("n") - 1; $tahun = date("Y");
			}
			$data = getMostly('play','M',0,$bulan,$tahun,$catid);
			break;
		case "alltime":
			$data = getMostly('play','A',0,0,0,$catid);
			break;
		default:
	}
	
	// thumbkontenpremiumradio ---> mostplayed
	foreach($data as $item){
		$vtmp = array();
		$vtmp['title'] = $item->post_title;
		$vtmp['id'] = $item->postid;
		$ditem = get_post( $item->postid );
		$vtmp['post_date'] = $ditem->post_date;
		$vtmp['tags'] = wp_get_post_tags($item->postid);
		$vtmp['file'] = suararadio_getPodcastUrl($item->postid);
        $media = get_post_meta($item->postid, META_PODPRESSMEDIA, true);
        foreach ($media as $vmedia) {
            $vparse = parse_url($vmedia['URI']);
            if ($vparse['host']!=$_SERVER['HTTP_HOST']) {
                $vtmp['file_redirect'] = $vmedia['URI'];
            }
        }
        
		$vtmp['url'] = "http://".$_SERVER['HTTP_HOST'].get_permalink($item->postid);
		if (META_RADIO_NAME!='') {
			$vtmp['radio'] = get_post_meta($item->postid, META_RADIO_NAME, true);
		} else {
			$vtmp['radio'] = $radio;
		}
		$vtmp['thumb'] = get_post_meta($item->postid, META_THUMB_MOST_PLAYED, true);
		$vtmp['played'] = $item->jmlplay;
		$vtmp['play_count'] = podPress_playCount($item->postid);
		$tmp[] = $vtmp;
	}
	
	echo json_encode($tmp);
	return true;
} //endfunc suararadio_most_played

function suararadio_data_konten_pilihan() {
	global $suararadio;
	global $current_user;
	global $wpdb;

	$tmp = array();
	// kontenpilihanutama --> advimage
	$sql = "SELECT wposts.ID,wposts.post_title,wpostmeta.meta_key,wposts.post_date ".
		"FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta ".
		"WHERE wposts.ID = wpostmeta.post_id ". 
		"AND wpostmeta.meta_key = '".META_THUMB_KONTEN_PILIHAN."' ".
		"AND wposts.post_type='post' ".
		"AND wposts.post_status = 'publish' ".																			    
		"ORDER BY wposts.post_modified DESC limit 0,4";
	$pageposts = $wpdb->get_results($sql, OBJECT);
	foreach($pageposts as $post){
		$vtmp = array();
		$vtmp['title'] = $post->post_title;
		$vtmp['id'] = $post->ID;
		$vtmp['post_date'] = $post->post_date;
		$vtmp['tags'] = wp_get_post_tags($post->ID); 
		$vtmp['file'] = suararadio_getPodcastUrl($post->ID);
        $media = get_post_meta($post->ID, META_PODPRESSMEDIA, true);
        foreach ($media as $vmedia) {
            $vparse = parse_url($vmedia['URI']);
            if ($vparse['host']!=$_SERVER['HTTP_HOST']) {
                $vtmp['file_redirect'] = $vmedia['URI'];
            }
        }
		$vtmp['url'] = "http://".$_SERVER['HTTP_HOST'].get_permalink($post->ID);
		if (META_RADIO_NAME!='') {
			$vtmp['radio'] = get_post_meta($item->postid, META_RADIO_NAME, true);
		} else {
			$vtmp['radio'] = $radio;
		}
		$vtmp['thumb'] = get_post_meta($post->ID, $post->meta_key, $single = true);
		$vtmp['play_count'] = podPress_playCount($post->ID); 
		$tmp[] = $vtmp;
	}

	echo json_encode($tmp);
	return true;
} //endfunc suararadio_konten_pilihan

function suararadio_data_category() {
	global $suararadio;
	global $current_user;
	global $wpdb;
	
	$params = $_REQUEST;

	$tmp = array();
	$params['type'] = ($params['type']) ? $params['type']:'post';
	$params['child_of'] = ($params['child_of']) ? $params['child_of']:'1';
	$params['orderby'] = ($params['orderby']) ? $params['orderby']:'count';
	$params['parent'] = ($params['parent']) ? $params['parent']:'0';
	$params['order'] = ($params['order']) ? $params['order']:'desc';
	$params['hide_empty'] = ($params['hide_empty']) ? $params['hide_empty']:'1';
	$params['hierarchical'] = ($params['hierarchical']) ? $params['hierarchical']:'1';
	$params['taxonomy'] = ($params['taxonomy']) ? $params['taxonomy']:'category';
	
	$tmp = get_categories($params);

	echo json_encode($tmp);
	return true;
} //endfunc suararadio_category

function suararadio_data_lists($catid) {
	global $suararadio;
	global $current_user;
	global $wpdb;
	
	$tmp = array();
	$param = array("cat"=>$category_id,
					 "post_type"=>"post",
					 "post_status"=>"publish",			 
					 "meta_key"=>META_THUMB_MOST_PLAYED,
					 "meta_value"=>"__NULL__",
					 "meta_compare"=>"!=",
 					 "orderby"=>"modified",
					 "order"=>"DESC",
					 "posts_per_page"=>1
		
	);
	$the_query = new WP_Query( $param );
	$the_query->have_posts();
	$the_query->the_post();
	$flag = 0;
	if(count($post) > 0){
		$flag = 1;
		
		$vtmp = array();
		$vtmp['title'] = $post->post_title;
		$vtmp['id'] = $post->ID;
		$vtmp['post_date'] = $post->post_date;
		$vtmp['tags'] = wp_get_post_tags($post->ID);
		$vtmp['file'] = suararadio_getPodcastUrl($post->ID);
        $media = get_post_meta($post->ID, META_PODPRESSMEDIA, true);
        foreach ($media as $vmedia) {
            $vparse = parse_url($vmedia['URI']);
            if ($vparse['host']!=$_SERVER['HTTP_HOST']) {
                $vtmp['file_redirect'] = $vmedia['URI'];
            }
        }
		$vtmp['url'] = "http://".$_SERVER['HTTP_HOST'].get_permalink($post->ID);
		if (META_RADIO_NAME!='') {
			$vtmp['radio'] = get_post_meta($item->postid, META_RADIO_NAME, true);
		} else {
			$vtmp['radio'] = $radio;
		}
		$vtmp['thumb'] = get_post_meta($post->ID, $post->meta_key, $single = true);
		$vtmp['play_count'] = podPress_playCount($post->ID);
		$tmp[] = $vtmp;
		
	}
	echo json_encode($tmp);
	return true;
} //endfunc suararadio_list_contents

function suararadio_data_detail($postid) {
	global $wpdb;
	
	$tmp = array();
	$tmp = get_post( $postid );
	echo json_encode($tmp);
	return true;
} //endfunc suararadio_detail


