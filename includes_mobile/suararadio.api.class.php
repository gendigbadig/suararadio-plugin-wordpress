<?php


/**
 * Suararadio API Exception
 * @author izhur2001
 * 
 * error list:
 * E001 : Unkown mode on search integration
 * E002 : Unkown get property
 */
class SuararadioException extends Exception { 
	
}

if (!defined(DELIMA_MERCHANT_CODE)) define('DELIMA_MERCHANT_CODE','195106067343');
if (!defined(DELIMA_URL)) define('DELIMA_URL','https://em.telkomdelima.com/jets-delima/WebCollection.action');
if (!defined(API_SUARARADIO_URL)) define ('API_SUARARADIO_URL','http://api.suararadio.com/');

/**
 * Enter description here ...
 * @author izhur2001
 *
 */
class SuararadioAPI{

    protected $curl;
    protected $sapiUrl;
    protected $clcode;
    protected $userID;
    protected $userPasswd;
    
    private $cookname;
    private $cname = "webapi";
    private $cpass = "dzNiNHAxX2FjYw==";

    function __construct($sapiUrl='',$clientName='',$clientPasswd='') {
        $this->curl = curl_init();
        $this->sapiUrl = API_SUARARADIO_URL;
        if ($sapiUrl!='') $this->sapiUrl = $sapiUrl;
        if ($clientName!='') $this->cname = $clientName;
        if ($clientPasswd!='') $this->cpass = $clientPasswd;
        $this->cookname = md5($_SERVER['HTTP_HOST']);
    }
    
    // setter & getter
    public function __get($name) {
    	if (array_key_exists($name, get_class_vars(__CLASS__))) {
    		return $this->name;
    	} else {
    		throw new SuararadioException("Unkown get property", 'E002');
    	}
    }

    protected function startSapi(){
        try {
            curl_setopt($this->curl, CURLOPT_POST, 1);
            curl_setopt($this->curl, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
            curl_setopt($this->curl, CURLOPT_POSTFIELDS, 'username=' . urlencode($this->cname) . '&password=' . urlencode($this->cpass));
            curl_setopt($this->curl, CURLOPT_COOKIEJAR, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
            curl_setopt($this->curl, CURLOPT_URL, $this->sapiUrl . 'site/login');
            curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
            $result = curl_exec($this->curl);
			$this->chcode = @json_decode($result)->channelCd;
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    
    public function init() {
    	return $this->startSapi();
    } //endfunc init
    
    
    public function klubVoucher($vars) {
    	$var = http_build_query($vars);
    	
    	curl_setopt($this->curl, CURLOPT_POST, 1);
    	curl_setopt($this->curl, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
    	curl_setopt($this->curl, CURLOPT_URL, $this->sapiUrl . "klub/voucher");
    	curl_setopt($this->curl, CURLOPT_POSTFIELDS, $vars);
    	curl_setopt($this->curl, CURLOPT_COOKIEJAR, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
    	curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
    	
    	$result = curl_exec($this->curl);
    	#var_dump($vars);
    	$result = json_decode($result,true);
    	if(@$result['code']=="E001"){
    		$this->startSapi();
    		return $this->klubVoucher($vars);
    	}else{
    		return $result;
    	}
    }
    
    public function klubPrice($vars) {
    	$var = http_build_query($vars);
    	
    	$code = "";
    	if (isset($vars['code']) && $vars['code']!='') {
    		$code = "/code/".$vars['code'];
    	}
    	 
    	curl_setopt($this->curl, CURLOPT_POST, 1);
    	curl_setopt($this->curl, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
    	curl_setopt($this->curl, CURLOPT_URL, $this->sapiUrl . "klub/price".$code);
    	curl_setopt($this->curl, CURLOPT_POSTFIELDS, $vars);
    	curl_setopt($this->curl, CURLOPT_COOKIEJAR, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
    	curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
    	 
    	$result = curl_exec($this->curl);
    	$result = json_decode($result,true);
    	if(@$result['code']=="E001"){
    		$this->startSapi();
    		return $this->klubPrice($vars);
    	}else{
    		return $result;
    	}
    } //endfunc klubPrice
    
    public function klubRegisterMelon($vars) {
    	$var = http_build_query($vars);
    	 
    	curl_setopt($this->curl, CURLOPT_POST, 1);
    	curl_setopt($this->curl, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
    	curl_setopt($this->curl, CURLOPT_URL, $this->sapiUrl . "klub/registermelon");
    	curl_setopt($this->curl, CURLOPT_POSTFIELDS, $var);
    	curl_setopt($this->curl, CURLOPT_COOKIEJAR, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
    	curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
    
    	$result = curl_exec($this->curl);
    	#var_dump($result,$var);
    	$result = json_decode($result,true);
    	if(@$result['code']=="E001"){
    		$this->startSapi();
    		return $this->klubRegisterMelon($vars);
    	}else{
    		return $result;
    	}
    } //endfunc klubRegisterMelon
    
    public function klubPromo($vars) {
    	$var = http_build_query($vars);
    	 
    	curl_setopt($this->curl, CURLOPT_POST, 1);
    	curl_setopt($this->curl, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
    	curl_setopt($this->curl, CURLOPT_URL, $this->sapiUrl . "klub/promo");
    	curl_setopt($this->curl, CURLOPT_POSTFIELDS, $var);
    	curl_setopt($this->curl, CURLOPT_COOKIEJAR, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
    	curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
    	 
    	$result = curl_exec($this->curl);
    	#var_dump($result);
    	$result = json_decode($result,true);
    	if(@$result['code']=="E001"){
    		$this->startSapi();
    		return $this->klubPromo($vars);
    	} else {
    		return $result;
    	}
    } //endfunc klubPromo
    
    public function delimaInvoice($vars) {
    	$var = http_build_query($vars);
    	
    	curl_setopt($this->curl, CURLOPT_POST, 1);
    	curl_setopt($this->curl, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
    	curl_setopt($this->curl, CURLOPT_URL, $this->sapiUrl . "delima/invoice");
    	curl_setopt($this->curl, CURLOPT_POSTFIELDS, $vars);
    	curl_setopt($this->curl, CURLOPT_COOKIEJAR, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
    	curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
    	
    	$result = curl_exec($this->curl);
    	$result = json_decode($result,true);
    	if(@$result['code']=="E001"){
    		$this->startSapi();
    		return $this->delimaInvoice($vars);
    	}else{
    		return $result;
    	}
    } //endfunc delimaInvoice
    
    public function delimaMembership($vars) {
    	$var = http_build_query($vars);
    	 
    	curl_setopt($this->curl, CURLOPT_POST, 1);
    	curl_setopt($this->curl, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
    	curl_setopt($this->curl, CURLOPT_URL, $this->sapiUrl . "delima/membership");
    	curl_setopt($this->curl, CURLOPT_POSTFIELDS, $vars);
    	curl_setopt($this->curl, CURLOPT_COOKIEJAR, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
    	curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
    	 
    	$result = curl_exec($this->curl);
    	$result = json_decode($result,true);
    	if(@$result['code']=="E001"){
    		$this->startSapi();
    		return $this->delimaInvoice($vars);
    	}else{
    		return $result;
    	}
    } //endfunc delimaMembership
    
    public function getRadioID($vars) {
    	$var = http_build_query($vars);
    	 
    	curl_setopt($this->curl, CURLOPT_POST, 1);
    	curl_setopt($this->curl, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
    	curl_setopt($this->curl, CURLOPT_URL, $this->sapiUrl . "klub/radio");
    	curl_setopt($this->curl, CURLOPT_POSTFIELDS, $var);
    	curl_setopt($this->curl, CURLOPT_COOKIEJAR, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
    	curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
    	 
    	$result = curl_exec($this->curl);
    	$result = json_decode($result,true);
    	if(@$result['code']=="E001"){
    		$this->startSapi();
    		return $this->getRadioID($vars);
    	}else{
    		return $result;
    	}
    }
    
    public function getDelimaLink($vars) {
    	return DELIMA_URL."?MerchantCode=".DELIMA_MERCHANT_CODE."&invoiceNo=".$vars['invoice_no'];
    } //endfunc getDelimaLink
    
    public function getMember($vars) {
    	$var = http_build_query($vars);
    	
    	curl_setopt($this->curl, CURLOPT_POST, 1);
    	curl_setopt($this->curl, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
    	curl_setopt($this->curl, CURLOPT_URL, $this->sapiUrl . "klub/member");
    	curl_setopt($this->curl, CURLOPT_POSTFIELDS, $var);
    	curl_setopt($this->curl, CURLOPT_COOKIEJAR, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
    	curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
    	
    	$result = curl_exec($this->curl);
    	$result = json_decode($result,true);
    	if(@$result['code']=="E001"){
    		$this->startSapi();
    		return $this->getMember($vars);
    	} else {
    		return $result;
    	}
    } //endfunc getMember
    
    public function melonConnect($vars) {
    	$var = http_build_query($vars);
    	
    	curl_setopt($this->curl, CURLOPT_POST, 1);
    	curl_setopt($this->curl, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
    	curl_setopt($this->curl, CURLOPT_URL, $this->sapiUrl . "klub/melonconnect");
    	curl_setopt($this->curl, CURLOPT_POSTFIELDS, $var);
    	curl_setopt($this->curl, CURLOPT_COOKIEJAR, $_SERVER['DOCUMENT_ROOT'] . "/data/".$this->cookname);
    	curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
    	
    	$result = curl_exec($this->curl);
    	$result = json_decode($result,true);
    	if(@$result['code']=="E001"){
    		$this->startSapi();
    		return $this->melonConnect($vars);
    	} else {
    		return $result;
    	}
    }
} //endclass
