<?
/*
License:
 ==============================================================================

    Copyright 2010  M Zhuhriansyah R  (email : izhur2001@yahoo.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-107  USA
    
    5/5/2010 4:05:13 PM MZR
*/
include_once 'melon.class.php'; 

class suararadioMusic_class extends suararadio_class {
	var $num_per_page = 10;
	var $rep_from = array('-','/');
	var $rep_to = array('_','.');
	var $mapi;
	var $mode = 'melon'; // melon | rise
	
	//ctor
	function suararadioMusic_class() {
		$this->mapi = new MelOnAPI(MELON_API_URL,MELON_CLIENT,MELON_PASSWORD);
	}
	
	function getGenres() {
		global $wprisedb;
		
		$genres = $wprisedb->get_results(
			$wprisedb->prepare("select genre from t_genre where stat='1' and id_branch= %s ",RISE_ID_BRANCH),
			ARRAY_A);
		return $genres;
	}
	
	function getTypes() {
		global $wprisedb;
		
		$types = $wprisedb->get_results(
			$wprisedb->prepare("select type from t_type where stat='1' and id_branch= %s ",RISE_ID_BRANCH),
			ARRAY_A);
		return $types;
	}
	
	function getBeats() {
		global $wprisedb;
		
		$beats = $wprisedb->get_results(
			$wprisedb->prepare("select beat from t_type where stat='1' and id_branch= %s ",RISE_ID_BRANCH),
			ARRAY_A);
		return $beats;
	}
	
	function getLanguages() {
		global $wprisedb;
		
		$langs = $wprisedb->get_results(
			$wprisedb->prepare("select bahasa from t_bahasa where stat='1' and id_branch= %s ",RISE_ID_BRANCH),
			ARRAY_A);
		return $langs;
	}
	
	function getStyles() {
		global $wprisedb;
		
		$langs = $wprisedb->get_results(
			$wprisedb->prepare("select bahasa from t_bahasa where stat='1' and id_branch= %s ",RISE_ID_BRANCH),
			ARRAY_A);
		return $langs;
	}
	
	function getGender() {
		global $wprisedb;
		
		$langs = $wprisedb->get_results(
			$wprisedb->prepare("select bahasa from t_bahasa where stat='1' and id_branch= %s ",RISE_ID_BRANCH),
			ARRAY_A);
		return $langs;
	}
	
	function getLagu($lagu_id) {
		if ($this->mode=='melon') {
			return $this->getLaguMelon($lagu_id);
		} else {
			return $this->getLaguRise($lagu_id);
		}	
	}
	
	function getLaguRise($lagu_id) {
		global $wprisedb;
		
		$lagu = $wprisedb->get_row(
			$wprisedb->prepare("select * from t_lagu where id_branch= %s and id_lagu = %s ",RISE_ID_BRANCH,$lagu_id),
			ARRAY_A);
		return $lagu;
	}
	
	function getAlbumMelon($albumId) {
		return $this->mapi->getAlbums(array('albumId'=>$albumId));
	}
	
	function getLaguMelon($lagu_id) {
		$params = array();
		$params['songId'] = $lagu_id;
		return $this->mapi->getSongs($params);
	}
	
	function getListLagu($params=array()) {
		if ($this->mode=='melon') {
			return $this->getListLaguMelon($params);
		} else {
			return $this->getListLaguRise($params);
		}
	}
	
	function getListLaguMelon($params=array()) {
		global $max_page;
		global $paged;
		
		$limitrow = ($params['numrow'])? $params['numrow']:$this->num_per_page;
		$page = ($params['page'])? $params['page']:1;
		
		// paged limit
		if ($limitrow!='') {
			$start = ($params['start'])? $params['start']:0;
			if ($page>1) {
				$start = $limitrow*($page-1);
			}
		}
		
		$genreId = '';
		if ($params['genre']!='') {
			$genreId = $params['genre']; 
		}
		$keyword = '';
		if ($params['keyword']!='') {
			$keyword = $params['keyword'];
		}
		// query data
		$mpars = array();
		$mpars['offset'] = $start;
		$mpars['limit'] = $limitrow;
		if ($params['show']=='new') {
			$mpars['mode'] = 'new';
			$mpars['genreId'] = $genreId;
			$data = $this->mapi->getSongs($mpars);
			goto list_end;
		}
		if ($params['show']=='chartd') {
			$data = $this->mapi->getChart('daily','latest',$mpars);
			goto list_end;
		}
		if ($params['show']=='chartw') {
			$chart = $this->mapi->getChart('weekly');
			$data = $this->mapi->getChart('weekly',$chart[0]['startDate'],$mpars);
			goto list_end;
		}
		if ($params['show']=='chartm') {
			$chart = $this->mapi->getChart('monthly');
			$data = $this->mapi->getChart('monthly',$chart[0]['startDate'],$mpars);
			goto list_end;
		}
		if (trim($keyword)=='') {
			$mpars['mode'] = 'genre';
			$mpars['genreId'] = $genreId;
			$data = $this->mapi->getSongs($mpars);
			goto list_end;
		} 
		if (trim($keyword)!='') {
			$mpars['mode'] = 'song';
			$mpars['keyword']=trim($keyword);
			$data = $this->mapi->searchIntegration($mpars);
			goto list_end;
		}
		list_end:
		$nums = $data['totalSize'];
		$max_page = ceil($nums / $limitrow);
		return $data['dataList'];
	}
	function getListLaguRise($params=array()){
		global $wprisedb;
		global $max_page;
		global $paged;
		
		//$params['start'] = ;
		$limitrow = ($params['numrow'])? $params['numrow']:$this->num_per_page;
		$page = ($params['page'])? $params['page']:1;

		$select = "select * ";
		$query = " from t_lagu where id_branch= '".RISE_ID_BRANCH."' and stat='1' ";
		$limit = "";
		$order = "";
		
		if ($params['genre']!='') {
			$query .= " and genre= '".$params['genre']."' ";
		}
		if ($params['keyword']!='') {
			$query .= " and (judul like '%".$params['keyword']."%' or penyanyi like '%".$params['keyword']."%') ";
		}
		
		// paged limit
		if ($limitrow!='') {
			$start = ($params['start'])? $params['start']:0;
			if ($page>1) {
				$start = $limitrow*($page-1);
			}
			$limit = " limit  ".$start.",".$limitrow; // mysql specific.
		}

		$lagus = $wprisedb->get_results($select.$query.$order.$limit,ARRAY_A);
		$select = "select count(*) as num ";

		$nums = $wprisedb->get_var($select.$query);
		#$nums_per_page = get_option('posts_per_page');
		$max_page = ceil($nums / $limitrow);
		
		return $lagus;
	}
	
	function convertFromLaguId($lagu_id) {
		return str_replace($this->rep_from,$this->rep_to,$lagu_id).'.mp3';
	}
	
	function convertToLaguId($path) {
		$repto = $this->rep_to;
		array_unshift($repto,".mp3");
		$repfrom = $this->rep_from;
		array_unshift($repfrom,"");
		return str_replace($repto,$repfrom,$path);
	}
	
	/**
   * mengubah data rise lagu menjadi path url wordpress portal radio
   */
  function convertLaguToPodcastWebPath($lagu_id){
  		$link = $this->convertFromLaguId($lagu_id);
		$url = get_option('siteurl').'/suararadio_api/play_lagu/'.$link;
		return $url;
  } ##endfunc convert to podcast web path	
  
  function convertMelonToPodcastWebPath($songId) {
  		$url = get_option('siteurl').'/suararadio_api/melonplay/?songId='.$songId;
  		return $url;
  }
  
  /**
   *
   */
  function convertPodcastWebPathToLagu($path) {
  	global $wprisedb;
  	
  	$lagu_id = $this->convertToLaguId($path);
  	$lagu = $this->getLagu($lagu_id);
  	
  	//http://prod.rise-app.com/musicdirector/dir_music/KOTAKKOTAK_KEDUA%20-%20KEMBALI_UNTUKMU.MP3
  	$filename = '';
  	if ($lagu['nama_file']!='') {
	  	$filename = RISE_URL_DIR_MUSIC.rawurlencode($lagu['nama_file']);
  	} 
  	return $filename;
  } #endfunc convertPodcastWebPathToLagu
  
  
  /**
   * mengecek ukuran file di tempat remote.
   */
  function getRemoteFileHeader($url) {
        $sch = parse_url($url, PHP_URL_SCHEME);
        if (($sch != "http") && ($sch != "https") && ($sch != "ftp") && ($sch != "ftps")) {
            return false;
        }
        if (($sch == "http") || ($sch == "https")) {
            $headers = get_headers($url, 1);
            if ((!array_key_exists("Content-Length", $headers))) { return false; }
            return $headers["Content-Length"];
        }
        if (($sch == "ftp") || ($sch == "ftps")) {
            $server = parse_url($url, PHP_URL_HOST);
            $port = parse_url($url, PHP_URL_PORT);
            $path = parse_url($url, PHP_URL_PATH);
            $user = parse_url($url, PHP_URL_USER);
            $pass = parse_url($url, PHP_URL_PASS);
            if ((!$server) || (!$path)) { return false; }
            if (!$port) { $port = 21; }
            if (!$user) { $user = "anonymous"; }
            if (!$pass) { $pass = "phpos@"; }
            switch ($sch) {
                case "ftp":
                    $ftpid = ftp_connect($server, $port);
                    break;
                case "ftps":
                    $ftpid = ftp_ssl_connect($server, $port);
                    break;
            }
            if (!$ftpid) { return false; }
            $login = ftp_login($ftpid, $user, $pass);
            if (!$login) { return false; }
            $ftpsize = ftp_size($ftpid, $path);
            ftp_close($ftpid);
            if ($ftpsize == -1) { return false; }
            return $ftpsize;
        }
   } #getRemoteFileSize
////////////////////////////////////////
///////// function MELON ////////////
////////////////////////////////////////
   function getCategories() {
   		$result = $this->mapi->getCategories(array('mode'=>'all'));
   		$cats = array();
   		foreach($result as $obres) {
   			$cats[$obres['genreId']] = array($obres['orderNum'],$obres['genreName']);
   		}
   		
   		ksort($cats,SORT_STRING);
   		return $cats;
   }
   
   function getCertification($songId) {
   		global $current_user;
   		
   		if ($current_user->melon_uid!='' && $current_user->melon_stat=='a') {
   			$supportStream = "Y";
   			#$services = $this->mapi->serviceList($current_user->melon_uid);
   			#var_dump($services);
   			#foreach($services as $v){
   			#	if(@$v['supportStreamYN'] == "Y") $supportStream = "Y";
   			#}
   			$data = $this->mapi->getCertificationMusic($current_user->melon_uid, $songId, 'M');
   			$data['supportStream'] = $supportStream;
   			return $data;
   		}
   }
   
   function getAlbumsByArtist($artistId) {
   		$params = array();
   		$params['mode'] = 'artist';
   		$params['artistId'] = $artistId;
   		
   		return $this->mapi->getAlbums($params);
   }
   
   function getSongsByAlbum($albumId) {
   		$params = array();
   		$params['mode'] = 'album';
   		$params['albumId'] = $albumId;
   		$params['offset']  = 0;
   		$params['limit']  = 25;
   		return $this->mapi->getSongs($params);
   }
   
   function getDurasi($secs) {
   		$min = floor($secs/60);
   		$sec = $secs-($min*60);
   		return $min.":".$sec;
   }
   
   function formatDate($strdate) {
   		if (preg_match('/(\d{4})(\d{2})(\d{2})/',$strdate,$matches)) {
   			return $matches[3].".".$matches[2].".".$matches[1];
   		}
   		return false;
   }
   
////////////////////////////////////////////
///////// END function MELON ////////////
////////////////////////////////////////////
     
////////////////////////////////////////
///////// function PLAYLIST ////////////
////////////////////////////////////////  
  function getSessionList() {
  	return $_SESSION[SR_PLAYLIST_VAR];
  }
  
  function getPlaylistCount($listid) {
  	global $wpdb;
  	
  	$sql = "select count(*) as num from wp_suararadio_playlist_item where list_id= %s";
		$nums = $wpdb->get_var($wpdb->prepare($sql,$listid));
  	return $nums;
  }
  
  /**
   * mengambil daftar playlist
   */
  function getListPlaylist($params=array()){
		global $wpdb;
		global $max_page;
		global $paged;
		global $current_user;
		
		//$params['start'] = ;
		$limitrow = ($params['numrow'])? $params['numrow']:$this->num_per_page;
		$page = ($params['page'])? $params['page']:1;
		$uid = $current_user->ID;

		$select = "select * ";
		$query = " from wp_suararadio_playlist where user_id='$uid' ";
		$limit = "";
		$order = "";
		
		// paged limit
		if ($limitrow!='') {
			$start = ($params['start'])? $params['start']:0;
			if ($page>1) {
				$start = $limitrow*($page-1);
			}
			$limit = " limit  ".$start.",".$limitrow; // mysql specific.
		}

		$list = $wpdb->get_results($select.$query.$order.$limit,ARRAY_A);
		$select = "select count(*) as num ";

		$nums = $wpdb->get_var($select.$query);
		#$nums_per_page = get_option('posts_per_page');
		$max_page = ceil($nums / $limitrow);
		
		return $list;
  }
  
  
  /**
   * $params['list_id'] ==> edit list_id
   * $params['name']==> * nama playlist
   * $params['type']
   * $params['']
   */
  function savePlaylist($params) {
  	global $wpdb;
  	global $current_user;
  	
  	$arr = $_SESSION[SR_PLAYLIST_VAR];
  	$list_id = ($params['list_id'])? $params['list_id']:null;
  	$uid = $current_user->ID;
  	
  	if (isset($list_id)) {
  		// update list.
  		if ($_SESSION[SR_PLAYLIST_ID]==$list_id) { // check sure
  			// clear yg lama
  			$wpdb->query("delete from wp_suararadio_playlist_item where list_id='$list_id'");
  			$i = 1;
	  		foreach($arr as $varr) {
	  			$data = array(
  					'list_id'=>$list_id,'list_num'=>$i,'item_type'=>$varr['type'],
  					'item_loc'=>$varr['location'],
  					'item_meta'=>serialize($varr));
  				$wpdb->insert('wp_suararadio_playlist_item',$data);
  				$i++;
	  		}
  		}
  	} else {
  		// insert list
  		if ($params['name']=='') {
  			return false;
  		}
  		$data = array('user_id'=>$uid,'list_type'=>'ul','list_name'=>$params['name'],'list_desc'=>$params['desc']);
  		$wpdb->insert('wp_suararadio_playlist',$data);
  		if ($wpdb->insert_id) {
  			// berhasil simpan, lanjutkan dengan data itemnya
  			$list_id = $wpdb->insert_id;
  			$i = 1;
  			foreach($arr as $varr) {
  				$data = array(
  					'list_id'=>$list_id,'list_num'=>$i,'item_type'=>$varr['type'],
  					'item_loc'=>$varr['location'],
  					'item_meta'=>serialize($varr));
  				$wpdb->insert('wp_suararadio_playlist_item',$data);
  				$i++;
  			}
  		}
  	} #endif list_id
  	return true;
  } #endfunc savePlaylist
  
  function loadPlaylist($listid) {
  	global $wpdb;
  	
  	$data = array();
  	
  	$sql = $wpdb->prepare("select * from wp_suararadio_playlist where list_id= %d ",$listid);
  	$playlist = $wpdb->get_row($sql,ARRAY_A);
  	
  	$sqlitem = $wpdb->prepare("select * from wp_suararadio_playlist_item where list_id= %d order by list_num",$listid);
  	$items = $wpdb->get_results($sqlitem,ARRAY_A);
  	
  	for($i=0;$i<count($items);$i++) {
  		$items[$i]['item_meta'] = unserialize($items[$i]['item_meta']);
  	}
  	
  	$data['playlist'] = $playlist;
  	$data['items'] = $items;
  	
  	return $data;
  } #endfunc loadPlaylist
  
  function delPlaylist($listid) {
  	global $wpdb;
  	
  	$sql = $wpdb->prepare("delete from wp_suararadio_playlist where list_id=%d",$listid);
  	$sql2 = $wpdb->prepare("delete from wp_suararadio_playlist_item where list_id=%d",$listid);
  	
  	$wpdb->query($sql2);
  	$wpdb->query($sql);
  	return true;
  } #endfunc delPlaylist
  
  
  
////////////////////////////////////////////
///////// END function PLAYLIST ////////////
////////////////////////////////////////////


}

?>