<?
/*
License:
 ==============================================================================

    Copyright 2010  M Zhuhriansyah R  (email : izhur2001@yahoo.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-107  USA
    
    5/5/2010 4:05:13 PM MZR
*/
define('SR_MEMBER_VAR','suararadio_member_stat');

class suararadio_class {
	var $api =  null;
	
	function getBaseReq() {
		$arreq = explode('/',str_ireplace("?".$_SERVER['QUERY_STRING'],"",$_SERVER['REQUEST_URI']));
		$basereq = array();
		foreach ($arreq as $vreq) {
			if ($vreq!='page') {
				if ($vreq!='') $basereq[]=$vreq;
			} else {
				break;
			}
		}
		$basereq = "/".implode("/",$basereq)."/";
		return $basereq;
	}
	
	function getStatData() {
		$basereq = $this->getBaseReq();
		$filter = $_SESSION['_member'][$basereq];
		
		if (isset($_POST['morder'])) $filter['morder'] = $_POST['morder'];
		if (isset($_POST['mcat'])) $filter['mcat'] = $_POST['mcat'];
		if (isset($_POST['mgenre'])) $filter['mgenre'] = $_POST['mgenre'];
		if (isset($_POST['mkeyword'])) $filter['mkeyword'] = $_POST['mkeyword'];
		if (isset($_POST['msubmenu'])) $filter['msubmenu'] = $_POST['msubmenu'];
		
		$_SESSION['_member'][$basereq] = $filter;
		#$_SESSION['member']["ADE"] = $filter;

		#$_SESSION['member']["xxx"] =  "bukan novie";
		#echo $basereq." basereq<BR>".$_SESSION['member']["/member/music/"]."<--";
		//var_dump($basereq,$filter,$_SESSION);
#var_dump($_POST,$_SESSION);
#		echo "<BR>".$_SESSION['member'][$basereq]." fff";
		return $filter;
	}

	function setSessData() {
		if ($_GET['__sid']!='') {
			$sid = $_GET['__sid'];
			$spath=session_save_path();
			if (file_exists($spath.DIRECTORY_SEPARATOR."sess_".$sid)) {
				$sdata=file_get_contents($spath.DIRECTORY_SEPARATOR."sess_".$sid);
				session_decode($sdata);
			}
		}
	}

	function getSessID() {
		$sid = session_id();
		return "__sid=".$sid;
	}
	
	function getLastLog() {
		global $wpdb;
		global $user_ID;
		
		$logs = $wpdb->get_row($wpdb->prepare("SELECT * FROM wp_podpress_stats WHERE userID=%s order by id desc limit 1",$user_ID),ARRAY_A);
		return $logs;
	}
	
	/**
   	 * mengambil data point tersisa untuk user yang aktif
	 */
	function getBalance() {
		global $wpdb,$user_ID;
		
		include_once WPINC."/user.php";

		if (function_exists('get_user_meta')) {
			$balance = get_user_meta($user_ID, "account_balance", true);
		} else {
			$balance = get_usermeta($user_ID, "account_balance", true);
		}
		return $balance;
	}
	
	function setBalance($ammount) {
		global $wpdb,$user_ID;

		include_once WPINC."/user.php";
		if (function_exists('get_user_meta')) {
			update_user_meta($user_ID, "account_balance", $ammount);
		} else {
			update_usermeta($user_ID, "account_balance", $ammount);
		}
		return $balance;
	}
	
	function initAPI() {
		include_once 'suararadio.api.class.php';
		$this->api = new SuararadioAPI();
		#var_dump($this->api);
		$res = $this->api->init();
		#var_dump($res);
	}
}

?>