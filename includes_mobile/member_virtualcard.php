<link rel="stylesheet" href="<?php echo WORDPRESS_SOCIAL_LOGIN_PLUGIN_URL . "/assets/css/login.css"; ?>" type="text/css" />
<?php
/*
  Member virtualcard
*/
global $current_user;

include_once 'suararadio.api.class.php';
$api = new SuararadioAPI();

$type = $current_user->member_type?$current_user->member_type:"Buddy";
$imgA = SUARARADIO_PLUGIN_URL."/images/transparent-bg.png";
if ($current_user->expired) {
	preg_match('/(\d{2})(\d{2})/',$current_user->expired,$exps);
	$strexp = $exps[2]."/".$exps[1];
} else {
	$strexp = "00/00";
}
$strextexp = "";
if ($current_user->ext_expired) {
	preg_match('/(\d{4})(\d{2})(\d{2})/',$current_user->ext_expired,$exps);
	$strextexp = $exps[3]."/".$exps[2]."/".$exps[1];
}
switch($type) {
	case "Listener":
		$imgA = SUARARADIO_PLUGIN_URL."/images/star1.png";
		break;
	case "Fans":
		$imgA = SUARARADIO_PLUGIN_URL."/images/star2.png";
		break;
	case "Lover":
		$imgA = SUARARADIO_PLUGIN_URL."/images/star3.png";
		break;
	case "Mania":
		$imgA = SUARARADIO_PLUGIN_URL."/images/star4.png";
		break;
	case "Legend":
		$imgA = SUARARADIO_PLUGIN_URL."/images/star5.png";
		break;
}

$img_profile_url = suararadio_get_profile_photo(null,"large");
?>
  
				
			
						<!--<header class="krMemberType krmt<?php echo $type; ?>">VIRTUAL CARD KLUB RADIO</header>	-->
		
		<div class="krbgBuddy_vcard">
		<!--<figure id="accountFoto"><img src="<?php echo $img_profile_url; ?>"></figure>-->
		<div id="vcard"><img src="<?php echo SUARARADIO_PLUGIN_URL."/images/KlubRadio_klite.png"; ?>"></div>
		<section id="accountDetail_vcard">
			<div class="text_vcard1"><span style="text-decoration: underline; font-size: 35px;">KlubRadio</span><br>
			KLUB PECINTA MUSIK & RADIO</div>
			<div class="logo_bri"><img src="<?php echo SUARARADIO_PLUGIN_URL."/images/bri-syariah.png"; ?>"></div>
			<div class="logo_radio_card"><img src="<?php echo SUARARADIO_PLUGIN_URL."/images/KLITE.png"; ?>"></div>
			<div class="nm_radio">KLITE<br>BANDUNG</div>
			<div class="no_card">1234 5678 9876</div>
			<div class="valid_true">VALID<br>THRU</div> 
			<div class="valid_date">
				
				<span class="text_month_year">MONTH/YEAR</span>
				<span class="month_year">12/15</span>
			</div>
			<div class="text_name"><span><?php  echo $current_user->display_name; ?></span></div>
			<div class="no_bwh">1234 12345 123</div>
			<div class="logo">
				<img class="support" src="<?php echo SUARARADIO_PLUGIN_URL."/images/modis.png"; ?>" width="50">
				<img class="support" src="<?php echo SUARARADIO_PLUGIN_URL."/images/laris.png"; ?>" width="50" height="16"><br>
				<img class="support" src="<?php echo SUARARADIO_PLUGIN_URL."/images/melon.png"; ?>" width="50">
				<img class="support" src="<?php echo SUARARADIO_PLUGIN_URL."/images/delima.png"; ?>" width="50">
			</div>


			


		  <!--<fieldset>
			<div class="row">
				<label class="txt<?php echo $type; ?>">Nama</label>
				<span class="txt<?php echo $type; ?>">: <?php echo $current_user->display_name; ?></span>
			</div>
			<div class="row">
				<label class="txt<?php echo $type; ?>">Email</label>
				<span class="txt<?php echo $type; ?>">: <?php echo $current_user->user_email; ?>&nbsp;</span>
			</div>
			<div class="row">
				<label class="txt<?php echo $type; ?>">Membership</label>
				<span class="txt<?php echo $type; ?>">: <?php echo $type; ?></span>
				
			</div>
			<div class="row">
				<label class="txt<?php echo $type; ?>">Card expired: </label>
				<span class="txt<?php echo $type; ?>">: <?php echo $strexp; ?></span>
			</div>
			<?php if ($strextexp!='') { ?>
			<div class="row">
				<label>extended expired</label>
				<span class="txt<?php echo $type; ?>">: <?php echo $strextexp; ?></span>
			</div>
			<?php } ?>

			<div class="row">
				<label class="txt<?php echo $type; ?>">Member since</label>
				<span class="txt<?php echo $type; ?>">: <?php echo $current_user->user_registered; ?></span>
			</div>
		  </fieldset>-->
	</section>
		</div>
	<div class="clear" style="height: 20px;"></div>


<script type="text/javascript">
$(function() {
	$("#dialogUpgrade").dialog({ autoOpen: false, height: 260, width: 370 });
	$("#accUpgrade").click(function() {
		$("#dialogUpgrade").dialog({ autoOpen: false, height: 260, width: 370 });
		$("#dialog1").show();
		$("#dialog2").hide();
		$("#dialog3").hide();
		$("#dialog4").hide();
		$("#dialogUpgrade").dialog('open');
	});
	// button dialog pertama
	$("#buVoucher").click(function(){
		$("#dialog1").hide();
		$("#dialog2").show();
	});
	$("#buDelima").click(function(){
		$("#dialogUpgrade").dialog('close');
		$("#dialogUpgrade").dialog({ position: 'center', height: 400, width: 780 });
		$("#dialog1").hide();
		$("#dialog3").show();
		$("#frmPaket").show();
		$("#frmPayDelima").hide();
		$("#dialogUpgrade").dialog('open');
	});
	$("#buPromo").click(function(){
		$("#dialog1").hide();
		$("#dialog4").show();
	});
	$("#buDoVoucher").click(function(){
		$.ajax({
			type: "POST",
			dataType: 'json',
			url: "/wp-admin/admin-ajax.php",
			data:{ 
				action: "suararadio_voucher_process",
				voucher: $('#voucher').val(),
				nokartu: $('#nokartu').val(),
				cvc: $('#cvc').val(),
				expkartu_bln: $('#expkartu_bln').val(),
				expkartu_thn: $('#expkartu_thn').val(),
			},
			success: function (data) {
				if (data.code=='1' || data.code=='2') {
					window.location.href="/member/account/";
				} else if (data.code=='3') {
					alert(data.message);
					window.location.href="/member/account/";
				} else {
					alert(data.code+': '+data.message);
				}
			}
		});
		$('#voucher').val('');
		$('#dialogUpgrade').dialog( "close" );
	});
	$("#buDoPromo").click(function(){
		$.ajax({
			type: "POST",
			dataType: 'json',
			url: "/wp-admin/admin-ajax.php",
			data:{ 
				action: "suararadio_promo_process"
			},
			success: function (data) {
				if (data.code=='1') {
					window.location.href="/member/account/";
				} else {
					alert(data.code+': '+data.message);
				}
			}
		});
		$('#dialogUpgrade').dialog( "close" );
	});
	// dukungan
	$("#buDoDelima").click(function(){
		$.ajax({
			type: "POST",
			dataType: 'json',
			url: "/wp-admin/admin-ajax.php",
			data: {
				action: "suararadio_delima_process",
				paket: $('#pgPaket').val(),
			},
			success: function(data) {
				if (data.code=='1') {
					$("#dialogUpgrade").dialog('close');
					$("#dialogUpgrade").dialog({height: 260, width: 370 });
					$("#doPayDelima").attr("href",data.link);
					$("#delimaInfoPay").text($("#pgPaket option:selected").text());
					$("#frmPaket").hide();
					$("#frmPayDelima").show();
					$("#dialogUpgrade").dialog('open');
				} else {
					alert(data.code+': '+data.message);
				}
			}
		});
	});
	// sd
	$("#doPayDelima").click(function(){
		var link = $(this).attr("href");
		window.open(link, 'DELIMA E-MONEY', 'width=700, height=500, location=yes, menubar=no, statusbars=no, resizable=no');
	});

	// melon connect
	$("#buMelon").click(function(){
		$("#dialogMelon").dialog({ autoOpen: false, height: 260, width: 370 });
		$("#dialogMelon").dialog('open');
	});
	$("#buDoMelon").click(function (){
		$.ajax({
			type: "POST",
			dataType: 'json',
			url: "/wp-admin/admin-ajax.php",
			data:{ 
				action: "suararadio_melon_connect",
				username: $('#username').val(),
				password: $('#password').val()
			},
			success: function (data) {
				if (data.code=="1") {
					window.location.reload();
				} else {
					alert(data.code+': '+data.message);
				}
			}
		});
	});
});
</script>
<?php
$listprice = $api->klubPrice($pars); 
#var_dump($listprice);
?>
<div id="dialogUpgrade" title="Upgrade" style="display: none;">
<form id="frmUpgrade">
<div id="dialog1">
  <p class="">Pilih metode pembayaran Anda.</p>
  <div id="dialogPay" title="Payment">
	<ul class="imgList">
		<li><img width="80px" src="<?php echo SUARARADIO_PLUGIN_URL."/images/albumart.png"; ?>"><br/><input id="buVoucher" type="button" class="button-secondary" name="buVoucher" value="Voucher"></li>
		<li><img width="80px" src="<?php echo SUARARADIO_PLUGIN_URL."/images/delima.jpg"; ?>"><br/><input id="buDelima" type="button" class="button-secondary" name="buDelima" value="Delima"></li>
		<!-- li><img width="80px" src="<?php echo SUARARADIO_PLUGIN_URL."/images/paypal.jpg"; ?>"><br/><input id="buPaypal" type="button" class="button-secondary" name="buPaypal" value="Paypal"></li -->
		<?php if ($current_user->member_deposit!='') { ?>
		<li><img width="80px" height="80px" src="<?php echo SUARARADIO_PLUGIN_URL."/images/KR.png"; ?>"><br/><input id="buPromo" type="button" class="button-secondary" name="buPromo" value="Promo"></li>
		<?php } ?>
	</ul>
  </div>
</div>
<div id="dialog2" class="clear" style="display: none;">
  <p id="txKartu" class="" style="display: none;">Masukkan data kartu Anda.</p>
  <fieldset id="frmKartu" style="display: none;">
  	<label for="nokartu">Nomor Kartu</label>
	<input type="text" name="nokartu" id="nokartu" style="width: 200px;" class="text ui-widget-content ui-corner-all" />
	<input type="text" name="cvc" id="cvc" maxlength="3" style="width: 50px;" class="text ui-widget-content ui-corner-all" />
	<label for="expkartu">Kadaluarsa Kartu</label>
	Bln &nbsp;<input type="text" name="expkartu_bln" maxlength="2" id="expkartu_bln" style="width: 30px;" class="text ui-widget-content ui-corner-all" />
	Thn &nbsp;<input type="text" name="expkartu_thn" maxlength="2" id="expkartu_thn" style="width: 30px;" class="text ui-widget-content ui-corner-all" />
	<input id="buDoKartu" type="button" class="button-secondary" name="buDoKartu" value="Kirim">
  </fieldset>
  <p id="txVoucher" class="">Masukkan kode voucher Anda.</p>
  <fieldset id="frmVoucher">
  	<label for="voucher">Kode Voucher</label>
	<input type="text" name="voucher" id="voucher" style="width: 250px;" class="text ui-widget-content ui-corner-all" />
	<input id="buDoVoucher" type="button" class="button-secondary" name="buDoVoucher" value="Kirim">
  </fieldset>
</div>
<div id="dialog3" class="clear" style="display: none;">
  <fieldset id="frmPaket">
  	<p class="">Pilih upgrade sesuai dengan keinginan anda.</p>
	<label for="pgPaket">Paket Upgrade</label>
	<select style="width: 300px;" class="text ui-widget-content ui-corner-all" name="pgPaket" id="pgPaket">
<?php
	$opts1 = "";
	$opts2 = "";
	$group = ""; 
	foreach ($listprice as $arvl) {
		if ($group!=$arvl['group']) {
			if ($group!="") $opts1 .= "</optgroup>";
			$opts1 .= "<optgroup label=\"".$arvl['group']."\">";
			$group = $arvl['group'];
		} 
		$opts1 .= "<option value=\"".$arvl['code']."\">".$arvl['name']." - Rp. ".number_format($arvl['price'],0,',','.')."</option>";
	}
	$opts2 .= "</optgroup>";
	echo $opts1.$opts2;
?>
	</select> &nbsp; <input id="buDoDelima" type="button" class="button-secondary" name="buDoDelima" value="Kirim">
	<section id="inPaket">
  		<aside class="logInfo fans">
			<header>Radio Fans</header>
			<ul>
			<li><img src="<?php echo WORDPRESS_SOCIAL_LOGIN_PLUGIN_URL ?>/assets/img/info/fa_1.png"/><p>All The Features of Radio Listener</p></li>
			<li><img src="<?php echo WORDPRESS_SOCIAL_LOGIN_PLUGIN_URL ?>/assets/img/info/fa_2.png"/><p>Unlimited Music Streaming Provided by MelOn</p></li>
			<li><img src="<?php echo WORDPRESS_SOCIAL_LOGIN_PLUGIN_URL ?>/assets/img/info/fa_3.png"/><p>Free Advertising for one product/service on LARIS for 2 days per month</p></li>
		</ul>
		<footer>&nbsp;</footer>
		</aside>
		<aside class="logInfo lover">
			<header>Radio Lover</header>
			<ul>
				<li><img src="<?php echo WORDPRESS_SOCIAL_LOGIN_PLUGIN_URL ?>/assets/img/info/lo_1.png"/><p>All The Features of Radio Fans</p></li>
				<li><img src="<?php echo WORDPRESS_SOCIAL_LOGIN_PLUGIN_URL ?>/assets/img/info/lo_2.png"/><p>Unlimited Radio Collaboration  Content</p></li>
				<li><img src="<?php echo WORDPRESS_SOCIAL_LOGIN_PLUGIN_URL ?>/assets/img/info/lo_3.png"/><p>Free Advertising for one product/service on LARIS for 4 days per month</p></li>
			</ul>
			<footer>&nbsp;</footer>
		</aside>
		<aside class="logInfo mania">
			<header>Radio Mania</header>
			<ul>
				<li><img src="<?php echo WORDPRESS_SOCIAL_LOGIN_PLUGIN_URL ?>/assets/img/info/ma_1.png"/><p>All The Features of Radio Lover</p></li>
				<li><img src="<?php echo WORDPRESS_SOCIAL_LOGIN_PLUGIN_URL ?>/assets/img/info/ma_2.png"/><p>Unlimited Legal Music Streaming & Download Provided by MelOn</p></li>
				<li><img src="<?php echo WORDPRESS_SOCIAL_LOGIN_PLUGIN_URL ?>/assets/img/info/ma_3.png"/><p>Free Advertising for one product/service on LARIS for 7 days per month</p></li>
			</ul>
			<footer>&nbsp;</footer>
		</aside>
		<aside class="logInfo legend">
			<header>Radio Legend</header>
			<ul>
				<li><img src="<?php echo WORDPRESS_SOCIAL_LOGIN_PLUGIN_URL ?>/assets/img/info/lg_1.png"/><p>All The Features of Radio Mania</p></li>
				<li><img src="<?php echo WORDPRESS_SOCIAL_LOGIN_PLUGIN_URL ?>/assets/img/info/lg_2.png"/><p>Realtime Monitoring Facility for: Rundown, Request, Radio & Listener profile</p></li>
				<li><img src="<?php echo WORDPRESS_SOCIAL_LOGIN_PLUGIN_URL ?>/assets/img/info/lg_3.png"/><p>Free Advertising for one product/service on LARIS for 30 days per month</p></li>
			</ul>
			<footer>&nbsp;</footer>
		</aside>
	  </section>
  </fieldset>
  <fieldset id="frmPayDelima">
  	<section style="text-align: center;">
  	<h3 id="delimaInfoPay"></h3>
  	<br/>
  	<img width="80px" src="<?php echo SUARARADIO_PLUGIN_URL."/images/delima.jpg"; ?>">
  	<br/>
  	<input href=�� id="doPayDelima" type="button" class="button-secondary" name="doPayDelima" value="Beli dengan DELIMA E-MONEY">
  	</section>
  </fieldset>
</div>
<div id="dialog4" class="clear" style="display: none;">
  <p id="txVoucher" class=""><b>Promo Radio Lover</b></p>
  <fieldset id="frmPromo">
  	<label for="promo">Klik promo untuk mengaktifkan, membership Radio Lover.</label>
  	<p>Membership Radio Lover promo berlaku selama 1 bulan (30 hari). Setelah itu harus di perpanjang lagi. Promo ini berlaku untuk pembelian paket Radio Fans</p>
	<input id="buDoPromo" type="button" class="button-secondary" name="buDoPromo" value="Daftar Promo!">
  </fieldset>
</div>
</form>
</div>
<div id="dialogMelon" title="Koneksi MelOn" style="display: none;">
<form id="frmMelon">
<div id="dialog5" class="clear">
  <p id="txVoucher" class="">Masukkan userid/email dan password Anda untuk konek ke MelOn</p>
  <fieldset id="frmConnect">
  	<label for="username">Username / Email</label>
	<input type="text" name="username" id="username" style="width: 200px;" class="text ui-widget-content ui-corner-all" />
	<label for="password">Password</label>
	<input type="password" name="password" id="password" style="width: 200px;" value="" class="text ui-widget-content ui-corner-all" />
	<input id="buDoMelon" type="button" class="button-secondary" name="buDoMelon" value="Koneksi ke MelOn!">
  </fieldset>
</form>
</div>
</div>
