<?php
/**
 * xspf template
 *
 * @package WordPress
 */

header('Content-type: application/xspf+xml; charset=' . get_settings('blog_charset'), true);
#header('Content-Type: text/xml; charset=' . get_option('blog_charset'), true);
$more = 1;

?>
<?php echo '<?xml version="1.0" encoding="'.get_option('blog_charset').'"?'.'>'; ?>
<playlist version="0" xmlns="http://xspf.org/ns/0/">
	<title><?php bloginfo_rss('name') ?></title>
	<annotation>xspf playlist</annotation>
	<creator><?php the_author()?></creator>
	<location><?php bloginfo_rss('url') ?></location>
	<license>http://creativecommons.org/licenses/by-sa/1.0/</license>
	<trackList>
		<?php 
		  while( have_posts()) : the_post(); 
			$nm_file_ = getPodcastMeta($post->ID);
			
			if (!empty($nm_file_)) {
				$meta = get_post_meta($post->ID, 'podPressMedia', true);
		
				$fname = basename($meta[$pos]["URI"]); 
				$fimg = $meta[$pos]["previewImage"]; 
				$img = (trim($fimg)!=""? $fimg:$imgpath = "http://".$_SERVER["HTTP_HOST"]."/wp-content/plugins/suararadio/images/vpreview_center.png");
		?>
		<track>
		  <location><?= $nm_file_ ?></location>
			<annotation><?php the_title() ?></annotation>
			<title><?php the_title_rss() ?></title>
			<image><?= $img ?></image>
		</track>
		<?php } endwhile; ?>
  </trackList>
</playlist>