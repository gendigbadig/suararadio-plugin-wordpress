<?php
/**
 * pls playlist template
 *
 * @package WordPress
 */

header('Content-type: audio/x-scpls', true);
#header('Content-type: text/plain', true);
#header('Content-Disposition: attachment; filename="playlist.pls"');
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
$more = 1;

$data = array();
while( have_posts()) : the_post(); 
	$nm_file_ = getPodcastMeta($post->ID);

	if (!empty($nm_file_)) {
		$meta = get_post_meta($post->ID, 'podPressMedia', true);
	
		$fname = basename($meta[$pos]["URI"]); 
		$fimg = $meta[$pos]["previewImage"]; 
		$title = $post->post_title;
		$entry = array("title"=>$title,"file"=>$nm_file_);
		$data[] = $entry;
	} 
endwhile;
wp_reset_query();
?>
[playlist]
NumberOfEntries=<?= count($data)."\n" ?>

<?php
$i = 1;
foreach ($data as $val) { ?>
File<?=$i?>=<?= $val['file']."\n" ?>
Title<?=$i?>=<?= $val['title']."\n" ?>
Length<?=$i?>=-1

<? $i++; } ?>

Version=2
