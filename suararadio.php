<?php
/**
 * @package Suara Radio
 * @author M. Zhuhriansyah R.
 * @version 1.0.22
 */
/*
Plugin Name: Suara Radio
Plugin URI: http://www.zamrudtechnology.com
Description: Plugin untuk menangani fungsi-fungsi suara radio seperti playlist players.
Author: M. Zhuhriansyah R.
Version: 1.0.22
Author URI: http://www.facebook.com/izhur
history:
- auto play on buka web
- perbaikan api playlist json
- update player, add peak data, and auto next play
- pindahan settingan ke wp-config.php
- tambahan icon list playlist
- AUTH_KEY,SECURE_AUTH_KEY,LOGGED_IN_KEY,NONCE_KEY harus diubah agar WP tidak bingung ketika plugin membaypass pemanggilan, komponen
- tambahan update kontenpilihan
- akses ke melon api melon dilewatkan ke api.suararadio.com
*/
/*  Copyright 2011  M. Zhuhriansyah R.  (email : izhur2001@yahoo.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License
 for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

define('SUARARADIO', '1.0.22');
define('SR_PLAYLIST_VAR', 'suararadio_playlist');
define('SR_PLAYLIST_ID', 'suararadio_playlist_id');
define('SR_PLAYLIST_NAME', 'suararadio_playlist_name');
define('SUARARADIO_PLUGIN_URL', get_option( 'siteurl' ) . '/wp-content/plugins' .'/suararadio');
define('SUARARADIO_PLUGIN_DIR', ABSPATH . 'wp-content/plugins' .'/suararadio');
define('SUARARADIO_MEMBER', true);
define('SUARARADIO_API', 'http://api.suararadio.com');
define('SUARARADIO_PAYMENT', 'sandbox');  // sandbox,live,beta

/**
 * settingan pindah ke wp-config.php
 **/

#var_dump(WP_PLUGIN_DIR.'/suararadio/includes/mobileagent/MobileUserAgent.php');

/**
 * Converts MySQL DATETIME field to user specified date format.
 *
 * If $dateformatstring has 'G' value, then gmmktime() function will be used to
 * make the time. If $dateformatstring is set to 'U', then mktime() function
 * will be used to make the time.
 *
 * The $translate will only be used, if it is set to true and it is by default
 * and if the $wp_locale object has the month and weekday set.
 *
 * @since 0.71
 *
 * @param string $dateformatstring Either 'G', 'U', or php date format.
 * @param string $mysqlstring Time from mysql DATETIME field.
 * @param bool $translate Optional. Default is true. Will switch format to locale.
 * @return string Date formated by $dateformatstring or locale (if available).
 */

global $wpdb,$wprisedb,$wprisesmsdb,$wprisedb2,$wprisesmsdb2,$wpriseuserdb,$wpdbsr;
global $wpkontenpil;

function suararadio_init() {
	 global $wp_rewrite;
	 global $pagenow;
	 global $is_mobile_status;

	 add_action('wp_head', suararadio_wp_head);
	 add_action('get_footer', suararadio_wp_footer);

	 // feed
	 add_feed('xspf', suararadio_do_feed_xspf);
	 add_feed('playlist.m3u', suararadio_do_feed_m3u);
	 add_feed('playlist.pls', suararadio_do_feed_pls);
	 add_feed('playlist.asx', suararadio_do_feed_asx);

	 if (!get_option('suararadio_post_rows')) {
	 	add_option('suararadio_post_rows','4');
	 }

	 #add_action('do_feed_xspf', suararadio_get_xspf);
	 #add_action('generate_rewrite_rules', 'suararadio_rewrite_rules');

   	#$wp_rewrite->flush_rules();
   	#var_dump($wp_rewrite->rules);


	 //wp_enqueue_script('jquery');
	 if ( !is_admin() && $pagenow!='wp-login.php') {
	 	 wp_deregister_script('jquery');

	 	 wp_enqueue_script('jquery', plugins_url($path = 'suararadio/js/jui/jquery-1.8.2.min.js'), array(), '1.8.2');
		 wp_enqueue_script('swfobject', plugins_url($path = 'suararadio/js/swfobject/swfobject.js'), array(), '2.2');
		 wp_enqueue_script('jquery.ui', plugins_url($path = 'suararadio/js/jui/jquery-ui-1.8.1.custom.min.js'), array('jquery'), '1.0');
		 wp_enqueue_script('suararadio', plugins_url($path = 'suararadio/js/suararadio.js'), array('jquery'), '1.0');
		 wp_enqueue_script('soundmanager2', plugins_url($path = 'suararadio/sm/script/soundmanager2-nodebug-jsmin.js'), array('jquery'), '1.0');


	 	 //if (!$is_mobile_status) {
			 wp_enqueue_script('suararadio.player', plugins_url($path = 'suararadio/js/suararadio.player.js'), array('jquery','swfobject','soundmanager2'), '1.0');
		     wp_enqueue_script('suararadio.playlst', plugins_url($path = 'suararadio/js/suararadio.playlist.js'), array(), '1.0');

         //} else {
             //wp_enqueue_script('suararadio.player', plugins_url($path = 'suararadio/js/suararadio.player.m.js'), array('jquery','swfobject','soundmanager2'), '1.0');
         //}
	 }

	 #wp_enqueue_script('jquery.ui.core', plugins_url($path = 'suararadio/js/jui/jquery.ui.core.js'), array('jquery'), '1.0');
	 #wp_enqueue_script('jquery.ui.widget', plugins_url($path = 'suararadio/js/jui/jquery.ui.widget.js'), array('jquery'), '1.0');
	 #wp_enqueue_script('jquery.ui.mouse', plugins_url($path = 'suararadio/js/jui/jquery.ui.mouse.js'), array('jquery','jquery.ui.core','jquery.ui.widget'), '1.0');
	 #wp_enqueue_script('jquery.ui.sortable', plugins_url($path = 'suararadio/js/jui/jquery.ui.sortable.js'), array('jquery','jquery.ui.core','jquery.ui.mouse','jquery.ui.widget'), '1.0');

	 // penanganan suararadio_api
	 if ($pathquery = @parse_url($_SERVER['REQUEST_URI'])) {
		$pathquery = $pathquery['path'];
		if (preg_match("/^\/(apis|suararadio_api)/", $pathquery)) {
			header("Access-Control-Allow-Origin: *");
			$paths = explode("/",$pathquery);

			switch ($paths[2]) {
				case "play_lagu":
					suararadio_play_lagu($paths);
					break;
				case "add_playlist_item":
					playlist_add_item($paths[3],$paths[4]);
					break;
				case "del_playlist_item":
					playlist_del_item($paths[3]);
					break;
				case "reorder_playlist":
					playlist_reorder();
					break;
				case "del_playlist":
					playlist_del($paths[3]);
					break;
				case "clear_playlist":
					playlist_clear();
					break;
				case "clear_playlist_all":
					playlist_clear_all();
					break;
				case "save_playlist":
					playlist_save();
					break;
				case "load_playlist_item":
					playlist_item_load($paths[3]);
					break;
				case "show_playlist":
					if($paths[3]=="playlist.xspf") {
						suararadio_get_xspf();
					} else if($paths[3]=="playlist.m3u") {
						suararadio_get_m3u();
					} else if($paths[3]=="playlist.asx") {
						suararadio_get_asx();
					} else if($paths[3]=="playlist.pls") {
						suararadio_get_pls();
					} else {
                        suararadio_get_json();
                    }
					break;
				case "paypal":
					include SUARARADIO_PLUGIN_DIR.'/includes/paypal_functions.php';
					if($paths[3]=="success") {
						suararadio_paypal_success();
					}
					if($paths[3]=="cancel") {
						suararadio_paypal_cancel();
					}
					if($paths[3]=="notify") {
						suararadio_paypal_notify();
					}
					break;
				case "melon":
					include SUARARADIO_PLUGIN_DIR.'/includes/melon_functions.php';
					suararadio_melon_process($paths);
					break;
				case "data":
					include SUARARADIO_PLUGIN_DIR.'/includes/data_functions.php';
					suararadio_data_process($paths);
					break;
				case "radio":
					include SUARARADIO_PLUGIN_DIR.'/includes/radio_functions.php';
					suararadio_data_process($paths);
					break;
				case "channels":
					include SUARARADIO_PLUGIN_DIR.'/includes/channel_functions_3.php';
					suararadio_channel_process($paths);
					break;
				case "kanal":
					include SUARARADIO_PLUGIN_DIR.'/includes/channel_functions_2.php';
					suararadio_channel_process($paths);
					break;

				case "radiocontent":
					include SUARARADIO_PLUGIN_DIR.'/includes/radiocontent_functions.php';
					suararadio_radiocontent_process($paths);
					break;


				//case "permalink":
				//	include SUARARADIO_PLUGIN_DIR.'/includes/functions.php';
				//	getPermalink($paths);
				//	break;

                case "tune":
					include SUARARADIO_PLUGIN_DIR.'/includes/tune_functions.php';
					suararadio_tune_process($paths);
					break;
                case "user":
					include SUARARADIO_PLUGIN_DIR.'/includes/user_functions.php';
					suararadio_user_process($paths);
					break;
				default:
					status_header('404');
			}
			exit;
		} else if (preg_match("/^\/member/", $pathquery)) {
			include SUARARADIO_PLUGIN_DIR.'/includes/member.php';
			exit;
		}

	} #if suararadio_api

	add_action('wp_ajax_suararadio_melon_connect','suararadio_melon_connect');
	add_action('wp_ajax_nopriv_suararadio_melon_connect','suararadio_melon_connect');
	add_action('wp_ajax_suararadio_melon_create','suararadio_melon_create');
	add_action('wp_ajax_nopriv_suararadio_melon_create','suararadio_melon_create');
	add_action('wp_ajax_suararadio_voucher_process','suararadio_voucher_process');
	add_action('wp_ajax_nopriv_suararadio_voucher_process','suararadio_voucher_process');
	add_action('wp_ajax_suararadio_delima_process','suararadio_delima_process');
	add_action('wp_ajax_nopriv_suararadio_delima_process','suararadio_delima_process');
	add_action('wp_ajax_suararadio_callback_process','suararadio_callback_process');
	add_action('wp_ajax_nopriv_suararadio_callback_process','suararadio_callback_process');
	add_action('wp_ajax_suararadio_promo_process','suararadio_promo_process');
	add_action('wp_ajax_nopriv_suararadio_promo_process','suararadio_promo_process');
}

#function suararadio_rewrite_rules($wp_rewrite) {
#    $new_rules = array(
#    	'feed/(suararadio\.xspf|m3u|pls|asx)' => 'index.php?feed='.$wp_rewrite->preg_index(1)
#    );
#    $wp_rewrite->rules = $new_rules + $wp_rewrite->rules;
#}

function suararadio_wp_head() {
	global $is_mobile_status;
	global $suararadio;
	global $current_user;
	global $wpdb;
	    global $suararadio_connect;

	$wsl_concat = (defined('WSL_CONCAT')) ? WSL_CONCAT:'';

	//if (!$is_mobile_status) {
		echo '<link rel="stylesheet" href="'.plugins_url('suararadio/suararadio.css').'" type="text/css" />'."\n";

        //} else {
		//echo '<link rel="stylesheet" href="'.plugins_url('suararadio/suararadio_mobile.css').'" type="text/css" />'."\n";
	//}
	echo '<link rel="stylesheet" type="text/css" href="'.plugins_url('suararadio/flashblock.css').'" />'."\n";
	echo '<link rel="stylesheet" href="'.plugins_url('suararadio/css/jquery-ui-1.8.1.css').'" type="text/css" />'."\n";

    echo '<link href="http://fonts.googleapis.com/css?family=Maven+Pro" rel="stylesheet" type="text/css">';


    $whatINeed = explode('/', $_SERVER['REQUEST_URI']);
    echo $whatINeed = $whatINeed[0];

    #var_dump(is_single());

      if (!is_user_logged_in()){
        echo '<script type="text/javascript">
            function button_playlist_kr(){
                jQuery("#Save_button").hide();
                jQuery("#Load_button").hide();
                jQuery("#clearPlaylist").hide();

            }
        </script>';
    }else{
        echo '<script type="text/javascript">
            function button_playlist_kr(){
                jQuery("#Save_button").show();
                jQuery("#Load_button").show();
                jQuery("#clearPlaylist").show();
            }
        </script>';
    }


	if (is_single()) {
	   echo '<script type="text/javascript">
            $(document).ready(function() {
                $(".podpress_downloadlink").addClass("apply-nolazy");
          });
        </script>';


		$appId = get_option( "wsl_settings_Facebook".$wsl_concat."_app_id" );
		$url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		$jdl = get_the_title();

        $post = get_post();
		//$isi =  get_the_content();

        $isi_konten =  apply_filters( 'the_content', $post->post_content );

        $isi = preg_replace('/((\\\\n)+)/'," ",preg_replace("/(\r\n|\n)/",'\\n',$isi_konten));
        $isi = remove_HTML($isi);

    	$quotes = array('/"/',"/'/");
    	//$replacements = array('%22','%27');
    	$replacements = array('%22','%27');
        $isi = preg_replace($quotes,$replacements,$isi);
		//
        $isi_iklan = template_iklan();
		$imageLink = getImagePostStayTune($post->ID, $post, get_the_content());

		$loc_audio = podPress_getAvailablePodcast($post->ID);
		$loc = $loc_audio['free'][0];
	//	echo "<!-- var_dump"; var_dump($loc_audio); echo "-->";
		$src = "http://api.suararadio.com/assets/player.swf?file=$loc&autostart=true&image=https://api.suararadio.com/stayTuned/image&stretching=exactfit&logo.hide=false&logo.file=https://api.suararadio.com/stayTuned/logo";
        $src_secure = "https://api.suararadio.com/assets/player.swf?file=$loc&autostart=true&image=https://api.suararadio.com/stayTuned/image&stretching=exactfit&logo.hide=false&logo.file=https://api.suararadio.com/stayTuned/logo";


      echo '<img src="'.$imageLink.'" width="200" height="200" style="display:none;" />';
	  echo '<meta property="fb:app_id" content="'.$appId.'" />
          <meta property="og:url" content="'.$url.'" />
          <meta property="og:type" content="website" />
          <meta property="og:title" content="'.$jdl.'" />
          <meta property="og:description" content="'.$isi_iklan.'" />
          <meta property="og:image" content="'.$imageLink.'" />
          <meta property="og:video" content="'.$src.'"/>
          <meta property="og:video:secure_url" content="'.$src_secure.'"/>
          <meta property="og:video:height" content="250"/>
          <meta property="og:video:width" content="403"/>
          <meta property="og:video:type" content="application/x-shockwave-flash"/>

          <meta name="twitter:card" content="player">
          <meta name="twitter:site" content="@diradiodotnet">
          <meta name="twitter:creator" content="@diradiodotnet">
          <meta name="twitter:title" content="'.$jdl.'">
          <meta name="twitter:url" content="'.$url.'">
          <meta name="twitter:description" content="'.$isi.'">
          <meta name="twitter:image" content="http://www.suararadio.com/wp-content/themes/suararadio/images/ad_2.png">
          <meta name="twitter:player" content="'.$src.'">
          <meta name="twitter:player:width" content="640">
          <meta name="twitter:player:height" content="480">
          <meta name="twitter:domain" content="diradio.net">
          ';
	}

     # KONTEN PILIHAN
    $uri = $_SERVER['REQUEST_URI'];
	$id = explode("/", $uri);
    $uri_kontenpil = $id[1];
    $song_id = $id[2];

    if($uri_kontenpil=='single_kp'){

        global $suararadio;
    	global $current_user;
    	global $wpdb;
        global $wpkontenpil;


        $querystr = "
    		SELECT wposts.*
    		FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta
    		WHERE wposts.ID = wpostmeta.post_id
    		AND wposts.ID = '".$song_id."'
    		ORDER BY wposts.post_date DESC
     	";
     	$pageposts = $wpkontenpil->get_results($querystr, OBJECT);
        $konten = $pageposts[0]->post_content;
    	$isi = preg_replace('/((\\\\n)+)/'," ",preg_replace("/(\r\n|\n)/",'\\n',stripslashes($pageposts[0]->post_content)));
        //$jdl = preg_replace('/\"/'," ",preg_replace("/(\r\n|\n)/",'\\n',stripslashes($pageposts[0]->post_title)));
        $jdl = $pageposts[0]->post_title;

        $wsl_concat = (defined('WSL_CONCAT')) ? WSL_CONCAT:'';

        if (!get_option('wsl_settings_Facebook'.$wsl_concat.'_enabled')) return false;

       // echo '<script src="http://connect.facebook.net/en_US/all.js"></script>'."\n";
         $appId = get_option( "wsl_settings_Facebook".$wsl_concat."_app_id" );


        $isi = remove_HTML($isi);
    	$quotes = array('/"/',"/'/");
    	//$replacements = array('%22','%27');
    	$replacements = array('%22','%27');
        $link = get_permalink($pageposts[0]->ID);
        $isi = preg_replace($quotes,$replacements,$isi);
        //$url = $_SERVER['HTTP_HOST'].$link;
        $isi_iklan = template_iklan();

        $url = "http://".$_SERVER['HTTP_HOST']."/single_kp/".$pageposts[0]->ID;
        $url_direct = "http://".$_SERVER['HTTP_HOST']."/single_kp/".$pageposts[0]->ID;

        $loc_audio = podPress_getAvailablePodcast_kp($pageposts[0]->ID);
        $loc = $loc_audio['free'][0];

    	$imageLink = getImagePostStayTune($postId, $pageposts,$konten);
        //$src = "https://api.suararadio.com/assets/player.swf?file=$loc&autostart=true&image=https://api.suararadio.com/stayTuned/image&stretching=exactfit&logo.hide=false&logo.file=https://api.suararadio.com/stayTuned/logo";
        $src = "https://api.suararadio.com/assets/player.swf?file=$loc&autostart=true&image=$imageLink&stretching=exactfit&logo.hide=false";
        $src_secure = "https://api.suararadio.com/assets/player.swf?file=$loc&autostart=true&image=$imageLink&stretching=exactfit&logo.hide=false";


         echo '
          <meta property="fb:app_id" content="'.$appId.'" />
          <meta property="og:url" content="'.$url.'" />
          <meta property="og:type" content="website" />
          <meta property="og:title" content="'.$jdl.'" />
          <meta property="og:description" content="'.$isi_iklan.'" />
          <meta property="og:image" content="'.$imageLink.'" />
          <meta property="og:video" content="'.$src.'"/>
          <meta property="og:video:secure_url" content="'.$src_secure.'"/>
          <meta property="og:video:height" content="250"/>
          <meta property="og:video:width" content="500"/>
          <meta property="og:video:type" content="application/x-shockwave-flash"/>
          ';
    }



} #endfunc suararadio_wp_head


function suararadio_wp_footer() {
	global $is_mobile_status;
	global $namaradio;

	if (!defined('SUARARADIO_PLAYER_SHOWED')) {
		//if (!SUARARADIO_MEMBER)
		//suararadio_show_player2();
		//suararadio_show_player();
		define('SUARARADIO_PLAYER_SHOWED',true);
	}

    echo '<div id="results"></div>';
	echo '<div class="flash-msg" id="flash-msg" style="display: none;"></div>'."\n";
	echo '<div id="dialogRegister" title="Register & Surveri" style="display: none;"></div>'."\n";
	echo '<div class="bottomMsg" style="display:none;"><header>RADIO 2.0 LUCKY LISTENER:</header><div>
Registrasi & survei pendengar radio berhadiah Samsung Galaxy Grand & Voucher Coaching bernilai ratusan juta rupiah.</div><br/><br/><ul class="memClickable2"><li><a id="regOpen">Registrasi</a></li><li><a id="regClose">Tutup</a></li></ul></div>'."\n";
	echo '<div class="progress" id="progress" style="display: none;"><img src="'.plugins_url('suararadio/images/loading.gif').'"></div>'."\n";

    /*global $suararadio_connect;
    suararadio_get_fbuser();
    $fbuser = $suararadio_connect['facebook']['user'];
    */

   /* echo '<script type="text/javascript">
    function online_request(){
    var fbuserid = "'.$fbuser['id'].'";
    var fbusername = "'.$fbuser['name'].'";
    var requestmessage = $("#request-message").val();
    //alert(fbuserid);
    //alert(fbusername);
    //alert(requestmessage);
    if(fbuserid ==""){
        alert("fbuserid kosong.. !")
        return false;
    }
        if(requestmessage !=""){
            $.ajax({
               type: "POST",
               url: "/apis/data/message",
               data: {sender: fbusername, message: requestmessage,type:"3",id_creator:fbuserid},
                success: function(data) {
                alert("Message Telah Terkirim.. !");
                window.location = "/community/"
              }
            });
        }else{
            alert("Silahkan isi from message.. !");
        }


   }';*/



    echo '<script type="text/javascript">
	jQuery("div#progress").bind("ajaxStart", function(){
    		jQuery(this).show();
	}).bind("ajaxStop", function(){
    		jQuery(this).hide();
	});
	var regClosed = false;
	//$(function(){
		jQuery("#regOpen").click(function(){
			var page = "http://api.suararadio.com/klubradio/reg?id='.IDRADIO.'&namaradio='.$namaradio.'";
			var wWidth = $(window).width();
			var dWidth = wWidth * 0.8;
			var wHeight = $(window).height();
			var dHeight = wHeight * 0.9;
			var dReg = $("#dialogRegister").html(\'<iframe style="border: 0px; " src="\' + page + \'" width="100%" height="100%"></iframe>\').dialog({
			//var dialog = $("#dialogRegister").html(\'<div class="loading"><div>loading...</div></div>\').load(page).dialog({
				autoOpen: false,
				modal: true,
				height: dHeight,
				width: dWidth,
				overlay: { opacity: 0.1, background: "black" },
				title: "Registrasi & Survei"
			});
			dReg.dialog("open");
		});
		jQuery("#regClose").click(function(){
			$("div.bottomMsg").hide();
			regClosed = true;
		});
	//});
	var offset = 220;
	var duration = 500;
	//alert("ccc");

	/*jQuery(window).scroll(function() {
		if (jQuery(this).scrollTop() > offset && !regClosed) {
			jQuery(".bottomMsg").fadeIn(duration);
		} else {
			jQuery(".bottomMsg").fadeOut(duration);
		}
	});*/


';



if (!$is_mobile_status) {
	echo '
	jQuery("a.soundplayer img").bind("mouseover",function() {
		jQuery(this).attr("src","'.plugins_url('suararadio/images/spka.png').'");
	});
	jQuery("a.soundplayer img").bind("mouseout",function() {
		if (jQuery(this).attr("played")=="1") {
			jQuery(this).attr("src","'.plugins_url('suararadio/images/spka.png').'");
		} else {
			jQuery(this).attr("src","'.plugins_url('suararadio/images/spk.png').'");
		}
	});
	';
}
if (defined('STREAMING_ON_HOME')) {
	echo 'var streaming_on_home = 1;';
} else {
  echo 'var streaming_on_home = 0;';
}
echo '</script>'."\n";

} #endfunc suararadio_wp_footer

include SUARARADIO_PLUGIN_DIR.'/includes/functions.php';
include SUARARADIO_PLUGIN_DIR.'/includes/wpdbext.php';
#session_name("webuserid");
#session_start();
add_action('init', suararadio_init);
add_filter('wp_title', suararadio_update_title);

// checkHostAlive
// Check Host before connect to database
// to prevent website down when RISE is not connected to internet
// By : Rahmat Nugraha
// 20 November 2016
// Update :
// 27 Nov 2016 : use explode to split port, assign fsockopen result to variable

function checkHostAlive ($host) {
  $pieces = explode(":", $host);
  $host_name = $pieces[0];
  $host_port = $pieces[1];

  if (is_null($host_port)) {
    $host_port = '3306';
  }

  $fp = fsockopen($host_name, $host_port, $host, $host, 1);

  if ($fp) {
    return true;
  } else {
    return false;
  }
}

if (defined('RISE_DB_HOST2')) {
  $check_host = checkHostAlive(RISE_DB_HOST2);
  if ($check_host == false) {
  	define('RISE_DB_HOST2', false);
  }
}

if ( !isset($wprisedb) && defined('RISE_DB_USER') && defined('RISE_DB_NAME')) {
	//$wprisedb = new wpdb(RISE_DB_USER, RISE_DB_PASSWORD, RISE_DB_NAME, RISE_DB_HOST);
    if (class_exists('hyperdb')) {
        $wprisedb = new hyperdb;
        $wprisedb->add_database(array(
    	'host'     => RISE_DB_HOST,     // If port is other than 3306, use host:port.
    	'user'     => RISE_DB_USER,
    	'password' => RISE_DB_PASSWORD,
    	'name'     => RISE_DB_NAME,
    ));
    }else{
        $wprisedb = new wpdb(RISE_DB_USER, RISE_DB_PASSWORD, RISE_DB_NAME, RISE_DB_HOST);
    }
}

// untuk ambil rundown & request
if ( !isset($wpdbsr) && defined('DB_USER') && defined('DB_SR_NAME')) {
	//$wpdbsr = new wpdb(DB_USER, DB_PASSWORD, DB_SR_NAME, DB_HOST);
    if (class_exists('hyperdb')) {
        $wpdbsr = new hyperdb;
        $wpdbsr->add_database(array(
    	'host'     => DB_HOST,     // If port is other than 3306, use host:port.
    	'user'     => DB_USER,
    	'password' => DB_PASSWORD,
    	'name'     => DB_SR_NAME,
    ));
    }else{
        $wpdbsr = new wpdb(DB_USER, DB_PASSWORD, DB_SR_NAME, DB_HOST);
    }
}

if ( !isset($wprisesmsdb) && defined('RISE_DB_SMS_USER') && defined('RISE_DB_SMS_NAME')) {
	//$wprisesmsdb = new wpdb(RISE_DB_SMS_USER, RISE_DB_SMS_PASSWORD, RISE_DB_SMS_NAME, RISE_DB_SMS_HOST);
    if (class_exists('hyperdb')) {
        $wprisesmsdb = new hyperdb;
        $wprisesmsdb->add_database(array(
    	'host'     => RISE_DB_SMS_HOST,     // If port is other than 3306, use host:port.
    	'user'     => RISE_DB_SMS_USER,
    	'password' => RISE_DB_SMS_PASSWORD,
    	'name'     => RISE_DB_SMS_NAME,
    ));
    }else{
        $wprisesmsdb = new wpdb(RISE_DB_SMS_USER, RISE_DB_SMS_PASSWORD, RISE_DB_SMS_NAME, RISE_DB_SMS_HOST);
    }
}

if ( !isset($wprisedb2) && defined('RISE_DB_USER') && defined('RISE_DB_HOST2')) {
	//$wprisedb2 = new wpdb(RISE_DB_USER, RISE_DB_PASSWORD, RISE_DB_NAME, RISE_DB_HOST2);
    if (class_exists('hyperdb')) {
        $wprisedb2 = new hyperdb;
        $wprisedb2->add_database(array(
    	'host'     => RISE_DB_HOST2,     // If port is other than 3306, use host:port.
    	'user'     => RISE_DB_USER,
    	'password' => RISE_DB_PASSWORD,
    	'name'     => RISE_DB_NAME,
    ));
     }else{
        $wprisedb2 = new wpdb(RISE_DB_USER, RISE_DB_PASSWORD, RISE_DB_NAME, RISE_DB_HOST2);
    }

}

if ( !isset($wprisesmsdb2) && defined('RISE_DB_SMS_NAME') && defined('RISE_DB_HOST2')) {
	//$wprisesmsdb2 = new wpdb(RISE_DB_USER, RISE_DB_PASSWORD, RISE_DB_SMS_NAME, RISE_DB_HOST2);
    if (class_exists('hyperdb')) {
        $wprisesmsdb2 = new hyperdb;
        $wprisesmsdb2->add_database(array(
    	'host'     => RISE_DB_HOST2,     // If port is other than 3306, use host:port.
    	'user'     => RISE_DB_USER,
    	'password' => RISE_DB_PASSWORD,
    	'name'     => RISE_DB_SMS_NAME,
    ));
    }else{
        $wprisesmsdb2 = new wpdb(RISE_DB_USER, RISE_DB_PASSWORD, RISE_DB_SMS_NAME, RISE_DB_HOST2);
    }
}


if ( !isset($wpriseuserdb) && defined('RISE_DB_USER_NAME') && defined('RISE_DB_HOST2')) {
	//$wpriseuserdb = new wpdb(RISE_DB_USER, RISE_DB_PASSWORD, RISE_DB_USER_NAME, RISE_DB_HOST2);
    if (class_exists('hyperdb')) {
        $wpriseuserdb = new hyperdb;
        $wpriseuserdb->add_database(array(
    	'host'     => RISE_DB_HOST2,     // If port is other than 3306, use host:port.
    	'user'     => RISE_DB_USER,
    	'password' => RISE_DB_PASSWORD,
    	'name'     => RISE_DB_USER_NAME,
    ));
    }else{
        $wpriseuserdb = new wpdb(RISE_DB_USER, RISE_DB_PASSWORD, RISE_DB_USER_NAME, RISE_DB_HOST2);
    }
}

if ( !isset($wpkontenpil) && defined('KP_DB_USER') && defined('KP_DB_HOST') ) {
	//$wpkontenpil = new wpdb(KP_DB_USER, KP_DB_PASSWORD, KP_DB_NAME, KP_DB_HOST);
    if (class_exists('hyperdb')) {
        $wpkontenpil = new hyperdb;
        $wpkontenpil->add_database(array(
    	'host'     => KP_DB_HOST,     // If port is other than 3306, use host:port.
    	'user'     => KP_DB_USER,
    	'password' => KP_DB_PASSWORD,
    	'name'     => KP_DB_NAME,
    ));
    }else{
        $wpkontenpil = new wpdb(KP_DB_USER, KP_DB_PASSWORD, KP_DB_NAME, KP_DB_HOST);
    }
}

if ( !isset($suararadio) ) {
	include SUARARADIO_PLUGIN_DIR.'/includes/suararadio.class.php';
	include SUARARADIO_PLUGIN_DIR.'/includes/suararadio_music.class.php';
	$suararadio = new suararadioMusic_class();
}

?>
