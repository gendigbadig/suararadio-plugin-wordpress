<?php
/**
 * asx template
 *
 * @package WordPress
 */

header('Content-type: video/x-ms-asf', true);
#header('Content-Type: text/xml; charset=' . get_option('blog_charset'), true);
#header('Content-Disposition: attachment; filename="playlist.m3u"');
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

?>
<ASX version = "3.0">
<TITLE><?php bloginfo_rss('name') ?></TITLE>
		<?php 
		  while( have_posts()) : the_post(); 
			$nm_file_ = getPodcastMeta($post->ID);
			
			if (!empty($nm_file_)) {
				$meta = get_post_meta($post->ID, 'podPressMedia', true);
		
				$fname = basename($meta[$pos]["URI"]); 
				$fimg = $meta[$pos]["previewImage"]; 
				$img = (trim($fimg)!=""? $fimg:$imgpath = "http://".$_SERVER["HTTP_HOST"]."/wp-content/plugins/suararadio/images/vpreview_center.png");
		?>
		<ENTRY>
			<TITLE><?php the_title_rss() ?></TITLE>
		  <REF HREF="<?= $nm_file_ ?>" />
			<AUTHOR>e-Broadcasting Institute</AUTHOR>
		</ENTRY>
		<?php } endwhile; ?>
</ASX>